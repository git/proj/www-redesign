<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mainpage SYSTEM "/dtd/guide.dtd">
<mainpage>
<title>Gentoo Linux Contacts</title>

<author title="Author">
  <mail link="wolf31o2@gentoo.org">Chris Gianelloni</mail>
</author>

<abstract>
How to contact Gentoo
</abstract>

<!-- The content of this document is licensed under the CC-BY-SA license -->
<!-- See http://creativecommons.org/licenses/by-sa/2.5 -->
<license/>

<version>2</version>
<date>2010-07-22</date>

<chapter>
<title>Who do I contact?</title>
<section>
<title>Contents</title>
<body>

<ul>
  <li><uri link="#intro">Introduction</uri></li>
  <li><uri link="#pr">Public Relations/Events</uri></li>
  <li><uri link="#www">Webmaster</uri></li>
  <li><uri link="#trustees">The Gentoo Foundation</uri></li>
</ul>

</body>
</section>

<section id="intro">
<title>Introduction</title>
<body>

<p>
"Gentoo" is many things. It is a community-based distribution of Linux. It is a
package management philosophy. It is also a non-profit organization. This
document is designed to get you in touch with the right group within Gentoo, so
your correspondence can be handled as quickly as possible.
</p>

<p>
For information regarding upcoming events, interviews, general questions, or
simply just to find out more about Gentoo and Gentoo's products, you will
likely want to speak with someone from Gentoo's <uri link="#pr">Public
Relations</uri> project.
</p>

<impo>
The Public Relations team does not provide user support or troubleshooting. If
you need help with your Gentoo system, please visit our <uri
link="http://forums.gentoo.org">forums</uri>, <uri link="/main/en/irc.xml">IRC
channels</uri> or other <uri link="/main/en/support.xml">support venues</uri>.
</impo>

<p>
For information regarding this website, or any other Gentoo website hosted on a
<c>gentoo.org</c> domain, you will likely want to speak with Gentoo's <uri
link="#www">Webmasters</uri>.
</p>

<p>
For information about The Gentoo Foundation, or anything else related to Gentoo
intellectual property, trademarks, and copyrights, you will likely want to
speak to the Gentoo Foundation's <uri link="#trustees">Board of Trustees</uri>.
</p>

<p>
If you are looking for support on Gentoo Linux or any other Gentoo product or
project, then you should head over to our <uri
link="/main/en/support.xml">support</uri> page for further information.
</p>

</body>
</section>

<section id="pr">
<title>Public Relations</title>
<body>

<p>
The Gentoo <uri link="/proj/en/pr/">Public Relations</uri> project, and its
<uri link="/proj/en/pr/events/">Events</uri> subproject are responsible for
putting the word out about Gentoo, as well as coordinating the Gentoo presence
within the community at at trade shows and exhibitions.
</p>

<p>
If you are looking for materials for an article, someone to interview, or simply
want to know more about Gentoo and what it has to offer, send a message to the
PR team. To contact the PR team, simply send an email to
<mail>pr@gentoo.org</mail> and someone from the team will contact you as soon as
possible.
</p>

<impo>
The Public Relations team does not provide user support or troubleshooting. If
you need help with your Gentoo system, please visit our <uri
link="http://forums.gentoo.org">forums</uri> or <uri link="/main/en/irc.xml">IRC
channels</uri>.
</impo>

</body>
</section>

<section id="www">
<title>Webmasters</title>
<body>

<p>
Maintaining Gentoo's presence on the web is a daunting task. Gentoo has a
dedicated team of webmasters who keep our site looking sharp and up-to-date. If
you have a question or comment about any Gentoo web site, then this team is
what you are looking for. They can be contacted by sending an email to
<mail>www@gentoo.org</mail>.
</p>

</body>
</section>

<section id="trustees">
<title>The Gentoo Foundation Board of Trustees</title>
<body>

<p>
<uri link="/foundation/en/">The Gentoo Foundation</uri> is a non-profit
organization that was formed to be the protector of Gentoo's intellectual
property. The Board of Trustees are the elected keepers of the legal entity
that is Gentoo within the United States. If your question is legal in nature,
then you will want to send an email to <mail>trustees@gentoo.org</mail> and a
member of the board will get back to you as soon as possible.
</p>

</body>
</section>

</chapter>
</mainpage>
