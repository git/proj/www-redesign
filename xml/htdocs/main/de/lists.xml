<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE mainpage SYSTEM "/dtd/guide.dtd">

<!-- $Header: /var/cvsroot/gentoo/xml/htdocs/main/de/lists.xml,v 1.8 2010/05/15 13:01:39 keytoaster Exp $ -->

<mainpage lang="de">
<title>Gentoo Mailing-Listen</title>

<author title="Autor">
  <mail link="lcars">Andrea Barisani</mail>
</author>
<author title="Autor">
  <mail link="cybersystem">Sascha Schwabbauer</mail>
</author>
<author title="Bearbeiter">
  <mail link="curtis119">Curtis Napier</mail>
</author>
<author title="Bearbeiter">
  <mail link="klieber">Kurt Lieber</mail>
</author>
<author title="Bearbeiter">
  <mail link="robbat2">Robin H. Johnson</mail>
</author>
<author title="Bearbeiter">
  <mail link="nightmorph"/>
</author>
<author title="Übersetzer">
  <mail link="martin_winkler@gmx.de">Martin Winkler</mail>
</author>

<abstract>
Gentoo öffentliche Mailing-Listen
</abstract>

<license/>
<version>4</version>
<date>2010-04-09</date>

<chapter>
<title>Mailing-Listen</title>
<section>
<body>

<p>
Das Gentoo Projekt hat eine Anzahl öffentlicher Mailing-Listen, welche eine
Vielfalt von Gentoo-bezogenen Themen abdecken. Unsere Mailing-Listen werden mit
<uri link="http://mlmmj.mmj.dk">mlmmj</uri> betrieben und stellen
<c>List-Id:</c> Mail-Header und <c>[Listenname]</c> Präfixe zum Betreff zur
Verfügung, entsprechend moderner Standards und Konventionen zur Verwaltung von
Mailing-Listen.
</p>

<p>
Man kommuniziert mit <c>mlmmj</c> über E-Mail. Um eine Liste zu abonnieren,
schicken Sie eine leere E-Mail an:
</p>

<p><c>Listenname+subscribe@lists.gentoo.org</c></p>

<note>
Ersetzen Sie 'Listenname' durch den tatsächlichen Namen der Liste, die Sie
abonnieren möchten, zum Beispiel:
<c>gentoo-user+subscribe@lists.gentoo.org</c>, um die Mailing-Liste <c>gentoo-user</c>
zu abonnieren.
</note>

<p>Nachdem Sie die Liste einmal abonniert haben, können Sie Artikel an sie
senden, indem Sie eine E-Mail schreiben an:</p>
<p><c>Listenname@lists.gentoo.org</c></p>
<p>Um das Abonnement einer Liste zu beenden,
schicken Sie eine leere E-Mail an:</p>
<p><c>Listenname+unsubscribe@lists.gentoo.org</c></p>

<p>
Alle unsere Listen haben auch eine entsprechende Liste mit Zusammenstellungen
(engl.: digest). Die Digest-Listen schicken Ihnen alle paar Tage eine einzige
E-Mail anstatt individueller E-Mails für jeden einzelnen Beitrag. Wenn Sie die
Digest-Variante einer Mailing-Liste abonniert haben und das Abonnement beenden
wollen, müssen Sie sich ausdrücklich von der Digest-Variante abmelden. Unter
Gebrauch der E-Mail-Adresse, mit der Sie sich an- oder abmelden möchten,
schicken Sie eine leere E-Mail an die folgenden Adressen, um sich bei einer
Mailing-Liste entweder an- oder abzumelden:
</p>

<p>
<c>Listenname+subscribe-digest@lists.gentoo.org</c><br/>
<c>Listenname+unsubscribe-digest@lists.gentoo.org</c>
</p>

<p>
Einige Anwender möchten vielleicht Beiträge an eine Liste schicken, dabei aber
keine Mails von ihr empfangen (zum Beispiel diejenigen, die die Listen über
einen anderen Weg lesen möchten, wie etwa gmane). Diese Anwender können die
"nomail"-Variante der jeweiligen Liste abonnieren:
</p>

<p>
<c>Listenname+subscribe-nomail@lists.gentoo.org</c><br/>
<c>Listenname+unsubscribe-nomail@lists.gentoo.org</c>
</p>
<p>
Sie können mehr über die Fähigkeiten von mlmmj erfahren, indem Sie eine leere
Mail an die folgende Adresse schicken:
</p>

<p>
<c>Listenname+help@lists.gentoo.org</c><br/>
</p>

<impo>
Sie können auch mehr über das Gentoo Mailing-Listen-System und die akzeptablen
Verhaltensweisen erfahren, indem Sie die kurze Gentoo <uri
link="#faq">Mailing-Listen FAQ</uri> lesen, die weiter unten in diesem Dokument
erscheint.
</impo>

</body>
</section>
<section>
<title>Die primären Gentoo Mailing-Listen</title>
<body>

<table>
<tr>
  <th>Listenname</th>
  <th>Beschreibung</th>
</tr>
<tr>
  <ti><c>gentoo-user</c></ti>
  <ti>Mailing-Liste zur allgemeinen Anwenderunterstützung und Diskussion</ti>
</tr>
<tr>
  <ti><c>gentoo-announce</c></ti>
  <ti>Mailing-Liste für allgemeine Ankündigungen (neue Releases, Behebung von Sicherheitslücken)</ti>
</tr>
<tr>
  <ti><c>gentoo-dev</c></ti>
  <ti>Allgemeine Mailing-Liste für Gentoo-Entwickler</ti>
</tr>
<tr>
  <ti><c>gentoo-dev-announce</c></ti>
  <ti>Liste für Bekanntmachungen zur Gentoo-Entwicklung</ti>
</tr>
<tr>
  <ti><c>gentoo-project</c></ti>
  <ti>Zur Diskussion nichttechnischer Angelegenheiten bei Gentoo</ti>
</tr>
<tr>
  <ti><c>gentoo-security</c></ti>
  <ti>Zur Diskussion von Sicherheitsproblemen und diesbezüglicher Fehlerbehebungen</ti>
</tr>
<tr>
  <ti><c>gentoo-gmn</c></ti>
  <ti>Monatlicher Gentoo Newsletter</ti>
</tr>
<tr>
  <ti><c>gentoo-doc</c></ti>
  <ti>Für Beiträge zur Dokumentation, Vorschläge, Verbesserungen und Übersetzungen</ti>
</tr>
<tr>
  <ti><c>gentoo-doc-cvs</c></ti>
  <ti>Abonnieren Sie diese Liste, wenn Sie über Änderungen bezüglich unserer
  Dokumentation benachrichtigt werden möchten.</ti>
</tr>
<tr>
  <ti><c>gentoo-commits</c></ti>
  <ti>Abonnieren Sie diese Liste, wenn Sie über Änderungen in unserem CVS- und
  SVN-Verzeichnisbäumen benachrichtigt werden möchten. Dies ist eine
  Nur-Lesen-Mailing-Liste mit starkem Verkehr!</ti>
</tr>
<tr>
  <ti><c>gentoo-translators</c></ti>
  <ti>Zur Diskussion von Übersetzungsangelegenheiten für Dokumente</ti>
</tr>
<tr>
  <ti><c>gentoo-ppc-user</c></ti>
  <ti>Zur Anwenderunterstützung und Diskussion von Gentoo Linux/PowerPC</ti>
</tr>
<tr>
  <ti><c>gentoo-ppc-dev</c></ti>
  <ti>Für Diskussionen von Gentoo Linux/PowerPC Entwicklern</ti>
</tr>
<tr>
  <ti><c>gentoo-alpha</c></ti>
  <ti>Für Gentoo Linux/Alpha Anwenderunterstützung und Diskussion</ti>
</tr>
<tr>
  <ti><c>gentoo-amd64</c></ti>
  <ti>Für Gentoo Linux/AMD64 Anwenderunterstützung und Diskussion</ti>
</tr>
<!--
Disabled per vapier's request
<tr>
  <ti><c>gentoo-arm</c></ti>
  <ti>Discussions about running Gentoo on the ARM architecture</ti>
</tr>
-->
<tr>
  <ti><c>gentoo-hppa</c></ti>
  <ti>Diskussionen über das Betreiben von Gentoo auf der HPPA Architektur</ti>
</tr>
<tr>
  <ti><c>gentoo-ia64</c></ti>
  <ti>Für Gentoo Linux/ia64 Anwenderunterstützung und Diskussion</ti>
</tr>
<tr>
  <ti><c>gentoo-mips</c></ti>
  <ti>Diskussionen über das Betreiben von Gentoo auf der MIPS Architektur</ti>
</tr>
<tr>
  <ti><c>gentoo-sparc</c></ti>
  <ti>Für Gentoo Linux/Sparc Anwenderunterstützung und Diskussion</ti>
</tr>
<tr>
  <ti><c>gentoo-bsd</c></ti>
  <ti>Diskussion über Gentoo/BSD</ti>
</tr>
<tr>
  <ti><c>gentoo-xbox</c></ti>
  <ti>Diskussion über Gentoo für Xbox</ti>
</tr>
<tr>
  <ti><c>gentoo-cygwin</c></ti>
  <ti>Für Gentoo cygwin Anwenderunterstützung und Diskussion</ti>
</tr>
<tr>
  <ti><c>gentoo-alt</c></ti>
  <ti>Diskussionen über das <uri link="/proj/en/gentoo-alt/">Gentoo on Alternate Platforms Projekt</uri></ti>
</tr>
<tr>
  <ti><c>gentoo-kernel</c></ti>
  <ti>Releaseankündigungen für gentoo-sources, vesafb-tng, fbsplash und Diskussion</ti>
</tr>
<tr>
  <ti><c>gentoo-laptop</c></ti>
  <ti>Diskussionen über Energiesparen, pcmcia und andere Laptop-bezogene Dinge</ti>
</tr>
<tr>
  <ti><c>gentoo-desktop</c></ti>
  <ti>Mailing-Liste, die sich Gentoo am Desktop widmet</ti>
</tr>
<tr>
  <ti><c>gentoo-desktop-research</c></ti>
  <ti>Diskussionen, wie sich Gentoo am Desktop verbessern lässt</ti>
</tr>
<tr>
  <ti><c>gentoo-performance</c></ti>
  <ti>Diskussionen über das Verbessern der Performance von Gentoo</ti>
</tr>
<tr>
  <ti><c>gentoo-hardened</c></ti>
  <ti>Für eine sicherheits-gehärtete Version von Gentoo</ti>
</tr>
<tr>
  <ti><c>gentoo-portage-dev</c></ti>
  <ti>Diskussionen über Interna von Portage und die
  Schnittstellenentwicklung von Portage</ti>
</tr>
<tr>
  <ti><c>gentoo-catalyst</c></ti>
  <ti>Mailing-Liste, die sich catalyst widmet</ti>
</tr>
<tr>
  <ti><c>gentoo-server</c></ti>
  <ti>Diskussionen über Gentoo in Produktionsumgebungen</ti>
</tr>
<tr>
  <ti><c>gentoo-admin</c></ti>
  <ti>Diskussionen über Administrationsthemen in Gentoo Linux</ti>
</tr>
<tr>
  <ti><c>gentoo-cluster</c></ti>
  <ti>Diskussionen über Gentoo in Cluster-Umgebungen</ti>
</tr>
<tr>
  <ti><c>gentoo-devhelp</c></ti>
  <ti>Diskussionen und Hilfe für Anwender bei der Entwicklung von Ebuilds</ti>
</tr>
<tr>
  <ti><c>gentoo-web-user</c></ti>
  <ti>Diskussionen zur Web-Konfiguration und -Administration
  in Bezug auf Gentoos Web-Werkzeuge</ti>
</tr>
<tr>
  <ti><c>gentoo-embedded</c></ti>
  <ti>Für Gentoo Linux/embedded Anwender- und Entwickler-Diskussionen</ti>
</tr>
<tr>
  <ti><c>gentoo-releng</c></ti>
  <ti>Mailing-Liste für das Gentoo Releasemanagement-Team</ti>
</tr>
<tr>
  <ti><c>gentoo-pr</c></ti>
  <ti>Mailing-Liste für alle Diskussionen zur Öffentlichkeitsarbeit
  von Gentoo</ti>
</tr>
<tr>
  <ti><c>gentoo-qa</c></ti>
  <ti>Diskussionen über Qualitätssicherung und deren Verbesserung in Gentoo</ti>
</tr>
<tr>
  <ti><c>gentoo-devrel</c></ti>
  <ti>Mailing-Liste zur Zusammenarbeit mit Gentoo-Entwicklern</ti>
</tr>
<tr>
  <ti><c>gentoo-userrel</c></ti>
  <ti>Mailing-Liste für Beziehungen zu Gentoo-Anwendern</ti>
</tr>
<tr>
  <ti><c>gentoo-council</c></ti>
  <ti>Gentoo Council Mailing-Liste</ti>
</tr>
<tr>
  <ti><c>gentoo-mirrors</c></ti>
  <ti>Ankündigungen und Diskussionen unter Gentoo-Mirror-Administratoren bezüglich Releases und anderer Themen</ti>
</tr>
<tr>
  <ti><c>gentoo-dev-lang</c></ti>
  <ti>Diskussionen zur Unterstützung von Programmiersprachen in Gentoo und verwandten Themen</ti>
</tr>
<tr>
  <ti><c>gentoo-perl</c></ti>
  <ti>Diskussionen zu Perl auf Gentoo</ti>
</tr>
<tr>
  <ti><c>gentoo-java</c></ti>
  <ti>Diskussionen zu Java auf Gentoo</ti>
</tr>
<tr>
  <ti><c>gentoo-science</c></ti>
  <ti>Diskussionen über wissenschaftliche Anwendungen und Integration in Gentoo</ti>
</tr>
<tr>
  <ti><c>gentoo-media</c></ti>
  <ti>Diskussionen über Medien-Pakete in Gentoo</ti>
</tr>
<tr>
  <ti><c>gentoo-gnustep</c></ti>
  <ti>Diskussionen über GNUstep auf Gentoo</ti>
</tr>
<tr>
  <ti><c>gentoo-installer</c></ti>
  <ti>Diskussionen über das <uri link="/proj/en/releng/installer/">Gentoo Linux Installer Projekt</uri></ti>
</tr>
<tr>
  <ti><c>gentoo-accessibility</c></ti>
  <ti>Diskussionen über das <uri link="/proj/en/desktop/accessibility/">Gentoo Accessibility Projekt</uri></ti>
</tr>
<tr>
  <ti><c>gentoo-scire</c></ti>
  <ti>Diskussionen über das <uri link="/proj/en/scire/">Systems Configuration, Installation and Replication Environment Projekt</uri></ti>
</tr>
<tr>
  <ti><c>gentoo-uk</c></ti>
  <ti>Diskussionen unter Entwicklern im United Kingdom und Organisation von UK-basierten Veranstaltungen</ti>
</tr>
<tr>
  <ti><c>gentoo-au</c></ti>
  <ti>Diskussionen unter australischen Entwicklern und Organisation lokaler Veranstaltungen</ti>
</tr>
<tr>
  <ti><c>gentoo-forum-translations</c></ti>
  <ti>Mailing-Liste zur Übersetzung von Gentoo Foren</ti>
</tr>
<tr>
  <ti><c>gentoo-soc</c></ti>
  <ti>Diskussion über Gentoo-Aktivitäten in Bezug auf Google's Summer of Code</ti>
</tr>
<tr>
  <ti><c>gentoo-lisp</c></ti>
  <ti>Diskussionen über Lisp auf Gentoo</ti>
</tr>
<tr>
  <ti><c>gentoo-vdr</c></ti>
  <ti>Diskussionen über VDR auf Gentoo</ti>
</tr>
<tr>
  <ti><c>gentoo-nfp</c></ti>
  <ti>Die Gentoo NFP/Trustees Mailing-Liste</ti>
</tr>
<tr>
  <ti><c>gentoo-scm</c></ti>
  <ti>Diskussionen über die Migration der primären Gentoo Repositories in andere SCMs</ti>
</tr>
<tr>
  <ti><c>gentoo-pms</c></ti>
  <ti>Diskussionen über die Gentoo Package Manager Spezifikation</ti>
</tr>
</table>

</body>
</section>
<section>
<title>Nicht-englische Mailing-Listen</title>
<body>

<table>
<tr>
  <th>Listenname</th>
  <th>Beschreibung</th>
</tr>
<tr>
  <ti><c>gentoo-user-de</c></ti>
  <ti>deutschsprachige Gentoo-Anwender-Mailing-Liste</ti>
</tr>
<tr>
  <ti><c>gentoo-user-br</c></ti>
  <ti>Brasilianische Gentoo-Anwender-Mailing-Liste</ti>
</tr>
<tr>
  <ti><c>gentoo-user-el</c></ti>
  <ti>Griechische Gentoo-Anwender-Mailing-Liste</ti>
</tr>
<tr>
  <ti><c>gentoo-user-es</c></ti>
  <ti>Lista para la ayuda y discusion de usuarios hispano-hablantes de Gentoo</ti>
</tr>
<tr>
  <ti><c>gentoo-user-fr</c></ti>
  <ti>Französische Gentoo-Anwender-Mailing-Liste</ti>
</tr>
<tr>
  <ti><c>gentoo-user-hu</c></ti>
  <ti>Ungarische Gentoo-Anwender-Mailing-Liste</ti>
</tr>
<tr>
  <ti><c>gentoo-user-id</c></ti>
  <ti>Indonesische Gentoo-Anwender-Mailing-Liste</ti>
</tr>
<tr>
  <ti><c>gentoo-user-kr</c></ti>
  <ti>Koreanische Gentoo-Anwender-Mailing-Liste</ti>
</tr>
<tr>
  <ti><c>gentoo-user-nl</c></ti>
  <ti>Holländische Gentoo-Anwender-Mailing-Liste</ti>
</tr>
<tr>
  <ti><c>gentoo-user-pl</c></ti>
  <ti>Polnische Gentoo-Anwender-Mailing-Liste</ti>
</tr>
<tr>
  <ti><c>gentoo-user-cs</c></ti>
  <ti>Tschechische und Slowakische Gentoo-Anwender-Mailing-Liste</ti>
</tr>
<tr>
  <ti><c>gentoo-user-ru</c></ti>
  <ti>Russische Gentoo-Anwender-Mailing-Liste</ti>
</tr>
<tr>
  <ti><c>gentoo-gwn-de</c></ti>
  <ti>Deutscher Gentoo Weekly Newsletter</ti>
</tr>
<tr>
  <ti><c>gentoo-gwn-es</c></ti>
  <ti>Spanischer Gentoo Weekly Newsletter</ti>
</tr>
<tr>
  <ti><c>gentoo-gwn-fr</c></ti>
  <ti>Französischer Gentoo Weekly Newsletter</ti>
</tr>
<tr>
  <ti><c>gentoo-gwn-nl</c></ti>
  <ti>Holländischer Gentoo Weekly Newsletter</ti>
</tr>
<tr>
  <ti><c>gentoo-gwn-pl</c></ti>
  <ti>Polnischer Gentoo Weekly Newsletter</ti>
</tr>
<tr>
  <ti><c>gentoo-doc-de</c></ti>
  <ti>Deutsche Mailing-Liste zur Übersetzung von Gentoo Dokumentation</ti>
</tr>
<tr>
  <ti><c>gentoo-doc-el</c></ti>
  <ti>Griechische Mailing-Liste zur Übersetzung von Gentoo Dokumentation</ti>
</tr>
<tr>
  <ti><c>gentoo-doc-es</c></ti>
  <ti>
    Lista de correo dedicada a la traduccion y creacion de documentacion en
    Espanol de Gentoo
  </ti>
</tr>
<tr>
  <ti><c>gentoo-doc-fi</c></ti>
  <ti>Finnische Mailing-Liste zur Übersetzung von Gentoo Dokumentation</ti>
</tr>
<tr>
  <ti><c>gentoo-doc-fr</c></ti>
  <ti>Französische Mailing-Liste zur Übersetzung von Gentoo Dokumentation</ti>
</tr>
<tr>
  <ti><c>gentoo-doc-hu</c></ti>
  <ti>Ungarische Mailing-Liste zur Übersetzung von Gentoo Dokumentation</ti>
</tr>
<tr>
  <ti><c>gentoo-doc-id</c></ti>
  <ti>Indonesische Mailing-Liste zur Übersetzung von Gentoo Dokumentation</ti>
</tr>
<tr>
  <ti><c>gentoo-docs-it</c></ti>
  <ti>Italienische Mailing-Liste zur Übersetzung von Gentoo Dokumentation</ti>
</tr>
<tr>
  <ti><c>gentoo-doc-lt</c></ti>
  <ti>Litauische Mailing-Liste zur Übersetzung von Gentoo Dokumentation</ti>
</tr>
<tr>
  <ti><c>gentoo-doc-nl</c></ti>
  <ti>Holländische Mailing-Liste zur Übersetzung von Gentoo Dokumentation</ti>
</tr>
<tr>
  <ti><c>gentoo-doc-pl</c></ti>
  <ti>Polnische Mailing-Liste zur Übersetzung von Gentoo Dokumentation</ti>
</tr>
<tr>
  <ti><c>gentoo-doc-ru</c></ti>
  <ti>Russische Mailing-Liste zur Übersetzung von Gentoo Dokumentation</ti>
</tr>
</table>

</body>
</section>
<section>
<title>Andere Mailing-Listen</title>
<body>

<table>
<tr>
  <th>Listenname</th>
  <th>Beschreibung</th>
</tr>
<tr>
  <ti><c>libconf</c></ti>
  <ti>Zur Diskussion der Entwicklung von libconf</ti>
</tr>
<tr>
  <ti><c>bug-wranglers</c></ti>
  <ti>
    Spezialliste für die "Gentoo Bug Wranglers". Diese Mailing-Liste bedarf
    einer Einladung. Wenn Sie sich anschließen möchten, werden Sie einfach auf
    Bugzilla aktiv und helfen Sie unseren bestehenden Mitgliedern beim
    Durchkauen von Fehlern. Sie werden benachrichtigt und eingeladen, ein
    "Bug Wrangler" zu werden, wenn es angebracht ist.
  </ti>
</tr>
<tr>
  <ti><c>www-redesign</c></ti>
  <ti>Widmet sich der Entwicklung der neuen Gentoo Website</ti>
</tr>
</table>

</body>
</section>

<section>
<title>Archive</title>
<body>

<p>
Gentoo Mailing-Listen-Archive werden vorgehalten auf<br/>
<uri link="http://archives.gentoo.org">archives.gentoo.org</uri>.
</p>

<p>
Die folgenden Stellen halten ebenfalls Archive der meisten der Mailing-Listen
bereit.<br/>
<uri link="http://news.gmane.org/search.php?match=gentoo">Gmane</uri><br/>
<uri link="http://marc.theaimsgroup.com/">MARC: Mailing list ARChives</uri><br/>
<uri link="http://www.mail-archive.com">Mail-Archive</uri>
</p>

</body>
</section>

<section id="faq">
<title>Mailing-Listen Mini-FAQ</title>
<body>

<p>
<b>Ich habe eine Liste mit meiner privaten E-Mail-Adresse abonniert, aber ich
kann vom Arbeitsplatz aus nicht an diese Liste senden. Wie kann ich das Problem
beheben?</b>
</p>

<p>
Um Spam zu reduzieren, sind alle unsere Listen so konfiguriert, dass sie nur
Beiträge von E-Mail-Adressen offizieller Abonnenten erlauben.
Glücklicherweise unterstützt <c>mlmmj</c> "nomail"-Abonnements, die es Ihnen
erlauben, andere E-Mail-Adressen zu registrieren, die nur dazu benutzt werden
können, an die Liste zu senden. Hier ein Beispiel, wie dies funktioniert:
Nehmen wir an, Sie haben die Liste <c>gentoo-dev</c> als <c>jim@home.com</c>
abonniert, aber Sie möchten auch mit Ihrer <c>james@work.com</c>
E-Mail-Adresse an die Liste senden. Um dies zu erreichen, schicken Sie (als
<c>james@work.com</c>) eine Nachricht an
<c>gentoo-dev+subscribe-nomail@lists.gentoo.org</c>.
Dann sollte es Ihnen möglich sein, an <c>gentoo-dev</c> sowohl mit ihrer
privaten als auch mit Ihrer geschäftlichen E-Mail-Adresse zu senden.
</p>

<p>
In Übereinstimmung mit dem eigentlichen Ziel, Spam zu vermeiden, werden Ihre
Mails nach /dev/null versendet, wenn Sie an eine Liste senden und die
Absenderadresse die Liste nicht abonniert hat. Sie erhalten <b>keine</b>
Bounce-Nachrichten von unseren Servern. Dadurch wird vermieden, dass Spammer
Ihre Adresse fälschen können, um Ihnen einen Bounce zu schicken.
</p>

<p>
<b>Ich möchte zwischen normaler Zustellung und gesammelter (digest) Zustellung
wechseln. Wie erreiche ich das?</b>
</p>

<p>
Beenden Sie das Abonnement der normalen Liste und abonnieren Sie die
Digest-Liste. Für die Liste <c>Listenname</c> ginge das durch das Senden leerer
E-Mails an die folgenden beiden Adressen:
</p>

<p><c>Listenname+unsubscribe@lists.gentoo.org</c><br/>
<c>Listenname+subscribe-digest@lists.gentoo.org</c>
</p>

<p>
<b>Wie verwende ich procmail, um Nachrichten der Gentoo Mailing-Listen
zu filtern?</b>
</p>

<p>
Um von der Liste <c>Listenname</c> eingehende E-Mails zu filtern,
benutzen Sie die folgende <c>procmail</c>-Regel:
</p>

<pre caption="procmail Beispielregel">
:0:
* ^List-Id:.*Listenname\.gentoo\.org
Mail/Listenname
</pre>

<p>
Auf dieselbe Weise würden Sie eingehende
<e>Mailman</e>-Mailing-Listen-Manager-Mails filtern.
</p>


<p>
<b>Verschiedene Listen-Richtlinien</b>
</p>

<p>
HTML-Emails sollten nicht verschickt werden, aber sind nicht verboten (es gibt
einige MUA, besonders Web-basierte, die es einem sehr schwer machen, HTML
komplett abzuschalten). Seien Sie sich bewusst, dass einige Benutzer ihre Seite
so konfiguriert haben, dass ankommendes HTML komplett ignoriert wird. Daher kann
es so aussehen, als würden Sie ignoriert werden, besonders wenn Sie eine
HTML-only Email geschickt haben. Sie sollten sich bemühen, folgendes zu senden
(in der empfohlenen Reihenfolge): text/plain, multipart mit text/plain vor
text/html, text/html. MIME ist akzeptabel und oft genutzt.
</p>

<p>
Bitte schicken Sie keinerlei Abwesenheits- oder Nicht-im-Büro-Nachrichten an
die Liste. Im Interesse der Reduzierung von Listen-Spam werden wir, wenn Sie
irgendwelche automatischen Antwortmails einstellen, die an die Listen gehen, Ihr
Konto von ALLEN Listen abmelden. Alle zuvor registrierten Adressen, die
Mailserver-Nachrichten an die Listen senden, werden ebenfalls entfernt.
</p>

<note>
Zu generellen Verhaltensregeln auf Mailing-Listen bieten <uri
link="http://www.dtcc.edu/cs/rfc1855.html">diese Leitfäden</uri> einen
exzellenten Einstieg.
</note>

</body>
</section>

</chapter>
</mainpage>
