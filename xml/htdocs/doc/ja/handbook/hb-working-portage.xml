<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE sections SYSTEM "/dtd/book.dtd">

<!-- The content of this document is licensed under the CC-BY-SA license -->
<!-- See http://creativecommons.org/licenses/by-sa/2.5 -->

<!-- $Header: /var/cvsroot/gentoo/xml/htdocs/doc/ja/handbook/hb-working-portage.xml,v 1.18 2010/03/14 22:41:41 shindo Exp $ -->

<sections>

<abstract>
この章では、システムにおけるソフトウェアの管理をしていくのに、
ユーザーが知っておくべき「簡単な」手順を説明します。
</abstract>


<version>1.67</version>
<date>2010-03-02</date>
<!-- Original revision: 1.74 -->
<!-- Translator: kobayashi -->

<section>
<title>Portageへようこそ</title>
<body>

<p>
PortageはもしかするとGentooのソフトウェア管理における最も注目すべき革新なものかもしれません。
その高い柔軟性と非常に多くの機能で、たびたびLinuxで利用できる最高のソフトウェア管理ツールとされています。
</p>

<p>
Portageは全て<uri link="http://www.python.org">Python</uri>と<uri link="http://www.gnu.org/software/bash">Bash</uri>によって書かれており、
両方ともスクリプト言語であるため、ユーザーにとって全体的に見通しのよいものになっています。
</p>

<p>
ほとんどのユーザーが<c>emerge</c>ツールを通してPortageを利用しています。
この章はemergeのmanページで得られる情報の複製を意図したものではありません。
emergeのオプションについての完全な概要を得るためには、manページを調べてください。
</p>

<pre caption="emergeのmanページを読む">
$ <i>man emerge</i>
</pre>

</body>
</section>
<section>
<title>Portageツリー</title>
<subsection>
<title>ebuild</title>
<body>

<p>
パッケージについて話す場合、たいていは、私たちGentooユーザーがPortageツリーを通して利用可能なソフトウェアのタイトルを意味しています。
Portageツリーは<e>ebuild</e>という、Portageがソフトウェアを維持管理する(インストール、検索など)ために必要な全ての情報を含んだファイルのコレクションです。
デフォルトでは、これらebuildは<path>/usr/portage</path>に置かれています。
</p>

<p>
Porgageにソフトウェアタイトルに関して、何か実行するよう指示するときはいつでも、システムにあるebuildがベースに使われます。
故に、Portageに新しいソフトウェアやセキュリティアップデートなどが知らされるように、定期的にebuildを更新することが重要です。
</p>

</body>
</subsection>
<subsection>
<title>Portageツリーの更新</title>
<body>

<p>
Portageツリーは一般に<uri link="http://rsync.samba.org/">rsync</uri>という高速ファイル転送ユーティリティで更新されます。
更新はrsyncのフロントエンドを提供する<c>emerge</c>コマンドを使うという、とても簡単なものです。
</p>

<pre caption="Portageツリーの更新">
# <i>emerge --sync</i>
</pre>

<p>
もしファイアーウォールの制限などによりrsyncが実行できないときには、毎日作成されるPortageツリーのスナップショットを利用することによってPortageを更新することができます。
<c>emerge-webrsync</c>ツールは自動的に最新のスナップショットを取得し、システムにインストールしてくれます。
</p>

<pre caption="emerge-webrsyncを実行">
# <i>emerge-webrsync</i>
</pre>

</body>
</subsection>
</section>
<section>
<title>ソフトウェアのメンテナンス</title>
<subsection>
<title>ソフトウェアの検索</title>
<body>

<p>
Portageツリーからソフトウェアタイトルを検索するには、<c>emerge</c>に内蔵された検索能力を利用することができます。
デフォルトでは、<c>emerge --search</c>は検索単語に(完全もしくは部分)一致したパッケージ名を返します。
</p>

<p>
例えば、"pdf"を名前に持つパッケージを検索するにはこうします。
</p>

<pre caption="pdfと名付けられたパッケージを検索">
$ <i>emerge --search pdf</i>
</pre>

<p>
もし詳細を検索したいのなら<c>--searchdesc</c>(もしくは <c>-S</c>)スイッチを使うことができます。
</p>

<pre caption="pdfに関連したパッケージを検索">
$ <i>emerge --searchdesc pdf</i>
</pre>

<p>
出力を見れば、それがたくさんの情報を提供していることに気付くでしょう。
その内容が意味するところがわかるように、各フィールドにははっきりとラベル付けがされています。
</p>

<pre caption="'emerge --search'の結果の例">
*  net-print/cups-pdf
      Latest version available: 1.5.2
      Latest version installed: [ Not Installed ]
      Size of downloaded files: 15 kB
      Homepage:    http://cip.physik.uni-wuerzburg.de/~vrbehr/cups-pdf/
      Description: Provides a virtual printer for CUPS to produce PDF files.
      License:     GPL-2
</pre>

</body>
</subsection>
<subsection>
<title>ソフトウェアのインストール</title>
<body>

<p>
一度好みのソフトウェアを見つけたら、<c>emerge</c>で簡単にインストールすることができます。ただパッケージの名前を追加するだけです。
例えば、<c>gnumeric</c>をインストールするにはこうします。
</p>

<pre caption="gnumericのインストール">
# <i>emerge gnumeric</i>
</pre>

<p>
たくさんのアプリケーションがお互いに依存し合っているため、あるソフトウェアを確実にインストールしようとすると、
結果的にいくつかの依存関係もインストールすることになるでしょう。
ご心配なく、Portageは依存関係をうまく扱ってくれます。
もしパッケージをインストールしたいときにPortageが何をインストール<e>しようとしている</e>か確かめたいときには、
<c>--pretend</c>スイッチを追加します。例えばこのようにします。
</p>

<pre caption="gnumericをインストールするふりをする">
# <i>emerge --pretend gnumeric</i>
</pre>

<p>
Portageにパッケージをインストールするよう要求したときは、必要なソースコードをインターネットからダウンロードし、
デフォルトでは<path>/usr/portage/distfiles</path>に保持します。
この後、解凍し、パッケージをコンパイルしてインストールします。
Portageにソースをダウンロードするだけでインストールは行って欲しくないときには、<c>emerge</c>コマンドに<c>--fetchonly</c>コマンドを追加します。
</p>

<pre caption="gnumericのソースコードをダウンロード">
# <i>emerge --fetchonly gnumeric</i>
</pre>

</body>
</subsection>
<subsection>
<title>インストール済みパッケージのドキュメントを検索</title>
<body>

<p>
多くのパッケージはドキュメントと共にインストールされます。
<c>doc</c>USEフラグがパッケージのドキュメントをインストールするかしないかを定義する場合もあります。
現在の<c>doc</c>USEフラグを確認するには<c>emerge -vp &lt;package name&gt;</c>コマンドを使用します。
</p>

<pre caption="現在のdoc USEフラグを確認">
<comment>(もちろんalsa-libはただの例です)</comment>
# <i>emerge -vp alsa-lib</i>
[ebuild  N    ] media-libs/alsa-lib-1.0.14_rc1  -debug +doc 698 kB
</pre>

<p>
<c>doc</c>USEフラグを有効にするのに一番良い方法は、<path>/etc/portage/package.use</path>によってパッケージごとに行うやり方です。
そのため、興味のあるパッケージの分だけドキュメントを取得できるようになります。
このフラグを全体に有効にしてしまうと、循環依存の問題を起こす原因となることが知られています。
詳しくは、<uri link="?part=2&amp;chap=2">USEフラグ</uri>の章を読んでください。 
</p>

<p>
パッケージがインストールされると、ドキュメントは一般的には<path>/usr/share/doc</path>ディレクトリの下のパッケージ名のディレクトリの中に置かれます。
<c>app-portage/gentoolkit</c><uri link="/doc/en/gentoolkit.xml">パッケージ</uri>（<uri link="/doc/ja/gentoolkit.xml">日本語訳</uri>）の一部である<c>equery</c>ツールを利用すると、全てのインストールされたファイルの一覧を表示できます。
</p>

<pre caption="パッケージドキュメントの位置">
# <i>ls -l /usr/share/doc/alsa-lib-1.0.14_rc1</i>
total 28
-rw-r--r--  1 root root  669 May 17 21:54 ChangeLog.gz
-rw-r--r--  1 root root 9373 May 17 21:54 COPYING.gz
drwxr-xr-x  2 root root 8560 May 17 21:54 html
-rw-r--r--  1 root root  196 May 17 21:54 TODO.gz

<comment>(このほかにも、equeryを使用してファイルの位置を表示する方法もあります)</comment>
# <i>equery files alsa-lib | less</i>
media-libs/alsa-lib-1.0.14_rc1
* Contents of media-libs/alsa-lib-1.0.14_rc1:
/usr
/usr/bin
/usr/bin/alsalisp
<comment>(省略)</comment>
</pre>

</body>
</subsection>
<subsection>
<title>ソフトウェアの削除</title>
<body>

<p>
システムからパッケージを削除したいときは、<c>emerge --unmerge</c>を利用します。
これはPortageにパッケージによってインストールされた設定ファイル<e>をのぞく</e>全てのファイルを削除するよう命令します。
設定ファイルを残しておくと、もう一度インストールしようと決めたときに作業を続けることが可能です。
</p>

<p>
しかし、<brite>大きな警告</brite>があります:Portageは削除したいパッケージが他から必要とされているかを確認<e>しません</e>。
とにかく、unmergeするとシステムを破壊する様な重要なパッケージを削除するときには気をつけろと言うことです。
</p>

<pre caption="gnumericをシステムから削除">
# <i>emerge --unmerge gnumeric</i>
</pre>

<p>
システムからパッケージを削除するときには、インストール時に依存関係により自動的にインストールされたソフトウェアは残されます。
Portageにある削除可能な依存関係を削除するには、<c>emerge</c>の<c>--depclean</c>機能を利用します。
これについては後で話します。
</p>

</body>
</subsection>
<subsection>
<title>システムの更新</title>
<body>

<p>
システムを完全な形に保つ(もちろん最新のセキュリティアップデートをインストールすることも)には、システムを定期的に更新する必要があります。
Portageはツリーの中のebuildsのみ確認するので、最初にPortageツリーを更新しなければなりません。
Portageツリーが更新されたら、<c>emerge --update world</c>でシステムを更新できます。
次の例では<c>--ask</c>スイッチを使用しています。
これは、更新するパッケージの一覧をPortageに表示させ、続行するかを尋ねます。
</p>

<pre caption="システムを更新する">
# <i>emerge --update --ask world</i>
</pre>

<p>
Portageはインストールされているアプリケーションの新しいバージョンを検索します。
しかし、これは<e>明示的に</e>インストールされたものしか確かめません（そうしたアプリケーションは<path>/var/lib/portage/world</path>にリストされています）。
つまり、それらの依存関係は確認しないということです。
システムの<e>個々のパッケージすべて</e>を更新したいときには<c>--deep</c>引数を与えてやります。
</p>

<pre caption="システムの全てを更新する">
# <i>emerge --update --deep world</i>
</pre>

<p>
明示的にインストールしていない（だが他のプログラムの依存によりインストールされた）パッケージのセキュリティアップデートがあるかもしれないので、このコマンドを時々実行することが推奨されています。
</p>

<p>
もし最近<uri link="?part=2&amp;chap=2">USEフラグ</uri>を変更したのなら、<c>--newuse</c>を追加したくなるでしょう。
そうするとPortageは新しいパッケージのインストールか既にあるものの再コンパイルが必要かを確認します。
</p>

<pre caption="完全な更新の実行">
# <i>emerge --update --deep --newuse world</i>
</pre>

</body>
</subsection>
<subsection>
<title>メタパッケージ</title>
<body>

<p>
Portageツリーのいくらかのパッケージは実際には何も含まれていませんが、複数のパッケージをインストールするのに利用されるものがあります。
例えば、<c>kde-meta</c>パッケージは、依存関係にある様々なKDE関連のパッケージを制御することで、システムに完全なKDE環境をインストールします。
</p>

<p>
このようなパッケージをシステムから削除したいときには、<c>emerge --unmerge</c>をパッケージに対して実行しても依存関係が残ってしまうのでたいした効果は得られないでしょう。
</p>

<p>
Portageは残された依存関係を削除する機能性を持っていますが、動的に依存関係を変更するソフトウェアが存在するので、
USEフラグに変更を加えたときも含めてまずシステムを完全に更新する必要があります。
その後残された依存関係を削除するために<c>emerge --depclean</c>を実行してください。
これが完了したら、今削除されたソフトウェアを動的にリンクしているが、もはやそれを必要としないアプリケーションを再ビルドします。
</p>

<p>
これら全ては以下の３つのコマンドで処理できます。
</p>

<pre caption="残された依存関係を削除する">
# <i>emerge --update --deep --newuse world</i>
# <i>emerge --depclean</i>
# <i>revdep-rebuild</i>
</pre>

<p>
<c>revdep-rebuild</c>は<c>gentoolkit</c>によって提供されます;まず最初にemergeすることを忘れないでください。
</p>

<pre caption="gentoolkitパッケージのインストール">
# <i>emerge gentoolkit</i>
</pre>

</body>
</subsection>
</section>

<section id="license">
<title>ライセンス</title>
<subsection>
<body>
  	 
 <p>
Portage バージョン2.1.7から、ソフトウェアのライセンスをもとにして、ソフトウェアのインストール可否を制御できます。
Portageツリーにある全てパッケージで、ebuildに<c>LICENSE</c>項目が含まれています。
<c>emerge --search packagename</c>を実行すれば、パッケージのライセンスがわかります。
</p>
  	 
<p>
デフォルトでは、使用許諾契約書(EULA)に同意しなければならないものを除いて、Portageは全てのライセンスを許可します。
</p>
  	 
<p>
許可するライセンスをコントロールするのは、<c>ACCEPT_LICENSE</c>です。これは、<path>/etc/make.conf</path>のなかで定義されます。
</p>
  	 
<pre caption="/etc/makc.conf でのデフォルト ACCEPT_LICENSE">
ACCEPT_LICENSE="* -@EULA"
</pre>
  	 
<p>
この設定により、インストール中に使用許諾契約書に同意する行為が発生するようなパッケージは、<e>インストールされず、</e>
使用許諾契約書がないパッケージが<e>インストールされます。</e>
</p>
  	 
<p>
<c>ACCEPT_LICENSE</c>はシステム全体では<path>/etc/make.conf</path>で定義可能です。
また、パッケージごとに<path>/etc/portage/package.license</path>でも指定可能です。
</p>
  	 
<p>
たとえば、<c>app-crypt/truecrypt</c>に対して、<c>truecrypt-2.7</c>ライセンスを許可する場合、
次の内容を<path>/etc/portage/package.license</path>に追加してください。
</p>
  	 
<pre caption="package.licenseでtruecryptライセンスを指定する">
app-crypt/truecrypt truecrypt-2.7
</pre>
  	 
<p>
これによって、<c>truecrypt-2.7</c>ライセンスになっているtruecryptがインストール可能になりますが、<c>truecrypt-2.8</c>はインストールされません。
</p>
  	 
<impo>
各ライセンスは<path>/usr/portage/licenses</path>に置かれ、ライセンスグループは<path>/usr/portage/license_groups</path>に書かれています。
license_groupsの各行で、大文字から始まる項目はライセンスグループの名前で、その後に個々のライセンスが並びます。
</impo>
  	 
<p>
<c>ACCEPT_LICENSE</c>で定義されたライセンスグループは、<b>@</b>記号が前につきます。
次の例ではシステム全体で、他の個々のライセンスと同様に、GPL互換ライセンスグループを許可しています。
</p>
  	 
<pre caption="/etc/make.conf での ACCEPT_LICENSE">
ACCEPT_LICENSE="@GPL-COMPATIBLE @OSI-APPROVED @EULA atheros-hal BitstreamVera"
</pre>
  	 
<p>
もし、フリーソフトウエアとドキュメントだけのシステムにしたい場合は、次のようにすればよいでしょう。
</p>
  	 
<pre caption="フリーのライセンスだけを使用する">
ACCEPT_LICENSE="-* @FREE"
</pre>
  	 
<p>
この場合の"free"とは、ほぼ<uri link="http://www.gnu.org/philosophy/free-sw.html">FSF</uri> と <uri link="http://www.opensource.org/docs/osd">OSI</uri>で定義されたものです。
これらの定義に合致しないライセンスを持つパッケージはインストールされません。
</p>
  	 
</body>
</subsection>
</section>

<section>
<title>Portageが不満を言ったら</title>
<subsection>
<title>SLOT、Virtual、ブランチ、アーキテクチャ、そしてProfilesについて</title>
<body>

<p>
既に述べたように、Portageはとても強力で他のソフトウェア管理ツールに無い多くの機能をサポートしています。
このことを理解するために、あまり細かくなり過ぎるのを避けて、もう少しPortageの特徴について説明します。
</p>

<p>
Portageでは、一つのパッケージで複数のバージョンをシステム上に共存させることができます。
他のディストリビューションがそれらの名前にバージョンを付けている(<c>freetype</c>と<c>freetype2</c>の様に)のに対し、
Portageは<e>SLOT</e>と呼ばれる技術を使っています。
ebuildはそのバージョンの確かなSLOTを宣言します。異なったSLOTのebuildはシステムに共存できます。
例えば、<c>freetype</c>パッケージは<c>SLOT="1"</c>と<c>SLOT="2"</c>のebuildを持っています。
</p>

<p>
同じ機能を提供しますが異なった実装のパッケージもまた存在します。
例えば、<c>metalogd</c>、<c>sysklogd</c>、<c>syslog-ng</c>は全てシステムロガーです。
「システムロガー」の機能を必要とするアプリケーションは、例えば<c>metalogd</c>に依存することはできません。他のシステムロガーも同様に問題のない選択だからです。
Portageは<e>virtuals</e>を考慮に入れます:他のアプリケーションが<c>virtual/syslog</c>に依存できるように、それぞれのシステムロガーは<c>virtual/syslog</c>を規定します。
</p>

<p>
Portageツリーのソフトウェアは異なったブランチに所属することができます。
デフォルトではシステムはGentooがstableだと思うもののみ受け付けます。
ほとんどのコミットされた新しいソフトウェアのタイトルは、stableにされる前にもっとテストが必要だという意味のテストブランチに追加されます。
これらのソフトウェアのebuildをPortageツリーで見かけても、Portageはstableブランチに置かれるまでは更新しようとしないでしょう。
</p>

<p>
いくらかのソフトウェアは少しのアーキテクチャのみで利用可能です。
すなわちそのソフトウェアは他のアーキテクチャでは動作しないか、もっとテストが必要か、
Portageツリーにソフトウェアをコミットした開発者が異なったアーキテクチャで動作するかどうか確認できないかです。
</p>

<p>
各々のGentooのインストールはある<c>プロファイル</c>と一緒になっています。
それには、他の情報と一緒に、システムが正常に動作するために必要なパッケージのリストが含まれています。
</p>

</body>
</subsection>
<subsection id="blocked">
<title>ブロックされたパッケージ</title>
<body>

<pre caption="ブロックされたパッケージに対するPortageの警告(--pretendを利用)">
[blocks B     ] mail-mta/ssmtp (is blocking mail-mta/postfix-2.2.2-r1)
</pre>

<pre caption="ブロックされたパッケージに対するPortageの警告(--pretendを利用しない)">
!!! Error: the mail-mta/postfix package conflicts with another package.
!!!        both can't be installed on the same system together.
!!!        Please use 'emerge --pretend' to determine blockers. 
</pre>

<p>
ebuildはPortageの依存関係に関する特別なフィールドを含んでいます。
依存関係には２つの種類があります: <c>DEPEND</c>によって宣言されたビルド依存と、<c>RDEPEND</c>よって宣言された実行時依存です。
これらの依存関係で明示的に互換性<e>のない</e>パッケージまたはvirtualが指定されている場合、インストールがブロックされます。
</p>

<p>
Portageの最近のバージョンでは、些細なブロックについてはユーザーへの注意を促すことなく対処することができますが、
場合によっては、次に説明する方法で自身で修正しなければなりません。
</p>

<p>
ブロックを解決するには、パッケージのインストールを行わないか、衝突しているパッケージを先にunmergeするかのどちらかを選べます。
上記の例では、<c>postfix</c>のインストールを諦めるか、先に<c>ssmtp</c>を削除するかのどちらかです。
</p>

<p>
<b>&lt;</b>media-video/mplayer-1.0_rc1-r2.のように特定のパッケージ識別子（atom）を伴いブロックされていることがあるかもしれません。
この場合、ブロックしているパッケージをより新しいバージョンに更新すれば、ブロックを取り除くことができかもしれません。
</p>

<p>
既にインストールされた2つのパッケージがお互いをブロックし合っていることもあります。
このまれな場合は、何故それらをインストールする必要があるのかを知るべきです。
ほとんどの場合、1つのパッケージのみで事足ります。
そうでなければ、<uri link="http://bugs.gentoo.org">Gentooのバグトラックシステム</uri>にバグを報告してください。
</p>

</body>
</subsection>
<subsection id="masked">
<title>マスクされたパッケージ</title>
<body>

<pre caption="マスクされたパッケージに対するPortageの警告">
!!! all ebuilds that could satisfy "bootsplash" have been masked. 
</pre>

<pre caption="マスクされたパッケージに対するPortageの警告に関する理由">
!!! possible candidates are:

- gnome-base/gnome-2.8.0_pre1 (masked by: <i>~x86 keyword</i>)
- lm-sensors/lm-sensors-2.8.7 (masked by: <i>-sparc keyword</i>)
- sys-libs/glibc-2.3.4.20040808 (masked by: <i>-* keyword</i>)
- dev-util/cvsd-1.0.2 (masked by: <i>missing keyword</i>)
- games-fps/unreal-tournament-451 (masked by: <i>package.mask</i>)
- sys-libs/glibc-2.3.2-r11 (masked by: <i>profile</i>)
- net-im/skype-2.1.0.81 (masked by: skype-eula <i>license</i>(s))
</pre>

<p>
システムで利用できないパッケージをインストールしようとしたときに、このマスクエラーを受け取るでしょう。
あなたはシステムで利用できる他のアプリケーションのインストールを試みるか、パッケージが利用可能になるまで待つかをするべきです。
パッケージがマスクされているのにはいつも理由があります。
</p>

<ul>
  <li>
    <b>~arch keyword</b> はstableブランチに置くには十分なテストが行われていないことを意味します。
    何日か何週間か待ってもう一度試してみてください。
  </li>
  <li>
    <b>-arch keyword</b>か<b>-* keyword</b>はあなたのアーキテクチャでは動作しないことを意味します。
    もしパッケージが動作すると信じているなら<uri link="http://bugs.gentoo.org">bugzilla</uri>にバグを提出してください。
  </li>
  <li>
    <b>missing keyword</b>はあなたのアーキテクチャでは未だテストされていないことを意味します。
     アーキテクチャのポーティングチームにテストするよう頼むか、彼らのために自分でテストして気付いたことを<uri link="http://bugs.gentoo.org">bugzilla</uri>まで報告してください。
  </li>
  <li>
    <b>package.mask</b>はパッケージが壊れている、unstableもしくは非推奨であるということが判明していて、故意に利用禁止とマークされていることを意味します。
  </li>
  <li>
    <b>profile</b>はあなたのプロファイルには適当ではないことを意味します。
    インストールするとシステムを破壊するおそれがあるか、もしくは単にあなたのプロファイルと互換性がないかのどちらかです。
  </li>
  <li>
    <b>license</b>はパッケージのライセンスが<c>ACCEPT_LICENSE</c>と合致しないことを意味します。
    <path>/etc/make.conf</path>か<path>/etc/portage/package.license</path>に定義することで、
    明示的にそのライセンスかライセンスグループを許可しなければいけません。
    詳しくは、<uri link="#license">ライセンス</uri>を参照してください。
  </li>

</ul>

</body>
</subsection>
<subsection id="missingdependencies">
<title>依存関係の喪失</title>
<body>

<pre caption="依存関係の喪失に対するPortageの警告">
emerge: there are no ebuilds to satisfy "&gt;=sys-devel/gcc-3.4.2-r4".

!!! Problem with ebuild sys-devel/gcc-3.4.2-r2
!!! Possibly a DEPEND/*DEPEND problem. 
</pre>

<p>
インストールしようとしているアプリケーションはシステムで利用できない他のパッケージに依存しています。
<uri link="http://bugs.gentoo.org">bugzilla</uri>に既に報告されているか確認して、まだであれば報告してください。
ブランチを混ぜていない限りこれが起こることはありませんので、それはバグであると言えます。
</p>

</body>
</subsection>
<subsection id="ambiguousebuild">
<title>曖昧なebuild名</title>
<body>

<pre caption="曖昧なebuild名に対するPortageの警告">
[ Results for search key : listen ]
[ Applications found : 2 ]

*  dev-tinyos/listen [ Masked ]
      Latest version available: 1.1.15
      Latest version installed: [ Not Installed ]
      Size of files: 10,032 kB
      Homepage:      http://www.tinyos.net/
      Description:   Raw listen for TinyOS
      License:       BSD

*  media-sound/listen [ Masked ]
      Latest version available: 0.6.3
      Latest version installed: [ Not Installed ]
      Size of: 859 kB
      Homepage:      http://www.listen-project.org
      Description:   A Music player and management for GNOME
      License:       GPL-2

!!! The short ebuild name "listen" is ambiguous. Please specify
!!! one of the above fully-qualified ebuild names instead.
</pre>

<p>インストールしようとしているアプリケーション名が２つ以上と一致しています。
正しいカテゴリー名を追加する必要があります。
Portageは可能性のある一致した選択肢を知らせるでしょう。
</p>

</body>
</subsection>
<subsection id="circulardependencies">
<title>循環依存</title>
<body>

<pre caption="循環依存に対するPortageの警告">
!!! Error: circular dependencies: 

ebuild / net-print/cups-1.1.15-r2 depends on ebuild / app-text/ghostscript-7.05.3-r1
ebuild / app-text/ghostscript-7.05.3-r1 depends on ebuild / net-print/cups-1.1.15-r2 
</pre>

<p>
２つ(もしくはそれ以上)のインストールしたいパッケージがお互いに依存し合っているためにインストールすることができません。
これはほとんどはPortageツリーのバグです。
<uri link="http://bugs.gentoo.org">bugzilla</uri>に既に報告されているか確認して、まだであれば報告してください。
</p>

</body>
</subsection>
<subsection id="fetchfailed">
<title>取得失敗</title>
<body>

<pre caption="取得失敗に対するPortageのエラー">
!!! Fetch failed for sys-libs/ncurses-5.4-r5, continuing...
<comment>(...)</comment>
!!! Some fetch errors were encountered.  Please see above for details.
</pre>

<p>
Portageがアプリケーションのソースのダウンロードに失敗し、他のアプリケーションのインストールを続けようとしています。
この失敗はミラーが正しく同期されていないか、ebuildが正しくない場所を示しているからかもしれません。
ソースを置いているサーバーが何らかの理由でダウンしているのかもしれません。
</p>

<p>
この問題がいつまでも続くようであれば１時間後にもう一度試してください。
</p>

</body>
</subsection>
<subsection id="profileprotect">
<title>システムプロファイルの保護</title>
<body>

<pre caption="プロファイルによって保護されたパッケージに対するPortageの警告">
!!! Trying to unmerge package(s) in system profile. 'sys-apps/portage'
!!! This could be damaging to your system.
</pre>

<p>
あなたはシステムのコアの一部であるパッケージを削除しようとしました。
それはプロファイルに必要であると載っているのでシステムから削除できません。
</p>

</body>
</subsection>
<subsection id="digesterror">
<title>ダイジェスト検証の失敗</title>
<body>

<p>
ときどき、あるパッケージをemergeしようとして、次のようなメッセージと共に失敗することがあるかもしれません。
</p>

<pre caption="ダイジェスト検証の失敗">
&gt;&gt;&gt; checking ebuild checksums
!!! Digest verification failed:
</pre>

<p>
これはPortageツリーにどこかおかしい所がある兆候です。
よく、開発者がパッケージをツリーにコミットするとき失敗したことが原因で起こります。
</p>

<p>
Digestの検証に失敗するとき、そのパッケージをあなた自身で再度ダイジェスト作成しようとしては<e>いけません。</e>
この問題は<c>ebuild foo manifest</c>では修正されない上に、ほぼ間違いなく悪化します。
</p>

<p>
かわりに、1時間か2時間ツリーが安定するのを待っていてください。
おそらくエラーはすぐに検知されると思いますが、修正がPortageツリーに流れるのに少し時間がかかります。
待っている間、<uri link="http://bugs.gentoo.org">Bugzilla</uri>をチェックし、誰かがこの問題をすでに報告しているか調べてください。
もし報告している人がいなければ、すぐ壊れているパッケージのバグ報告をしてください。
</p>

<p>
いったんバグが修正されたことを確認したら、再び同期し修正されたダイジェストを取得する必要があるでしょう。
</p>

<impo>
これは何度もツリーの同期をしてもよいという意味では<e>ありません</e>！
rsycnポリシーの中で述べられているように（<c>emerge --sync</c>を走らせたとき）、
あまり頻繁に同期するユーザは接続を禁止されることになります！
実際には次の定期的な同期まで待ち、そしてrsyncサーバに負荷をかけないようにしてください。
</impo>

</body>
</subsection>
</section>
</sections>
