<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE sections SYSTEM "/dtd/book.dtd">

<!-- The content of this document is licensed under the CC-BY-SA license -->
<!-- See http://creativecommons.org/licenses/by-sa/2.5 -->

<!-- $Header: /var/cvsroot/gentoo/xml/htdocs/doc/de/handbook/hb-working-features.xml,v 1.20 2008/03/06 01:05:44 grahl Exp $ -->

<sections>

<abstract>
Entdecken Sie die Funktionen von Portage, wie Unterstützung für verteilte
Kompilierung, ccache und mehr.
</abstract>

<version>1.32</version>
<date>2008-03-02</date>

<section>
<title>Portage Features</title>
<body>

<p>
Portage hat einige zusätzliche Features, die das Gentoo Erlebnis noch ein wenig
besser machen. Viele dieser Features beruhen auf Software Tools um die
Performance, Funktionssicherheit, Sicherheit, etc. zu verbessern.
</p>

<p>
Um diese Portage Features zu aktivieren oder deaktivieren müssen Sie die
<c>FEATURES</c> Variable in <path>/etc/make.conf</path> anpassen. Diese
Variable enthält verschiedene Feature Keywords, welche durch Leerzeichen
getrennt werden.In vielen Fällen müssen Sie ebenfalls zusätzliche Software
installieren.
</p>

<p>
Nicht alle Features, die Portage anbietet werden hier aufgelistet. Für einen
vollständigen Überblick schauen Sie in die <path>make.conf</path> Manpage:
</p>

<pre caption="Lesen der make.conf Manpage">
$ <i>man make.conf</i>
</pre>

<p>
Um herauszufinden, welche FEATURES per default aktiviert sind führen Sie
<c>emerge --info</c> aus und suchen Sie nach der FEATURES Variable, oder filtern
diese aus:
</p>

<pre caption="Herausfinden welche FEATURES bereits aktiv sind">
$ <i>emerge --info | grep FEATURES</i>
</pre>

</body>
</section>
<section>
<title>Distributed Compiling</title>
<subsection>
<title>Benutzung von distcc</title>
<body>

<p>
<c>distcc</c> ist ein Programm das "Kompilierungen" über mehrere, nicht
notwendigerweise identische Computer, über ein Netzwerk verteilt. Der
<c>distcc</c> - Client sendet alle notwendigen Informationen zu den
erreichbaren distcc - Servern (die den <c>distccd</c> laufen haben), so
dass diese Teile des Quellcodes vom Client kompilieren können. Als Resultat
wird Zeit beim Kompilieren eingespart.
</p>

<p>
Sie finden tiefer gehende Informationen über <c>distcc</c> (und wie Sie es bei
Gentoo zum Laufen bringen) in unserer <uri link="/doc/de/distcc.xml">Gentoo
distcc Dokumentation</uri>.
</p>

</body>
</subsection>
<subsection>
<title>distcc installieren</title>
<body>

<p>
Distcc bringt einen grafischen Monitor mit, der Sie über alle Aufgaben
informiert, die der Computer zum Kompilieren wegsendet. Falls Sie
Gnome benutzen, setzen Sie 'gnome' in Ihrer USE-Variable.
Wenn Sie nicht Gnome nutzen, den Monitor aber trotzdem haben
wollen, sollten Sie 'gtk' in Ihrer USE-Variable setzen.
</p>

<pre caption="distcc installieren">
# <i>emerge distcc</i>
</pre>

</body>
</subsection>
<subsection>
<title>Unterstützung für Portage aktivieren</title>
<body>

<p>
Fügen Sie <c>distcc</c> der FEATURES Variable in <path>/etc/make.conf</path>
hinzu. Anschließen editieren Sie die MAKEOPTS Variable entsprechend Ihren
Wünschen. In den meisten Fällen ist es hinreichend "-jX" anzugeben, wobei X der
Nummer der der CPUs die den <c>distccd</c> ausführen entsprechen (inklusive dem
aktuellen Host) plus eins, möglicherweise haben Sie mit anderen Werten bessere
Ergebnisse.
</p>

<p>
Nun rufen Sie <c>distcc-config</c> auf und tragen eine Liste der verfügbaren
Server ein. Als einfaches Beispiel nehmen wir einmal an, dass die verfügbaren
distcc Server <c>192.168.1.102</c> (der momentane Host),
<c>192.168.1.103</c> und <c>192.168.1.104</c> (zwei "entfernte" Hosts) sind:
</p>

<pre caption="distcc für die Nutzung dreier verfügbarer distcc Server konfigurieren">
# <i>distcc-config --set-hosts "192.168.1.102 192.168.1.103 192.168.1.104"</i>
</pre>

<p>
Vergessen Sie bitte nicht, auch den <c>distccd</c> - Dämonen zu starten:
</p>

<pre caption="distcc Dämonen starten">
# <i>rc-update add distccd default</i>
# <i>/etc/init.d/distccd start</i>
</pre>

</body>
</subsection>
</section>
<section>
<title>Caching Compilation</title>
<subsection>
<title>Über ccache</title>
<body>

<p>
<c>ccache</c> ist ein schneller Compiler - Cache. Wenn Sie ein Programm
kompilieren, werden Zwischenresultate gecacht, so dass bei einer Rekompilierung
des Programms die Zeit zum Kompilieren viel kürzer ist. Das kann bei normalen
Kompilierungen eine 5 bis 10 Mal schnellere Kompilierung ausmachen.
</p>

<p>
Falls Sie an den Vor- und Nachteilen von <c>ccache</c> interessiert sind,
besuchen Sie bitte die <uri link="http://ccache.samba.org">ccache
Homepage</uri>.
</p>

</body>
</subsection>
<subsection>
<title>ccache installieren</title>
<body>

<p>
Zur Installation von <c>ccache</c> führen Sie <c>emerge ccache</c> aus:
</p>

<pre caption="ccache installieren">
# <i>emerge ccache</i>
</pre>

</body>
</subsection>
<subsection>
<title>Unterstützung für Portage aktivieren</title>
<body>

<p>
Öffnen Sie <path>/etc/make.conf</path> und fügen Sie <c>ccache</c> zu
<c>FEATURES</c> hinzu. Anschließen erstellen Sie eine neue Variable namens
CCACHE_SIZE und setzen diese auf "2G":
</p>

<pre caption="CCACHE_SIZE in /etc/make.conf editieren">
CCACHE_SIZE="2G"
</pre>

<p>
Um zu schauen ob ccache funktioniert, fragen Sie ccache nach seinen
Statistiken. Weil Portage ein anderes Home-Verzeichnis verwendet, müssen Sie
auch die <c>CCACHE_DIR</c> Variable setzen:
</p>

<pre caption="Ccache - Statistiken anschauen">
# <i>CCACHE_DIR="/var/tmp/ccache" ccache -s</i>
</pre>

<p>
Der <path>/var/tmp/ccache</path> Pfad ist das Standard ccache Home-Verzeichnis
von Portage. Wenn Sie diese Einstellungen ändern möchten, können Sie die
<c>CCACHE_DIR</c> Variable in <path>/etc/make.conf</path> setzen.
</p>

<p>
Wenn Sie aber <c>ccache</c> ausführen würden, würde es den Standardpfad
<path>${HOME}/.ccache</path> verwenden. Deswegen mussten Sie auch die
<c>CCACHE_DIR</c> Variable setzen, als Sie nach den (Portage)
ccache-Statistiken fragten.
</p>

</body>
</subsection>
<subsection>
<title>Nutzung von ccache außerhalb von Portage</title>
<body>

<p>
Wenn Sie ccache für Kompilierungen außerhalb von Portage nutzen möchten fügen
Sie <path>/usr/lib/ccache/bin</path> dem Beginn der PATH Variable hinzu (vor
<path>/usr/bin</path>). Dies kann durch Editieren von
<path>.bash_profile</path> in Ihrem Home-Verzeichnis erreicht werden. Die
Verwendung von <path>.bash_profile</path> ist eine Möglichkeit Ihre
PATH-Variable zu definieren.
</p>

<pre caption="Editieren von .bash_profile">
PATH="<i>/usr/lib/ccache/bin</i>:/opt/bin:${PATH}"
</pre>

</body>
</subsection>
</section>
<section>
<title>Binäre Pakete</title>
<subsection>
<title>Binärpakete erstellen</title>
<body>

<p>
Portage unterstützt die Installation von vorkompilierten Paketen. Obwohl Gentoo
keine vorkompilierten Pakete anbietet (mit Ausnahme der GRP Schnappschüsse) ist
die Funktion vollständig implementiert.
</p>

<p>
Um ein vorkompiliertes Paket zu erstellen können Sie <c>quickpg</c> benutzen,
sofern das Programm bereits in Ihrem System installiert ist oder Sie benutzen
die <c>--buildpkg</c> oder <c>--buildpkgonly</c> Optionen.
</p>

<p>
Falls Sie wollen, dass Portage dies standardmäßig macht, sollten Sie das
Schlüsselword <c>buildpkg</c> in der FEATURES Variablen setzen.
</p>

<p>
Erweiterte Unterstützung zum Erstellen von vorkompilierten Paketen finden Sie in
<c>catalyst</c>. Für weitere Informationen zu <c>catalyst</c> lesen Sie bitte
die <uri link="http://www.gentoo.org/proj/en/releng/catalyst/faq.xml">Catalyst
FAQ</uri>.
</p>

</body>
</subsection>
<subsection>
<title>Vorkompilierte Pakete Installieren</title>
<body>

<p>
Auch wenn Gentoo keine anbietet, Sie können ein zentrales Repository anlegen, in
dem Sie vorkompilierte Pakete ablegen. Wenn Sie dieses Repository nutzen möchten
müssen Sie Portage mit der PORTAGE_BINHOST Variable den Ort des Repository
bekannt machen. Wenn sich die Pakete zum Beispiel auf ftp://buildhost/gentoo
befinden:
</p>

<pre caption="Setzen der PORTAGE_BINHOST Variable in /etc/make.conf">
PORTAGE_BINHOST="ftp://buildhost/gentoo"
</pre>

<p>
Wenn Sie vorkompilierte Pakete installieren wollen fügen Sie <c>--getbinpkg</c>
Option an das entsprechende emerge Kommando (welches ebenfalls <c>--usepkg</c>
enthalten muss) an. Die erste Option weist Portage an ein Binärpaket
herunterzuladen, die zweite zuerst zu versuchen ein Binärpaket zu installieren,
bevor Sourcen heruntergeladen und kompiliert werden.
</p>

<p>
Um zum Beispiel <c>gnumeric</c> mit vorkompilierten Paketen zu installieren:
</p>

<pre caption="Installation des vorkompilierten gnumeric Pakets">
# <i>emerge --usepkg --getbinpkg gnumeric</i>
</pre>

<p>
Weitere Informationen zur Binärpaket Funktionalität von <c>emerge</c> finden Sie
in der emerge Manpage:
</p>

<pre caption="Lesen der emerge Manpage">
$ <i>man emerge</i>
</pre>

</body>
</subsection>
</section>
<section>
<title>Dateien abrufen</title>
<subsection>
<title>Paralleler Abruf</title>
<body>

<p>
Wenn Sie eine Serie von Paketen mit emerge isntallieren kann Portage die
Quelldateien für das nächste Paket in der Liste schon herunterladen, während es
noch ein anderes Paket kompiliert, und dadurch die Kompilierzeit reduzieren. Um
von dieser Fähigkeit Gebrauch zu machen, fügen Sie "parallel-fetch" zu Ihren
FEATURES hinzu.
</p>

</body>
</subsection>
<subsection>
<title>Userfetch</title>
<body>

<p>
Wenn Portage als root ausgeführt wird erlaubt FEATURES="userfetch" Portage das
Senken der root-Privilegien, während es Paketquellen abruft. Dies verbessert die
Sicherheit ein wenig.
</p>

</body>
</subsection>
</section>
</sections>
