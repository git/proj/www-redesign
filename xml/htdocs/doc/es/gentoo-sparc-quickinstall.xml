<?xml version='1.0' encoding="UTF-8"?>
<!-- $Header: /var/cvsroot/gentoo/xml/htdocs/doc/es/gentoo-sparc-quickinstall.xml,v 1.5 2007/07/25 16:04:48 chiguire Exp $ -->

<!DOCTYPE guide SYSTEM "/dtd/guide.dtd">

<guide link="/doc/es/gentoo-sparc-quickinstall.xml" lang="es">
<title>Guía de instalación rápida de Gentoo Linux para Sparc</title>

<author title="Autor">
  <mail link="ciaranm@gentoo.org">Ciaran McCreesh</mail>
</author>
<author title="Traductor">
  <mail link="anpereir@gentoo.org">Andrés Pereira</mail>
</author>
<author title="Traductor">
  <mail link="chiguire@gentoo.org">John Christian Stoddart</mail>
</author>

<abstract>
Esta guía de instalación rápida cubre todos los detalles del proceso de
instalación en sparc de manera concisa. Los usuarios ya deben tener
experiencia previa con la instalación de Gentoo Linux en caso de que quieran
seguir esta guía.
</abstract>

<!-- The content of this document is licensed under the CC-BY-SA license -->
<!-- See http://creativecommons.org/licenses/by-sa/2.5 -->
<license/>

<version>1.15</version>
<date>2007-05-07</date>

<chapter>
<title>Guía de instalación rápida para Sparc</title>
<section>
<body>

<p>
La presente guía está dirigida a personas que tienen experiencia previa en
instalar Gentoo Linux. Para instrucciones más detalladas, por favor consulte el
<uri link="http://www.gentoo.org/doc/es/handbook">Manual de Gentoo</uri>.
</p>

<p>
Los imágenes ISO de instalación y netboot (arranque por red) están en los
<uri link= "http://www.gentoo.org/main/en/mirrors.xml">servidores réplica de
Gentoo</uri>. Los CDs 'universal' y 'minimal' son capaces de realizar el
proceso de arranque y contienen las herramientas necesarias para instalar
Gentoo. El CD 'universal' también contiene fases (stages) y algunos distfiles.
El CD 'packages' contiene paquetes precompilados adicionales para instalaciones
GRP.
</p>

<p>
Use <c>stop+A</c> (teclado) o envíe un break (consola serial) mientras esté
iniciando para entrar a OBP. Arranque a partir del CD 'minimal' o 'universal'
de Gentoo usando el comando <c>boot cdrom</c>, o desde las imágenes de netboot
usando <c>boot net</c>. Presione intro para usar el núcleo por defecto, o
presione la tecla tab para ver una lista de núcleos alternativos.
</p>

<note>
Para más información sobre OBP (en inglés), vea la <uri
link="/doc/en/gentoo-sparc-obpreference.xml">Referencia de PROM
OpenBoot (OBP) </uri> o consulte el documento "OpenBoot 3.x Command
Reference" de Sun (parte número 802-3242).
</note>

<pre caption="Configuraciones iniciales">
# <i>date</i>  (Asegúrese que su hora y fecha son correctas. Si se equivoca, ajústela con <i>date MMDDhhmmCCYY</i>)
# <i>modprobe nombre_del_módulo</i>  (Opcional - Cargue los módulos necesarios)
# <i>ifconfig eth0 a.b.c.d netmask e.f.g.h broadcast i.j.k.l</i> (Configure la red)
# <i>route add -net default gw a.b.c.d netmask 0.0.0.0 metric 1 eth0</i> (Configure la pasarela (gateway) por defecto)
# <i>echo "nameserver a.b.c.d" > /etc/resolv.conf</i> (Ajuste el DNS)
# <i>fdisk /dev/sda</i> (Particione su disco duro)
</pre>

<p>
Se requiere una etiqueta de disco (disclabel) sun para máquinas sparc. Esta
se puede crear usando el comando 's' de fdisk.
</p>

<p>
No se recomienda una partición /boot separada para sparc, la partición raíz
debe estar por completo dentro del primer gigabyte de disco para todas las
máquinas sparc32. El sistema de ficheros recomendado es ext3. Se requiere de
al menos 512 MB de memoria RAM + memoria de intercambio (swap) para el
bootstrap y algunas compilaciones más grandes.
</p>

<p>
Inicialice sus particiones usando <c>mke2fs</c> (Ext2), <c>mke2fs -j</c> (Ext3)
y <c>mkswap</c> (partición de intercambio). Por ejemplo:
<c>mke2fs -j /dev/sda1</c>.
</p>

<p>
Continúe montando las particiones y extrayendo el fichero de la fase apropiada.
</p>

<pre caption="Preparar la instalación">
(Active la partición de intercambio)             # <i>swapon /dev/sdax</i>
(Monte la partición raíz)                        # <i>mount /dev/sdax /mnt/gentoo</i>
(Vaya al punto de montaje)                       # <i>cd /mnt/gentoo</i>
(Extraiga el tarball con la fase...)             # <i>tar xvjpf /mnt/cdrom/stages/stage?-*.tar.bz2</i>
(<comment>o</comment> descargue el último                           # <i>links http://www.gentoo.org/main/en/mirrors.xml</i>
tarball y extráigalo)                            # <i>tar xvjpf stage*</i>
(Opcional: desempaque el árbol portage)          # <i>tar xvjf /mnt/cdrom/snapshots/portage-*.tar.bz2 -C /mnt/gentoo/usr</i>
(Opcional: copie los distfiles)                  # <i>cp -R /mnt/cdrom/distfiles /mnt/gentoo/usr/portage/distfiles</i>
(Seleccione un servidor réplica)                 # <i>mirrorselect -a -s4 -o &gt;&gt; /mnt/gentoo/etc/make.conf</i>
(Copie la información de los servidores DNS)     # <i>cp /etc/resolv.conf /mnt/gentoo/etc/resolv.conf</i>
(Monte el sistema de ficheros proc)              # <i>mount -t proc none /mnt/gentoo/proc</i>
(Entre a la jaula en el nuevo entorno)           # <i>chroot /mnt/gentoo /bin/bash</i>
(Cargue las variables necesarias)                # <i>env-update; source /etc/profile</i>
(Sólo instalaciones con red y no-GRP:            # <i>emerge --sync</i>
actualice Portage)
</pre>

<p>
Ahora instale Gentoo:
</p>

<pre caption="Instalar Gentoo">
(Sólo para Fase1 (Stage1):)
(Cambie las variables USE, CFLAGS y CXXFLAGS. No cambie CHOST)  # <i>nano -w /etc/make.conf</i>
(Realice el bootstrap del sistema)                              # <i>cd /usr/portage; scripts/bootstrap.sh</i>
(Sólo para Fase1 y Fase2:)
(Instale el sistema base)                                       # <i>emerge system</i>
</pre>

<note>
Las CFLAGS apropiadas para sistemas Sparc son
<c>-mcpu=su_cpu -O2 -pipe</c>, donde <c>su_cpu</c> es uno de los siguientes:
<c>ultrasparc3</c>, <c>ultrasparc</c>, <c>v9</c> (sistemas de 64 bits) o
<c>hypersparc</c>, <c>supersparc</c>, <c>v8</c> o <c>v7</c> (sistemas de 32
bits). La bandera <c>-frename-registers</c> también puede ser de interés. Note
que, a diferencia de los sistemas x86, <c>-fomit-frame-pointer</c> no es
recomendado.
</note>

<p>
Luego configure la información necesaria:
</p>

<pre caption="Ajustando la zona horaria">
<comment>(Listar las zonas horarias disponibles)</comment>

# <i>ls /usr/share/zoneinfo</i>

<comment>(Usando a Brussels como ejemplo)</comment>
# <i>nano -w /etc/conf.d/clock</i>
TIMEZONE="Europe/Brussels"
</pre>

<p>
A continuación, modifique <path>/etc/fstab</path>:
</p>

<pre caption="Configurando fstab">
# <i>nano -w /etc/fstab</i>
</pre>

<p>
Use lo siguiente como <e>plantilla</e> (no la copie textualmente) para el
fichero <path>/etc/fstab</path>:
</p>

<pre caption="/etc/fstab">
# &lt;fs&gt;                  &lt;mountpoint&gt;    &lt;type&gt;          &lt;opts&gt;                  &lt;dump/pass&gt;
/dev/sdax               none            swap            sw                      0 0
/dev/sdax               /               ext3            noatime                 0 1
/dev/cdroms/cdrom0      /mnt/cdrom      auto            noauto,user             0 0
none                    /proc           proc            defaults                0 0
none                    /dev/shm        tmpfs           nodev,nosuid,noexec     0 0
</pre>

<p>
Continúe instalando el núcleo Linux. Los paquetes disponibles para
Sparc son <c>sparc-sources</c> (2.4) y <c>gentoo-sources</c> (2.6). El
parámetro USE <c>ultra1</c> debería configurarse en los modelos Ultra
1 y Netra i 1 "Enterprise" y "Creator" para el soporte adecuado de la
controladora Ethernet HME integrada.
</p>

<pre caption="Instalar el núcleo">
(Instale las fuentes del núcleo)                           # <i>emerge <comment>&lt;paquete-del-núcleo-aquí&gt;</comment></i>
                                                           # <i>cd /usr/src/linux; make menuconfig</i>
(   (2) Incluya shmfs, procfs, devfs si no está usando udev)
(   (3) Compile su núcleo)
(       - Núcleos 2.4 en sparc64)                          # <i>make dep &amp;&amp; make clean vmlinux image modules modules_install</i>
(       - Núcleos 2.6 en sparc64)                          # <i>make &amp;&amp; make image modules_install</i>
(   (4) Copie el núcleo)
(       - Núcleos 2.4 en sparc64)                          # <i>cp arch/sparc64/boot/image /boot</i>
(       - Núcleos 2.6 en sparc64)                          # <i>cp arch/sparc64/boot/image /boot</i>
</pre>

<p>
Ahora instale otras herramientas necesarias:
</p>

<pre caption="Instale las herramientas del sistema importantes">
(Instale el gestor de bitácoras del sistema; escoja uno de
 los siguientes: sysklogd, metalog, msyslog, syslog-ng)                # <i>emerge syslog-ng </i>
(Haga que el gestor de bitácoras se inicie automáticamente
 en el arranque)                                                       # <i>rc-update add syslog-ng default</i>
(Instale el demonio cron ; escoja entre: vixie-cron, dcron, fcron)     # <i>emerge vixie-cron</i>
(Haga que el demonio cron se inicie automáticamente
 en el arranque)                                                       # <i>rc-update add vixie-cron default</i>
(Script de inicio de nombre de dominio)                                # <i>rc-update add domainname default</i>
(Sólo para núcleos 2.6.x: udev puede ser usado en lugar de devfs)      # <i>emerge udev</i>
</pre>

<note>
Para mayor información sobre la configuración de udev, vea la
<uri link="/doc/es/udev-guide.xml">Guía udev en Gentoo</uri>. Note que la
opción <c>RC_DEVICE_TARBALL="no"</c> es de preferencia en sistemas Sparc.
</note>

<p>
Finalice los ajustes para su sistema Gentoo:
</p>

<pre caption="Finalice los ajustes de configuración">
(Establezca la contraseña del superusuario (root))       # <i>passwd</i>
(Cree un usuario)                                        # <i>useradd su_usuario -m -G users,wheel,audio -s /bin/bash</i>
(Ajuste la contraseña para dicho usuario)                # <i>passwd su_usuario</i>
(Ajuste el nombre del host)                              # <i>echo mi_máquina &gt; /etc/hostname</i>
(Ajuste el nombre de dominio del sistema)                # <i>echo mi_dominio.tld &gt; /etc/dnsdomainname</i>
(Ajuste el fichero hosts, por ejemplo:
 "127.0.0.1 localhost mi_máquina")                       # <i>nano -w /etc/hosts</i>
(Ajuste algunas configuraciones básicas; siga los
 comentarios)                                            # <i>nano -w /etc/rc.conf</i>
</pre>

<pre caption="Configurar la red">
(Configure la red; los usuarios de dhcp deberían configurar
 config_eth0="dhcp")                                              # <i>nano -w /etc/conf.d/net</i>
(Liste los módulos que serán cargados al inicio)                 # <i>nano -w /etc/modules.autoload.d/kernel-<comment>&lt;version&gt;</comment></i>
(Inicie la red automáticamente en el arranque)                   # <i>rc-update add net.eth0 default</i>
(Sólo si tiene múltiples interfaces de red:)
   ((1) Cree los scripts de inicio para cada interfaz)           # <i>ln -s /etc/init.d/net.eth0 /etc/init.d/net.ethx</i>
   ((2) Inicie los scripts automáticamente al arrancar)          # <i>rc-update add net.ethx default</i>
</pre>

<p>
Ahora instale el gestor de arranque.
</p>

<pre caption="Instalar SILO">
# emerge silo
# nano -w /etc/silo.conf
partition = 1
root = /dev/sda1
timeout = 150

image = /boot/image
    label = Gentoo
# silo
</pre>

<p>
Luego desmonte todas las particiones y reinicie su nuevo sistema:
</p>

<pre caption="Finalizar e instalar la GUI">
(Saliendo de la jaula)                          # <i>exit; cd /</i>
(Desmontando las particiones)                   # <i>umount /mnt/gentoo/proc /mnt/gentoo</i>
(Reinicie;Saque el CD de instalación del lector)# <i>reboot</i>
(Luego de arrancar:)
(Sólo para usuarios de GRP)
(  (1) Monte el CD #2)                          # <i>mount /dev/cdroms/cdrom0 /mnt/cdrom</i>
(  (2) Copie los paquetes precompilados)        # <i>cp -a /mnt/cdrom/packages/* /usr/portage/packages/</i>
(  (3) Instale el software extra)               # <i>emerge -k xorg-x11 gnome kde</i>
(  (4) Configure su servidor X)                 # <i>$EDITOR /etc/X11/xorg.conf</i>
</pre>

<p>
Puede obtener más información en la
<uri link="http://www.gentoo.org/doc/es/index.xml">Documentación de
Gentoo</uri>.
</p>
</body>
</section>
</chapter>
</guide>
