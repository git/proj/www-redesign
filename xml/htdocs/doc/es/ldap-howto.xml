<?xml version = '1.0' encoding = 'UTF-8' ?>
<!-- $Header: /var/cvsroot/gentoo/xml/htdocs/doc/es/ldap-howto.xml,v 1.17 2010/07/20 06:57:59 nimiux Exp $ -->

<!DOCTYPE guide SYSTEM "/dtd/guide.dtd">

<guide disclaimer="draft" lang="es">
<title>Guía Gentoo para la autenticación con OpenLDAP</title>

<author title="Autor">
  <mail link="sj7trunks@pendulus.net">Benjamin Coles</mail>
</author>
<author title="Editor">
  <mail link="swift@gentoo.org">Sven Vermeulen</mail>
</author>
<author title="Editor">
  <mail link="tseng@gentoo.org">Brandon Hale</mail>
</author>
<author title="Editor">
  <mail link="bennyc@gentoo.org">Benny Chuang</mail>
</author>
<author title="Editor">
 <mail link="jokey"/>
</author>
<author title="Editor">
  <mail link="nightmorph"/>
</author>
<author title="Traductor">
  <mail link="chiguire@gentoo.org">John Christian Stoddart</mail>
</author>
<author title="Traductor">
  <mail link="yoswink@gentoo.org">José Luis Rivero</mail>
</author>
<author title="Traductor" >
  <mail link="carles@carles.no-ip.info">Carles Ferrer Peris</mail>
</author>

<abstract>
Esta guía explica los aspectos básicos de LDAP y muestra cómo instalar
OpenLDAP con la finalidad de conseguir la autenticación entre un grupo de
máquinas Gentoo.
</abstract>

<!-- The content of this document is licensed under the CC-BY-SA license -->
<!-- See http://creativecommons.org/licenses/by-sa/2.5 -->
<license/>

<version>4</version>
<date>2010-07-13</date>

<chapter>
<title>Empezando con OpenLDAP</title>
<section>
<title>¿Qué es LDAP?</title>
<body>

<p>
LDAP significa <e>Lightweight Directory Access Protocol</e> (Protocolo
Ligero de Acceso a Directorios). Basado en X.500, abarca muchas de sus
funciones principales, pero carece de las funciones más esotéricas de
X.500. Pero, ¿qué es este X.500 y por qué hay un LDAP?
</p>

<p>
X.500 es un modelo de Servicio de Directorio basado en el concepto OSI
(interconexión de sistemas abiertos). Contiene definiciones de
espacios de nombres y los protocolos para preguntar y actualizar el
directorio. Sin embargo, X.500 ha sido creado para ser excesivamente
estricto en algunas situaciones. Entrando en LDAP, al igual que X.500,
proporciona un modelo de datos/espacio de nombres para el directorio y
el protocolo. No obstante, LDAP está diseñado para ejecutarse
directamente sobre la pila TCP/IP. Vea a LDAP como una versión ligera
de X.500.
</p>
</body>
</section>

<section>
<title>No lo entiendo. ¿Qué es un directorio?</title>
<body>

<p>
Un directorio es una base de datos especializada diseñada para
frecuentes consultas pero para infrecuentes actualizaciones. A
diferencia de las bases de datos generalistas, no contiene soporte
para transacciones o funcionalidad de vuelta atrás
(&quot;roll-back&quot;). Los directorios son fácilmente replicados
para incrementar disponibilidad y fiabilidad. Cuando los directorios
son replicados, se permiten inconsistencias temporales con tal de que
acaben siendo finalmente sincronizadas.
</p>
</body>
</section>

<section>
<title>¿Cómo está estructurada la información?</title>
<body>

<p>
Toda la información dentro de un directorio está estructurada
jerárquicamente. Aún más, si intenta introducir datos en el
directorio, el directorio debe conocer cómo almacenar estos datos
en un árbol. Eche un vistazo a la compañía de ficción y a su
organigrama:
</p>

<pre caption="Estructura organizativa de GenFic, una empresa Gentoo Ficticia" >
dc:         com
             |
dc:        genfic <comment>(Organización)</comment>
          /      \
ou:  Personas servidores<comment>(Unidades organizativas)</comment>
      /    \     ..
uid: ..   John    <comment>(Datos específicos de las UO)</comment>
</pre>

<p>
Puesto que no puede alimentar la base de datos con este tipo de
&quot;ascii-art&quot;, cada nodo de tal árbol debe ser definido. Para
nombrar tales nodos, LDAP usa un sistema de definición de nombres. La
mayor parte de distribuciones de LDAP (incluyendo OpenLDAP) ya
contienen un buen número de esquemas predefinidos (y comúnmente
aprobados), como el &quot;inetorgperson&quot;, frecuentemente
utilizado para definir usuarios.
</p>

<p>
Animamos a las personas interesadas a leer la <uri
link="http://www.openldap.org/doc/admin24/">Guía de Administración
OpenLDAP</uri>.
</p>
</body>
</section>

<section>
<title>Pero ... ¿para qué se utiliza?</title>
<body>

<p>
LDAP puede ser utilizado con varios propósitos. En este documento se
trata la administración centralizada de usuarios, manteniendo todas
las cuentas de usuario en una única ubicación LDAP (lo que no
significa que esté albergada en un único servidor, puesto que LDAP
soporta alta disponibilidad y redundancia), y sin embargo LDAP puede
utilizarse igualmente para otros fines:
</p>

<ul>
  <li>Infraestructura de clave pública</li>
  <li>Calendario compartido</li>
  <li>Libreta de direcciones compartida</li>
  <li>Almacenamiento para DHCP, DNS, ...</li>
  <li>
     Directivas de configuración para las clases del sistema (guardando
     registro de las configuraciones de varios servidores)
  </li>
  <li>...</li>
</ul>
</body>
</section>
</chapter>

<chapter>
<title>Configurando OpenLDAP</title>
<section>
<title>Configuración inicial</title>
<body>

<note>
En este documento utilizamos la dirección genfic.com como
ejemplo. Deberá, desde luego, cambiarlo. Sin embargo, asegúrese
que el nodo superior es un dominio oficial de primer nivel (net,
com, cc, be, ...).
</note>

<p>
Primero instale OpenLDAP:
</p>

<pre caption="Instalación de OpenLDAP" >
# <i>emerge openldap</i>
</pre>

<p>
Ahora cree una contraseña que usará después:
</p>

<pre caption="Generar una contraseña">
# <i>slappasswd</i>
New password: mi-contraseña
Re-enter new password: mi-contraseña
{SSHA}EzP6I82DZRnW+ou6lyiXHGxSpSOw2XO4
</pre>

<p>
Ahora edite la configuración del Servidor LDAP en
<path>/etc/openldap/slapd.conf</path>:
</p>

<pre caption="/etc/openldap/slapd.conf" >
<comment># Incluya los esquemas de datos necesarios debajo de core.schema</comment>
include         /etc/openldap/schema/cosine.schema
include         /etc/openldap/schema/inetorgperson.schema
include         /etc/openldap/schema/nis.schema

<comment># Descomente el modulepath y el módulo hdb</comment>
modulepath    /usr/lib/openldap/openldap
# moduleload    back_shell.so
# moduleload    back_relay.so
# moduleload    back_perl.so
# moduleload    back_passwd.so
# moduleload    back_null.so
# moduleload    back_monitor.so
# moduleload    back_meta.so
moduleload    back_hdb.so
# moduleload    back_dnssrv.so

<comment># Descomente las restricciones de acceso de ejemplo (Nota:
¡mantenga la indentación!)
</comment>
access to dn.base="" by * read
access to dn.base="cn=Subschema" by * read
access to *
   by self write
   by users read
   by anonymous auth

<comment># Definición de la base de datos BDB</comment>

database        hdb
suffix          "dc=genfic,dc=com"
checkpoint      32      30 # &lt;kbyte&gt; &lt;min&gt;
rootdn          "cn=Manager,dc=genfic,dc=com"
rootpw          <i>{SSHA}EzP6I82DZRnW+ou6lyiXHGxSpSOw2XO4</i>
directory       /var/lib/openldap-ldbm
index           objectClass     eq
</pre>

<p>
Luego edite el fichero de configuración del cliente LDAP:
</p>

<pre caption="/etc/openldap/ldap.conf" >
# <i>nano -w /etc/openldap/ldap.conf</i>
  <comment>(Añada lo siguiente ...)</comment>

BASE         dc=genfic, dc=com
URI          ldap://auth.genfic.com:389/
TLS_REQCERT  allow
</pre>

<p>
Ahora edite <path>/etc/conf.d/slapd</path> y elimine el
comentario de la siguiente línea OPTS:
</p>

<pre caption="/etc/conf.d/slapd" >
<comment># Nota: no usamos cn=config aquí, por tanto quédese con
esta línea:</comment>
OPTS="-F /etc/openldap/slapd.d -h 'ldaps:// ldap:// ldapi://%2fvar%2frun%2fopenldap%2fslapd.sock'"
</pre>

<p>
Inicie slapd:
</p>

<pre caption="Iniciar SLAPd" >
# <i>/etc/init.d/slapd start</i>
</pre>

<p>
Puede probarlo con la siguiente instrucción:
</p>

<pre caption="Prueba del servicio SLAPd" >
# <i>ldapsearch -x -D "cn=Manager,dc=genfic,dc=com" -W</i>
</pre>

<p>
Si recibe un error, pruebe a añadir <c>-d 255</c> para incrementar el
nivel de detalle de los avisos y poder resolver el problema que pueda
tener.
</p>
</body>
</section>
</chapter>

<chapter>
<title>Configuración del cliente</title>
<section>
<title>Migrar los datos existentes a ldap</title>
<body>

<p>
Vaya a <uri
link="http://www.padl.com/OSS/MigrationTools.html">
http://www.padl.com/OSS/MigrationTools.html</uri>
y busque los guiones allí. La configuración está establecida en la
página.  Nosotros ya no los proporcionamos porque los guiones son un
potencial agujero de seguridad si los deja en el sistema después de
haberlos trasladado. Cuando haya acabado de migrar los datos, continue
en la sección siguiente.
</p>
</body>
</section>

<section>
<title>Configuración de PAM</title>
<body>

<p>
En primer lugar, configuraremos PAM para permitir la autorización con
LDAP. Instalemos <c>sys-auth/pam_ldap</c> ya que PAM soporta la
autentificación LDAP, y <c>sys-auth/nss_ldap</c> porque nuestro
sistema puede negociar con servidores LDAP para obtener información
adicional (usado por <path>nsswitch.conf</path>).
</p>

<pre caption="Instalar pam_ldap y nss_ldap" >
# <i>emerge pam_ldap nss_ldap</i>
</pre>

<p>
Ahora añada las siguientes líneas en los lugares adecuados de
<path>/etc/pam.d/system-auth</path>:
</p>

<pre caption="/etc/pam.d/system-auth" >
<comment># Nota: Sólo añádalas. ¡No suprima cosas o su sistema podría no
volver a arrancar de nuevo!
</comment>
auth    sufficient  pam_ldap.so use_first_pass
account    sufficient   pam_ldap.so
password   sufficient   pam_ldap.so use_authtok use_first_pass
session    optional     pam_ldap.so

<comment># Fichero de ejemplo:</comment>
#%PAM-1.0

auth       required     pam_env.so
auth       sufficient   pam_unix.so try_first_pass likeauth nullok
<i>auth       sufficient   pam_ldap.so use_first_pass</i>
auth    required    pam_deny.so

<i>account    sufficient   pam_ldap.so</i>
account    required     pam_unix.so

password   required     pam_cracklib.so difok=2 minlen=8 dcredit=2 ocredit=2 try_first_pass retry=3
password   sufficient   pam_unix.so try_first_pass use_authtok nullok md5 shadow
<i>password   sufficient   pam_ldap.so use_authtok use_first_pass</i>
password    required pam_deny.so

session required    pam_limits.so
session required    pam_unix.so
<i>session    optional     pam_ldap.so</i>
</pre>

<p>
Ahora cambie <path>/etc/ldap.conf</path> para que tenga:
</p>

<pre caption="/etc/ldap.conf" >
<comment>#host 127.0.0.1</comment>
<comment>#base dc=padl,dc=com</comment>

suffix          &quot;dc=genfic,dc=com&quot;
<comment>#rootbinddn uid=root,ou=People,dc=genfic,dc=com</comment>

uri ldap://auth.genfic.com/
pam_password exop

ldap_version 3
pam_filter objectclass=posixAccount
pam_login_attribute uid
pam_member_attribute memberuid
nss_base_passwd ou=People,dc=genfic,dc=com
nss_base_shadow ou=People,dc=genfic,dc=com
nss_base_group  ou=Group,dc=genfic,dc=com
nss_base_hosts  ou=Hosts,dc=genfic,dc=com

scope one
</pre>

<p>
Luego, copie el fichero (OpenLDAP) <path>ldap.conf</path> del servidor en el
cliente para que éste tenga en cuenta el entorno LDAP:
</p>

<pre caption="Copiar el OpenLDAP ldap.conf">
<comment>(Sustituya ldap-server con el nombre de su servidor LDAP)</comment>
# <i>scp ldap-server:/etc/openldap/ldap.conf /etc/openldap</i>
</pre>

<p>
Finalmente, configure sus clientes para que verifiquen en LDAP las
cuentas de sistema:
</p>

<pre caption="/etc/nsswitch.conf" >
passwd:         files ldap
group:          files ldap
shadow:         files ldap
</pre>

<p>
Para probar los cambios, escriba:
</p>

<pre caption="Prueba de la autorización con LDAP" >
# <i>getent passwd|grep 0:0</i>
<comment>(Debería devolver dos entradas:)</comment>
root:x:0:0:root:/root:/bin/bash
root:x:0:0:root:/root:/bin/bash
</pre>

<p>
Si observa, una de las lineas copiadas en su
<path>/etc/ldap.conf</path> está comentada (la línea
<c>rootbinddn</c>): no la necesita salvo que quiera cambiar la
contraseña de un usuario como superusuario. En este caso, necesita
escribir la contraseña de root en <path>/etc/ldap.secret</path> con
texto en claro. Esto es <brite>PELIGROSO</brite> por lo que debería
tener permisos &quot;600&quot;. Lo que yo hago es mantener el fichero
vacío y cuando necesito cambiar cualquier contraseña que está tanto
en ldap como en <path>/etc/passwd</path> escribo la contraseña en él
durante 10 segundos mientras hago los cambios y la borro cuando he
acabado.
</p>
</body>
</section>
</chapter>

<chapter>
<title>Seguridad del servidor LDAP</title>
<section>
<title>Permisos de OpenLDAP</title>
<body>

<p>
Si echamos un vistazo a <path>/etc/openldap/slapd.conf</path> podrá
ver que se pueden especificar ACLs (listas de control de acceso, o
permisos, si lo prefiere) de qué datos pueden los usuarios leer y/o
escribir:
</p>

<pre caption="/etc/openldap/slapd.conf" >
access to *
  by dn=&quot;uid=root,ou=People,dc=genfic,dc=com&quot; write
  by users read
  by anonymous auth

access to attrs=userPassword,gecos,description,loginShell
  by self write
</pre>

<p>
Esto le da acceso a todo a lo que a un usuario le está permitido
cambiar. Si es su información, entonces tiene acceso de escritura; si
es la de otro usuario, entonces puede leerla; un usuario anónimo puede
enviar un usuario/contraseña para ser conectado. Hay cuatro niveles,
del menor al mayor: <c>autorización búsqueda lectura escritura</c>.
</p>

<p>
La siguiente ACL es un poco más segura puesto que impide a los
usuarios normales leer la contraseña enmascarada de otros usuarios:
</p>

<pre caption="/etc/openldap/slapd.conf" >
access to attrs=&quot;userPassword&quot;
  by dn=&quot;uid=root,ou=People,dc=genfic,dc=com&quot; write
  by dn=&quot;uid=John,ou=People,dc=genfic,dc=com&quot; write
  by anonymous auth
  by self write
  by * none

access to *
  by dn=&quot;uid=John,ou=People,dc=genfic,dc=com&quot; write
  by * search
</pre>

<p>
Este ejemplo le da acceso a root y John para leer/escribir/buscar en
todo el árbol por debajo de <path>dc=genfic,dc=com</path>. También
permite a los usuarios cambiar su propia <path>userPassword</path>. Y
en la sentencia final cualquier otro tiene la capacidad de búsqueda,
lo que significa que puede introducir un filtro de búsqueda pero no
puede leer los resultados de la búsqueda. Puede tener múltiples ACLs
pero la regla de procesamiento es de abajo arriba, por lo que su nivel
superior debe ser el más restrictivo.
</p>
</body>
</section>
</chapter>

<chapter>
<title>Trabajando con OpenLDAP</title>
<section>
<title>Manteniendo el directorio</title>
<body>

<p>
Puede empezar utilizando el directorio para autentificar usuarios en
apache/proftpd/qmail/samba. Puede administrarlo con Webmin, que
proporciona una interfaz de administración sencilla. Puede también
usar phpldapadmin, diradm, jxplorer o lat.
</p>
</body>
</section>
</chapter>

<chapter>
<title>Agradecimientos</title>
<section>
<body>

<p>
Quisiera agradecer a Matt Heler el préstamo de su equipo para poder
realizar esta guía. Gracias también a toda la gente simpática de #ldap
@ irc.freenode.net.
</p>
</body>
</section>
</chapter>
</guide>
