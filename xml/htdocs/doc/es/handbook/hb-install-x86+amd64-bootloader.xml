<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE sections SYSTEM "/dtd/book.dtd">

<!-- The content of this document is licensed under the CC-BY-SA license -->
<!-- See http://creativecommons.org/licenses/by-sa/2.5 -->

<!-- $Header: /var/cvsroot/gentoo/xml/htdocs/doc/es/handbook/hb-install-x86+amd64-bootloader.xml,v 1.16 2010/07/21 00:07:07 nimiux Exp $ -->

<sections>

<version>6.7</version>
<date>2010-07-16</date>

<section>
<title>Realizando su elección</title>
<subsection>
<title>Introducción</title>
<body>

<p>
Ahora que su núcleo está configurado y compilado y los archivos de
configuración necesarios han sido llenados correctamente, ha llegado
la hora de instalar el programa que iniciará el núcleo cuando arranque
el sistema. Este programa es conocido como <e>gestor de arranque</e>.
</p>
</body>

<body test="contains('AMD64 x86', func:keyval('arch'))">
<p>
Para <keyval id="arch"/>, Gentoo dispone de <uri
link="#grub">GRUB</uri> y <uri link="#lilo">LILO</uri>.
</p>
</body>

<body>
<p>
Pero antes de instalar un gestor de arranque, le informaremos sobre
como configurar la memoria de imagen (framebuffer), siempre que usted
quiera, claro. Con la memoria de imagen puede ejecutar la línea de
comandos de Linux con algunas características gráficas (limitadas)
como, por ejemplo, el empleo de un bonita imagen de Gentoo durante el
inicio (bootsplash).
</p>
</body>
</subsection>

<subsection>
<title>Opcional: Memoria de imagen (Framebuffer)</title>
<body>

<p>
<e>Si</e> ha configurado su núcleo con soporte framebuffer (o ha
utilizado la configuración predeterminada de <c>genkernel</c>) puede
activarlo añadiendo el parámetro <c>video</c> al archivo de
configuración del gestor de inicio.
</p>

<p>
Lo primero que necesita es conocer que tipo de dispositivo de
framebuffer está utilizando. Debería usar <c>uvesafb</c> como
<e>controlador VESA</e>.
</p>

<p>
El parámetro <c>video</c> controla las opciones de
visualización. Necesita que se le indique el controlador de
framebuffer (<c>vesafb</c> para núcleos 2.6, o <c>vesa</c> para
núcleos 2.4) seguido de los parámetro de control que quiera
activar. Todas las variables aparecen listadas en
<path>/usr/src/linux/Documentation/fb/uvesafb.txt</path>. Las opciones
más utilizadas son:
</p>

<table>
<tr>
  <th>Control</th>
  <th>Descripción</th>
</tr>
<tr>
  <ti>ywrap</ti>
  <ti>
    Asume que su tarjeta gráfica puede volver sobre su
    memoria. (ej. continuar al principio cuando se aproxima al final)
  </ti>
</tr>
<tr>
  <ti>mtrr:<c>n</c></ti>
  <ti>
    Configura los registros MTRR. <c>n</c> puede ser: <br/>
    0 - desactivados <br/>
    1 - no almacenables en caché <br/>
    2 - write-back <br/>
    3 - write-combining <br/>
    4 - write-through
  </ti>
</tr>
<tr>
  <ti><c>mode</c></ti>
  <ti>
    Configura la resolución, la profundidad de color y la tasa de
    refresco. Por ejemplo, <c>1024x768-32@85</c> para una resolución
    de 1024x768, profundidad de color 32 bit y una tasa de refresco de
    85 Hz.
  </ti>
</tr>
</table>

<p>
El resultado sería algo como:
<c>video=uvesafb:mtrr:3,ywrap,1024x768-32@85</c>. Recuerde (o anote)
ésta configuración. La necesitará dentro de poco.
</p>

<p test="func:keyval('arch')='IA64'">
Ahora, debería instalar el <uri link="#elilo">gestor de arranque
elilo</uri>.
</p>

<p test="func:keyval('arch')='IA64'">
Ahora continue con la instalación de <uri link="#grub">GRUB</uri>
<e>o</e> <uri link="#lilo">LILO</uri>.
</p>
</body>
</subsection>
</section>

<section id="grub" test="contains('AMD64 x86',func:keyval('arch'))">
<title>Predeterminado: Usando GRUB</title>
<subsection>
<title>Comprendiendo la terminología de GRUB</title>
<body>

<p>
La parte más crítica para la comprensión de GRUB, es el habituarse a
la manera en que GRUB se refiere a los discos duros y las
particiones. Su partición Linux <path>/dev/sda1</path> es denominada
<path>(hd0,0)</path> por GRUB. Note los paréntesis alrededor de
<path>hd0,0</path>, que son obligatorios.
</p>

<p>
La numeración de los discos duros comienza con un cero, en lugar de
utilizar una "a" y las particiones empiezan con cero en lugar de con
un uno. Advertimos también que entre los dispositivos hd, sólo se
cuentan los discos duros y no los dispositivos atapi-ide como cdroms y
grabadoras. Además, la especificación es igual para dispositivos
SCSI. (Usualmente son asignados números mayores, excepto cuando el
bios está configurada para arrancar desde los dispositivos
scsi). Cuando se le indica la BIOS que arranque desde un disco duro
diferente (por ejemplo, el esclavo primario), <e>ese</e> disco duro se
verá como <path>hd0</path>.
</p>

<p>
Asumiendo que tiene un disco duro en <path>/dev/sda</path> y dos más
en <path>/dev/sdb</path> y <path>/dev/sdc</path>, la partición
<path>/dev/sdd7</path> que descrita como <path>(hd1,6)</path>. A lo
mejor puede sonar raro y bien podría serlo, pero como veremos, GRUB
ofrece un mecanismo de terminación por tabulador que es de gran ayuda
para los que tienen una gran cantidad de discos duros y que andan algo
perdidos con este esquema de numeración.
</p>

<p>
Habiéndole tomado la medida, es hora de instalar GRUB.
</p>
</body>
</subsection>

<subsection>
<title>Instalando GRUB</title>
<body>

<p>
Para instalar GRUB, primero lo instalamos con emerge:
</p>

<impo test="func:keyval('arch')='AMD64'">
Si está utilizando un <uri
link="?part=1&amp;chap=6#doc_chap2">perfil</uri> no multilib,
<b>no</b> debería instalar <c>grub</c>, en su lugar debería instalar
<c>grub-static</c>. Si planea usar un perfil no multilib <e>y</e>
tiene <b>desactivado</b> la emulación IA-32 en el núcleo, entonces
debería usar <c>lilo</c>.
</impo>

<pre caption = "Instalando GRUB">
# <i>emerge grub</i>
</pre>

<p>
Aunque GRUB esté instalado, todavía necesitamos crear un archivo de
configuración para él e instalar GRUB en nuestro MBR para que pueda
arrancar automáticamente nuestro núcleo recién creado.  Cree el
archivo <path>/boot/grub/grub.conf</path> con <c>nano</c>, o cualquier
otro editor:
</p>

<pre caption = "Creando el archivo /boot/grub/grub.conf">
# <i>nano -w /boot/grub/grub.conf</i>
</pre>

<p>
Ahora vamos a escribir el <path>grub.conf</path>. A continuación
encontrará dos posibles archivos <path>grub.conf</path> para el
ejemplo de particiones que manejamos en esta guía. Sólo hemos
comentado exhaustivamente el primer <path>grub.conf</path>. Asegúrese
de utilizar el nombre de <e>su</e> archivo de imagen del núcleo y, si
es necesario, el nombre de <e>su</e> imagen initrd.
</p>

<ul>
  <li>
    El primer <path>grub.conf</path> es para quienes no hayan usado
    <c>genkernel</c> para construir su núcleo
  </li>
  <li>
    El segundo <path>grub.conf</path> es para quienes hayan utilizado
    <c>genkernel</c> para construir su núcleo.
  </li>
</ul>

<note>
Grub asigna las designaciones según el BIOS. Al cambiar la
configuración del BIOS, cambiarán también las designaciones de los
dispositivos. Por ejemplo, si cambia el orden de los dispositivos para
el arranque, tal vez tenga que cambiar la configuración grub también.
</note>

<note>
Si su sistema de fichero para root es JFS, <e>deberá</e> añadir "ro"
a la línea del <c>núcleo</c> ya que JFS necesita leer su log antes de
permitir montajes de lectura-escritura.
</note>

<pre caption = "grub.conf para quienes no hayan usado genkernel">
<comment># Cual título arrancar por defecto. 0 es el primero, 1 el segundo, etc.</comment>
default 0
<comment># Cuantos segundos esperar antes de arrancar el título por defecto.</comment>
timeout 30
<comment># Una bella, hermosa imagen para ensalzar las cosas un poco :)
# Coméntela si no tiene una tarjeta gráfica instalada </comment>
splashimage=(hd0,0)/boot/grub/splash.xpm.gz

title Gentoo Linux <keyval id="kernel-version"/>
<comment># Partición donde se encuentra la imagen del núcleo (o sistema operativo)</comment>
root (hd0,0)
kernel /boot/<keyval id="kernel-name"/> root=/dev/sda3

title Gentoo Linux <keyval id="kernel-version"/> (rescate)
<comment># Partición donde se encuentra la imagen del núcleo (o sistema operativo)</comment>
root (hd0,0)
kernel /boot/<keyval id="kernel-name"/> root=/dev/sda3 init=/bin/bb

<comment># Las siguientes cuatro líneas sólo se usan en caso de arranque dual con un sistema Windows.</comment>
<comment># En este caso, Windows reside en la partición /dev/sda6.</comment>
title Windows XP
rootnoverify (hd0,5)
makeactive
chainloader +1
</pre>

<pre caption = "grub.conf para usuarios del genkernel">
default 0
timeout 30
splashimage=(hd0,0)/boot/grub/splash.xpm.gz

title Gentoo Linux <keyval id="kernel-version"/>
root (hd0,0)
kernel /boot/<keyval id="genkernel-name"/> root=/dev/ram0 init=/linuxrc ramdisk=8192 real_root=/dev/sda3
initrd /boot/<keyval id="genkernel-initrd"/>

<comment># Sólo en caso de arranque dual</comment>
title Windows XP
rootnoverify (hd0,5)
makeactive
chainloader +1
</pre>

<p>
Si se utiliza un esquema de particiones y/o imagen de núcleo distinta,
haga los ajustes respectivos. Sin embargo, asegúrese que cualquier
cosa que siga un dispositivo GRUB (tal como <path>(hd0,0)</path>) sea
relativa al punto de montaje y no de la raíz. En otras palabras,
<path>(hd0,0)/grub/splash.xpm.gz</path> es en realidad
<path>/boot/grub/splash.xpm.gz</path> ya que <path>(hd0,0)</path> es
<path>/boot</path>.
</p>

<p>
Además, si se eligió utilizar un esquema de particionamiento diferente
y no colocó <path>/boot</path> en una partición separada, el prefijo
<path>/boot</path> empleado en los ejemplos anteriores, es realmente
<e>necesario</e>. Si ha seguido el plan de particionamiento sugerido,
el prefijo <path>/boot</path> no es obligatorio, un enlace simbólico
<path>/boot</path>lo hace funcionar. En resumen, los ejemplos
anteriores deberían funcionar si ha definido una partición separada
para <path>/boot</path> o no.
</p>

<p>
Si necesita pasar algún parámetro adicional al núcleo, sencillamente
agréguelo al final de la línea de comando del núcleo. Ya estamos
pasando una opción (<c>root=/dev/sda3</c> o
<c>real_root=/dev/sda3</c>), pero se pueden pasar otras también, como
el parámetro <c>video</c> del que hablamos previamente.
</p>

<p>
Si su archivo de configuración del gestor de arranque contiene el
parámetro real_root, use el parámetro real_rootflags para establecer
las opciones de montaje del sistema de archivos raíz.
</p>

<p>
Si está utilizando un núcleo 2.6.7 o superior y ha puenteado su disco
duro porque la BIOS no puede manejar discos duros grandes, necesitará
añadir <c>sda=stroke</c>.
</p>

<p>
Los usuarios de <c>genkernel</c> deben saber que sus núcleos usan las
mismas opciones de arranque que el CD de instalación. Por ejemplo, si
tiene dispositivos scsi, debería agregar el parámetro <c>doscsi</c> al
núcleo.
</p>

<p>
Ahora grabe el archivo <path>grub.conf</path> y salga. Aún necesita
instalar GRUB en el MBR (Master Boot Record) para que GRUB se ejecute
automáticamente cuando arranque su sistema.
</p>

<p>
Los desarrolladores de GRUB recomiendan utilizar
<c>grub-install</c>. Sin embargo, si por alguna razón
<c>grub-install</c> no funciona correctamente todavía tiene la opción
de instalar GRUB manualmente.
</p>

<p>
Continué con <uri link="#grub-install-auto">Predeterminado: Configurando
GRUB utilizando grub-install</uri> o <uri
link="#grub-install-manual">Alternativa: Configurando GRUB a mano,
utilizando instrucciones</uri>.
</p>
</body>
</subsection>

<subsection id="grub-install-auto">
<title>Predeterminado: Configurando GRUB utilizando grub-install</title>
<body>

<p>
Para instalar GRUB necesita ejecutar el comando
<c>grub-install</c>. Sin embargo <c>grub-install</c> no funcionará tal
cual se instala, ya que estamos dentro de un entorno chroot.  Antes de
seguir, necesitará actualizar <path>/etc/mtab</path>, el cual contiene
la información relativa a todos los sistemas de archivos
montados. Afortunadamente hay una manera sencilla de realizar esto,
simplemente copie <path>/proc/mounts</path> a <path>/etc/mtab</path>,
excluyendo la línea <c>rootfs</c> si no ha creado una partición
separada para boot. El siguiente comando fucionará en ambos casos:
</p>

<pre caption="Crear /etc/mtab">
# <i>grep -v rootfs /proc/mounts &gt; /etc/mtab</i>
</pre>

<p>
Ahora podemos instalar GRUB utilizando <c>grub-install</c>:
</p>

<pre caption="Ejecutar grub-install">
# <i>grub-install --no-floppy /dev/sda</i>
</pre>

<p>
Si tiene alguna pregunta más acerca de GRUB, por favor consulte el
<uri link="http://www.gnu.org/software/grub/grub-faq.html">FAQ de GRUB
</uri>, la <uri link="http://grub.enbug.org/GrubLegacy">
Wiki de GRUB</uri>, o lea <c>info grub</c> en su terminal (estos
documentos están en inglés).
</p>

<p>
Continué con <uri link="#reboot">Reiniciando el sistema</uri>.
</p>
</body>
</subsection>

<subsection id="grub-install-manual">
<title>Alternativa: Configurando GRUB a mano, utilizando
instrucciones</title>
<body>

<p>
Para comenzar a configurar GRUB, debe ejecutar los comandos dentro de
<c>grub</c>.  Se le presentará el intérprete de comandos propio de
grub <path>grub&gt;</path>. Ahora necesita ejecutar los comandos
necesarios para instalar el registro de arranque de GRUB en su disco
duro.
</p>

<pre caption =" Iniciando el intérprete de comandos de GRUB">
# <i>grub --no-floppy</i>
</pre>

<note>
Si su sistema no tiene ningún dispositivo de disquetes, añada la
opción <c>--no-floppy</c> al comando anterior para evitar que grub
pruebe los dispositivos (no existentes) de disquetes.
</note>

<p>
En el ejemplo de configuración queremos instalar GRUB para que lea la
información de la partición de arranque <path><keyval
id="/boot"/></path>, e instala el registro de arranque de GRUB en el
MBR (Master boot Record) de su disco duro para que lo primero que
veamos aparecer al encender el ordenador sea GRUB. Por supuesto, si no
ha seguido el ejemplo de configuración durante la instalación, cambie
los comandos de acuerdo a su modelo:
</p>

<p>
El mecanismo de completar comandos por tabulación de GRUB puede
utilizarse dentro de GRUB. Por ejemplo, si escribe "<c>root (</c>"
seguido de una tabulación, notará que se le presenta una lista de
dispositivos (como pueda ser <path>hd0</path>). Si tecleamos "<c>root
(hd0,</c>" seguido de una tabulación recibiremos una lista de
particiones disponibles para elegir (como pueda ser
<path>hd0,0</path>).
</p>

<p>
Utilizando este mecanismo de completar por tabulación, configurar GRUB
no debería resultar tan duro. Ahora vamos a por ello, configuremos
GRUB!.
</p>

<pre caption =" Instalando GRUB en el MBR">
grub&gt; <i>root (hd0,0)</i>          <comment>(Especifique donde tiene su partición /boot)</comment>
grub&gt; <i>setup (hd0)</i>           <comment>(Instalamos GRUB en el MBR)</comment>
grub&gt; <i>quit</i>                  <comment>(Salimos del intérprete de comandos de GRUB)</comment>
</pre>

<note>
Si quiere instalar GRUB en una partición concreta en lugar del MBR,
debe modificar el comando <c>setup</c> para que apunte a la partición
correcta. Por ejemplo, si quiere que GRUB se instale en
<path>/dev/sda3</path>, el comando adecuado sería <c>setup
(hd0,2)</c>.  Sin embargo, pocos usuarios querrán hacer esto.
</note>

<p>
Si tiene alguna pregunta más acerca de GRUB, por favor consulte el
<uri link="http://www.gnu.org/software/grub/grub-faq.html">FAQ de GRUB
</uri>, la <uri link="http://grub.enbug.org/GrubLegacy">
Wiki de GRUB</uri>, o lea <c>info grub</c> en su terminal (estos
documentos están en inglés).
</p>

<p>
Continúe con <uri link="#reboot">Reiniciando el sistema</uri>.
</p>
</body>
</subsection>
</section>

<section id="lilo" test="contains('AMD64 x86', func:keyval('arch'))">
<title>Alternativa: Utilizando LILO</title>
<subsection>
<title>Instalando LILO</title>
<body>

<p>
LILO, representa LInuxLOader, y es el caballito de batalla probado y
comprobado de los gestores de inicio de Linux. Sin embargo, carece de
algunas características de GRUB (razón por la cual GRUB actualmente
está ganando popularidad). La razón por la cual LILO sigue en uso es
que en algunos sistemas, GRUB no funciona mientras que LILO sí. Por
supuesto también se usa porque hay muchos que lo conocen y prefieren
seguir con este gestor. De cualquier manera, Gentoo soporta ambos
gestores y por lo visto, ha elegido usar LILO.
</p>

<p>
Instalar LILO es fácil, sencillamente use <c>emerge</c>.
</p>

<pre caption = "Instalando LILO">
# <i>emerge lilo</i>
</pre>
</body>
</subsection>

<subsection>
<title>Configurando LILO</title>
<body>

<p>
Para configurar LILO, debe crear el archivo
<path>/etc/lilo.conf</path>. Use su editor de textos preferido (en el
manual usamos <c>nano</c> para ser consistentes) y creemos este
archivo.
</p>

<pre caption = "Creando /etc/lilo.conf">
# <i>nano -w /etc/lilo.conf</i>
</pre>

<p>
Algunas secciones atrás, le pedimos que se acordara del nombre de la
imagen del núcleo creado. En el siguiente ejemplo de
<path>lilo.conf</path> haremos uso del esquema ejemplo de
particionamiento. Hay dos partes separadas:
</p>

<ul>
  <li>
    Para los que no hayan usado <c>genkernel</c> para construir su
    núcleo
  </li>
  <li>
   Para los que hayan usado un <c>genkernel</c> para construir su
   núcleo
  </li>
</ul>

<p>
Asegúrese de utilizar el nombre de <e>su</e> archivo de imagen del
núcleo y, si es necesario, el nombre de <e>su</e> imagen initrd.
</p>

<note>
Si su sistema de ficheros para root es JFS, <e>deberá</e> añadir la
línea <c>append="ro"</c> después de cada elemento de arranque, ya que
JFS necesita leer su log antes de permitir montajes de
lectura-escritura.
</note>

<pre caption = "Ejemplo de /etc/lilo.conf">
boot=/dev/sda            <comment># Instalar LILO en el MBR</comment>
prompt                     <comment># Darle al usuario una oportunidad de seleccionar otra opción</comment>
timeout=50                <comment># Esperar 5 (cinco) segundos antes de arrancar la opción por defecto</comment>
default=gentoo          <comment># Al transcurrir el plazo de espera, arrancar la opción "gentoo"</comment>

<comment># Para los que no usaron genkernel</comment>
 image=/boot/<keyval id="kernel-name"/>
 label=gentoo            <comment># El nombre de la sección</comment>
 read-only               <comment># Comience con la raíz solo lectura. ¡No modifique!</comment>
 root=/dev/sda3          <comment># Ubicación del sistema raíz de archivos</comment>

 image=/boot/<keyval id="kernel-name"/>
 label=gentoo.rescue     <comment># El nombre de la sección</comment>
 read-only               <comment># Comience con la raíz solo lectura. ¡No modifique!</comment>
 root=/dev/sda3          <comment># Ubicación del sistema raíz de archivos</comment>
 append="init=/bin/bb"   <comment># Lanzar la shell estática de rescate de Gentoo</comment>

<comment># Para usuarios de genkernel</comment>
 image=/boot/<keyval id="genkernel-name"/>
 label=gentoo
 read-only
 root=/dev/ram0
 append="init=/linuxrc ramdisk=8192 real_root=/dev/sda3"
 initrd=/boot/<keyval id="genkernel-initrd"/>

<comment># La siguientes dos línea solo corresponden si hace arranque
dual con un sistema Windows.</comment>
<comment># En este caso, Windows se encuentra en /dev/sda6.</comment>
other=/dev/sda6
  label=windows
</pre>

<note>
Si usa un esquema de particionamiento o imagen de núcleo distinta,
haga los ajustes correspondientes.
</note>

<p>
Si hace falta pasar alguna opción adicional al núcleo, incluya un
enunciado <c>append</c> a la sección. A modo de ejemplo, agregamos un
enunciado <c>video</c> para activar framebuffer:
</p>

<pre caption = "Utilizar append para agregar opciones al núcleo">
 image=/boot/<keyval id="kernel-name"/>
 label=gentoo
 read-only
 root=/dev/sda3
<i>append="video=uvesafb:mtrr,ywrap,1024x768-32@85"</i>
</pre>

<p>
Si está utilizando un núcleo 2.6.7 o superior y ha puenteado su disco
duro porque la BIOS no puede manejar discos duros grandes, necesitará
añadir <c>sda=stroke</c>.
</p>

<p>
Usuarios de <c>genkernel</c> deben saber que sus núcleos usan las
mismas opciones de arranque que el CD de instalación. Por ejemplo, si
tiene dispositivos SCSI, debe agregar <c>doscsi</c> como opción del
núcleo.
</p>


<p>
Ahora, salve el archivo y salga del editor. Para terminar, debe
ejecutar el comando <c>/sbin/lilo</c> para poder aplicar
<path>/etc/lilo.conf</path> a su sistema (que se instale en el disco).
Acuérdese de que debe volver a ejecutar <c>/sbin/lilo</c> cada vez que
instale un nuevo núcleo o haga cambios en el menú.
</p>

<pre caption = "Terminando la instalación de LILO">
# <i>/sbin/lilo</i>
</pre>

<p>
Si tiene más preguntar con respecto a LILO, por favor, consulte su
<uri link="http://es.wikipedia.org/wiki/Lilo_%28Linux%29">página en la
wikipedia</uri>.
</p>

<p>
Ahora puede continuar con <uri link="#reboot">Reiniciando el
Sistema</uri>.
</p>
</body>
</subsection>
</section>

<section id="elilo" test="func:keyval('arch')='IA64'">
<title>Predeterminado: instalar elilo</title>
<body>

<p>
En la plataforma IA64, el cargador de arranque se llama elilo. Quizá
necesite instalarlo en su máquina primero.
</p>

<pre caption="Instalar elilo">
# <i>emerge elilo</i>
</pre>

<p>
Puede encontrar el archivo de configuración en
<path>/etc/elilo.conf</path> y un archivo de ejemplo en el directorio
usual de documentos
<path>/usr/share/doc/elilo-&lt;ver&gt;/</path>. Aquí tiene otro
ejemplo de configuración:
</p>

<pre caption="ejemplo /etc/elilo.conf">
boot=/dev/sda1
delay=30
timeout=50
default=Gentoo
append="console=ttyS0,9600"
prompt

image=/vmlinuz
        label=Gentoo
        root=/dev/sda2
        read-only

image=/vmlinuz.old
        label=Gentoo.old
        root=/dev/sda2
        read-only
</pre>

<p>
La linea <c>boot</c> le indica a elilo la ubicación de la partición de
boot (en este caso <path>/dev/sda1</path>). La línea <c>delay</c>
configura el número de segundos antes de que se inicie el arranque de
manera automática cuando se está en modo no interactivo. La línea
<c>timeout</c> es igual que delay pero para el modo interactivo. La
línea <c>default</c> establece la entrada de núcleo predeterminado (la
cual se lista debajo). La línea <c>append</c> añade opciones extra a
la línea de invocación del núcleo. <c>prompt</c> configura el
comportamiento predeterminado de elilo con el que interactuar.
</p>

<p>
Las secciones que comienzan con <c>image</c> definen distintas images
arrancables. Cada imagen tiene una etiqueta (<c>label</c>), un sistema
de ficheros raíz (<c>root</c>) y montarán la raíz del sistema en modo
solamente lectura (<c>read-only</c>).
</p>

<p>
Cuando la configuración está terminada, simplemente ejecute
<c>elilo --efiboot</c>. La opción <c>--efiboot</c> añade una entrada
al menú para Gentoo Linux en el Gestor de Arranque EFI.
</p>

<pre caption="Aplicar la configuración de elilo">
# <i>elilo --efiboot</i>
</pre>

<p>
Ahora continue con <uri link="#reboot">reiniciando el sistema</uri>.
</p>
</body>
</section>

<section id="reboot">
<title>Reiniciando el sistema</title>
<subsection>
<body>

<p>
Salga del entorno chroot y desmonte todas las particiones que
continúen montadas. Después podemos ejecutar el mágico comando que
hemos estado esperando: <c>reboot</c>.
</p>

<pre caption="Desmontar todas las particiones y reiniciar" test="func:keyval('arch')='IA64'">
# <i>exit</i>
cdimage ~# <i>cd</i>
cdimage ~# <i>umount /mnt/gentoo/boot /mnt/gentoo/sys /mnt/gentoo/dev /mnt/gentoo/proc /mnt/gentoo</i>
cdimage ~# <i>reboot</i>
</pre>

<pre caption="Desmontar todas las particiones y reiniciar" test="not(func:keyval('arch')='IA64')">
# <i>exit</i>
cdimage ~# <i>cd</i>
cdimage ~# <i>umount /mnt/gentoo/boot  /mnt/gentoo/dev /mnt/gentoo/proc /mnt/gentoo</i>
cdimage ~# <i>reboot</i>
</pre>

<p>
Por supuesto, no olvide quitar el CD arrancable, o el CD será
arrancado de nuevo en lugar de su nuevo sistema Gentoo.
</p>

<p test="func:keyval('arch')='IA64'">
Cuando reinicie el sistema debería poder observar una nueva opción en
el gestor de arranque EFI para Gentoo Linux que le permitirá
arrancarlo.
</p>

<p>
Una vez que haya reiniciado su instalación de Gentoo, termínela con
<uri link="?part=1&amp;chap=11">Finalizando su instalación de
Gentoo</uri>.
</p>
</body>
</subsection>
</section>
</sections>
