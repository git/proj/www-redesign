<?xml version='1.0' encoding="UTF-8"?>
<!DOCTYPE guide SYSTEM "/dtd/guide.dtd">

<!-- $Header: /var/cvsroot/gentoo/xml/htdocs/doc/es/handbook/index.xml,v 1.60 2010/07/21 01:05:33 nimiux Exp $ -->

<guide link="/doc/es/handbook/index.xml" lang="es">
<title>Manual Gentoo</title>

<author title="Autor">
  <mail link="swift"/>
</author>
<author title="Traductor">
  <mail link="chiguire"/>
</author>
<author title="Traductor">
  <mail link="yoswink"/>
</author>
<author title="Traductor">
  <mail link="carles@carles.no-ip.info">Carles Ferrer</mail>
</author>
<author title="Traductor">
  <mail link="anpereir@gentoo.org">Andrés Pereira</mail>
</author>
<author title="Traductor">
  <mail link="nimiux"/>
</author>

<abstract>
El Manual Gentoo es un esfuerzo para centralizar documentación en
forma de un manual coherente. Este manual contiene las instrucciones
para una instalación basada en conexión a Internet y
los capítulos referentes al trabajo con Gentoo y Portage.
</abstract>

<license/>

<version>0.51</version>
<date>2009-06-14</date>

<chapter>
<title>El proyecto de Manual Gentoo</title>
<section>
<title>Idiomas disponibles</title>
<body>

<p>
El Manual Gentoo está disponible en los siguientes idiomas:
</p>

<p>
<uri link="/doc/de/handbook/">alemán</uri> |
<!-- <uri link="/doc/cs/handbook/">checo</uri> | -->
<uri link="/doc/zh_cn/handbook/">chino simplificado</uri> |
<!-- <uri link="/doc/da/handbook/">danés</uri> | -->
<uri link="/doc/es/handbook/">español</uri> |
<uri link="/doc/fr/handbook/">francés</uri> |
<!--<uri link="/doc/id/handbook/">indonesio</uri> |-->
<uri link="/doc/en/handbook/">inglés</uri> |
<uri link="/doc/it/handbook/">italiano</uri> |
<uri link="/doc/ja/handbook/">japonés</uri> |
<uri link="/doc/pl/handbook/">polaco</uri>
<!-- <uri link="/doc/pt_br/handbook/">portugués brasileño</uri> | -->
<!-- <uri link="/doc/ro/handbook/">rumano</uri> | -->
<!-- <uri link="/doc/ru/handbook/">ruso</uri> -->
</p>
</body>
</section>

<section>
<title>Introducción</title>
<body>

<p>
Bienvenidos al Manual Gentoo. Aquí encontrará explicaciones que
deberían responder la mayoría de las preguntas sobre el
manual. Hablaremos acerca de las ideas detrás del mismo, su estado
actual, planes futuros y cómo gestionar los informes de errores,
etc.
</p>

<impo>
Actualmente el manual está disponible en castellano para las
arquitecturas: amd64, ppc, sparc y x86.
</impo>

</body>
</section>
</chapter>

<chapter>
<title>Ver el Manual</title>
<section>
<body>

<table>
<tr>
  <th>Formato</th>
  <th>Descripción</th>
  <th>Enlaces</th>
</tr>
<tr>
  <ti>HTML</ti>
  <ti>Última versión, una página por capítulo, perfecto para ver en línea</ti>
  <ti>
    <uri link="handbook-x86.xml">x86</uri>,
    <uri link="handbook-sparc.xml">sparc</uri>,
    <uri link="handbook-amd64.xml">amd64</uri>,
    <uri link="handbook-ppc.xml">ppc</uri>,
    <uri link="/doc/en/handbook/handbook-ppc64.xml">ppc64</uri>,
    <uri link="/doc/en/handbook/handbook-alpha.xml">alpha</uri>,
    <uri link="/doc/en/handbook/handbook-hppa.xml">hppa</uri>,
    <uri link="/doc/en/handbook/handbook-mips.xml">mips</uri>,
    <uri link="/doc/en/handbook/handbook-ia64.xml">ia64</uri>,
    <uri link="/doc/en/handbook/handbook-arm.xml">arm</uri>
  </ti>
</tr>
<tr>
  <ti>HTML</ti>
  <ti>Última versión, todo en una sola página</ti>
  <ti>
    <uri link="handbook-x86.xml?full=1">x86</uri>,
    <uri link="handbook-sparc.xml?full=1">sparc</uri>,
    <uri link="handbook-amd64.xml?full=1">amd64</uri>,
    <uri link="handbook-ppc.xml?full=1">ppc</uri>,
    <uri link="/doc/en/handbook/handbook-ppc64.xml?full=1">ppc64</uri>,
    <uri link="/doc/en/handbook/handbook-alpha.xml?full=1">alpha</uri>,
    <uri link="/doc/en/handbook/handbook-hppa.xml?full=1">hppa</uri>,
    <uri link="/doc/en/handbook/handbook-mips.xml?full=1">mips</uri>,
    <uri link="/doc/en/handbook/handbook-ia64.xml?full=1">ia64</uri>,
    <uri link="/doc/en/handbook/handbook-arm.xml?full=1">arm</uri>
  </ti>
</tr>

<tr>
  <ti>HTML</ti>
  <ti>Última versión, todo en una sola página, versión para imprimir</ti>
  <ti>
    <uri link="handbook-x86.xml?style=printable&amp;full=1">x86</uri>,
    <uri link="handbook-sparc.xml?style=printable&amp;full=1">sparc</uri>,
    <uri link="handbook-amd64.xml?style=printable&amp;full=1">amd64</uri>,
    <uri link="handbook-ppc.xml?style=printable&amp;full=1">ppc</uri>,
    <uri link="/doc/en/handbook/handbook-ppc64.xml?style=printable&amp;full=1">
    ppc64</uri>,
    <uri link="/doc/en/handbook/handbook-alpha.xml?style=printable&amp;full=1">
    alpha</uri>,
    <uri link="/doc/en/handbook/handbook-hppa.xml?style=printable&amp;full=1">
    hppa</uri>,
    <uri link="/doc/en/handbook/handbook-mips.xml?style=printable&amp;full=1">
    mips</uri>,
    <uri link="/doc/en/handbook/handbook-ia64.xml?style=printable&amp;full=1">
    ia64</uri>,
    <uri link="/doc/en/handbook/handbook-arm.xml?style=printable&amp;full=1">
    arm</uri>
  </ti>
</tr>
</table>

</body>
</section>

<section>
<title>Histórico de versiones</title>
<body>

<p>
Con la intención de proporcionar un archivo histórico, permanecen en
línea los manuales de versiones anteriores (desde la 2004.2). Se
pueden obtener versiones "para imprimir" de las páginas individuales
usando el enlace "Print" en la esquina superior derecha. Para obtener
la versión "todas las páginas en una", añada <path>?full=1</path> al
URL.
</p>

<warn>
Estos manuales <e>no</e> son actualizados.
</warn>

<table>
<tr>
  <th>Versión</th>
  <th>Arquitecturas</th>
</tr>
<tr>
  <ti>2004.2</ti>
  <ti>
    <uri link="/doc/en/handbook/2004.2/handbook-alpha.xml">alpha</uri>,
    <uri link="/doc/en/handbook/2004.2/handbook-amd64.xml">amd64</uri>,
    <uri link="/doc/en/handbook/2004.2/handbook-hppa.xml">hppa</uri>,
    <uri link="/doc/en/handbook/2004.2/handbook-mips.xml">mips</uri>,
    <uri link="/doc/en/handbook/2004.2/handbook-ppc.xml">ppc</uri>,
    <uri link="/doc/en/handbook/2004.2/handbook-sparc.xml">sparc</uri>,
    <uri link="2004.2/handbook-x86.xml">x86</uri>
</ti>
</tr>
 <tr>
  <ti>2004.3</ti>
  <ti>
    <uri link="2004.3/handbook-amd64.xml">amd64</uri>,
    <uri link="2004.3/handbook-hppa.xml">hppa</uri>,
    <uri link="2004.3/handbook-ppc.xml">ppc</uri>,
    <uri link="2004.3/handbook-sparc.xml">sparc</uri>,
    <uri link="2004.3/handbook-x86.xml">x86</uri>
  </ti>
</tr>
<tr>
  <ti>2005.0</ti>
  <ti>
    <uri link="/doc/en/handbook/2005.0/handbook-alpha.xml">alpha</uri>,
    <uri link="/doc/en/handbook/2005.0/handbook-amd64.xml">amd64</uri>,
    <uri link="/doc/en/handbook/2005.0/handbook-hppa.xml">hppa</uri>,
    <uri link="/doc/en/handbook/2005.0/handbook-ppc.xml">ppc</uri>,
    <uri link="/doc/en/handbook/2005.0/handbook-sparc.xml">sparc</uri>,
    <uri link="2004.3/handbook-x86.xml">x86</uri>
  </ti>
</tr>
<tr>
   <ti>2005.1</ti>
   <ti>
     <uri link="/doc/en/handbook/2005.1/handbook-alpha.xml">alpha</uri>,
     <uri link="/doc/en/handbook/2005.1/handbook-amd64.xml">amd64</uri>,
     <uri link="/doc/en/handbook/2005.1/handbook-hppa.xml">hppa</uri>,
     <uri link="/doc/es/handbook/2005.1/handbook-ppc.xml">ppc</uri>,
     <uri link="/doc/en/handbook/2005.1/handbook-ppc64.xml">ppc64</uri>,
     <uri link="/doc/en/handbook/2005.1/handbook-sparc.xml">sparc</uri>,
     <uri link="/doc/es/handbook/2005.1/handbook-x86.xml">x86</uri>
   </ti>
</tr>
<tr>
   <ti>2006.0</ti>
   <ti>
     <uri link="/doc/en/handbook/2006.0/handbook-alpha.xml">alpha</uri>,
     <uri link="/doc/es/handbook/2006.0/handbook-amd64.xml">amd64</uri>,
     <uri link="/doc/en/handbook/2006.0/handbook-hppa.xml">hppa</uri>,
     <uri link="/doc/es/handbook/2006.0/handbook-ppc.xml">ppc</uri>,
     <uri link="/doc/en/handbook/2006.0/handbook-ppc64.xml">ppc64</uri>,
     <uri link="/doc/en/handbook/2006.0/handbook-sparc.xml">sparc</uri>,
     <uri link="/doc/es/handbook/2006.0/handbook-x86.xml">x86</uri>
   </ti>
</tr>
<tr>
   <ti>2006.1</ti>
   <ti>
     <uri link="/doc/en/handbook/2006.1/handbook-alpha.xml">alpha</uri>,
     <uri link="/doc/en/handbook/2006.1/handbook-amd64.xml">amd64</uri>,
     <uri link="/doc/en/handbook/2006.1/handbook-hppa.xml">hppa</uri>,
     <uri link="/doc/en/handbook/2006.1/handbook-ppc.xml">ppc</uri>,
     <uri link="/doc/en/handbook/2006.1/handbook-ppc64.xml">ppc64</uri>,
     <uri link="/doc/en/handbook/2006.1/handbook-sparc.xml">sparc</uri>,
     <uri link="/doc/en/handbook/2006.1/handbook-x86.xml">x86</uri>
   </ti>
</tr>
<tr>
   <ti>2007.0</ti>
   <ti>
     <uri link="/doc/en/handbook/2007.0/handbook-amd64.xml">amd64</uri>,
     <uri link="/doc/en/handbook/2007.0/handbook-hppa.xml">hppa</uri>,
     <uri link="/doc/en/handbook/2007.0/handbook-ppc.xml">ppc</uri>,
     <uri link="/doc/en/handbook/2007.0/handbook-ppc64.xml">ppc64</uri>,
     <uri link="/doc/en/handbook/2007.0/handbook-sparc.xml">sparc</uri>,
     <uri link="/doc/en/handbook/2007.0/handbook-x86.xml">x86</uri>
   </ti>
</tr>
<tr>
  <ti>2008.0</ti>
  <ti>
     <uri link="/doc/es/handbook/2008.0/handbook-amd64.xml">amd64</uri>,
     <uri link="/doc/en/handbook/2008.0/handbook-hppa.xml">hppa</uri>,
     <uri link="/doc/en/handbook/2008.0/handbook-ppc.xml">ppc</uri>,
     <uri link="/doc/en/handbook/2008.0/handbook-ppc64.xml">ppc64</uri>,
     <uri link="/doc/en/handbook/2008.0/handbook-sparc.xml">sparc</uri>,
     <uri link="/doc/es/handbook/2008.0/handbook-x86.xml">x86</uri>
  </ti>
</tr>
</table>

</body>
</section>
</chapter>

<chapter>
<title>Información acerca del Manual</title>
<section>
<title>Propósito</title>
<body>

<p>
El propósito del Manual Gentoo es la creación de un documento
coherente que describa todos los aspectos posibles de Gentoo
Linux. Combina las guías existentes en un manual consistente, de
manera que el mismo documento sea usado para todas las arquitecturas
posibles, todos los tipos de instalación y todos los usuarios. Esto no
solo es más fácil de mantener, sino que el usuario puede percatarse
que son pocas las diferencias y que las demás arquitecturas no sean
vistas como entidades separadas.
</p>

<p>
También nos permite tratar más los puntos difíciles y dar más
ejemplos. Además, como ya no estamos obligados a escribir toda la
documentación en una sola página, ésta podrá leerse más fluidamente.
</p>
</body>
</section>

<section>
<title>Estado actual</title>
<body>

<p>
Actualmente, la cuarta sección acerca de <uri
link="handbook-x86.xml?part=4">Configuración de redes en Gentoo</uri>
está terminada y oficialmente disponible. Cubre en profundidad las
opciones para trabajar con redes para el nuevo baselayout estable.
</p>

<p>
Si le interesa el desarrollo del Manual, por favor use la <mail
link="gentoo-doc@gentoo.org">lista de correo gentoo-doc</mail> para
sus comentarios.
</p>
</body>
</section>

<section>
<title>Reportando errores y peticiones para mejoras</title>
<body>

<p>
Si encuentra un error en el Manual, por favor visite nuestro <uri
link="http://bugs.gentoo.org">Gentoo Linux Bugzilla</uri> y archive un
reporte de <e>Documentation</e>, para el componente <e>Installation
Handbook</e>.
</p>
</body>
</section>
</chapter>

<chapter id="faq">
<title>Preguntas de uso frecuente (PUF)</title>
<section>
<title>¿No se puede tener un Manual creado dinámicamente
para cada opción?</title>
<body>

<p>
Todo es posible, pero hemos optado no hacerlo por varios motivos.
</p>

<p>
El <b>mantenimiento</b> de la guía sería más difícil. No solo
tendríamos que cotejar ciertos reportes de error contra ciertas
decisiones (pocos reportes de error nos dirían qué se ha escogido
anteriormente) pero se haría mucho más difícil escribir la guía con
agilidad, poniendo mucha atención a cada línea en el Manual, la
consistencia, etc.
</p>

<p>
Aunque actualmente tenemos solamente una versión en línea, <b>otros
formatos</b>, tales como PDF, pueden ser desarrollados. Si creásemos
PDFs para cada arquitectura y cada opción, tendríamos que proporcionar
una cantidad enorme de PDFs con pocas diferencias, lo cual sería una
pérdida de recursos :).
</p>

<p>
Las opciones que escoja un usuario se hacen <b>en distintos
lugares</b>. Esto haría más difícil que el usuario imprima el manual
-- de forma que tendríamos que informar al usuario de todas las
opciones posibles antes de comenzar, probablemente dándole un buen
susto.
</p>

<p>
La parte acerca de la "Instalación de Gentoo" <b>no es la única
parte</b> del Manual Gentoo. Es la primera, pero todas las siguientes
son independientes respecto a las opciones escogidas por el
usuario. La creación dinámica del manual sólo por las instrucciones
de instalación sería demasiado.
</p>

<p>
El no separar las instrucciones para cada opción posible, permite al
usuario ver los resultados de otras opciones con facilidad,
ofreciéndole al usuario <b>una mejor perspectiva de las instrucciones
de instalación</b>.
</p>
</body>
</section>

<section>
<title>No puedo encontrar la información sobre stage1 en el Manual de
Gentoo</title>
<body>

<p>
Las instrucciones sobre el empleo de un archivo stage1 o stage2 están
ahora disponibles en el <uri link="/doc/es/faq.xml#stage12">Preguntas
frecuentes en Gentoo</uri>. La instalación desde stage3 es el único
método soportado.
</p>
</body>
</section>

<section>
<title>No estoy de acuerdo con ...</title>
<body>

<p>
Le agradecemos que por favor <uri
link="http://bugs.gentoo.org">reporte</uri> su idea, ya que el no
estar de acuerdo y no suministrar ideas constructivas no mejora la
situación. La documentación se hace para la comunidad, por lo que la
retro-alimentación por parte de la comunidad es muy apreciada.
</p>

<p>
Sin embargo, es de notar que la mayoría de las decisiones en el
proceso de desarrollo de la documentación se hacen por consenso. Como
es <e>imposible</e> escribir o estructurar documentación de una manera
que satisfaga a todos, a veces debemos estar dispuestos a aceptar un
"no" si creemos que la implementación actual beneficia a la gran
mayoría.
</p>
</body>
</section>
</chapter>
</guide>
