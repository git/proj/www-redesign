<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE sections SYSTEM "/dtd/book.dtd">

<!-- The content of this document is licensed under the CC-BY-SA license -->
<!-- See http://creativecommons.org/licenses/by-sa/2.5-->

<!-- $Header: /var/cvsroot/gentoo/xml/htdocs/doc/es/handbook/hb-working-features.xml,v 1.15 2010/07/14 00:39:29 nimiux Exp $ -->

<sections>

<abstract>
Descubra las características de Portage, como el soporte para la
compilación distribuida, ccache y más.
</abstract>

<version>1.33</version>
<date>2010-07-12</date>

<section>
<title>Características de Portage</title>
<body>

<p>
Portage tiene varias características adicionales que hacen de su experiencia
con Gentoo algo mucho mejor. Muchas de estas características residen en
ciertas herramientas software que mejoran el rendimiento, la estabilidad, la
seguridad, ...
</p>

<p>
Para activar o desactivar ciertas características de Portage necesita editar
la variable <c>FEATURES</c> del archivo <path>/etc/make.conf</path>. Esta
variable contiene una lista con las palabras clave de cada característica
separadas por un espacio en blanco. En algunos casos necesita además instalar
la herramienta que implementa la característica.
</p>

<p>
No todas las características que soporta Portage están aquí reflejadas.
Para una consulta completa por favor revise la página de la ayuda referente
a <path>make.conf</path>
</p>

<pre caption="Consultando la página de ayuda sobre make.conf">
$ <i>man make.conf</i>
</pre>

<p>
Para conocer qué características están siendo utilizadas por defecto, ejecute
<c>emerge --info</c> y busque la variable FEATURES o utilice grep:
</p>

<pre caption="Conociendo que características están configuradas">
$ <i>emerge --info | grep FEATURES</i>
</pre>
</body>
</section>

<section>
<title>Compilación Distribuida</title>
<subsection>
<title>Usando distcc</title>
<body>

<p>
<c>distcc</c> es un programa para distribuir un trabajo de compilación a
través de muchas, no necesariamente idénticas, máquinas en una red. Los
clientes de <c>distcc</c> envían toda la información necesaria a los
servidores DistCC disponibles (corriendo <c>distccd</c>) así pueden compilar
trozos de código fuente para el cliente. El resultado final, es un tiempo de
compilación más rápido.
</p>

<p>
Puede encontrar información más detallada sobre <c>distcc</c> (e información
de como tenerlo funcionando sobre Gentoo) en nuestra <uri
link="/doc/es/distcc.xml">Documentación Gentoo de Distcc</uri>.
</p>
</body>
</subsection>

<subsection>
<title>Instalando distcc</title>
<body>

<p>
Distcc se distribuye con un monitor gráfico para monitorizar las tareas que su
computador está enviando para compilar. Si usa Gnome entonces ponga 'gnome' en
su configuración <c>USE</c>. De todas formas, si no usa Gnome pero sigue
deseando disponer de un monitor, entonces debería poner 'gtk' en su
configuración <c>USE</c>.
</p>

<pre caption="Instalando distcc">
# <i>emerge distcc</i>
</pre>
</body>
</subsection>

<subsection>
<title>Activando el soporte en Portage</title>
<body>

<p>
Añada <c>distcc</c> a la variable FEATURES dentro de
<path>/etc/make.conf</path>. Hecho esto, edite la variable <c>MAKEOPTS</c>
a sus necesidades. Una pauta conocida para configurarla es poner <c>-jX</c> con
<c>X</c> representando el número de CPUs que ejecutan <c>distccd</c>
(incluyendo el host local) más uno, pero quizá obtenga mejores resultados
con otros números.
</p>

<p>
Ahora ejecute <c>distcc-config</c> y cree una lista de los servidores distcc
disponibles. Para un ejemplo simple, supondremos que los servidores DistCC son
192.168.1.102 (el host local), 192.168.1.103 y <c>192.168.1.104</c>
(los dos hosts "remotos"):
</p>

<pre caption="Configurando distcc para usar los tres servidores DistCC
disponibles">
# <i>distcc-config --set-hosts "192.168.1.102 192.168.1.103 192.168.1.104"</i>
</pre>

<p>
Por supuesto, no se olvide ejecutar también el demonio <c>distccd</c>:
</p>

<pre caption="Arrancando el demonio distcc">
# <i>rc-update add distccd default</i>
# <i>/etc/init.d/distccd start</i>
</pre>
</body>
</subsection>
</section>

<section>
<title>Compilación utilizando caché</title>
<subsection>
<title>Acerca de ccache</title>
<body>

<p>
<c>ccache</c> es un caché de compilación rápida. Cuando compila un programa,
puede cachear resultados intermedios, de forma que, si recompila el mismo
programa, el tiempo de compilación se reducirá ampliamente. La primera
vez que se ejecuta ccache, ésta será más lenta que una compilación normal.
Recompilaciones posteriores deberían ser más rápidas. La herramienta
ccache sólo es útil si va a recompilar la misma aplicación muchas veces;
por lo tanto en la mayoría de los casos es útil únicamente para los
desarrolladores de software.
</p>

<p>
Si esta interesado en los pros y los contras de <c>ccache</c>, por favor
visite la <uri link="http://ccache.samba.org">página web de ccache</uri>.
</p>

<warn>
<c>ccache</c> puede causar numerosos fallos de compilación. Algunas
veces ccache mantendrá objetos con código obsoleto o ficheros corruptos
que pueden llevar a que no se pueda hacer emerge de ciertos paquetes. Si
esto ocurre (Si obtiene errores como "File not recognized: File
truncated"), intente recompilar la aplicación con ccache deshabilitado
(<c>FEATURES="-ccache"</c> en <path>/etc/make.conf</path>) <e>antes</e>
de informar de un bug. A menos que esté realizando trabajo de desarrollo,
<e>no active ccache</e>.
</warn>

</body>
</subsection>

<subsection>
<title>Instalando ccache</title>
<body>

<p>
Para instalar <c>ccache</c>, ejecute <c>emerge ccache</c>:
</p>

<pre caption="Instalando ccache">
# <i>emerge ccache</i>
</pre>
</body>
</subsection>

<subsection>
<title>Activando el Soporte en Portage</title>
<body>

<p>
Primero, edite el <path>/etc/make.conf</path> y añada a la variable
<c>FEATURES</c> la palabra clave <c>ccache</c>. A continuación,
añada una nueva variable llamada CCACHE_SIZE y dele el valor de "2G":
</p>

<pre caption="Editando CCACHE_SIZE en /etc/make.conf">
CCACHE_SIZE="2G"
</pre>

<p>
Para comprobar si ccache funciona, pídale a ccache que te muestre
las estadísticas. Ya que Portage utiliza un directorio diferente para
guardar los datos, se necesita fijar la variable <c>CCACHE_DIR</c> para
reflejar esto:
</p>

<pre caption="Observando las estadísticas de ccache">
# <i>CCACHE_DIR="/var/tmp/ccache" ccache -s</i>
</pre>

<p>
La ruta <path>/var/tmp/ccache</path> es el directorio por defecto que
emplea Portage para ccache; si quiere cambiar esta variable, configure
<c>CCACHE_DIR</c> en <path>/etc/make.conf</path>.
</p>

<p>
Sin embargo, si ejecuta <c>ccache</c>, empleará como directorio por defecto
<path>${HOME}/.ccache</path>, que es la razón por la cual necesita configurar
la variable <c>CCACHE_DIR</c> cuando se le pide a Portage que muestre las
estadísticas de ccache.
</p>
</body>
</subsection>

<subsection>
<title>Utilizando ccache para compilaciones de C sin relación con Portage</title>
<body>

<p>
Si quiere utilizar ccache para compilaciones que no tengan que ver con Portage,
añada <path>/usr/lib/ccache/bin</path> al principio de su variable PATH
(antes de <path>/usr/bin</path>). Esto puede llevarse a cabo editando
el fichero <path>.bash_profile</path> de su directorio home de usuario.
<path>.bash_profile</path> es una de las maneras de definir las variables PATH.
</p>

<pre caption="Editar .bash_profile">
PATH="<i>/usr/lib/ccache/bin</i>:/opt/bin:${PATH}"
</pre>
</body>
</subsection>
</section>

<section>
<title>Soporte para Paquetes Binarios</title>
<subsection>
<title>Creando paquetes binarios</title>
<body>

<p>
Portage soporta la instalación de paquetes precompilados. A pesar de que
Gentoo no proporciona paquetes precompilados por sí mismo (excepto para
las imágenes GRP) Portage puede funcionar perfectamente con paquetes
precompilados.
</p>

<p>
Para crear un paquete precompilado puede utilizar <c>quickpkg</c> si el paquete
está instado en su sistema, o <c>emerge</c> con las opciones <c>--buildpkg</c>
o <c>--buildpkgonly</c>.
</p>

<p>
Si quiere que Portage cree paquetes precompilados de cada paquete individual
que instale, añada <c>buildpkg</c> a la variable FEATURES.
</p>

<p>
Puede encontrar mayor soporte para la creación de conjuntos de paquetes
precompilados con <c>catalyst</c>. Para más información sobre catalyst,
por favor lea las <uri link="/proj/es/releng/catalyst/faq.xml">
Preguntas frecuentes sobre Catalyst</uri> (en inglés).
</p>
</body>
</subsection>

<subsection>
<title>Instalando Paquetes Precompilados</title>
<body>

<p>
A pesar de que Gentoo no proporciona uno, puede crear un repositorio central
donde almacene paquetes precompilados. Si quiere utilizar este repositorio,
necesita que Portage lo conozca a través de la variable PORTAGE_BINHOST
que debe apuntar al repositorio. Por ejemplo, si los paquetes precompilados
están en ftp://buildhost/gentoo:
</p>

<pre caption="Configurando PORTAGE_BINHOST en /etc/make.conf">
PORTAGE_BINHOST="ftp://buildhost/gentoo"
</pre>

<p>
Cuando quiera instalar un paquete precompilado, añada la opción
<c>--getbinpkg</c> al comando emerge junto a  la opción <c>--usepkg</c>.
La primera le indica a emerge que descargue el paquete precompilado
del servidor definido previamente, mientras que el segundo indica a emerge
que intente instalar el paquete precompilado antes de buscar el código fuente
y compilarlo.
</p>

<p>
Por ejemplo, para instalar <c>gnumeric</c> a través de paquetes precompilados:
</p>

<pre caption="Instalando el paquete precompilado gnumeric">
# <i>emerge --usepkg --getbinpkg gnumeric</i>
</pre>

<p>
Más información sobre las opciones para utilizar paquetes precompilados con
emerge puede consultarse en la página de la ayuda:
</p>

<pre caption="Leyendo la página de ayuda sobre emerge">
$ <i>man emerge</i>
</pre>
</body>
</subsection>
</section>

<section>
<title>Descargando Ficheros</title>
<subsection>
<title>Parallel fetch</title>
<body>

<p>
Al hacer emerge a una serie de paquetes, Portage puede obtener las
fuentes para el siguiente paquete en el lista aún mientras está
compilando otro paquete, acortando los tiempos de instalación. Para
hacer uso de esta opción agregue "parallel-fetch" a su variable
FEATURES.
</p>
</body>
</subsection>

<subsection>
<title>Userfetch</title>
<body>

<p>
Cuando Portage se ejecuta por el usuario root, FEATURES="userfetch"
permitirá que Portage ejecute sin los privilegios de superusuario
mientras obtiene las fuentes. Este es una pequeña mejora en la
seguridad.
</p>
</body>
</subsection>
</section>
</sections>
