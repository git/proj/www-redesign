<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE sections SYSTEM "/dtd/book.dtd">

<!-- The content of this document is licensed under the CC-BY-SA license -->
<!-- See http://creativecommons.org/licenses/by-sa/2.5 -->

<!-- $Header: /var/cvsroot/gentoo/xml/htdocs/doc/es/handbook/hb-install-about.xml,v 1.18 2010/06/16 16:54:04 nimiux Exp $ -->

<sections>

<abstract>
Este capítulo presenta el método de instalación documentado en este manual.
</abstract>

<version>9.1</version>
<date>2010-06-14</date>

<section>
<title>Introducción</title>
<subsection>
<title>¡Bienvenido!</title>
<body>

<p>
Primero de todo, <e>bienvenido/a</e> a Gentoo. Está a punto de entrar en
un mundo de flexibilidad y rendimiento. Gentoo es la flexibilidad en sí.
Cuando instalas Gentoo, esto queda claro varias veces, puede elegir
cuánto quieres compilar tu mismo, cómo instalar Gentoo, que gestor de
registro prefieres, etc.
</p>

<p>
Gentoo es una metadistribución moderna, rápida, con un diseño limpio y
flexible. Gentoo está hecha alrededor del software libre y no oculta a sus
usuarios qué hay bajo la alfombra. Portage, el sistema de mantenimiento
de paquetes que usa Gentoo, está escrito en Python, por lo que el código
fuente es fácil de visualizar y modificar. El sistema de paquetes de Gentoo
se basa en el código fuente (aunque también soporta paquetes precompilados)
y para configurar Gentoo se utilizan archivos de texto plano. En otras
palabras, abierto a cualquiera.
</p>

<p>
Es muy importante que entienda que la <e>flexibilidad</e> es lo que
hace que Gentoo funcione. Intentamos no forzarle a entrar en algo que
no le guste. Si cree en algún momento que lo estamos haciendo mal, por favor,
<uri link="http://bugs.gentoo.org">envíe su opinión</uri>.
</p>

</body>
</subsection>
<subsection>
<title>¿Cómo está estructurada la instalación?</title>
<body>

<p>
La instalación de Gentoo puede verse como un procedimiento de 10 pasos,
los correspondientes a los capítulos 2 a 11. Cada paso da como resultado
un cierto estado:
</p>

<ul>
<li>
  Tras el paso 1, te encontrará en un entorno funcional preparado para
  instalar Gentoo
</li>
<li>
  Después del paso 2, la conexión a Internet estará funcionando y lista
  para instalar Gentoo.
</li>
<li>
  Tras el paso 3, sus discos duros estarán preparados para alojar tu
  instalación de Gentoo
</li>
<li>
  Tras el paso 4, el entorno de instalación estará preparado y se encontrará
  dentro de un entorno chroot.
</li>
<li>
  Después del paso 5, los paquetes principales, que son los mismos en toda
  instalación de Gentoo, estarán instalados
</li>
<li>
  Tras el paso 6, el kernel Linux estará compilado.
</li>
<li>
  Después del paso 7, la mayoría de los archivos de configuración de tu
  sistema Gentoo estarán preparados
</li>
<li>
  Tras el paso 8, las herramientas de sistema necesarias (podrá elegirlas
  de una hermosa lista) están instaladas.
</li>
<li>
  Al finalizar el paso 9, el gestor de arranque elegido estará instalado y
  configurado y estará dentro de su nueva instalación de Gentoo.
</li>
<li>
  Tras el paso 10, se encontrará dentro de su nueva Gentoo.
</li>
</ul>

<p>
Cuando se le pide una elección especial, intentamos explicar lo mejor posible
los pros y contras. También propondremos una opción por defecto, identificada
con "Por defecto:" en el título. Las otras posibilidades se titulan
"Alternativa:". Pero <e>no</e> crea que la opción por defecto es la que
recomendamos. Es la que pensamos que la mayoría de usuarios van a utilizar.
</p>

<p>
Algunas veces se puede seguir un paso opcional. Estos pasos son marcados como
"Opcional: " y no son necesarios para instalar Gentoo.  Sin embargo, algunos
pasos opcionales dependen de una decisión tomada previamente. Le informaremos
cuando se dé el caso, tanto cuando tome la decisión, como cuando se describa
el paso opcional.
</p>

</body>
</subsection>
<subsection>
<title>¿De qué opciones dispongo?</title>
<body>

<p>
Puede instalar Gentoo de diferentes formas. Puede descargar e instalar
uno de nuestros CDs de instalación, desde otra distribución, desde un
CD de arranque ajeno a Gentoo (como Knoppix), desde un arranque por red,
desde un disquete de arranque,etc.
</p>

<p>
Este documento abarca la instalación utilizando un <e>CD de instalación
de Gentoo</e> o, en algunos casos, instalación por red. La instalación
asume que quiere instalar la última versión de cada paquete.
</p>

<note>
Para encontrar ayuda acerca de otros procedimientos de instalación,
incluyendo el uso de CDs ajenos a Gentoo, por favor, lea nuestra <uri
link="/doc/es/altinstall.xml">Guía de Instalación Alternativa</uri>.
</note>

<p>
Si quiere realizar una instalación sin conexión a la red, debería leer
los <uri link="/doc/es/handbook/2008.0/index.xml">Manuales Gentoo
2008.0 </uri> el cual contiene las instrucciones de instalación para un
entorno sin conexión a la red.
</p>

<p>
También tenga en cuenta que, si está planeando utilizar GRP (Gentoo
Reference Platform, una colección de paquetes precompilados que pueden
ser utilizados justo después de la instalación de Gentoo), <e>debe</e>
seguir las instrucciones correspondientes a los <uri
link="/doc/es/handbook/2008.0/index.xml">Manuales Gentoo 2008.0 sin
conexión a la red</uri>.
</p>

<p>
También ofrecemos un documento sobre <uri
link="/doc/es/gentoo-x86-tipsntricks.xml">Consejos y trucos en
la instalación de Gentoo</uri> que también puede ser útil.
Si es un usuario Gentoo experimentado y simplemente necesita
una breve lista de comprobación sobre la instalación,
lea libremente la Guía de Instalación Rápida, disponible en
nuestros <uri link="/doc/es/index.xml">Recursos de Documentación</uri>
si su arquitectura tiene este documento disponible.
</p>

<p>
También existen otras posibilidades: puede compilar el sistema completo
desde el principio o utilizar un entorno pre-compilado para tener el
sistema listo en poco tiempo. Y, por supuesto, también hay soluciones
intermedias con las cuales no se compila todo el sistema pero se empieza
desde un sistema bastante completo.
</p>

</body>
</subsection>
<subsection>
<title>¿Problemas?</title>
<body>

<p>
Si tiene algún problema con la instalación (o con el documento
de instalación), por favor, visite nuestro
<uri link="http://bugs.gentoo.org"> Sistema de seguimiento
de errores</uri> y compruebe si el error es conocido. Si no lo es, por
favor cree un informe sobre él para que podamos encargarnos de él. No
tenga miedo de los desarrolladores que están asignados a sus
informes, normalmente no se comen a nadie.
</p>

<p>
Acuérdese que, a pesar de que el documento que está leyendo es
específico de la arquitectura, esté también contiene referencias a
otras arquitecturas. Esto es así porque el manual de Gentoo tiene
partes extensas de código que son comunes para todas las arquitecturas
(para evitar duplicar esfuerzos y el desgaste de los recursos de
desarrollo). Intentaremos reducir esto al mínimo para evitar la
confusión.
</p>

<p>
Si no está seguro que el problema es de usuario (algún error que
haya cometido al despistarse y no leer la documentación
cuidadosamente) o un problema de software (algún error que ha
cometido despistándose al probar la instalación y/o documentación),
es libre de entrar en #gentoo-es en irc.freenode.net. Por supuesto,
es bienvenido de todas formas :)
</p>

<p>
Si tiene cualquier pregunta concerniente a Gentoo, eche un vistazo a
las <uri link="/doc/es/faq.xml">Preguntas de Uso Frecuente</uri>,
disponibles en la <uri link="/doc/es/">Documentación de Gentoo</uri>.
También puede mirar los <uri
link="http://forums.gentoo.org/viewforum.php?f=40">FAQs</uri> en nuestros
<uri link="http://forums.gentoo.org">foros</uri>. Si no encuentras la
respuesta aquí, pregunta en #gentoo-es, nuestro canal IRC en
irc.freenode.net. Sí, algunos de nosotros somos frikis que aún usan el
IRC :-)
</p>

</body>
</subsection>
</section>
</sections>
