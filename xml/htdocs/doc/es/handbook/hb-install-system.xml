<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE sections SYSTEM "/dtd/book.dtd">

<!-- $Header: /var/cvsroot/gentoo/xml/htdocs/doc/es/handbook/hb-install-system.xml,v 1.45 2010/06/14 15:10:02 nimiux Exp $ -->

<sections>

<abstract>
Luego de instalar y configurar el stage3, el resultado eventual es que
tiene un sistema base Gentoo a su disposición. Este capítulo describe
cómo progresar hacia este estado.
</abstract>

<version>10.4</version>
<date>2010-06-13</date>

<section>
<title>Chrooting</title>
<subsection>
<title>Opcional: Seleccionando servidores réplica</title>
<body>

<p>
Para poder descargar el código fuente rápidamente se recomienda
seleccionar un servidor réplica rápido. Portage comprobará en su
archivo <path>make.conf</path> la variable GENTOO_MIRRORS y utilizará
los servidores que se especifican allí. Puede navegar en nuestra <uri
link="/main/en/mirrors.xml">lista de réplicas</uri> y buscar un
servidor (o servidores) que estén cerca de su localización (ya que
estos suelen resultar los más rápidos), pero no nosotros le
facilitamos una bonita herramienta llamada <c>mirrorselect</c> la cual
proporciona una interfaz amigable para seleccionar los servidores
réplicas que quiera.
</p>

<pre caption="Utilizar mirrorselect para la variable GENTOO_MIRRORS">
# <i>mirrorselect -i -o &gt;&gt; /mnt/gentoo/etc/make.conf</i>
</pre>

<warn test="func:keyval('arch')='PPC64'">
No seleccione ningún servidor con IPv6. Nuestros stages no soportan
actualmente IPv6.
</warn>

<p>
Otra importante configuración es la variable SYNC en <path>make.conf</path>.
Esta variable contiene el servidor rsync que quiere utilizar para actualizar
su árbol Portage (la colección de ebuilds, scripts que contienen toda la
información que Portage necesita para descargar e instalar software). Aunque
puede introducir manualmente el servidor SYNC, <c>mirrorselect</c> puede
encargarse también de esto:
</p>

<pre caption="Seleccionar un servidor rsync utilizando mirrorselect">
# <i>mirrorselect -i -r -o &gt;&gt; /mnt/gentoo/etc/make.conf</i>
</pre>

<p>
Después de ejecutar <c>mirrorselect</c> es recomendable que compruebe sus
configuraciones en <path>/mnt/gentoo/etc/make.conf</path>.
</p>

<note>
Si desea seleccionar un servidor SYNC manualmente en
<path>make.conf</path>, revise la <uri
link="/main/en/mirrors-rsync.xml">lista de servidores espejo
comunitarios</uri> para averiguar el servidor más
cercano. Recomendamos escoger uno de <e>rotación</e>, como
<c>rsync.us.gentoo.org</c>, en vez de uno individual. Esto ayuda a
repartir la carga y agrega seguridad en caso que el servidor espejo
individual esté fuera de línea.
</note>
</body>
</subsection>

<subsection>
<title>Copiar la información DNS</title>
<body>

<p>
Aún queda una cosa que hacer antes de entrar en el nuevo entorno,
copiar la información sobre los DNS en <path>/etc/resolv.conf</path>.
Necesita hacer esto para asegurarse de que la red continúe funcionando
después de entrar en el nuevo entorno. <path>/etc/resolv.conf</path>
contiene los servidores de nombres para su red.
</p>

<pre caption="Copiar la información de DNS">
<comment>(La opción "-L" es necesaria para asegurarnos que no copiamos un
enlace simbólico)</comment>
# <i>cp -L /etc/resolv.conf /mnt/gentoo/etc/</i>
</pre>
</body>
</subsection>

<subsection test="not(func:keyval('arch')='IA64')">
<title>Montar los sistema de archivos /proc y /dev</title>
<body>

<p>
Monte el sistema de ficheros <path>/proc</path> en
<path>/mnt/gentoo/proc</path> para permitir a la instalación
utilizar la información proporcionada por el kernel incluso dentro
del entorno chroot y, posteriormente, montar (de forma transparente) el
sistema de archivos <path>/dev</path>.
</p>

<pre caption="Montar /proc">
# <i>mount -t proc none /mnt/gentoo/proc</i>
# <i>mount -o bind /dev /mnt/gentoo/dev</i>
</pre>
</body>
</subsection>

<subsection test="func:keyval('arch')='IA64'">
<title>Montar los sistemas de archivos /proc, /sys y /dev</title>
<body>

<p>
Monte el sistema de ficheros <path>/proc</path> en
<path>/mnt/gentoo/proc</path> para permitir a la instalación
utilizar la información proporcionada por el kernel incluso dentro
del entorno chroot y, posteriormente, montar (de forma transparente) los
sistemas de archivos <path>/dev</path> y <path>/sys</path>.
</p>

<pre caption="Montar /proc /sys y /dev">
# <i>mount -t proc none /mnt/gentoo/proc</i>
# <i>mount -o bind /dev /mnt/gentoo/dev</i>
# <i>mount -o bind /sys /mnt/gentoo/sys</i>
</pre>
</body>
</subsection>

<subsection>
<title>Entrando al nuevo entorno</title>
<body>

<p>
Ahora que todas las particiones están inicializadas y el sistema base
instalado, es hora de entrar en nuestro nuevo entorno de instalación
<e>chrooting</e>. Esto significa pasar desde el actual
entorno de instalación (CD de instalación u otro medio) hacia tu
entorno de instalación (o sea, las particiones inicializadas).
</p>

<p>
El enjaulamiento se hace en tres pasos. Primero cambiamos la raíz
desde <path>/</path> (en el medio de instalación) a <path>/mnt/gentoo</path>
(en tus particiones) usando <c>chroot</c>. Después crearemos un nuevo entorno
usando <c>env-update</c>, el cual, en esencia crea las variables de entorno.
Finalmente, cargamos esas variables en memoria tecleando <c>source</c>.
</p>

<pre caption = "Entrar al nuevo entorno">
# <i>chroot /mnt/gentoo /bin/bash</i>
# <i>env-update</i>
>> Regenerating /etc/ld.so.cache...
# <i>source /etc/profile</i>
# <i>export PS1="(chroot) $PS1"</i>
</pre>

<p>
¡Enhorabuena! Estás dentro de tu nuevo entorno Gentoo Linux.
Por supuesto aún no hemos terminado, todavía quedan unas cuantas
secciones ;)
</p>
</body>
</subsection>
</section>

<section>
<title>Configurar Portage</title>
<subsection>
<title>Opcional: Actualizando el árbol Portage</title>
<body>

<p>
Debería actualizar ahora su árbol Portage a la última versión.
<c>emerge --sync</c> hará esto por nosotros.
</p>

<pre caption="Actualizar el árbol Portage">
# <i>emerge --sync</i>
<comment>(Si está utilizando un terminal lento como algunos framebuffers
o consolas seriales, puede añadir la opción --quiet para aumentar la
velocidad del proceso:)</comment>
# <i>emerge --sync --quiet</i>
</pre>

<p>
Si está detrás de un cortafuegos que bloquea el tráfico rsync, puede
utilizar <c>emerge-webrsync</c> el cual descargará e instalará una
imagen de Portage para su sistema.
</p>

<p>
Si ha advertido que está disponible una nueva versión de Portage y se debe
actualizar, debería hacerlo ejecutando <c>emerge --oneshot portage</c>.
</p>
</body>
</subsection>

<subsection>
<title>Eligiendo el perfil adecuado</title>
<body>

<p>
Primero, una pequeña definición:
</p>

<p>
Un perfil es la piedra inicial de cualquier sistema Gentoo. No solamente
especifica unos valores predeterminados para USE, CFLAGS, y otras
variables importantes, también bloquea del sistema ciertos rangos de
versiones de algunos paquetes. Son mantenidos por los desarrolladores de
Gentoo.
</p>

<p>
Tiempo atrás, los perfiles raramente los tocaba el usuario. Sin
embargo, puede haber situaciones en las cuales sea necesaria un cambio
de perfil.
</p>

<p>
Se puede ver el perfil actualmente utilizado con el siguiente comando:
</p>

<pre caption="Comprobar el perfil del sistema">
# <i>eselect profile list</i>
Available profile symlink targets:
  [1]   <keyval id="profile"/> *
  [2]   <keyval id="profile"/>/desktop
  [3]   <keyval id="profile"/>/server
</pre>

<p>
El perfil por defecto proporciona un sistema basado en Linux 2.6. Este
es el que recomendamos, pero también existe la posibilidad de elegir
otro.
</p>

<p>
También existen sub-perfiles <c>desktop</c> (escritorio) y
<c>server</c> (servidor) para algunas arquitecturas. Ejecutando
<c>eselect profile list</c> mostrará los perfiles disponibles.
</p>

<p>
Después de revisar los perfiles disponibles para su arquitectura,
puede utilizar uno diferente si lo desea:
</p>

<pre caption="Cambiar de perfil">
# <i>eselect profile set 2</i>
</pre>

<p test="func:keyval('arch')='AMD64'">
Si quiere disponer de un entorno puramente de 64 bits puros, sin
aplicaciones de 32 bits ni bibliotecas, debería utilizar un perfil no
multilib:
</p>

<pre test="func:keyval('arch')='AMD64'" caption="Cambiar a un perfil no multilib">
# <i>eselect profile list</i>
Available profile symlink targets:
  [1]   <keyval id="profile"/> *
  [2]   <keyval id="profile"/>/desktop
  [3]   <keyval id="profile"/>/no-multilib
  [4]   <keyval id="profile"/>/server
<comment>(Choose the no-multilib profile)</comment>
# <i>eselect profile set 3</i>
<comment>(Verify the change)</comment>
# <i>eselect profile list</i>
Available profile symlink targets:
  [1]   <keyval id="profile"/>
  [2]   <keyval id="profile"/>/desktop
  [3]   <keyval id="profile"/>/no-multilib *
  [4]   <keyval id="profile"/>/server
</pre>

<note>
El sub-perfil <c>developer</c> existe específicamente para labores de
desarrollo Gentoo Linux. <e>No</e> está supuesto de servir para ayudar
a establecer entornos generales de desarrollo.
</note>
</body>
</subsection>

<subsection id="configure_USE">
<title>Configurando la variable USE</title>
<body>

<p>
La variable <c>USE</c> es una de las más importantes que Gentoo
proporciona a sus usuarios. Muchos programas pueden ser compilados con
o sin soporte opcional para ciertas cosas. Por ejemplo, algunos
programas pueden ser compilados con soporte gtk, o con soporte
qt. Otros programas pueden ser compilados con o sin soporte
SSL. Algunos programas pueden ser compilados con soporte framebuffer
(svgalib) en lugar de soporte X11 (servidor X).
</p>

<p>
Muchas distribuciones compilan sus paquetes con el mayor soporte posible,
aumentando el tamaño de los programas y su tiempo de carga, sin mencionar
una cantidad enorme de dependencias. Con Gentoo puedes definir con que
opciones debe ser compilado un paquete. Ahí es donde actúa la variable
<c>USE</c>.
</p>

<p>
En la variable <c>USE</c> definimos palabras clave que son
transformadas a opciones de compilación. Por ejemplo <e>ssl</e>
compilará los programas que lo requieran con soporte ssl.<e>-X</e>
quitara el soporte para el servidor X (nótese el signo menos
delante). <e>gnome gtk -kde -qt4</e> compilará tus programas con
soporte para gnome (y gtk), pero sin soporte para kde (y qt), haciendo
tu sistema completamente compatible con GNOME.
</p>

<p>
Los valores por defecto de la variable <c>USE</c> se encuentran en
<path>make.defaults</path>, archivos de su perfil. Encontrará los archivos
<path>make.defaults</path> en el directorio al cual apunte
<path>/etc/make.profile</path> y todos sus directorios padres. El valor
predeterminado de configuración de la variable <c>USE</c> es la suma de todas
las configuraciones de <c>USE</c> en todos los archivos
<path>make.defaults</path>. Lo que modifique en <path>/etc/make.conf</path> se
calcula contra estos valores. Si pone algún valor en su <c>USE</c>, es añadido
a la lista por defecto. Si elimina algo en su variable <c>USE</c>, poniéndole
un signo menos delante, es eliminado de la lista por defecto (si estaba en
ella claro). <e>Nunca</e> cambie nada en <path>/etc/make.profile</path> ya que
¡se sobreescribirá cuando actualice Portage!
</p>

<p>
Puede encontrar una descripción completa sobre la variable <c>USE</c>
en la segunda parte del Manual de
Gentoo <uri link="?part=2&amp;chap=1">Capítulo 1: Variables
USE</uri>. Encontrará una descripción más extensa sobre las opciones
de la variable USE en su sistema, en
<path>/usr/portage/profiles/use.desc</path>.
</p>

<pre caption="Ver las opciones disponibles">
# <i>less /usr/portage/profiles/use.desc</i>
<comment>(Puede desplazarse arriba y abajo utilizando sus teclas de flechas y
salir pulsando 'q')</comment>
</pre>

<p>
Como ejemplo, te mostramos unas opciones <c>USE</c> para un sistema basado
en KDE con DVD, ALSA y soporte para grabar CD's.
</p>

<pre caption="Abrir /etc/make.conf">
# <i>nano -w /etc/make.conf</i>
</pre>

<pre caption="Configurar la variable USE">
USE="-gtk -gnome qt4 kde dvd alsa cdr"
</pre>
</body>
</subsection>

<subsection>
<title>Opcional: locales para glibc</title>
<body>

<p>
Probablemente querrá utilizar solamente una o dos configuraciones locales
en su sistema. Se puede especificar las locales que se necesitaran en
<path>/etc/locale.gen</path>.
</p>

<pre caption="Abrir /etc/locale.gen">
# <i>nano -w /etc/locale.gen</i>
</pre>

<p>
Las siguientes "locales" son un ejemplo para tener, al mismo tiempo, los
idiomas: inglés (Estados Unidos) y español (España) con sus respectivos
formatos (por ejemplo, UTF-8).
</p>

<pre caption="Especificar sus locales">
en_US ISO-8859-1
en_US.UTF-8 UTF-8
es_ES ISO-8859-15
es_ES@euro ISO-8859-15
</pre>

<p>
El siguiente paso es ejecutar <c>locale-gen</c>. Generará todas las locales
que tenemos especificadas en <path>/etc/locale.gen</path>.
</p>

<p>
Ahora continúe con <uri
link="?part=1&amp;chap=7">Configurando el Kernel</uri>.
</p>
</body>
</subsection>
</section>
</sections>
