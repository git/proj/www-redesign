<?xml version='1.0' encoding="UTF-8"?>
<!DOCTYPE guide SYSTEM "/dtd/guide.dtd">
<!-- $Header: /var/cvsroot/gentoo/xml/htdocs/doc/es/gentoo-x86-tipsntricks.xml,v 1.9 2007/10/06 14:59:15 chiguire Exp $ -->

<guide link="/doc/es/gentoo-x86-tipsntricks.xml" lang="es">
<title>Consejos y trucos en la instalación de Gentoo/x86</title>

<author title="Autor">
  <mail link="swift@gentoo.org">Sven Vermeulen</mail>
</author>
<author title="Editor">
  <mail link="neysx@gentoo.org">Xavier Neys</mail>
</author>
<author title="Traductor">
  <mail link="bass@gentoo.org">José Alberto Suárez López</mail>
</author>
<author title="Traductor">
  <mail link="chiguire@gentoo.org">John Christian Stoddart</mail>
</author>
<author title="Traductor">
  <mail link="nmiyasato@datafull.com">Nicolás Miyasato</mail>
</author>

<abstract>
Las diferentes formas de instalar Gentoo son muy flexibles de
realizar.  Como es casi imposible insertar cada consejo o truco
existente en las instrucciones de instalación, este documento tratará
de brindar todos los consejos y trucos que han sido recibidos de
manera de que se tengan como referencia.
</abstract>

<!-- The content of this document is licensed under the CC-BY-SA license -->
<!-- See http://creativecommons.org/licenses/by-sa/2.5 -->
<license/>

<version>1.15</version>
<date>2007-09-18</date>

<chapter>
<title>Introducción</title>
<section>
<title>Preliminar</title>
<body>

<p>
Este documento contiene varios consejos y trucos para la instalación
de Gentoo en la arquitectura x86. La mayoría de ellos son discutidos
en profundidad , la idea es que sirvan como un complemento a las
instrucciones de instalación y NO como un reemplazo.
</p>
</body>
</section>

<section>
<title>Contenido</title>
<body>

<p>
<b>Instalaciones avanzadas</b>
</p>

<ul>
  <li><uri link="#software-raid">Software RAID</uri></li>
  <li><uri link="#ata-raid-2.4">ATA RAID usando núcleos 2.4</uri></li>
  <li><uri link="#livecd-kernel">Utilizando el núcleo del CD de Instalación</uri></li>
</ul>

<p>
<b>Simplificando la instalación</b>
</p>

<ul>
  <li><uri link="#leave_terminal">Dejando la terminal</uri></li>
</ul>

<p>
<b>Arreglando errores/inconvenientes</b>
</p>

<ul>
  <li>
    <uri link="#checking-disks">Verificando extensivamente sus discos</uri>
  </li>
  <li>
    <uri
    link="#recover">Recuperación de una instalación en mal funcionamiento</uri>
  </li>
</ul>
</body>
</section>
</chapter>

<chapter>
<title>Instalaciones avanzadas</title>
<section id="software-raid">
<title>Software RAID</title>
<body>

<note>
Si no conoce lo que es software raid, por favor lea el siguiente COMO:
<uri
link="http://tldp.org/HOWTO/Software-RAID-HOWTO.html">Software-RAID-HOWTO</uri>.
</note>

<note>
Un procedimiento más detallado se encuentra en
nuestra <uri link="/doc/en/gentoo-x86+raid+lvm2-quickinstall.xml">Guía
de Instalación Rápida con Software Raid y LVM2 para x86</uri> (en
inglés).
</note>

<p>
Una vez que haya iniciado desde el CD de Instalación, cargue los
correspondientes módulos del RAID. Por ejemplo, si planea usar RAID-1:
</p>

<pre caption="Cargando el módulo de RAID-1">
# <i>modprobe raid1</i>
</pre>

<p>
Cuando particione sus discos, en vez de utilizar <c>83</c> (Linux
native) como tipo de partición, asegúrese de que sus particiones
utilicen <c>fd</c> (Linux raid autodetect). Luego puede cambiar el
tipo de partición mediante el comando <c>t</c> del <c>fdisk</c>
</p>

<p>
Ahora, antes de empezar a crear los "RAID arrays", necesitamos crear
los nodos metadispositivos:
</p>

<pre caption="Creando los nodos metadispositivos">
# <i>mknod /dev/md1 b 9 1</i>
# <i>mknod /dev/md2 b 9 2</i>
# <i>mknod /dev/md3 b 9 3</i>
</pre>

<p>
Luego del particionamiento, debe crear el archivo
<path>/etc/mdadm.conf</path> (sí, en el entorno del CD de Instalación)
usando
<c>mdadm</c>, una herramienta avanzada para
<uri link="http://www.linuxdevcenter.com/pub/a/linux/2002/12/05/RAID.html">
administrar el RAID</uri>. Por ejemplo, para tener la particion boot, swap y
root espejados (RAID-1) utilizando <path>/dev/sda</path> y <path>/dev/sdb
</path>, puede ejecutar lo siguiente:
</p>

<pre caption="Creando los dispositivos raid con el comando mdadm">
# <i>mdadm --create --verbose /dev/md1 --level=1 --raid-devices=2 /dev/sda1 /dev/sdb1</i>
# <i>mdadm --create --verbose /dev/md2 --level=1 --raid-devices=2 /dev/sda2 /dev/sdb2</i>
# <i>mdadm --create --verbose /dev/md3 --level=1 --raid-devices=2 /dev/sda3 /dev/sdb3</i>
</pre>

<impo>
No debería utilizar cualquier forma de "striping" tales como raid-0 o
raid-5 en la partición donde realiza el arranque.
</impo>

<p>
El driver del Linux Software RAID empezará a crear los
metadispositivos. Puede ver su progreso
en <path>/proc/mdstat</path>. Antes de proceder, espere hasta que los
metadispositivos hayan sido terminados.
</p>

<pre caption="Guardando la información de los dispositivos creados">
# <i>mdadm --detail --scan > /etc/mdadm.conf</i>
</pre>

<p>
De ahora en adelante, utilizaremos <path>/dev/md1</path> para la
partición boot,
<path>/dev/md2</path> para la partición de intercambio y
<path>/dev/md3</path> para la partición raíz.
</p>

<p>
Antes de realizar el enjaulado (chrooting), no olvide copiar
<path>/etc/mdadm.conf</path> a <path>/mnt/gentoo/etc</path>.
</p>

<p>
Cuando configure el núcleo, asegúrese de tener el soporte apropiado
para el RAID <e>dentro</e> de su núcleo y NO como un módulo.
</p>

<p>
Cuando instale las herramientas extra, también instale <c>mdadm</c>
vía emerge.  Observe que este no se encuentra disponible en todos los
CDs de Instalación, de manera que no podrá instalar Gentoo en un
Software RAID cuando esté realizando una instalación sin conexión a
Internet.
</p>

<p>
Cuando configure el gestor de arranque (LILO o GRUB), en caso de usar
un espejado, cerciórese de que se instale en <e>ambos</e> discos.
</p>
</body>
</section>

<section id="ata-raid-2.4">
<title>ATA RAID utilizando núcleos 2.4</title>
<body>

<p>
Asegúrese de reiniciar el sistema con su CD de Instalación utilizando
la opción
<c>doataraid</c>. Una vez que lo haya hecho, verifique los contenidos de
<path>/dev/ataraid</path>. Debería de contener varios directorios
<path>disc*</path> para cada disco duro disponible en el ATA RAID.
Un disco completo es mostrado como <path>disc</path> mientras que las
particiones son mostradas como <path>part*</path>.
</p>

<p>
Tome nota de las rutas de los diferentes archivos de dispositivos
(<path>/dev/ataraid/disc*/*</path>) que utilice para instalar Gentoo.
Necesitará reemplazar la línea <path>/dev/hda</path> en los ejemplos
de la instalación con dicha ruta.
</p>

<p>
Antes de realizar el chroot, debe montar el <path>/dev</path> con la
opción "bind" en el nuevo entorno:
</p>

<pre caption="Realizando el mount de /dev con la opción de bind">
# <i>mount -o bind /dev /mnt/gentoo/dev</i>
</pre>

<p>
Cuando esté configurando su núcleo, asegúrese de habilitar el soporte
para su sistema ATA RAID con sus respectivas opciones. Por ejemplo, un
sistema popular es el <e>Promise FastTrack built-in RAID</e>, en cuyo
caso definitivamente necesitará que las opciones para la <c>Promise
FastTrack</c> estén compiladas dentro de su núcleo, y no como módulo.
</p>

<p>
Cuando esté configurando el GRUB, primero tiene que crea el disco de
inicio del GRUB. Esto no es tan complicado como parece. Primero debe
instalar GRUB como siempre, pero cuando viene la parte en donde GRUB
es instalado en el MBR, siga estas instrucciones:
</p>

<pre caption="Creando un disco de inicio del GRUB">
# <i>cd /boot/grub</i>
# <i>dd if=stage1 of=/dev/fd0 bs=512 count=1</i>
# <i>dd if=stage2 of=/dev/fd0 bs=512 seek=1</i>
</pre>

<p>
Todavía necesita escribir el archivo <path>grub.conf</path>. Esto no
es diferente al de las instrucciones de instalación, solamente
asegúrese de que
<c>root=</c> apunte al dispositivo ATA RAID.
</p>

<p>
Luego de terminar la instalación, debe reiniciar el sistema utilizando
el disco de inicio del GRUB. Será bienvenido por la línea de comandos
del GRUB.  Ahora debe de configurar el GRUB para que inicie desde el
dispositivo ATA RAID.
</p>

<pre caption="Instalando GRUB en el ATA RAID">
grub&gt; root (hd0,x)
grub&gt; setup (hd0)
grub&gt; quit
</pre>

<p>
Ahora debe reiniciar (sin el disco de inicio del GRUB)
</p>

<p>
Los usuarios de LILO pueden seguir las instrucciones de instalación
sin modificación alguna.
</p>
</body>
</section>

<section id="livecd-kernel">
<title>Utilizando el núcleo del CD de Instalación</title>
<body>

<p>
Puede usar el núcleo del CD de Instalación si no quiere compilar el
núcleo Ud.  mismo y luego copiarlo a su sistema. Cuando llega al punto
en donde se le sugiere que compile el núcleo, cambie de terminal
(presione Alt-F2) y autentíquese utilizando la contraseña
correspondiente al superusuario.
</p>

<p>
Copia el núcleo y sus módulos a su sistema Gentoo:
</p>

<pre caption="Copiando el núcleo del CD de Instalación">
<comment>(${KN} es el nombre del núcleo, normalmente es algo como 'gentoo' o 'smp')</comment>
cdimage ~# <i>cp /mnt/cdrom/isolinux/${KN}
/mnt/cdrom/isolinux/${KN}.igz /mnt/gentoo/boot</i>
cdimage ~# <i>mkdir -p /mnt/gentoo/lib/modules</i>
cdiamge ~# <i>cp -Rp /lib/modules/`uname -r` /mnt/gentoo/lib/modules</i>
</pre>

<p>
Para obtener todos los módulos usados actualmente (del CD de
Instalación) por el sistema, ejecute el siguiente comando dentro del
entorno enjaulado:
</p>

<pre caption="Agregando todos los módulos que se cargaron al archivo modules.conf">
# <i>cat /proc/modules | cut -d ' ' -f 1 &gt;&gt; \</i>
  <i>/etc/modules.autoload.d/kernel-`uname -r | cut -d . -f -2`</i>
# <i>update-modules</i>
</pre>
</body>
</section>
</chapter>

<chapter>
<title>Simplificando la instalación</title>
<section id="leave_terminal">
<title>Dejando la terminal</title>
<body>

<p>
Muchas personas quieren dejar su sistema cuando está compilando. En
algunos casos esto es un poco difícil ya que la instalación podría
estar siendo hecha en un entorno público en donde no puede confiar en
nadie. Si este es el caso, seguramente quiera ser capaz de realizar la
compilación como un proceso que se ejecuta en el "background" y
salirse de todas las terminales.
</p>

<p>
Para esto hay muchas soluciones posibles. El primero de ellos es
utilizar el <c>screen</c>. Luego de reiniciar utilizando el CD de
Instalación, configure su contraseña del root y comience una sesión
del screen:
</p>

<note>
No todos los CDs de Instalación tienen el screen. Si este es el caso,
tendrá que usar algún otro método descrito en esta sección.
</note>

<pre caption="Comenzando una sesión de screen ">
# <i>screen -S gentoo</i>
</pre>

<p>
Una vez dentro de la sesión del screen puede realizar la instalación
entera. Cuando quiera dejar la terminal, simplemente presione
<c>Ctrl-a,d</c> (es decir, control y 'a' al mismo tiempo, y luego la
tecla 'd') para <e>despegar</e> tu sesión del screen. Ahora tu puede
tranquilamente salirse del sistema.
</p>

<p>
Para obtener acceso a su terminal nuevamente autentíquese como root
y <e>reenganche</e> (attach) la sesión de screen en ejecución:

</p>

<pre caption="Re-conectándose a la sesión de screen">
# <i>screen -x gentoo</i>
</pre>

<p>
Si no puede usar screen, hay otra manera para dejar su terminal. Sigue
las instrucciones de instalación, pero cuando llegue al punto en donde
se inicie una larga compilación (por ejemplo el paso
de <c>./scripts/bootstrap.sh</c>), utilice <c>nohup</c>, que permite
que un proceso continúe incluso cuando se sale del sistema. ¡No se
olvide seguir el comando con el "&amp;", de otra manera el proceso no
será puesto en el "background"! Recuerde donde se encuentra ya que lo
necesitará saber más adelante (el comando <c>pwd</c> le mostrará el
directorio actual).
</p>

<pre caption="Utilizando nohup">
# <i>pwd</i>
/usr/portage
# <i>nohup ./scripts/bootstrap.sh &amp;</i>
</pre>

<p>
Ahora debe salir del entorno en donde ha realizado el "chroot"
(utilice el comando <c>exit</c>) así también de la sesión del CD de
Instalación. Su compilación continuará en segundo plano.
</p>

<p>
Cuando quiera volver para verificar la compilación, autentíquese como
root (en el CD de Instalación) y realice nuevamente un "chroot" en el
entorno correspondiente. Luego tiene que ir al directorio que has
anotado anteriormente.
</p>

<pre caption="Volviendo a realizar la jaula">
# <i>chroot /mnt/gentoo /bin/bash</i>
# <i>env-update &amp;&amp; source /etc/profile</i>
# <i>cd /usr/portage</i>
</pre>

<p>
Ahora utilice el comando <c>less</c> en el
archivo <path>nohup.out</path> que se encuentra situado en ese
directorio. La compilación agregará su salida a ese archivo, así que
si quiere seguir el progreso de la compilación, ejecute
<c>less nohup.out</c> y presione <c>F</c> para seguir los cambios. Cuando la
compilación haya terminado, puede continuar con el siguiente paso de
instalación.
</p>

<p>
Si se cansa de seguir los cambios, presione <c>Ctrl-C</c> seguido de
un <c>q</c>. Esto no afectará al proceso de compilación, solamente al
proceso de
<c>less</c>.
</p>
</body>
</section>
</chapter>

<chapter>
<title>Arreglando errores/inconvenientes</title>
<section id="checking-disks">
<title>Verificando extensivamente tus discos</title>
<body>

<p>
Si cree que su disco necesita ser verificado para comproboar su
consistencia (sectores malos y esas cosas), puede usar la opción
<c>-c</c> cuando esté formateando ext2 o ext3 (usando
<c>mke2fs</c>). Esto formateará, producirá una prueba de lectura y
marcará todos los sectores malos y demás. Si realmente es paranoico
con respecto al tema, utilice la opción <c>-c -c</c> al formatear el
disco y se realizará una prueba intensiva de lectura y escritura.
</p>

<pre caption="Verificando la consistencia del disco">
# <i>mke2fs -j -c /dev/hda3</i>
</pre>
</body>
</section>

<section id="recover">
<title>Recuperándose de una instalación en mal funcionamiento</title>
<body>

<p>
Si por alguna razón su instalación de Gentoo ha fallado, no tiene que
rehacer toda la instalación nuevamente. En vez de eso, tranquilamente
puede "ir" al punto en donde cree que ha cometido el error (o a donde
cree que las instrucciones no son correctas) y tratar de realizarlo
nuevamente pero de alguna otra manera.
</p>

<p>
Primero debe realizar una jaula (chroot) a tu entorno de Gentoo
Linux. Siga las instrucciones nuevamente, pero ignore los pasos de
particionamiento, ya que sus particiones han sido creadas e incluso
utilizadas. Por lo tanto, inmediatamente puede montar aquellas
particiones en <path>/mnt/gentoo</path>. También debería ignorar los
pasos acerca de la extracción del archivo comprimido que contiene la
fase (stage), y también saltar la etapa en donde modifica
el <path>make.conf</path>. No quiere sobreescribir sus archivos, ¿o
sí?
</p>

<p>
Una vez hecho la jaula en su entorno Gentoo Linux, vaya inmediatamente
al paso en donde crea que debería tratar de realizarlo de otra
manera. No haga nuevamente todos los pasos como el "bootstrap", a
menos que ese sea el lugar en donde las cosas fallaron.
</p>

<p>
Por ejemplo, si cree que ha configurado erróneamente el archivo
<path>grub.conf</path>, inmediatamente puedes lanzar su editor para
actualizar <path>/boot/grub/grub.conf</path>.
</p>

<p>
Una vez que haya corregido el error, debería considerar cuántos de los
pasos siguientes debería de realizar nuevamente. Si los siguientes
pasos son dependientes a su cambio, necesitará realizarlos nuevamente.
</p>

<p>
Por ejemplo,
</p>

<ul>
  <li>
    Si ha cambiado una variable dentro del <path>make.conf</path>
    necesitará realizar nuevamente los pasos que requieran una
    compilación, ya que ellos dependen directamente de la
    configuración del archivo
<path>make.conf</path>
  </li>
  <li>
   Si ha cambiado el archivo <path>/boot/grub/grub.conf</path>,
   inmediatamente puede salir del entorno en donde ha realizado la
   jaula y reiniciar, ya que no hay pasos siguientes que dependan del
   archivo <path>grub.conf</path>.
  </li>
  <li>
   Si ha recompilado el núcleo, entonces solamente necesitará asegurarse de
que la configuración de su gestor de arranque esté apuntando a la imagen del
núcleo correcto (¡Verifique dos veces (o más) que ha montado el
<path>/boot</path>!), luego puede salir del entorno enjaulado y reiniciar.
  </li>
  <li>
  Si ha modificado el archivo <path>/etc/fstab</path> puedes sali del
  entorno en donde ha realizado la jaula y reiniciar.
  </li>
</ul>

<p>
Como puede ver, para la mayoría de las operaciones de recuperación
puede reiniciar inmediatamente. Solamente en ciertos casos necesitará
realizar algunos pasos siguientes de la instalación.
</p>
</body>
</section>
</chapter>
</guide>
