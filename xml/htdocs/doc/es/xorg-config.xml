<?xml version='1.0' encoding='UTF-8'?>

<!-- $Header: /var/cvsroot/gentoo/xml/htdocs/doc/es/xorg-config.xml,v 1.25 2010/05/28 22:03:23 nimiux Exp $ -->

<!DOCTYPE guide SYSTEM "/dtd/guide.dtd">

<guide lang="es">

<title>Guía de configuración del Servidor X</title>

<author title="Autor">
  <mail link="swift"/>
</author>
<author title="Autor">
  <mail link="nightmorph"/>
</author>
<author title="Traductor">
  <mail link="chiguire"/>
</author>
<author title="Traductor">
  <mail link="yoswink@gentoo.org">José Luis Rivero</mail>
</author>
<author title="Traductor">
  <mail link="enrique@barbeito.org">Enrique Barbeito García</mail>
</author>
<author title="Traductor">
  <mail link="nimiux"/>
</author>

<abstract>
Xorg es el servidor de ventanas X que permite a los usuarios disponer
de un entorno gráfico a su alcance inmediato. Está guía de
configuración explica qué es Xorg, cómo instalarlo y cuáles son las
distintas opciones de configuración.
</abstract>

<!-- The content of this document is licensed under the CC-BY-SA license -->
<!-- See http://creativecommons.org/licenses/by-sa/2.5 -->
<license/>

<version>1.29</version>
<date>2010-05-23</date>

<chapter>
<title>¿Qué es el Servidor de Ventanas X?</title>
<section>
<title>Entorno Gráfico vs Línea de Comandos</title>
<body>

<p>
La usuario medio puede tener miedo al pensar que va a tener que
introducir comandos. ¿Por qué no iba a ser capaz de comenzar su camino
a través de la libertad que proporciona Gentoo (y Linux en general)?
Bien, *gran sonrisa*, por supuesto que eres capaz de esto :-) Linux
ofrece una amplia variedad de llamativos interfaces de usuarios y
entornos los cuales pueden ser instalados sobre su instalación actual.
</p>

<p>
Esta es una de las mayores sorpresas que se llevan los nuevos
usuarios: un interfaz gráfico de usuario no es nada más que una
aplicación ejecutándose sobre tu sistema. <e>No</e> es parte del
núcleo de Linux o de nada interno al sistema. Es una potente
herramienta que activa completamente las habilidades gráficas de tu
sistema.
</p>

<p>
Puesto que los estándares son importantes, un estándar para dibujar y
mover ventanas en una pantalla, interactuando con el usuario a través
del ratón, el teclado y otros aspectos básicos también importantes han
sido creados y denominados <e>Sistema de Ventanas X (X Window
System)</e>, abreviado normalmente como <e>X11</e> o solamente
<e>X</e>. Es utilizado en Unix, Linux y Sistemas operativos basados en
Unix a lo largo de todo el mundo.
</p>

<p>
La aplicación que proporciona a los usuarios de Linux la posibilidad
de ejecutar interfaces gráficos de usuario y que utiliza el estándar
X11 es Xorg-X11, una bifurcación del proyecto XFree86. XFree86 ha
decidido utilizar una licencia que puede no ser compatible con la
licencia GPL, por tanto, se recomienda el empleo de Xorg. El árbol
oficial de Portage no proporcionará el paquete de XFree86 nunca más.
</p>
</body>
</section>

<section>
<title>El proyecto X.org</title>
<body>

<p>
El proyecto <uri link="http://www.x.org">X.org</uri> creó y mantiene
una implementación de código abierto, libre y redistribuible del
sistema X11. Es una infraestructura de escritorio basada en X11 de
código abierto.
</p>

<p>
Xorg proporciona una interfaz entre tu hardware y el software gráfico
que quieras ejecutar. Además, Xorg esta completamente preparado para
el trabajo en red, esto significa que es capaz de ejecutar una
aplicación en un sistema mientras la esta viendo en otro sistema
diferente.
</p>
</body>
</section>
</chapter>

<chapter>
<title>Instalando Xorg</title>
<section>
<title>Configuración del Núcleo</title>
<body>

<p>
Por defecto, Xorg usa <c>evdev</c>, un controlador de entrada
genérico.  Necesitará activar el soporte de <c>evdev</c> cambiando la
configuración de su núcleo. (Lea la <uri
link="/doc/es/kernel-config.xml">Guía de Configuración del
Núcleo</uri> si no sabe como configurar su núcleo.
</p>

<pre caption="Activando evdev en el núcleo">
Device Drivers ---&gt;
  Input device support ---&gt;
  &lt;*&gt;  Event interface
</pre>
</body>
</section>

<section>
<title>Configuración de make.conf</title>
<body>

<p>
Antes de instalar Xorg hay que configurar dos importantes variables en
el fichero <path>/etc/make.conf</path>.
</p>

<p>
La primera variable es <c>VIDEO_CARDS</c>. Se usa para establecer los
controladores de vídeo que tiene pensado utilizar. Su configuración
normalmente se basa en el tipo y marca de la tarjeta que tenga. Las
configuraciones más comunes son <c>nvidia</c> para tarjetas nVIDIA y
<c>fglrx</c> para tarjetas ATI Radeon. Estos son los controladores
propietarios de nVIDIA y ATI, respectivamente. Si le gustaría emplear
las versiones de código abierto de nVidia, utilice <c>nv</c> en vez de
<c>nvidia</c> para configurar la variable pero tenga en cuenta que el
uso de este controlador implica no disponer de una aceleración 3D
completa. Los controladores libres <c>radeon</c> y <c>radeonhd</c>
están disponibles para las tarjetas ATI y son más o menos iguales al
controlador propietario <c>fglrx</c>. El controlador <c>intel</c> se
puede usar en ordenadores de escritorio y portátiles con chipsets de
gráficos integrados Intel.  <c>VIDEO_CARDS</c> puede contener más de
un controlador; en este caso, cada uno de ellos debe estar separado
con espacios.
</p>

<p>
La segunda variable es <c>INPUT_DEVICES</c> y se utiliza para
determinar qué controladores han de ser creados para los dispositivos
de entrada. En la mayoría de los casos, con configurarla con
<c>evdev</c> debería funcionar correctamente.
</p>

<p>
Ahora debería decidir qué controladores utilizará y agregar la
configuración necesaria al fichero <path>/etc/make.conf</path>:
</p>

<pre caption="Entradas de ejemplo en make.conf">
<comment>(Para el soporte del ratón, teclado y touchpad Synaptics)</comment>
INPUT_DEVICES="evdev synaptics"
<comment>(Para tarjetas nVIDIA)</comment>
VIDEO_CARDS="nvidia"
<comment>(O para tarjetas ATI Radeon)</comment>
VIDEO_CARDS="radeon"
</pre>

<note>
Puede encontrar más instrucciones de cómo configurar tarjetas nVIDIA y
ATI en la <uri link="/doc/es/nvidia-guide.xml">Guía nVidia Gentoo
Linux</uri> y en las <uri link="/doc/es/ati-faq.xml">PUF (Preguntas de
Uso Frecuente/FAQ) de ATI en Gentoo Linux</uri>. Si no sabe qué
controladores debe escoger, diríjase a estas guías para más
información.
</note>

<p>
Si las configuraciones mencionadas anteriormente no le funcionan,
debería ejecutar <c>emerge -pv xorg-server</c>. Compruebe todas las
opciones disponibles y elija aquella que se ajuste a su sistema. Este
ejemplo es válido para un sistema con un teclado, ratón, touchpad
Synaptics y una tarjeta de vídeo Radeon.
</p>

<pre caption="Mostrar todos los controladores/opciones disponibles">
# <i>emerge -pv xorg-server</i>

These are the packages that would be merged, in order:

Calculating dependencies... done!
[ebuild   R   ] x11-base/xorg-server-1.6.3.901-r2  USE="hal nptl xorg -debug
-dmx -ipv6 -kdrive -minimal -sdl -tslib" 0 kB
[ebuild   R   ]  x11-base/xorg-drivers-1.6  INPUT_DEVICES="evdev synaptics
-acecad -aiptek -citron -elographics -fpit -hyperpen -joystick -keyboard -mouse
-mutouch -penmount -tslib -virtualbox -vmmouse -void -wacom"
VIDEO_CARDS="radeon -apm -ark -ast -chips -cirrus -dummy -epson -fbdev -fglrx
(-geode) -glint -i128 (-i740) (-impact) (-imstt) -intel -mach64 -mga -neomagic
(-newport) -nv -nvidia -r128 -radeonhd -rendition -s3 -s3virge -savage
-siliconmotion -sis -sisusb (-sunbw2) (-suncg14) (-suncg3) (-suncg6) (-sunffb)
(-sunleo) (-suntcx) -tdfx -tga -trident -tseng -v4l (-vermilion) -vesa -via
-virtualbox -vmware (-voodoo) (-xgi)" 0 kB
</pre>

<p>
Una vez establecidas las variables necesarias, ya puede instalar el
paquete Xorg.
</p>

<pre caption="Instalando Xorg">
# <i>emerge xorg-server</i>
</pre>

<note>
Podría instalar el meta-paquete <c>xorg-x11</c> en vez del de menor
tamaño <c>xorg-server</c>. En cuanto a funcionalidad, <c>xorg-x11</c>
y <c>xorg-server</c> son idénticos. De todos modos, <c>xorg-x11</c>
trae muchos más paquetes de los que probablemente vaya a necesitar,
como por ejemplo una gran variedad de fuentes en distintos idiomas. No
son necesarias para un entorno de escritorio corriente.
</note>

<p>
Cuando la instalación haya finalizado, tendrá que reinicializar
algunas variables de entorno antes de continuar. Simplemente ejecute
<c>env-update</c> seguido de <c>source /etc/profile</c> y ya está
configurado.
</p>

<pre caption="Reinicializando las variables de entorno">
# <i>env-update</i>
# <i>source /etc/profile</i>
</pre>

<p>
Ahora es el momento de arrancar el demonio de la Capa de Abstracción de
Hardware o Hardware Abstraction Layer (HAL), en inglés, y ajustarlo para
que arranque automáticamente cada vez que reinicie. Esto es necesaro para
tener un entorno X funcionando correctamente, de lo contrario los
dispositivos de entrada no serán detectados y probablemente lo único que
obtenga sea una pantalla negra. Cubriremos HAL en mayor profundidad en la
<uri link="#using_hal">siguiente sección</uri>.
</p>

<pre caption="Arrancando HAL">
# <i>/etc/init.d/hald start</i>
# <i>rc-update add hald default</i>
</pre>

</body>
</section>
</chapter>

<chapter>
<title>Configurando Xorg</title>
<section id="using_hal">
<title>Usando HAL</title>
<body>

<p>
Las versiones recientes del servidor X están diseñadas para funcionar
desde el momento en que son instaladas, sin necesidad de editar
manualmente los ficheros de configuración de Xorg.
</p>

<p>
Debe, en primer lugar, intentar <uri link="#using_startx">arrancar X</uri>
sin crear <path>/etc/X11/xorg.conf</path>.
</p>

<p>
Si su Xorg no arranca (si hay algo extraño en la pantalla o con su
ratón o teclado), entonces pueden intentar reparar los problemas
usando los ficheros de configuracion correctos.
</p>

<p>
Por defecto, Xorg usa HAL (Capa de Abstracción del Hardware ó Hardware
Abstraction Layer en inglés) para detectar y configurar los
dispositivos como los teclados y ratones.
</p>

<p>
HAL viene con muchas reglas de dispositivo preparadas, también
llamadas políticas. Estos ficheros de políticas se encuentran en
<path>/usr/share/hal/fdi/policy/</path>. Simplemente busque unas pocas
que se ajusten a sus necesidades de mejor forma y cópielas a
<path>/etc/hal/fdi/policy/</path>.
</p>

<impo>
¡No edite los ficheros en <path>/usr/share/hal/fdi/</path>!
Simplemente copie los que necesite y edítelos una vez están en la
localización <path>/etc</path> adecuada.
</impo>

<p>
Por ejemplo, para tener un funcionamiento básico de la combinación
teclado/ratón, podría copiar los siguientes ficheros a
<path>/etc/hal/fdi/policy/</path>:
</p>

<pre caption="Usando ficheros de política HAL">
# <i>cp /usr/share/hal/fdi/policy/10osvendor/10-input-policy.fdi /etc/hal/fdi/policy</i>
# <i>cp /usr/share/hal/fdi/policy/10osvendor/10-x11-input.fdi /etc/hal/fdi/policy</i>
</pre>

<p>
Hay algunas otras políticas HAL en <path>/usr/share/hal/fdi/</path>
que le pueden interesar, como configuraciones de portátiles, manejo de
dispositivos de almacenamiento, gestión de energía, y más. Simplemnte
copie cualquiera de las políticas a <path>/etc/hal/fdi/policy/</path>.
</p>

<impo>
Recuerde que <e>cada</e> vez que haga cambios a los ficheros de política
de HAL, debe reiniciar el demonio HAL, ejecutando
<c>/etc/init.d/hald restart</c>.
</impo>

<p>
Puede editar el fichero de políticas en
<path>/etc/hal/fdi/policy</path> a su gusto. Puede querer hacer algún
retoque u ofrecer funcionalidad añadida.  Vayamos con un ejemplo de
cómo retocar una política HAL.
</p>

<p>
Un truco muy conveniente es matar el servidor X completamente
presionando Ctrl-Alt-Retroceso. Esto es práctico cuando su servidor X
no está funcionando correctamente, congelado, etc. No es tan radical
como reiniciar la máquina completamente con Ctrl-Alt-Supr.
</p>

<p>
Las versiones recientes del servidor X deshabilitan esta combinación
por defecto.  Sin embargo puede rehabilitarla copiando
<path>10-x11-input.fdi</path> a <path>/etc/hal/fdi/policy</path> y
editándolo. Necesitará añadir una única línea a la sección apropiada,
como se muestra abajo:
</p>

<pre caption="Editando 10-x11-input.fdi">
<comment>(Abra el fichero con su editor favorito)</comment>
# <i>nano -w /etc/hal/fdi/policy/10-x11-input.fdi</i>
<comment>(Busque la sección "input.keys")</comment>
&lt;match key="info.capabilities" contains="input.keys"&gt;
<comment>(Añada la cadena "terminate" a la etiqueta merge, tal y como se muestra)</comment>
&lt;match key="info.capabilities" contains="input.keys"&gt;
      &lt;merge key="input.x11_driver" type="string"&gt;keyboard&lt;/merge&gt;
      <i>&lt;merge key="input.xkb.options" type="string"&gt;terminate:ctrl_alt_bksp&lt;/merge&gt;</i>
      &lt;match key="/org/freedesktop/Hal/devices/computer:system.kernel.name"
             string="Linux"&gt;
        &lt;merge key="input.x11_driver" type="string"&gt;evdev&lt;merge&gt;
      &lt;/match&gt;
    &lt;/match&gt;
</pre>

<p>
Una vez haya terminado, ejecute <c>/etc/init.d/hald restart</c> para que
HAL tenga en cuenta sus cambios.
</p>

<p>
Ahora ya tiene una forma práctica de matar un servidor X que no
responde. Esto es útil cuando los programas han congelado su pantalla
completamente o cuando ha configurado y retocado su entorno
Xorg. Tenga cuidado cuando mate su escritorio usando esta combinación
de teclas -- a muchos programas realmente no le gusta que los terminen
de esta forma, y puede perder algo (o todo) en lo que estuviera
trabajando.
</p>

<p>
Afortunadamente, simplemente trabajar con los ficheros de política de
HAL resulta en un escritorio X funcionando correctamente. Si Xorg no
arranca, o hay algún otro problema, necesitará configurar manualmente
<path>xorg.conf</path> como se muestra en la siguiente sección:
</p>
</body>
</section>

<section>
<title>El archivo xorg.conf</title>
<body>

<note>
La configuración de <path>xorg.conf</path> se debe contemplar como el
"último recurso". Es realmente deseable trabajar sin este fichero, si
es posible, y hacer toda su configuración a través de los ficheros de
política de HAL. Si no consigue hacer funcionar su configuración,
entonces continúe leyendo.
</note>

<p>
El archivo de configuración de Xorg se llama <path>xorg.conf</path> y
se encuentra en <path>/etc/X11</path>. Xorg proporciona un ejemplo de
configuración en <path>/etc/X11/xorg.conf.example</path> el cual puede
utilizarse para crear su propia configuración. Está muy comentado,
pero si necesita más información respecto a la sintaxis, no dude en
leer la página del manual (man).
</p>

<pre caption="Leyendo la página del manual sobre xorg.conf">
$ <i>man 5 xorg.conf</i>
</pre>
</body>
</section>

<section>
<title>Manera predeterminada: Generación automática de xorg.conf</title>
<body>

<p>
El propio Xorg es capaz de adivinar la mayoría de parámetros. En la
mayoría de los casos, sólo necesitara cambiar algunas líneas para
conseguir la resolución que quiere y marchando. Si está interesado en
un mayor conocimiento, asegúrese de consultar los recursos que se
encuentran al final de este capítulo. Pero primero, permítanos generar
un (esperemos que funcione) archivo de configuración Xorg.
</p>

<pre caption="Generando un archivo xorg.conf">
# <i>Xorg -configure</i>
</pre>

<p>
Asegúrese de leer las últimas lineas que aparecen en su pantalla
cuando Xorg ha terminado de probar su hardware. Si le informa de que
ha fallado en algún punto, forzosamente tendrá que editar manualmente
el archivo <path>xorg.conf</path>. Asumiremos que no ha fallado, le
debería informar que ha generado <path>/root/xorg.conf.new</path>
listo para que lo pruebe. Entonces, vamos a probarlo :)
</p>

<pre caption="Probando el archivo xorg.conf.new">
# <i>X -retro -config /root/xorg.conf.new</i>
</pre>

<p>
Si todo va bien, debería ver un patrón de trama sencillo blanco y
negro. Verifique que su ratón funciona correctamente y que la
resolución es la correcta. No será capaz de deducir la resolución
exacta, pero debería observar que es muy baja. Puede salir en
cualquier momento presionando Ctrl-Alt-Retroceso.
</p>
</body>
</section>

<section>
<title>Copiando xorg.conf</title>
<body>

<p>
Ahora, copiemos <path>xorg.conf.new</path> a
<path>/etc/X11/xorg.conf</path> para que no tengamos que ejecutar
contínuamente <c>X -config</c> -- el escribir solamente <c>startx</c>
es mucho más fácil :)
</p>

<pre caption="Copiando xorg.conf">
# <i>cp /root/xorg.conf.new /etc/X11/xorg.conf</i>
</pre>
</body>
</section>

<section id="using_startx">
<title>Ejecutar startx</title>
<body>

<p>
Ahora intente <c>startx</c> para iniciar su servidor X. <c>startx</c>
es un guión que ejecuta una <e>sesión X</e>, en otras palabras, inicia
el servidor X y algunas aplicaciones gráficas. Decide qué aplicaciones
debe correr mediante la siguiente lógica:
</p>

<ul>
  <li>
    Si existe un archivo de nombre <path>.xinitrc</path> en el
    directorio hogar, ejecutará los comandos allí contenidos.
  </li>
  <li>
    Sino, leerá el contenido de la variable de entorno XSESSION y
    ejecutará una de las sesiones disponibles en
    <path>/etc/X11/Sessions/</path>. Puede establecer el valor de
    XSESSION en <path>/etc/env.d/90xsession</path> para que sea el
    predeterminado para todos los usuarios del sistema. Por ejemplo,
    como superusuario, ejecute <c>echo XSESSION="Xfce4" >
    /etc/env.d/90xsession</c>. Esto creará el archivo
    <path>90xsession</path> y establecerá la sesión X predeterminada a
    Xfce4.
  </li>
</ul>

<pre caption="Iniciando X">
$ <i>startx</i>
</pre>

<p>
Puede matar la sesión X usando la combinación de teclas Ctrl-Alt-Backspace.
Esto, sin embargo, obligará a X a una salida algo vergonzosa -- tal vez algo
que no siempre quiera hacer.
</p>

<p>
Si todavía no ha instalado un gestor de ventanas lo que verá es la pantalla en
negro. Puesto que éste puede ser también un signo de que algo ha ido mal, puede
que quiera instalar <c>twm</c> y <c>xterm</c> <e>sólo para probar el servidor
X</e>
</p>

<p>
Una vez que ambos programas estén instalados, ejecute <c>startx</c> de nuevo.
Deberían de aparecer algunas ventanas de xterm haciendo que sea más fácil
comprobar que el servidor X está funcionando correctamente. Cuando quede
satisfecho con los resultados, ejecute <c>emerge --unmerge twm xterm</c> como
root para deshacerse de los paquetes de prueba. No los necesitará cuando
haya configurado su propio entorno de escritorio.
</p>

</body>
</section>
</chapter>

<chapter>
<title>Retocando los ajustes X</title>
<section>
<title>Configurando su resolución</title>
<body>

<p>
Si nota que la resolución de la pantalla no es correcta, necesitará
comprobar dos secciones en su configuración <path>xorg.conf</path>. En
primer lugar, tiene la sección <e>Screen</e> la cual tiene una lista
de resoluciones, si existe, que su servidor X puede soportar. De forma
predeterminada, esta sección no contiene ninguna lista de
resoluciones. Si es este el caso, Xorg estima las resoluciones
basándose en la información de la segunda sección, <e>Monitor</e>.
</p>

<p>
Lo que sucede es lo siguiente: Xorg comprueba la configuración de
<c>HorizSync</c> y <c>VertRefresh</c> en la sección <e>Monitor</e>
para calcular las resoluciones válidas. Por ahora, deje estas
configuraciones como están. Solamente cuando los cambios en la sección
<e>Screen</e> (serán descritos en un minuto) no funcionen, entonces
necesitará revisar las especificaciones de su monitor y rellenar con
los valores correctos.
</p>

<warn>
<b>No</b> cambie los valores de estas dos variable relacionadas con el
monitor sin haber consultado las especificaciones técnicas de su
monitor. Una configuración incorrecta conduce a errores de falta de
sincronización en el mejor de los casos y ha quemar pantallas en el
peor.
</warn>

<p>
Ahora vamos a cambiar las resoluciones. En el siguiente ejemplo desde
<path>/etc/X11/xorg.conf</path> añadimos líneas de resolución
(<c>Modes</c>) y la profundidad predeterminada (<c>DefaultDepth</c> )
para que su servidor de X comience con 24 bits y 1440x900. No importan
los textos entrecomillados -- son ejemplos y la mayoría serán
diferentes de las configuraciones de su sistema.
</p>

<pre caption="Modificar la sección Screen en /etc/X11/xorg.conf">
Section "Screen"
  Identifier  "Default Screen"
  Device    "RadeonHD 4550"
  Monitor   "Generic Monitor"
  <i>DefaultDepth  24</i>
  <comment># Omitimos parte del texto para mejorar la legibilidad</comment>
  SubSection "Display"
    Depth   24
    <i>Modes   "1440x900"</i>
  EndSubSection
EndSection
</pre>

<p>
Ejecute X (<c>startx</c>) para descubrir si utiliza la resolución que quiere.
</p>
</body>
</section>

<section>
<title>Configurando su teclado</title>
<body>

<p>
Para configurar y usar un teclado internacional, puede copiar el
contenido de
<path>/usr/share/doc/hal-*/*/use-estonian-layout.fdi.bz2</path> a
<path>/etc/hal/fdi/policy/10-xinput-configuration.fdi</path>:
</p>

<pre caption="Usando un fichero de configuración existente">
# <i>bzcat /usr/share/doc/hal-*/*/use-estonian-layout.fdi.bz2 >
/etc/hal/fdi/policy/10-xinput-configuration.fdi</i>
</pre>

<p>
Ahora puede simplemente editar
<path>10-xinput-configuration.fdi</path> y cambiar el modelo de
teclado Estonio (<c>ee</c>) al suyo, como Gran Betraña (<b>gb</b>) o
Polonia (<b>pl</b>)
</p>

<p>
Cuando haya terminado, ejecute <c>/etc/init.d/hald restart</c> como
root para asegurarse de que HAL toma los cambios de su fichero de
configuración.
</p>
</body>
</section>

<section>
<title>Terminando</title>
<body>

<p>
Ejecute <c>startx</c> y sea feliz con el resultado. Felicidades, ahora
(esperemos) tiene Xorg funcionando en su sistema. El siguiente paso es
instalar un gestor de ventanas con mayores funcionalidades (o incluso
un entorno de escritorio) como pueda ser KDE o GNOME, pero eso ya no
forma parte de esta guía :)
</p>
</body>
</section>
</chapter>

<chapter>
<title>Recursos</title>
<section>
<title>Creando y retocando xorg.conf</title>
<body>

<p>
Primero de todo, <c>man xorg.conf</c> y <c>man evdev</c> proporcionan
una rápida y completa referencia sobre la sintaxis utilizada por el
archivo de configuración. ¡Asegúrese de tenerlos abiertos en un
terminal cerca cuando edite el archivo de configuración!.
</p>

<p>
Asegúrese también de mirar el fichero
<path>/etc/X11/xorg.conf.example</path>. Le puede interesar copiarlo y
utilizarlo como base para escribir su propio fichero
<path>xorg.conf</path>.
</p>

<p>
Puede examinar las <uri link="http://www.x.org/wiki/FAQ">FAQ</uri> de
X.org que se proporcionan en su sitio web, además del resto de
documentación.
</p>

<p>
Hay también muchos recursos en línea sobre edición de
<path>xorg.conf</path>. Nosotros solamente listamos aquí unos pocos,
asegúrese de buscar en <uri link="http://www.google.com">Google</uri>
para encontrar más.
</p>
</body>
</section>

<section>
<title>Otros recursos</title>
<body>

<p>
Puede encontrar más información sobre cómo instalar y configurar
distintos entornos de escritorio y aplicaciones gráficas en la sección
de <uri link="/doc/es/?catid=desktop">Recursos de Documentación de
Gentoo</uri> de nuestra documentación.
</p>

<p>
Si está actualizando a xorg-server-1.6 desde una versión anterior, no
deje de consultar la <uri
link="/proj/es/desktop/x/x11/xorg-server-1.6-upgrade-guide.xml">guía
de migración</uri>.
</p>
</body>
</section>
</chapter>
</guide>
