<?xml version='1.0' encoding='UTF-8'?>
<!-- $Header: /var/cvsroot/gentoo/xml/htdocs/doc/es/xen-guide.xml,v 1.5 2010/05/19 12:22:46 chiguire Exp $ -->

<!DOCTYPE guide SYSTEM "/dtd/guide.dtd">

<guide link="/doc/es/xen-guide.xml" lang="es">

<title>Configurando Gentoo con Xen</title>

<author title="Autor">
  <mail link="swift@gentoo.org">Sven Vermeulen</mail>
</author>
<author title="Traductor">
  <mail link="nordri@gmail.com">Fede Díaz</mail>
</author>
<author title="Traductor">
  <mail link="chiguire@gentoo.org"/>
</author>

<abstract>
Esta guía describe como comenzar a usar Xen en su sistema Gentoo.
</abstract>

<!-- The content of this document is licensed under the CC-BY-SA license -->
<!-- See http://creativecommons.org/licenses/by-sa/2.5 -->
<license/>

<version>2</version>
<date>2010-05-14</date>

<chapter>
<title>Introducción</title>
<section>
<body>

<p>
La tecnología <uri link="http://www.xensource.com/">Xen</uri> le
permite correr múltiples sistemas operativos en un único sistema
físico, gobernar el consumo de recursos e incluso migrar dominios (los
cuales son entornos virtuales en los cuales corre un sistema
operativo) desde un sistema Xen hacia otro. Xen requiere que el
sistema operativo anfitrión soporte Xen (el cual, en este caso, será
un núcleo Linux) pero los sistemas operativos invitados pueden correr
sin modificaciones <e>si</e> su hardware soporta Intel Virtualization
Technology (VT-x) o AMD Virtualization Technology (SVM). En otro caso
sus sistemas operativos invitados deben también soportar Xen.
</p>

<p>
Esta guía charlará con usted a través de los pasos necesarios para
tener Xen levantado y corriendo en Gentoo Linux. No discutiremos sobre
el propio Xen (el proyecto tiene <uri
link="http://www.cl.cam.ac.uk/research/srg/netos/xen/readmes/user">una
documentación aceptable</uri> disponible) no trataremos
configuraciones especializadas que podrían ser muy interesantes para
Xen pero no son relativas a Xen (como exportar Portage a través de
NFS, arrancar Linux usando PXe, etc.)
</p>
</body>
</section>
</chapter>

<chapter>
<title>Preparando el Domain0</title>
<section>
<title>Introducción</title>
<body>

<p>
<e>Domain0</e> es el dominio principal bajo Xen, aloja el sistema
operativo anfitrión que gobernará todos los otros dominios bajo
Xen. En este capítulo prepararemos una instalación Gentoo existente
para que sea el sistema operativo anfitrión en este dominio y
construiremos un núcleo con soporte Xen para que Gentoo este listo
para albergar otros dominios Xen.
</p>
</body>
</section>

<section>
<title>¿Reconstruir la instalación de Gentoo?</title>
<body>

<p>
Un cambio dramático que podría ser necesario es la reconstrucción
completa de una instalación Gentoo con una configuración de
<c>CFLAGS</c> diferente. De otra manera, los sistemas operativos que
corran bajo Xen podrían observar una mayor degradación en cuanto al
rendimiento. Si usted, sin embargo, está planeando probar Xen en lugar
de instalarlo para un uso de producción y no encuentra tan terrible la
reconstrucción de todos los programas, puede omitir este paso. En este
caso notará una degradación del rendimiento pero será capaz de
utilizar Xen.
</p>

<impo>
Se advierte que, si cambia sus <c>CFLAGS</c> y construye su sistema
con un gcc inferior a la versión 4, no tenga establecido <c>-Os</c> ya
que se ha reportado que produce código erróneo.
</impo>

<pre caption="Editando las CFLAGS y reconstruyendo la instalación de Gentoo">
~# <i>nano -w /etc/make.conf</i>
<comment>(Añada -mno-tls-direct-seg-refs)</comment>
CFLAGS="-O2 -march=pentium4 -pipe <i>-mno-tls-direct-seg-refs</i>"

~# <i>emerge -e world</i>
</pre>

<p>
Si arranca su sistema usando un ramdisk inicial (initrd) necesita
reconstruir el initrd también (lo mejor es hacerlo siguiendo todos los
pasos a la hora de reconstruir su núcleo).
</p>
</body>
</section>

<section>
<title>Instalando Xen</title>
<body>

<p>
Ahora mismo, Xen contiene muchos componente, así necesitará instalar
un par de paquetes. Primero necesitará desenmascararlo añadiendo las
líneas necesarias a <path>/etc/portage/package.keywords</path> ya que
aun está <uri
link="/doc/en/handbook/handbook-x86.xml?part=3&amp;chap=3#doc_chap2">~arch
masked</uri> y entonces instalarlo.
</p>

<pre caption="Desenmascarando e instalando Xen">
~# <i>nano -w /etc/portage/package.keywords</i>
app-emulation/xen
app-emulation/xen-tools
sys-kernel/xen-sources

~# <i>emerge xen xen-tools xen-sources</i>
</pre>
</body>
</section>

<section>
<title>Construyendo el Núcleo</title>
<body>

<p>
Ahora construiremos el núcleo con soporte para Xen. Este núcleo, cuyas
fuentes están disponibles en <path>/usr/src/linux-2.6.x.z-xen</path>,
será nuestro principal núcleo (por ejemplo el que correrá en el
dominio 0). En la sección <c>XEN</c> encontrará controladores para
todos los tipos de entrada y salida, cada controlador tiene disponible
un <e>backend</e> y un <e>frontend</e> implementados. Para el núcleo
del dominio 0 necesita seleccionar la implementación <e>backend</e>:
esta será usada por otros dominios (que usan el controlador de
<e>frontend</e>) para comunicar directamente con el hardware.
</p>

<p>
Por supuesto, no olvide seleccionar <c>Xen-compatible</c> en
<c>Processor type and features</c>. Si está trabajando con
comunicaciones: cada interfaz en un dominio tiene un enlace
punto-punto a una interfaz en el dominio 0 (llamado
<path>vifX.Y</path> donde X es el número de interfaz e Y la y-ésima
interfaz en este dominio), así puede configurar su red en el modo que
quiera (puente, NAT, etc.)
</p>

<pre caption="Habilitando Soporte Xen para Núcleos i386">
Processor type and features  ---&gt;
      Subarchitecture Type (Xen-compatible)
</pre>

<pre caption="Habilitando Soporte Xen para Núcleos x86_64">
Processor type and features  ---&gt;
      Subarchitecture Type (PC-compatible)
  [*] Enable Xen compatible kernel
  [*] Support for hot-pluggable CPUs
</pre>

<pre caption="Configuración del Núcleo para el Dominio-0">
Bus options (PCI etc.)  ---&gt;
  [*] PCI support
  [ ]   Xen PCI Frontend Debugging

Networking  ---&gt;
  Networking options  ---&gt;
    &lt;*&gt; 802.1d Ethernet Bridging
    <comment>Sólo requerido para comunicaciones puente.</comment>

XEN  ---&gt;
  [*] Privileged Guest (domain 0)
  &lt;*&gt; Backend driver support
  &lt;*&gt;   Block-device backend driver
  &lt;*&gt;   Network-device backend driver
  &lt;*&gt;   PCI-device backend driver
           PCI Backend Mode (Virtual PCI) ---&gt;
  [*] Scrub memory before freeing it to Xen
  [*] Disable serial port drivers
      Xen version compatibility (3.0.4 and later)
</pre>

<pre caption="Configuración del Núcleo para el Dominio-U">
Bus options (PCI etc.)  ---&gt;
  [ ] PCI support

Device Drivers  ---&gt;
  SCSI device support  ---&gt;
    &lt; &gt; SCSI device support
    <comment>Deshabilitando el soporte SCSI libera los
    nombres de dispositivos /dev/sd* para usarlos
    como dispositivos de bloques virtuales de Xen.</comment>

XEN  ---&gt;
  [ ] Privileged Guest (domain 0)
  &lt;*&gt; Block-device frontend driver
  &lt;*&gt; Network-device frontend driver
  [*] Scrub memory before freeing it to Xen
  [*] Disable serial port drivers
      Xen version compatibility (3.0.4 and later)
</pre>

<p>
Una buena sugerencia es tener los procesos hechos por núcleo
almacenados en archivos objetos intermedios en cualquier otro lugar a
fin de que el mismo árbol del núcleo pueda ser reutilizado para otras
configuraciones.
</p>

<pre caption="Construcción del Núcleo">
~# <i>mkdir -p ~/build/dom0 ~/build/domU</i>
~# <i>make O=~/build/dom0 menuconfig</i>
<comment>(Configure el núcleo)</comment>
~# <i>make O=~/build/dom0 &amp;&amp; make O=~/build/dom0 modules_install</i>
</pre>

<p>
Una vez el núcleo está construido encontrará una imagen del núcleo en
el directorio de construcción (no dentro de <path>arch/</path> o algún
otro directorio) llamado <path>vmlinuz</path>. Cópielo en
<path>/boot</path> y entonces configure su gestor de arranque para
usar el hipervisor de Xen (uno de los componentes instalados
anteriormente) que está almacenado en <path>/boot/xen.gz</path>. En la
configuración del gestor de arranque, añada su recién construido
núcleo como el núcleo que Xen debería arrancar. Por ejemplo, para
GRUB:
</p>

<pre caption="Configuración de GRUB para Xen">
title Xen 3.0 / Gentoo Linux 2.6.x.y
root (hd0,0)
kernel /boot/xen.gz
module /boot/kernel-2.6.x.y-xen0 root=/dev/hda3
</pre>

<p>
Ahora reinicie su sistema en Xen. Una vez arrancado, necesita cargar
el demonio Xen:
</p>

<pre caption="Cargando el demonio Xen">
~# <i>/etc/init.d/xend start</i>
</pre>

<p>
Ahora compruebe que puede hacer todo lo que hace normalmente es su
sistema. Si este es el caso, puede editar la configuración del gestor
de arranque para que siempre arranque con Xen y añada el demonio Xen
al nivel de ejecución por defecto para que arranque automáticamente la
próxima vez que inicie el equipo.
</p>

<note>
Si desea que los dominios invitados arranquen automáticamente al
arrancar añada <c>xendomains</c> al nivel de ejecución por defecto
también y cree un enlace simbólico en <path>/etc/xen/auto/</path>
hacia los archivos de configuración de Xen para los dominios que
quiera iniciar.
</note>
</body>
</section>
</chapter>

<chapter>
<title>Creando un Dominio sin Privilegios</title>
<section>
<title>Construyendo el Núcleo</title>
<body>

<p>
Vaya al fuente del núcleo Linux con soporte Xen y actualice la
configuración.  Es sabio mantener tantas características como sea
posible en el núcleo principal excepto la configuración <c>XEN</c>
donde los controladores deberían tener su propia implementación
<e>frontend</e> seleccionada en lugar de la <e>backend</e>. Entonces
construya el núcleo y coloque el resultante archivo
<path>vmlinuz</path> donde quiera (asumimos que es en
<path>/mnt/data/xen/kernel</path>):
</p>

<pre caption="Construcción del núcleo invitado">
~# <i>make O=~/build/domU</i>
~# <i>cp ~/build/domU/vmlinuz /mnt/data/xen/kernel/kernel-2.6.x.y-xen</i>
</pre>

<p>
También es posible crear una única imagen del núcleo para ambos, el
dominio administrativo y el dominio sin privilegios. Puede encontrar
más información en el manual de usuario de Xen.
</p>
</body>
</section>

<section>
<title>Creando Discos de Dominio</title>
<body>

<p>
Para un mayor rendimiento, es mejor dedicar una partición (o volumen
lógico) a un dominio en lugar de un sistema de archivos basado en
archivos. Sin embargo, si principalmente usará Xen para pruebas usar
un sistema de archivos basado en archivos tiene sus ventajas
(especialmente en lo relacionado al mantenimiento).
</p>

<p>
Puede crear un sistema de ficheros basado en archivos usando <c>dd</c>
y <c>mke2fs</c> (o cualquier otra herramienta de creación de sistemas
de archivos). Por ejemplo, para crear un sistema de archivo ext3 de
2Gbytes:
</p>

<pre caption="Creando un sistema de archivos">
~# <i>dd if=/dev/zero of=/mnt/data/xen/disks/ext3root.img bs=1M count=2048</i>
~# <i>mke2fs -j /mnt/data/xen/disks/ext3root.img</i>
</pre>
</body>
</section>

<section>
<title>Configurando un Dominio</title>
<body>

<p>
Ahora crearemos un archivo de configuración para un dominio. Puede
almacenar estos archivos donde quiera, por ejemplo en
<path>/mnt/data/xen/configs</path>. Como un ejemplo, crearemos un
archivo de configuración para un pequeño entorno Gentoo que usa la
imagen de disco creada anteriormente.
</p>

<pre caption="Creando un archivo de configuración de dominio">
~# <i>nano -w /mnt/data/xen/configs/gentoo</i>

kernel = "/mnt/data/xen/kernel/kernel-2.6.x.y-xen"
memory = 512
name   = "gentoo"
<comment>(Mapea la imagen de disco al virtual /dev/sda1)</comment>
disk   = ['file:/mnt/data/xen/disks/ext3root.img,sda1,w']
root   = "/dev/sda1 ro"
</pre>

<p>
Si está usando un dispositivo de bloque (como un volumen lvm o
partición) para el disco utilice 'phy:' en lugar de 'file:' y elimine
/dev. Por ejemplo:
</p>

<pre caption="Utilizando un dispositivo de bloque">
<comment>(Volumen LVM)</comment>
disk = [ 'phy:lvm/xen-guest-root,sda1,w' ]

<comment>(Partición Física)</comment>
disk = [ 'phy:sdb6,sda1,w' ]
</pre>

<p>
Puede encontrar ejemplos de archivos de configuración en
<path>/etc/xen</path>.
</p>
</body>
</section>

<section>
<title>Lanzando el Nuevo Dominio</title>
<body>

<p>
Ahora tenemos todo configurado y podemos lanzar el nuevo dominio. Si
el disco imagen contiene un sistema operativo, podríamos crear y
adjuntar el dominio usando el comando <c>xm</c> (Xen manager):
</p>

<pre caption="Creando y arrancando un nuevo dominio">
~# <i>xm create /mnt/data/xen/configs/gentoo -c</i>
</pre>

<p>
El dominio debería ser arrancado dentro de la terminal en la cual
ejecutó el comando. Sin embargo, en nuestro caso, la imagen de disco
está vacía así el dominio no arrancara en nada útil. Para arreglar
esto, puede montar la imagen como dispositivo loop e instalar Gentoo
tal y como lo está usando.
</p>

<p>
Si quiere desconectar del dominio, presione
<path>Ctrl+]</path>. Siempre puede reconectar la consola del dominio
usando <c>xm console gentoo</c>. Sin embargo, sólo hay una consola por
dominio, así que solo use esto cuando no pueda acceder al dominio de
otra manera (por ejemplo, a través de SSH).
</p>
</body>
</section>
</chapter>

<chapter>
<title>Redes en Dominios sin Privilegios</title>
<section>
<title>Introducción</title>
<body>

<p>
Xen soporta al menos dos formas de configurar su red (virtual):
<e>enrutado</e> y <e>puente</e>.
</p>

<p>
Cuando selecciona el enfoque de <e>enrutado</e>, la interfaz dentro de
su dominio sin privilegos se conecta a la interfaz virtual en su
dominio administrativo. En su dominio administrativo (domain 0), la
interfaz virtual es enlazada conjunta con <path>eth0</path>. La
interfaz dentro de su dominio sin privilegios debería tener una
dirección IP en la misma red que la interfaz en su dominio
administrativo. Cualquier comunicación a esta dirección IP solo puede
proceder desde el dominio administrativo, a no ser que especifique
reglas de enrutado específicas.
</p>

<p>
Cuando seleccione el enfoque de <e>puente</e>, su interfaz de red por
defecto en el dominio administrativo se convertirá en un puente que
aceptará conexiones a los dominios virtuales así como a las
direcciones IP que tenga en su dominio administrativo.
</p>
</body>
</section>

<section>
<title>Interfaces con Rutas Regulares</title>
<body>

<p>
Antes de configurar las interfaces en su dominio sin privilegios,
asegúrese que los controladores de Xen <path>netloop</path> y
<path>netbk</path> están cargados. Un consejo rápido: si tiene
<path>netloop</path> como módulo, cárguelo con <c>nloopbacks=0</c>
para que no cree interfaces inútiles en el dispositivo de
loopback. Entonces, edite el archivo de configuración de su dominio y
añádale la instrucción <c>vif</c>.
</p>

<pre caption="Configurando una interfaz virtual">
~# <i>nano -w /mnt/data/xen/configs/gentoo</i>

<comment>(Añada la instrucción vif)</comment>
vif    = [ 'ip=192.168.1.101, vifname=veth1' ]
</pre>

<p>
En el ejemplo anterior, la interfaz se creó para el dominio sin
privilegios (en el cual se llamara <path>eth0</path>) y Xen se
asegurara que la dirección 192.168.1.101 sea accesible desde el
dominio administrativo a través de la interfaz <path>veth1</path>.
</p>

<p>
Esto no significa que a la interfaz virtual <path>eth0</path> se le
asigne automáticamente la IP 192.168.1.101, pero en lugar de eso, si
no le asigna esta IP, no se conectará con el dominio administrativo y
por lo tanto no podrá ser accesible.
</p>

<p>
Ahora edite <path>/etc/xen/xend-config.sxp</path> como sigue para
seleccionar la configuración de las rutas de red.
</p>

<pre caption="Editando xend-config.sxp">
~# <i>nano -w /etc/xen/xend-config.sxp</i>

<comment>(Descomente las siguientes líneas)</comment>
<i>#</i>(network-script network-bridge)
<i>#</i>(vif-script vif-bridge)

<comment>(Habilite las siguientes líneas)</comment>
(network-script network-route)
(vif-script vif-route)
</pre>
</body>
</section>

<section>
<title>Interfaces Puente</title>
<body>

<p>
A diferencia de las interfaces de ruta ahora necesita cargar el
controlador de <path>netloop</path> con <c>nloopbacks=1</c> (o
superior) como un dispositivo de loopback adicional para crear el
puente. Para los otro módulos también necesitará el módulo
<path>netbk</path> para la funcionalidad del puente (el módulo
<path>bridge</path> si está construido).
</p>

<p>
Ahora edite su dominio virtual y añada el constructor <c>vif</c>:
</p>

<pre caption="Configurando una interfaz virtual">
~# <i>nano -w /mnt/data/xen/configs/gentoo</i>

<comment>(Añada la instrucción vif)</comment>
vif    = [ 'ip=192.168.1.101, vifname=veth0' ]
</pre>

<p>
Edite <path>/etc/xen/xend-config.sxp</path> como sigue para elegir la
configuración de rutas de la red:
</p>

<pre caption="Editando xend-config.sxp">
~# <i>nano -w /etc/xen/xend-config.sxp</i>

<comment>(Habilite las siguientes líneas)</comment>
(network-script network-bridge)
(vif-script vif-bridge)

<comment>(Comente las siguiente líneas si no está hecho ya)</comment>
<i>#</i> (network-script network-route)
<i>#</i> (vif-script vif-route)
</pre>

<p>
Por defecto, el puente contendrá cualquier interfaz configurada para
ser la interfaz por defecto (el dispositivo listado bajo la ruta por
defecto a través de <c>ip route list</c>). Si quiere modificar esto,
edite <path>xend-config.sxp </path> como sigue:
</p>

<pre caption="Editando xend-config.sxp para cambiar la configuración del puente">
~# <i>nano -w /etc/xen/xend-config.sxp</i>

<comment>(Edite la línea network-script)</comment>
(network-script <i>'</i>network-bridge <i>netdev=eth0 bridge=xenbr0 vifnum=0'</i>)
</pre>

<p>
Una vez la configuración este hecha, reinicie el script de inicio
<c>xend</c> para tener Xen construido con el puente:
</p>

<pre caption="Reiniciando el demonio xend">
~# <i>/etc/init.d/xend restart</i>
</pre>
</body>
</section>
</chapter>

<chapter>
<title>Más Recursos</title>
<section>
<title>Documentación de Xen</title>
<body>

<ul>
  <li>
    <uri link="http://tx.downloads.xensource.com/downloads/docs/user/">Manual
    de usuario de Xen</uri>
  </li>
  <li>
    <uri link="http://xen.xensource.com/documentation.html">Documentación de
    la comunidad Xen</uri>
  </li>
  <li>
    <uri link="http://wiki.xensource.com/xenwiki/">Wiki de Xen</uri>
  </li>
</ul>
</body>
</section>

<section>
<title>Herramientas de Xen</title>
<body>

<ul>
  <li>
    <uri
    link="http://virt-manager.et.redhat.com/">app-emulation/virt-manager</uri>
    una herramienta gráfica para administrar máquinas virtuales
  </li>
</ul>
</body>
</section>
</chapter>
</guide>
