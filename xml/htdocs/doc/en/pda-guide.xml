<?xml version='1.0' encoding="UTF-8"?>
<!DOCTYPE guide SYSTEM "/dtd/guide.dtd">
<!-- $Header: /var/cvsroot/gentoo/xml/htdocs/doc/en/pda-guide.xml,v 1.4 2008/05/23 19:58:26 swift Exp $ -->

<guide link="/doc/en/pda-guide.xml" disclaimer="draft">
<title>Gentoo PDA Guide</title>

<author title="Author">
  <mail link="swift@gentoo.org">Sven Vermeulen</mail>
</author>
<author title="Author">
  <mail link="chriswhite@gentoo.org">Chris White</mail>
</author>

<abstract>
Nowadays, personal digital assistants, or PDAs, are quite popular. Not only do
they provide more features than before, they are also easy to handle, even by
people who aren't that computer literate. But how can you synchronise these PDAs
with your Gentoo Linux installation if they are almost all running Windows
Mobile?
</abstract>

<!-- The content of this document is licensed under the CC-BY-SA license -->
<!-- See http://creativecommons.org/licenses/by-sa/2.5 -->
<license/>

<version>1.0</version>
<date>2007-11-20</date>

<chapter>
<title>Setting up PDA Support</title>
<section>
<title>Introduction</title>
<body>

<p>
PDAs are everywhere. They are an evolution of the paper journals and address
books and have become very feature-rich: addresses and telephone numbers,
pictures and movies, small spreadsheets or documents, calender entries, ...
anything is possible with PDAs. And thanks to wireless technology, PDAs can
manipulate or receive data from various devices (like GPS devices or other PDAs)
or join the world-wide Internet.
</p>

<p>
However, most PDAs run a propriatary operating system and might not be easy to
synchronise with your computer if you are not running software of the same
vendor. Luckily, the free software community has made serious efforts in reverse
engineering access to the PDAs and some companies have even opened their
specification to connect with the PDA. This guide will cover accessing and
synchronizing PDAs with your system.
</p>

</body>
</section>
<section>
<title>Kernel Setup</title>
<body>

<p>
The first thing we need to do is get the kernel to recognize our device. Open up
the kernel configuration (for instance, through <c>make menuconfig</c>) and add
support for your PDA. If your device uses USB to connect to a computer you will
most likely find support for it under <e>Device Drivers -&gt; USB support -&gt;
USB Serial Converter support</e>. For instance, iPAQ users (and many others) can
use the <e>USB PocketPC PDA Driver</e>.
</p>

<pre caption="Kernel Driver Setup">
&lt;M&gt; USB Serial Converter support
[ ]   USB Serial Console device support (EXPERIMENTAL)
[ ]   USB Generic Serial Driver
&lt; &gt;   USB Belkin and Peracom Single Port Serial Driver
&lt; &gt;   USB ConnectTech WhiteHEAT Serial Driver
&lt; &gt;   USB Digi International AccelePort USB Serial Driver
&lt; &gt;   USB Cypress M8 USB Serial Driver
&lt; &gt;   USB Empeg empeg-car Mark I/II Driver
&lt; &gt;   USB FTDI Single Port Serial Driver (EXPERIMENTAL)
&lt; &gt;   USB Handspring Visor / Palm m50x / Sony Clie Driver
&lt;M&gt;   USB PocketPC PDA Driver
</pre>

<p>
You might wonder why it is called a <e>Serial Converter</e>. This is because you
are going to use USB (technology) to talk with your device, even though the
device itself is accessed using a serial protocol.
</p>

<p>
You will also need asynchronous PPP support:
</p>

<pre caption="Kernel Driver Setup for Async PPP">
Device Drivers ---&gt;
  Network device support ---&gt;
    &lt;M&gt; PPP (point-to-point protocol) support
      &lt;M&gt; PPP support for async serial ports
</pre>

<p>
Rebuild the kernel and its modules. If you only had to modify a module, you can
install the modules and continue. Otherwise, set up the new kernel in your boot
loader and reboot.
</p>

</body>
</section>
<section>
<title>Detecting Your PDA</title>
<body>

<p>
Ok, now that we have the kernel module setup, let's go ahead and get some
information.  Plug in your device and take a look at your modules list if
you have build support for the device as a kernel module:
</p>

<pre caption="Checking if your PDA is detected">
# <i>lsmod</i>
Module                  Size  Used by
ipaq                   30736  0
usbserial              25120  1 ipaq
<comment>(...)</comment>

# <i>dmesg | grep Pocket</i>
drivers/usb/serial/usb-serial.c: USB Serial support registered for PocketPC PDA
drivers/usb/serial/ipaq.c: USB PocketPC PDA driver v0.5
ipaq 3-2:1.0: PocketPC PDA converter detected
usb 3-2: PocketPC PDA converter now attached to ttyUSB0

# <i>ls -la /dev/ttyUSB0</i>
crw-rw---- 1 root uucp 188, 0 Sep 27 19:21 /dev/ttyUSB0
</pre>

<p>
As we can see, the device has been detected and is now accessible through
<path>/dev/ttyUSB0</path>.
</p>

</body>
</section>
</chapter>

<chapter>
<title>Using SynCE</title>
<section>
<title>Introduction</title>
<body>

<p>
<uri link="http://www.synce.org">SynCE</uri> is the tool you can use to connect
to and work with Windows Mobile and Windows CE powered devices. The tool
connects, through the device set up earlier, to the device and allows
applications to synchronise appointments, addresses and more.
</p>

<p>
A list of devices supported through SynCE can be found on the
<uri link="http://www.synce.org">SynCE website</uri>.
</p>

<p>
SynCE is not available through a stable ebuild though, so before installing it,
you will first need to mark them in <path>/etc/portage/package.keywords</path>.
</p>

<pre caption="List of packages to list in package.keywords">
app-pda/synce
app-pda/synce-software-manager
app-pda/synce-librapi2
app-pda/synce-libsynce
app-pda/synce-multisync_plugin
app-pda/synce-rra
dev-libs/libmimedir
app-pda/synce-dccm
app-pda/synce-trayicon
app-pda/orange
app-pda/dynamite
app-pda/synce-kde
app-pda/synce-gnomevfs
app-pda/synce-serial
</pre>

<p>
Then, install the <c>app-pda/synce</c> and <c>net-dialup/ppp</c> packages to
obtain the necessary tools.
</p>

</body>
</section>
<section>
<title>Configuring the PDA</title>
<body>

<p>
The next task is to configure the serial device we found earlier
(<path>/dev/ttyUSB0</path> in our example) to be used with SynCE:
</p>

<pre caption="Configuring the serial device for SynCE">
~# <i>synce-serial-config ttyUSB0</i>

You can now run synce-serial-start to start a serial connection.
</pre>

<p>
This step only has to be performed once: it stored the necessary PPP-related
information inside <path>/etc/ppp/peers/synce-device</path> which is read by PPP
when it is called by <c>synce-serial-start</c>.
</p>

</body>
</section>
<section>
<title>Accessing The Device</title>
<body>

<p>
Now, log on as the user who will be using the PDA and run the <c>vdccm</c>
command. This tool is the connection manager for SynCE through which programs
connect to the device.
</p>

<pre caption="Launching the connection manager">
~$ <i>vdccm</i>
</pre>

<p>
As root again, run <c>synce-serial-start</c> which will connect to the device.
With some PDAs, you will notice this on the PDA itself through a synchronisation
symbol or any other event.
</p>

<pre caption="Running synce-serial-start">
~# <i>synce-serial-start</i>
</pre>

<p>
In some cases, the first attempts fail but this isn't shown immediately. You
can rerun the command a few times until the command replies that a serial
connection is already started. To verify that the connection is done, check if a
ppp interface (like ppp0) is created and has an IP address attached to it.
</p>

<p>
When <path>ppp0</path> is started, the configuration starts the
<path>/etc/init.d/net.ppp0</path> init script which is configured through
<path>/etc/conf.d/net.ppp0</path>, so you definitely want to take a look at this
configuration file.
</p>

</body>
</section>
<section>
<title>File Navigation and Manipulation</title>
<body>

<p>
The SynCE project provides a number of programs to communicate with the PDA and
exchange files. The tools have similar namings as on a regular Unix system but
with a <c>p</c> prepended to it: <c>pcp</c>, <c>pls</c>, <c>pmv</c>, <c>prm</c>,
<c>pmkdir</c>, <c>prmdir</c>. Other tools are specific for PDAs, like
<c>prun</c> to launch a program, <c>synce-install-cab</c> to install a CAB file
or <c>pstatus</c> to display information about the device.
</p>

<p>
Beware though, the behavior of the tools is not all that intuïtive. For
instance, the <c>pls</c> tool shows the contents of the <path>My
Documents</path> directory whereas the <c>pcp</c> tool starts from the <path>My
Device</path> location. As an example, we'll upload a file to the <path>My
Documents</path> folder:
</p>

<pre caption="Uploading a file">
~$ <i>pls</i>
Directory               Tue 01 Jan 2002 01:00:00 PM CET  Business/
Directory               Tue 01 Jan 2002 01:00:00 PM CET  Personal/
Directory               Tue 01 Jan 2002 01:00:00 PM CET  Templates/
~$ <i>pcp music.mp3 :"My Documents/music.mp3"</i>
File copy of 3852416 bytes took 0 minutes and 38 seconds, that's 101379 bytes/s.
~$ <i>pls</i>
Archive        3852416  Wed 02 Jan 2002 07:05:06 PM CET  music.mp3
Directory               Tue 01 Jan 2002 01:00:00 PM CET  Business/
Directory               Tue 01 Jan 2002 01:00:00 PM CET  Personal/
Directory               Tue 01 Jan 2002 01:00:00 PM CET  Templates/
</pre>

</body>
</section>
<section>
<title>SynCE for KDE Project</title>
<body>

<p>
The <uri link="http://synce.sourceforge.net/synce/kde/">SynCE for KDE
Project</uri> aims to support PDAs within KDE by facilitating file exchange,
synchronisation and installation and even provide mirroring capabilities so you
can control your PDA from within KDE.
</p>


<p>
The <c>raki</c> tool is SynCE's PocketPC management tool for KDE. The first time
you launch it, it asks whether you use <c>dccm</c> or <c>vdccm</c>. Once you
selected the connection manager you use (in our example it would be
<c>vdccm</c>) your KDE session (or any environment which supports KDE applets)
will now have a PDA applet from which you can browse and synchronise your PDA.
</p>

<p>
However, KDE integration for PDAs is better maintained through KPilot...
</p>

</body>
</section>
</chapter>

<chapter>
<title>All-In-One Tools</title>
<section>
<title>KPilot</title>
<body>

<note>
The author did not get KPilot to run well on his iPAQ as it timed out before the
connection was made. If anyone knows a possible solution to this, please report
this at <uri link="https://bugs.gentoo.org/XXXXXX">bug XXXXXX</uri>.
</note>

<p>
The <c>kde-base/kpilot</c> application provides synchronisation support from the
PDA with Kontact (KDE) or Evolution (GNOME). Its setup is quite easy: install
the package and launch it. The first time you run it, it will ask you to
identify your PDA, either by passing on the device name or having KPilot
auto-detect the device.
</p>

<p>
Once configured, you can quickly synchronise tasks, data, addresses and messages
between your PDA and Kontact, the KDE PIM manager.
</p>

</body>
</section>
</chapter>

<!--
gnome-pilot
-->

<!--
  TODO
  - Sync ical files to pocket pc
-->

</guide>
