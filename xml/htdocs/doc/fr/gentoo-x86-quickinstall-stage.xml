<?xml version='1.0' encoding="UTF-8"?>
<!-- $Header: /var/cvsroot/gentoo/xml/htdocs/doc/fr/gentoo-x86-quickinstall-stage.xml,v 1.7 2009/11/08 20:19:37 cam Exp $ -->
<!DOCTYPE included SYSTEM "/dtd/guide.dtd">

<included>

<version>10</version>
<date>2009-10-04</date>

<section>
<title>Préparer le stage</title>
<body>

<p>
D'abord, assurez-vous que la date de votre système est correcte en utilisant
<c>date MMJJhhmmAAAA</c>. Utilisez le temps UTC.
</p>

<pre caption="Préciser la date et l'heure UTC">
<comment>(Vérifiez l'horloge.)</comment>
livecd gentoo # <i>date</i>
Mon Mar  6 00:14:13 UTC 2006

<comment>(Préciser la date et l'heure actuelle, si nécessaire.)</comment>
livecd gentoo # <i>date 030600162006</i> <comment>(Format : MMJJhhmmAAAA)</comment>
Mon Mar  6 00:16:00 UTC 2006
</pre>

<p>
Ensuite, téléchargez une archive «&nbsp;stage&nbsp;» à partir d'un de nos <uri
link="/main/en/mirrors.xml">miroirs</uri>&nbsp;:
</p>

<pre caption="Télécharger une archive «&nbsp;stage3&nbsp;»">
livecd ~ # <i>cd /mnt/gentoo</i>
livecd gentoo # <i>links http://www.gentoo.org/main/en/mirrors.xml</i>
<comment>(Choisissez un miroir, allez dans le répertoire releases/x86/current-stage3/,
sélectionnez le stage3 de votre choix, probablement le stage3 i686 et appuyez
sur D pour le télécharger.)</comment>

<comment>(<b>Ou</b> télécharger-le directement avec wget sans choisir un miroir proche.)</comment>
livecd ~ # <i>cd /mnt/gentoo</i>
livecd gentoo # <i>wget ftp://distfiles.gentoo.org/pub/gentoo/releases/x86/current-stage3/stage3-i686-*.tar.bz2</i>
</pre>

<p>
Rendez-vous dans le dossier <path>/mnt/gentoo</path> et décompressez l'archive
«&nbsp;stage&nbsp;» à l'aide de la commande <c>tar xjpf &lt;archive
stage3&gt;</c>.
</p>


<pre caption="Désarchiver l'archive «&nbsp;stage3&nbsp;»">
livecd gentoo # <i>time tar xjpf stage3*</i>

real  1m14.157s
user  1m2.920s
sys   0m7.530s
</pre>

<p>
Installez le dernier instantané de Portage. Pour ce faire, procédez comme pour
l'archive «&nbsp;stage3&nbsp;»&nbsp;: choisissez un miroir proche depuis notre
<uri link="/main/en/mirrors.xml">liste</uri>, téléchargez l'instantané le plus
récent et désarchivez-le.
</p>

<pre caption="Télécharger l'instantané Portage le plus récent">
livecd gentoo # <i>cd /mnt/gentoo/usr</i>
livecd usr # <i>links http://www.gentoo.org/main/en/mirrors.xml</i>
<comment>(Choisissez un miroir, allez dans le répertoire snapshots/, sélectionnez
<b>portage-latest.tar.bz2</b> et appuyez sur D pour le télécharger)</comment>

<comment>(<b>Ou</b> téléchargez le directement avec wget sans choisir un miroir proche)</comment>
livecd gentoo # <i>cd /mnt/gentoo/usr</i>
livecd usr # <i>wget http://distfiles.gentoo.org/snapshots/portage-latest.tar.bz2</i>
</pre>

<pre caption="Désarchiver l'instantané Portage">
livecd usr # <i>time tar xjf portage-lat*</i>

real  0m40.523s
user  0m28.280s
sys   0m8.240s
</pre>

</body>
</section>
<section>
<title>Exécution du chroot</title>
<body>

<p>
Montez les systèmes de fichiers <path>/proc</path> et <path>/dev</path>, copiez
et remplacez le fichier <path>/etc/resolv.conf</path>, puis utilisez
<c>chroot</c> pour entrer dans votre environnement Gentoo.
</p>

<pre caption="Exécution de chroot">
livecd usr # <i>cd /</i>
livecd / # <i>mount -t proc none /mnt/gentoo/proc</i>
livecd / # <i>mount -o bind /dev /mnt/gentoo/dev</i>
livecd / # <i>cp -L /etc/resolv.conf /mnt/gentoo/etc/</i>
livecd / # <i>chroot /mnt/gentoo /bin/bash</i>
livecd / # <i>env-update</i> &amp;&amp; <i>source /etc/profile</i>
>>> Regenerating /etc/ld.so.cache...
</pre>

</body>
</section>

<section>
<title>Configuration de votre fuseau horaire</title>
<body>

<p>
Choisissez votre fuseau horaire en copiant le fichier approprié de
<path>/usr/share/zoneinfo</path> vers <path>/etc/localtime</path>.
</p>

<pre caption="Copier le fichier correspondant à votre fuseau horaire">
livecd / # <i>ls /usr/share/zoneinfo</i>
<comment>(Utilisation de Paris comme exemple.)</comment>
livecd / # <i>cp /usr/share/zoneinfo/Europe/Paris /etc/localtime</i>

livecd / # <i>date</i>
Wed Mar  8 00:46:05 CET 2006
</pre>

</body>
</section>
<section>
<title>Configurer votre nom d'hôte et votre nom de domaine</title>
<body>

<p>
Configurer votre nom de domaine dans <path>/etc/conf.d/hostname</path> et dans
<path>/etc/hosts</path>. Dans l'exemple suivant, nous utilisons <c>mybox</c>
comme nom d'hôte et <c>at.myplace</c> comme nom de domaine. Vous pouvez soit
éditer les fichiers de configuration avec <c>nano</c> ou utiliser les commandes
suivantes&nbsp;:
</p>

<pre caption="Configuration du nom d'hôte et du nom de domaine">
livecd / # <i>cd /etc</i>
livecd etc # <i>echo "127.0.0.1 mybox.at.myplace mybox localhost" > hosts</i>
livecd etc # <i>sed -i -e 's/HOSTNAME.*/HOSTNAME="mybox"/' conf.d/hostname</i>
<comment>(Utilisez le nom d'hôte défini précédemment et testez)</comment>
livecd etc # <i>hostname mybox</i>
livecd etc # <i>hostname -f</i>
mybox.at.myplace
</pre>

</body>
</section>

</included>
