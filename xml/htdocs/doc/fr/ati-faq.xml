<?xml version="1.0" encoding="UTF-8"?>
<!-- $Header: /var/cvsroot/gentoo/xml/htdocs/doc/fr/ati-faq.xml,v 1.27 2010/05/05 15:13:43 titefleur Exp $ -->
<!DOCTYPE guide SYSTEM "/dtd/guide.dtd">

<guide link="/doc/fr/ati-faq.xml" lang="fr">
<title>FAQ Gentoo concernant les cartes ATI</title>

<author title="Auteur">
  <mail link="lu_zero"/>
</author>
<author title="Correcteur">
  <mail link="peesh"/>
</author>
<author title="Correcteur">
  <mail link="blubber"/>
</author>
<author title="Correcteur">
  <mail link="nightmorph"/>
</author>
<author title="Traducteur">
  <mail link="neysx"/>
</author>

<abstract>
Cette FAQ devrait vous aider à éviter des problèmes fréquents rencontrés
avec X11 et le DRI lors de l'installation de cartes graphiques ATI.
</abstract>

<!-- The content of this document is licensed under the CC-BY-SA license -->
<!-- See http://creativecommons.org/licenses/by-sa/2.5 -->
<license/>

<version>1.8</version>
<date>2009-11-30</date>

<chapter>
<title>Support matériel</title>
<section>
<title>Ma carte ATI est-elle supportée ?</title>
<body>

<p>
De nombreuses cartes ATI sont supportées par <uri
link="http://www.freedesktop.org/Software/xorg">Xorg-X11</uri>, au moins pour
l'accélération 2D. Le support de la 3D est fourni soit par <uri
link="http://dri.sf.net">le projet DRI</uri> qui fait partie de Xorg-X11, soit
par les <uri
link="http://mirror.ati.com/support/drivers/linux/radeon-linux.html">pilotes
propriétaires</uri> fournis par ATI. Les pilotes propriétaires fournis par ATI
ne supportent que les cartes de série R600 et plus récentes. Les cartes plus
anciennes devront utiliser l'ancien pilote xorg-x11.
</p>

<table>
<tr>
  <th>Type de carte</th>
  <th>Nom générique</th>
  <th>Support</th>
</tr>
<tr>
  <ti>Rage128</ti>
  <ti>Rage128</ti>
  <ti>Xorg</ti>
</tr>
<tr>
  <ti>R100</ti>
  <ti>Radeon7xxx, Radeon64</ti>
  <ti>Xorg</ti>
</tr>
<tr>
  <ti>R200, R250, R280</ti>
  <ti>Radeon8500, Radeon9000, Radeon9200</ti>
  <ti>Xorg</ti>
</tr>
<tr>
  <ti>R300, R400</ti>
  <ti>Radeon 9500 - X850</ti>
  <ti>Xorg</ti>
</tr>
<tr>
  <ti>R500</ti>
  <ti>Radeon X1300 et plus</ti>
  <ti>Xorg</ti>
</tr>
<tr>
  <ti>R600</ti>
  <ti>Série des Radeon HD 2000</ti>
  <ti>ATI DRI, xorg</ti>
</tr>
<tr>
  <ti>RV670</ti>
  <ti>Les séries Radeon HD 3000</ti>
  <ti>ATI DRI, xorg</ti>
</tr>
<tr>
  <ti>RV770</ti>
  <ti>Série des Radeon HD 4000</ti>
  <ti>ATI DRI, xorg</ti>
</tr>
</table>

</body>
</section>
<section>
<title>
J'ai une carte All-In-Wonder/Vivo. Les fonctionnalités multimédia sont-elles
disponibles&nbsp;?
</title>
<body>

<p>
Les fonctionnalités multimédia de la carte sont disponibles grâce au <uri
link="http://gatos.sf.net">projet GATOS</uri>. Ces pilotes ont été intégrés
dans Xorg. Vous n'avez besoin de rien de spécial&nbsp;;
<c>x11-drivers/xf86-video-ati</c> marchera très bien.
</p>

</body>
</section>
<section>
<title>
Je n'utilise pas une architecture compatible x86. Que puis-je faire&nbsp;?
</title>
<body>

<p>
Le support X11 des plates-formes PPC et Alpha est pratiquement identique à celui
des x86. Pourtant, les pilotes propriétaires d'ATI ne fonctionnent pas sur PPC
ni sur Alpha, vous ne pourrez donc utiliser les fonctionnalités 3D du GPU R300.
Si vous avez une telle carte et voulez pouvoir l'utiliser sous X11,
adressez-vous à <uri link="http://www.ati.com">ATI</uri> et demandez-leur de
fournir les spécifications pour votre GPU. Les pilotes propriétaires pour AMD64
sont sortis, les utilisateurs d'AMD64 peuvent donc enfin bénéficier des mêmes
fonctionnalités que les utilisateurs de x86.
</p>

<impo>
Vous devez désactiver le support K8 IOMMU pour pouvoir activer le support
agpgart avec certains chipsets amd64.
</impo>

</body>
</section>
<section>
<title>J'ai un portable. Ma carte ATI Mobility est-elle supportée&nbsp;?</title>
<body>

<p>
Normalement, oui, mais vous devrez probablement écrire le fichier de
configuration vous-même à cause d'un problème avec les identifications OEM des
puces sur ces cartes.
</p>

</body>
</section>
</chapter>

<chapter>
<title>Installation</title>
<section>
<title>Paquets</title>
<body>

<p>
Il y a deux façons d'obtenir les pilotes pour votre carte ATI&nbsp;:
</p>

<ul>
 <li>L'ebuild <c>xorg-x11</c> installe l'implémentation X11.</li>
 <li>
   Le paquet <c>ati-drivers</c> permet d'installer les pilotes et les modules
   propriétaires d'ATI.
 </li>
</ul>

<p>
Si vous voulez utiliser le pilote AGP <e>propre à ATI</e> au lieu de celui du
noyau, le pilote agpgart du noyau et celui de votre chipset doivent soit être
compilés sous forme de modules, ou pas compilés du tout.
</p>

<note>
Veuillez lire le <uri link="/doc/fr/dri-howto.xml">Guide de l'accélération 3D
matérielle</uri> pour plus d'informations sur l'installation des pilotes pour
votre carte graphique ATI.
</note>

</body>
</section>
<section>
<title>Configuration</title>
<body>

<p>
Il est possible que vous n'ayez pas besoin de créer manuellement le fichier
<path>xorg.conf</path>, ou besoin de le modifier. Essayez d'abord sans. Vous
pouvez également utiliser l'option d'autoconfiguration de Xorg&nbsp;:
</p>

<pre caption="Autoconfiguration de X">
# <i>X -configure</i>
</pre>

<p>
Pour plus d'informations à propos du fichier de configuration
<c>xorg.conf</c>, veuillez consulter le <uri
link="/doc/fr/xorg-config.xml">Guide Gentoo de configuration de X</uri>.
</p>

<note>
Les utilisateurs du paquet <c>ati-drivers</c> peuvent aussi utiliser
<c>aticonfig</c>.
</note>

<note>
Les utilisateurs de machines PPC peuvent utiliser l'outil de configuration
<c>Xorgautoconfig</c> disponible dans l'ebuild <c>Xorgautoconfig</c>.
</note>

<impo>
Si vous utilisez les pilotes <c>ati-drivers</c>, vous devrez désactiver le
support de <c>radeonfb</c> (et probablement les autres pilotes de framebuffer)
dans la configuration de votre noyau, sinon cela va entrer en conflit avec le
framebuffer compilé dans les pilotes <c>ati-drivers</c>.
</impo>

</body>
</section>
<section>
<title>Utiliser OpenGL</title>
<body>

<p>
Après avoir installé, configuré et démarré Xorg, il est possible d'utiliser les
bibliothèques OpenGL d'ATI&nbsp;:
</p>

<pre caption="Exécuter eselect">
# <i>eselect opengl set ati</i>
</pre>

</body>
</section>
</chapter>

<chapter>
<title>Informations supplémentaires</title>
<section>
<body>

<p>
Veuillez lire le <uri link="/doc/fr/dri-howto.xml">Guide de l'accélération 3D
matérielle</uri> pour plus d'informations sur la configuration de votre carte
graphique ATI.
</p>

<p>
Vous trouverez de plus amples informations à propos de Gentoo et des pilotes
binaires pour ATI Radeon sur la page «&nbsp;<uri
link="http://odin.prohosting.com/wedge01/gentoo-radeon-faq.html">Wedge
Unofficial Gentoo ATI Radeon FAQ</uri>&nbsp;».
</p>

</body>
</section>
</chapter>
</guide>
