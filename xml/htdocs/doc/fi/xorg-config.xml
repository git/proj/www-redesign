<?xml version='1.0' encoding='UTF-8'?>

<!-- $Header: /var/cvsroot/gentoo/xml/htdocs/doc/fi/xorg-config.xml,v 1.10 2010/07/23 16:57:17 flammie Exp $ -->

<!DOCTYPE guide SYSTEM "/dtd/guide.dtd">

<guide link="/doc/fi/xorg-config.xml" lang="fi">

<title>KUINKA X-palvelin säädetään kuntoon</title>

<author title="Tekijä">
  <mail link="swift"/>
</author>
<author title="Tekijä">
  <mail link="nightmorph"/>
</author>
<author title="Vastuullinen kääntäjä">
  <mail link="flammie@gentoo.org">Flammie Pirinen</mail>
</author>

<abstract>
Xorg on graafinen käyttöympäristö joka toimii X-ikkunointipalvelinperiaatteella.
Tämä KUINKA-dokumentti selvittää mikä Xorg on, kuinka se asennetaan ja mitä
säätömahdollisuuksia siihen liittyy.
</abstract>

<!-- The content of this document is licensed under the CC-BY-SA license -->
<!-- See http://creativecommons.org/licenses/by-sa/2.5 -->
<license/>

<version>1.29</version>
<date>2010-05-23</date>


<chapter>
<title>Mikä on X-ikkunointipalvelin?</title>
<section>
<title>Graafinen vs. komentorivi</title>
<body>

<p>
Keskivertokäyttäjiä saattaa pelottaa ajatus, että tietokonetta käytetään
kirjoittamalla merkkipohjaisia komentoja. Mikseipä olisikaan kätevämpää sohia
hiirellä ja kliksutella tiensä läpi Gentoo-järjestelmän? Tokihan tämä onkin
mahdollista, Linuxeissa on itse asiassa laajakin valikoima välkkyviä ja
värikkäitä käyttöliittymiä joita voi käyttää missä tahansa yhteydessä.
</p>

<p>
Graafinen käyttöliittymä ei kuitenkaan ole muuta kuin yksi sovellus, jota
ajetaan järjestelmässä. Se <e>ei</e> kuulu Linuxin ytimeen millään tavalla,
eikä muuhunkaan järjestelmän sisäiseen osaan. Se on vain työkalu, jolla
saa käyttöön graafisen liitynnän työasemaansa.
</p>

<p>
<e>X-ikkunointijärjestelmä</e> on eräs standardi, joka määrittelee grafiikan
piirtämisen ja ikkunoiden liikuttelun ruuduilla ja syötelaitteiden kuten
hiirten ja näppisten käsittelyn sekä muitakin. X-ikkunointijärjestelmästä
käytetään usein lyhennettä <e>X11</e> tai vain <e>X</e>. X:iä voidaan käyttää
kaikissa Unix-tyylisissä käyttöjärjestelmissä.
</p>

<p>
Linux-käyttäjien suosima sovellus, joka tarjoaa graafisen käyttöliittymän
X11-standardin mukaisesti, on Xorg-X11. Xorg on XFree86-projektista forkattu
ikkunointijärjestelmä. Kun XFree86 käytti lisenssiä, joka saattaa olla
yhteensopimaton GPL:n kanssa, on ruvettu suosittelemaan Xorgin käyttöä sen
sijaan; XFree86-paketteja ei enää ole Portagepuussa.
</p>

</body>
</section>
<section>
<title>X.org-projekti</title>
<body>

<p>
<uri link="http://www.x.org">X.org</uri>-projekti ylläpitää vapaasti
levitettävää open source -toteutusta X11-järjestelmästä.
</p>

<p>
Xorg on rajapinta laitteiston ja graafisen ohjelmiston välillä. Sen lisäksi
Xorgissa on verkkotuki, jonka avulla ohjelmia voi ajaa toisessa koneessa kuin
niitä käyttää.
</p>

</body>
</section>
</chapter>
<chapter>
<title>Xorgin asennus</title>
<section>
<title>Kernelin asetukset</title>
<body>

<p>
Xorg käyttää oletuksena <c>evdev</c>-ajureita syöttölaitteille. Kernelissä pitää
siis olla <c>evdev</c>-tuki päällä. <c>Evdev</c>in saa päälle asetuksista (ks.
<uri link="/doc/en/kernel-config.xml">Gentoon kerneliasetusohje</uri> jollet
vielä tiedä miten se tehdään).
</p>

<pre caption="Evdevin lisääminen kerneliin">
Device Drivers ---&gt;
  Input device support ---&gt;
  &lt;*&gt;  Event interface
</pre>

</body>
</section>
<section>
<title>Make.conf-asetukset</title>
<body>

<p>
Ennen Xorgin asentamista kannattaa asettaa kaksi muuttujaa tiedostossa
<path>/etc/make.conf</path>.
</p>

<p>
Ensimmäinen on <c>VIDEO_CARDS</c>, jolla kerrotaan mitkä näyttöajurit ovat
käytössä. Yleisimmät asetukset ovat <c>nvidia</c> nVidian laitteille
tai <c>fglrx</c> ATin Radeoneille. Nämä ovat nVidian ja ATin suljetut
ajuriversiot. Avoimet, vapaat vaihtoehdot ovat <c>nv</c>, jossa ei ole
ollenkaan 3D-kiihdytystä. Vapaat <c>radeon</c>- ja <c>radeonhd</c>-ajurit
sisältävät myös jokseenkin samanlaisen 3D-kiihdytystuen kuin suljettu
<c>fglrx</c>. <c>Intel</c>iä voi käyttää kaikissa laitteistoissa joissa
on Intelin näyttöajuri.
<c>VIDEO_CARDS</c>-asetukseen voi panna niin monta ajuria kuin haluaa, ja ne
erotellaan välilyönnein.
</p>

<p>
Toinen asetus on <c>INPUT_DEVICES</c>, joka kertoo mitkä syöttölaiteajurit
ovat käytössä. Yleensä asetus <c>evdev</c> riittää hyvin. Mutta jos
tarvitset muita ajureita kuten Synaptics-tyylisen kosketuslaatan, lisää se
ajureihin.
</p>

<p>
Lisää tarvittavat ajurit tiedostoon <path>/etc/make.conf</path>:
</p>

<pre caption="Ajurien lisäys make.confiin">
<comment>(Hiirelle, näppäimistölle ja synaptics-laatalle)</comment>
INPUT_DEVICES="evdev synaptics"
<comment>(nVidian näyttölaitteille)</comment>
VIDEO_CARDS="nvidia"
<comment>(ATin Radeoneille)</comment>
VIDEO_CARDS="radeon"
</pre>

<note>
Tarkemmat ohjeet nVidian ja ATin laitteiden käytöstä löytyvät
<uri link="/doc/fi/nvidia-guide.xml">nVidia-oppaasta</uri> ja
<uri link="/doc/en/ati-faq.xml">Gentoo Linux ATI FAQ:sta</uri>.
Jos et tiedä mikä ajuri kannattaa valita, lue nämä oppaat lisätietoa varten.
</note>

<p>
Jos asetukset eivät toimi, suorita <c>emerge -pv
xorg-server</c>, tarkista asetukset ja valitse sellaiset mitkä sopivat
järjestelmääsi. Esimerkki on yhdistelmälle näppäimistölle, hiirelle,
kosketuslaatalle ja Radeon-näytönohjaimelle.
</p>

<pre caption="Ajurivaihtoehtojen tarkastelu">
# <i>emerge -pv xorg-server</i>

These are the packages that would be merged, in order:

Calculating dependencies... done!
[ebuild   R   ] x11-base/xorg-server-1.6.3.901-r2  USE="hal nptl xorg -debug
-dmx -ipv6 -kdrive -minimal -sdl -tslib" 0 kB
[ebuild   R   ]  x11-base/xorg-drivers-1.6  INPUT_DEVICES="evdev synaptics
-acecad -aiptek -citron -elographics -fpit -hyperpen -joystick -keyboard -mouse
-mutouch -penmount -tslib -virtualbox -vmmouse -void -wacom"
VIDEO_CARDS="radeon -apm -ark -ast -chips -cirrus -dummy -epson -fbdev -fglrx
(-geode) -glint -i128 (-i740) (-impact) (-imstt) -intel -mach64 -mga -neomagic
(-newport) -nv -nvidia -r128 -radeonhd -rendition -s3 -s3virge -savage
-siliconmotion -sis -sisusb (-sunbw2) (-suncg14) (-suncg3) (-suncg6) (-sunffb)
(-sunleo) (-suntcx) -tdfx -tga -trident -tseng -v4l (-vermilion) -vesa -via
-virtualbox -vmware (-voodoo) (-xgi)" 0 kB
</pre>

<p>
Muuttujan asettamisen jälkeen Xorgin voi asentaa.
</p>

<pre caption="Xorgin asennus">
# <i>emerge xorg-server</i>
</pre>

<note>
On myös mahdollista asentaa metapaketti <c>xorg-x11</c> pelkän xorg-serverin
sijaan. Käytännössä, <c>xorg-x11</c> ja <c>xorg-server</c> ovat sama asia.
<c>Xorg-x11</c> kuitenkin riippuu useammasta paketista, kuten suuresta
määrästä fontteja täyden kielituen saavuttamiseksi, mitä useimmat eivät
tarvinne.
</note>


<p>
Kun asennus on valmis, ympäristömuuttujat pitää päivittää ennen jatkamista.
Ympäristön päivitys tapahtuu tavalliseen tapaan komennoilla <c>env-update</c>
ja <c>source /etc/profile</c>.
</p>

<pre caption="Ympäristömuuttujien päivitys">
# <i>env-update</i>
# <i>source /etc/profile</i>
</pre>

<p>
  Seuraavaksi asennetan HAL-palvelu, ja asetetaan se käynnistymään
  automaattisesti. Tämä on tarpeellinen X:ää varten, muuten syötelaitteita
  ei tunnisteta oikein. Lisää HALista
  <uri link="#using_hal">HAL-kappaleessa</uri>.
</p>
<pre caption="Starting HAL">
 # <i>/etc/init.d/hald start</i>
 # <i>rc-update add hald default</i>
</pre>

</body>
</section>
</chapter>
<chapter>
<title>Xorgin säädöt</title>
<section id="using_hal">
<title>HAL-asetukset</title>
<body>

<p>
Tuoreet X-palvelimet toimivat suoraan heittämällä ilman erillisiä
asetustiedostoja.
</p>

<p>
Kannattaa ensimmäisenä kokeilla <uri link="#using_startx">käynnistää X</uri>
ilman mitään asetuksia kuten <path>/etc/X11/xorg.conf</path>ia.
</p>

<p>
Jos Xorg ei käynnisty (esimerkiksi kun on vikaa näytön tai näppäimistön
asetuksissa), pitää korjata asetustiedostoja.
</p>

<p>
Oletuksena Xorg käyttää HALia laitteistojen tunnistamiseen ja asetuksiin.
</p>

<p>
HALissa on paljon laitteistoasetuksia, joita kutsutaan policyiksi. Näitä
asetuksia löytää hakemistosta <path>/usr/share/hal/fdi/policy/</path>.
Kopiomalla sopivat hakemistoon <path>/etc/hal/fdi/policy/</path> saa tehtyä
enimmät asetukset
</p>

<impo>
Tiedotsoja hakemistossa <path>/usr/share/hal/fdi/</path> ei kannata muokata!
Ne pitää kopioida ensin <path>/etc</path>:hen ensin.
</impo>

<p>
Esimerkiksi näppäimistön ja hiiren asetukset saisi mukautettua kopioimalla
<path>/etc/hal/fdi/policy/</path>-hakemistoon:
</p>

<pre caption="HALin policyjen käyttö">
# <i>cp /usr/share/hal/fdi/policy/10osvendor/10-input-policy.fdi /etc/hal/fdi/policy</i>
# <i>cp /usr/share/hal/fdi/policy/10osvendor/10-x11-input.fdi /etc/hal/fdi/policy</i>
</pre>

<p>
HAL-policyja on hakemistossa <path>/usr/share/hal/fdi/</path>. Esimerkiksi
kannettaville, muistiasemille, virransäästöön, jne. Ne tarvitsee vain kopioida
hakemistoon <path>/etc/hal/fdi/policy/</path>.
</p>

<p>
Voit muokata kopioituja tiedostoja hakemistossa
<path>/etc/hal/fdi/policy</path> mielesi mukaan.
</p>

<impo>
  <e>Jokainen</e> muutos vaatii HALin uudelleenkäynnistyksen toteutuakseen:
  <c>/etc/init.d/hald restart</c>.
</impo>


<p>
Eräs tyypillinen toiminto on X:n tappaminen näppäilullä Ctrl-Alt-Backspace.
Tätä tarvitaan usein jos X jumittuu. Se on kevyempi vaihtoehto
uudelleenkäynnistykselle esim. Ctrl-Alt-Delillä
</p>

<p>
Monissa uusissa X-versioissa tämä on poistettu käytöstä, ja se pitää lisätä
käyttöön kopioimalla <path>10-x11-input.fdi</path> hakemistoon
<path>/etc/hal/fdi/policy</path> ja muuttamalla seuraavasti:
</p>

<pre caption="HAL-policyn 10-x11-input.fdi muokkaus">
<comment>(Avaa jossain muokkaimessa)</comment>
# <i>nano -w /etc/hal/fdi/policy/10-x11-input.fdi</i>
<comment>(Etsi "input.keys")</comment>
  &lt;match key="info.capabilities" contains="input.keys"&gt;
<comment>(Lisää "terminate"-merge)</comment>
    &lt;match key="info.capabilities" contains="input.keys"&gt;
      &lt;merge key="input.x11_driver" type="string"&gt;keyboard&lt;/merge&gt;
      <i>&lt;merge key="input.xkb.options" type="string"&gt;terminate:ctrl_alt_bksp&lt;/merge&gt;</i>
      &lt;match key="/org/freedesktop/Hal/devices/computer:system.kernel.name"
             string="Linux"&gt;
        &lt;merge key="input.x11_driver" type="string"&gt;evdev&lt;merge&gt;
      &lt;/match&gt;
    &lt;/match&gt;
</pre>

<p>
Nyt X:n voi taas tappaa halutessaan. X:n tappaminen saattaa usein sotkea auki
olevia ohjelmia, joten sitä ei kannata käyttää jollei ole pakko.
</p>

<p>
Yleensä pelkästään HAL-tiedostoilla saa tarvittavan toimivan X:n aikaan. Jos
X ei käynnisty, pitää muokata myös <path>xorg.conf</path>ia.
</p>

</body>
</section>
<section>
<title>Tiedosto xorg.conf</title>
<body>

<note>
<path>Xorg.conf</path>in muokkaaminen on nykyään lähinnä viimeinen mahdollisuus,
jollei mikään muu auta. Yleensä on parempi tehdä asetuksia HAL-tiedostojen
kautta ja antaa X:n valita hyvät asetukset.
</note>

<p>
Xorgin asetustiedosto on <path>xorg.conf</path> hakemistossa
<path>/etc/X11</path>. Xorgissa on oletusasetusto mukana tiedostossa
<path>/etc/X11/xorg.conf.example</path>. Sitä voi käyttää pohjana omien
asetusten säätämisessä. Se on selkeästi kommentoitu, mutta jos tarvitset
lisäohjeita, niin niitähän löytyy ohjesivuilta:
</p>

<pre caption="Xorg.confin ohjesivut">
$ <i>man 5 xorg.conf</i>
</pre>

</body>
</section>
<section>
<title>Xorg.confin automaattinen luonti</title>
<body>

<p>
Xorg osaa itsekin arvata valtaosan asetuksista puolestasi. Useimmiten
tarvitsee vain muuttaa muutamia rivejä, että saa haluamansa resoluution
käyttöön. Tarkempiakin säätöjä voi tehdä, mutta niistä lisää kappaleen lopussa.
Mutta ensin luodaan Xorgin asetustiedosto.
</p>

<pre caption="Xorg.confin luonti">
# <i>Xorg -configure</i>
</pre>

<p>
Lue huolella myös Xorgin ruudulle tulostamat ohjeet, jos niissä kerrotaan
virheistä, pitää <path>xorg.conf</path>ia muokata käsin. Jollei virheitä
tapahtunut, ohjelma kertoo kirjoittaneensa uudet asetukset
tiedostoon <path>/root/xorg.conf.new</path>, ja näitä voi nyt testata:
</p>

<pre caption="Xorg.conf.new'n testaus">
# <i>X -retro -config /root/xorg.conf.new</i>
</pre>

<p>
Jos kaikki toimii, ruudulle ilmestyy mustavalkoinen punoskuvio. Varmista että
hiiri toimii odotuksen mukaisesti ja resoluutio näyttää hyvältä. Jos hiiri
kertoo virheitä tiedostossa /dev/mouse, kannattaa kokeilla asetusta
/dev/input/mice.
Poistuminen onnistuu painamalla Ctrl-Alt-Backspace.
</p>

</body>
</section>
<section>
<title>Xorg.confin kopiointi</title>
<body>

<p>
Kun <path>xorg.conf.new</path> kopioidaan <path>/etc/X11/xorg.conf</path>;ksi,
se luetaan automaattisesti X:n käynnistyessä, eikä tarvita enää
-config-valitsinta. Nyt käynnistyksen voi suorittaa komennolla <c>X</c> tai
<c>startx</c>.
</p>

<pre caption="Xorg.confin kopiointi">
# <i>cp /root/xorg.conf.new /etc/X11/xorg.conf</i>
</pre>

</body>
</section>
<section id="using_startx">
<title>Startx:n käyttö</title>
<body>

<p>
Käynnistetään nyt palvelin <c>startx</c>:llä. <c>Startx</c> on skripti,
joka käynnistää <e>X session</e>, eli ajaa X-palvelimen ja käynnistää
siihen joitain graafisia sovelluksia. Sovellukset se päättää seuraavan kaavan
mukaan:
</p>

<ul>
  <li>
    Jos kotihakemistossa on <path>.xinitrc</path>, suoritetaan sen sisältö.
  </li>
  <li>
    Muutoin suoritetaan ympäristömuuttujan XSESSION nimeämä sessio, joka
    löytyy hakemistosta <path>/etc/X11/Sessions/</path>.
    XSESSIONin voi muuttaa tiedostosta <path>/etc/env.d/90xsession</path>.
    Esimerkiksi roottina <c>echo XSESSION=Xfce4 > /etc/env.d/90xsession</c>
    asettaa kaikille oletukseksi XFCE:n.
  </li>
</ul>

<pre caption="X:n käynnistys">
$ <i>startx</i>
</pre>

<p>
X:n voi tappaa myös näppäilyllä Ctrl-Alt-Backspace, tällöin X kuitenkin sammuu
suoraan ja varmistuksetta, mikä ei aina ole odotettua.
</p>

<p>
  Jollei ikkunamanageria ole asennettu, käynnistys näyttää vain mustan ruudun.
  Sama tapahtuu myös virhetilanteissa, joten saattaa olla hyödyllistä asentaa
  <c>twm</c> ja <c>xterm</c> <e>ihan vain testaamista varten</e>.
</p>

<p>
  Kun ne on asennettu, <c>startx</c> käynnistää ikkunoita toimiessaan oikein.
  Jos kaikki toimii voi testaussovellukset poistaa komennolla
  <c>emerge --unmerge twm xterm</c>. Nämä sovellukset eivät ole välttämättömiä
  työpöytäympäristöä ajatellen.
</p>

</body>
</section>
</chapter>
<chapter>
<title>X:n asetusten säätäminen</title>
<section>
<title>Resoluutioasetukset</title>
<body>

<p>
Jos resoluutio näyttää väärältä, asetustiedostossa on kaksi kohtaa
tarkistettavaksi. Ensinnä <e>Screen</e>-osiossa on luettelo resoluutioista,
joita X-palvelin käyttää. Oletuksena tämä osio voi olla myös tyhjä, ja silloin
Xorg arpoo resoluutiot osiosta <e>Monitor</e>.
</p>

<p>
Käytännössä Xorg tarkistaa <e>Monitor</e>-osion <c>HorizSync</c> ja
<c>VertRefresh</c> saadakseen toimivat resoluutiot. Nämä asetukset on hyvä
aluksi jättää sellaisikseen, ja vasta jos <e>Screen</e>-osion muutokset
eivät toimi kannattaa näitä muokata. Näiden arvot pitää katsoa monitorin
ohjekirjasta.
</p>

<warn>
<b>Älä</b> muuttele monitoriasetuksia ilman monitorin ohjekirjan tarkkoja
tietoja, väärät arvot johtavat parhaimmillaan taajuusvirheilmoituksiin ja
pahimmillaan hajonneisiin näyttöihin.
</warn>

<p>
Muokataan aluksi resoluutioarvoja. Seuraavassa esimerkissä
lisätään <path>/etc/X11/xorg.conf</path>iin
<c>Modes</c>-asetukset ja <c>DefaultDepth</c>, jotta X käynnistyisi
1440×900-resoluutiossa 24-bittisellä värisyvyydellä. Esimerkkejä voi ja
kannattaa muuttaa mieltymysten mukaisiksi.
</p>

<pre caption="/etc/x11/xorg.confin säätäminen">
Section "Screen"
  Identifier  "Default Screen"
  Device    "RadeonHD 4550"
  Monitor   "Generic Monitor"
  <i>DefaultDepth  24</i>
  <comment># Ohitettu joitain rivejä</comment>
  SubSection "Display"
    Depth   24
    <i>Modes   "1440x900"</i>
  EndSubSection
EndSection
</pre>

<p>
Käynnistä X testataksesi että resoluutio on oikea ja toimii.
</p>

</body>
</section>
<section>
<title>Näppäimistöasetukset</title>
<body>

<p>
Suomalaisia näppäimistöasetuksia varten joutuu kopioimaan tiedoston
<path>/usr/share/doc/hal-*/*/use-estonian-layout.fdi.bz2</path> sisällön
hakemistoon <path>/etc/hal/fdi/policy/10-xinput-configuration.fdi</path>:
</p>

<pre caption="Olemassaolevan asetusten sisällön kopiointi">
# <i>bzcat /usr/share/doc/hal-*/*/use-estonian-layout.fdi > /etc/hal/fdi/policy/10-xinput-configuration.fdi</i>
</pre>

<p>
Seuraavaksi pitää muokata tiedostosta <path>10-xinput-configuration.fdi</path>
viro (ee) pois ja suomi (fi) tilalle.
</p>

<p>
Lopuksi pitää käynnistää hal uudestaan komennolla
<c>/etc/init.d/hald restart</c> roottina, ja varmistaa että asetukset toimivat.
</p>

</body>
</section>
<section>
<title>Loppuasetukset</title>
<body>

<p>
Komennolla <c>startx</c> voi taas varmistua lopputuloksesta. Nyt kasassa pitäisi
olla toimiva X-ympäristö. Seuraava askel olisi asentaa työpöytäympäristö, johon
löydät apua muista ohjeista.
</p>

</body>
</section>
</chapter>
<chapter>
<title>Lisätietoa</title>
<section>
<title>Xorg.confin säätö</title>
<body>

<p>
Ohjesivulla <c>man xorg.conf</c> ja <c>man evdev</c> on tiivis, mutta täysi
luettelo asetustiedoston sisältömahdollisuuksista. Se kannattaa pitää esillä
asetustiedostoa muokattaessa.
</p>

<p>
Tiedosto <path>/etc/X11/xorg.conf.example</path> sisältää myös hyviä
esimerkkejä ja sitä kannattanee käyttää pohjana jos tekee omia asetuksia
tiedostoon <path>xorg.conf</path>.
</p>

<p>
Lisätietoja löytyy <uri link="http://www.x.org/wiki/FAQ">X.orgin FAQ:sta</uri>
sekä muusta sivuston dokumentaatiosta.
</p>

<p>
Netissä on myös paljon tietoa asetuksista, alla on muutama
lueteltuna, mutta <uri link="http://www.google.com">Googlaamalla</uri>
löytyy lisää.
</p>

</body>
</section>
<section>
<title>Muut lähteet</title>
<body>

<p>
Muita ohjeita löytyy
<uri link="/doc/fi/?catid=desktop">Työpöytäopaskokoelmasta</uri>.
</p>

<p>
Päivitysohjeet xorg-serverin 1.6:ta vanhemmasta versiosta löytyy sivulta
<uri
link="/proj/en/desktop/x/x11/xorg-server-1.6-upgrade-guide.xml">xorg
migration guide</uri>.
</p>

</body>
</section>
</chapter>
</guide>
