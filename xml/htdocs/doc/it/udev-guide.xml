<?xml version='1.0' encoding="UTF-8"?>
<!DOCTYPE guide SYSTEM "/dtd/guide.dtd">
<!-- $Header: /var/cvsroot/gentoo/xml/htdocs/doc/it/udev-guide.xml,v 1.32 2010/05/19 18:10:02 scen Exp $ -->

<guide lang="it">
<title>Guida a udev su Gentoo</title>

<author title="Autore">
  <mail link="swift"/>
</author>
<author title="Contributi">
  <mail link="greg_g"/>
</author>
<author title="Redazione">
  <mail link="nightmorph"/>
 </author>
<author title="Traduzione">
  <mail link="scen"/>
</author>

<abstract>
Questo documento spiega cos'è e come si può usare udev su Gentoo.
</abstract>

<!-- The content of this document is licensed under the CC-BY-SA license -->
 <!-- See http://creativecommons.org/licenses/by-sa/2.5 -->
<license/>

<version>6</version>
<date>2010-05-14</date>

<chapter>
<title>Cos'è udev?</title>
<section>
<title>La directory /dev</title>
<body>

<p>
Quando gli utenti linux parlano del proprio hardware in presenza di persone che
pensano linux sia una sorta di strano virus od una nuova marca di caffè,
l'utilizzo di termini come "slash dev slash foo" vengono accolti con strani
sguardi. Ma alcuni fortunati utenti (che probabilmente includono anche chi sta
leggendo) utilizzano <path>/dev/hda1</path> per indicare velocemente il proprio
HD master sul primo controller IDE...giusto?
</p>

<p>
Tutti sanno cosa sia un file di periferica. Qualcuno sa anche perché questi
file hanno diversi numeri quando si da  un'occhiata più approfondita in
<path>/dev</path> con un <c>ls -l</c>. Ma quello che si dà per scontato è che
quando ci si riferisce al primo HD sul primo controller IDE si parla di
<path>/dev/hda</path>.
</p>

<p>
Si pensi alle periferiche PlugNPlay come le penne USB, IEEE1394, periferiche
PCI hot-swappable...qual è la prima periferica? E per quanto tempo? Come si
chiameranno le altre periferiche quando la prima verrà sconnessa? Questo come
influenzerà i flussi di dati in corso? Non sarebbe divertente che il proprio
lavoro venisse stampato, invece che sulla nuova stampante laser, sulla propria
sgangherata stampate ad aghi, solamente perché qualcuno ha deciso di staccare la
spina alla stampante laser che era vista dal sistema come primaria?
</p>

<p>
Le finalità del progetto udev sono sia interessanti che necessarie:
</p>

<ul>
  <li>Gira in spazio utente</li>
  <li>Crea e rimuove dinamicamente i file di periferica</li>
  <li>Assegna i nomi alle periferiche</li>
  <li>Mette a disposizione un API utilizzabile nello spazio utente</li>
</ul>

<p>
Per fornire queste caratteristiche, udev è sviluppato in tre diversi progetti:
<e>namedev</e>, <e>libsysfs</e> e, ovviamente, <e>udev</e>.
</p>

</body>
</section>
<section>
<title>namedev</title>
<body>

<p>
namedev consente di definire i nomi delle periferiche indipendentemente dal
programma udev. Questo permette di avere uno modello di assegnazione dei nomi
molto flessibile ed uno schema dei nomi sviluppato da diverse entità. Questo
sistema per assegnare i nomi ai dispositivi offre un'interfaccia standard con
cui udev può interfacciarsi
</p>

<p>
Attualmente namedev fornisce un solo modello per l'assegnazione dei nomi ai
dispositivi, fornito da LANANA, modello attualmente usato dalla maggior parte
dei sistemi Linux e quindi piuttosto indicato per la maggior parte degli utenti
Linux.
</p>

<p>
namedev usa un procedimento suddiviso in cinque passi per assegnare un nome a
una data periferica. Se si trova un nome per la periferica durante uno di
questi passi, viene usato questo nome e la procedura si interrompe. I cinque
passi, nell'ordine, sono:
</p>

<ul>
  <li>etichetta o numero seriale</li>
  <li>numero della periferica sul bus</li>
  <li>topologia del bus</li>
  <li>nome assegnato staticamente</li>
  <li>nome fornito dal kernel</li>
</ul>

<p>
Il primo passaggio (<e>etichetta o numero seriale</e>) controlla se la
periferica ha un identificatore univoco.  Per esempio i dispositivi USB hanno
un numero seriale unico; le periferiche SCSI hanno un UUID unico. Se nel file
di configurazione in uso esiste una regola che assegna il nome al dispositivo
in base a questo identificatore, namedev applica la regola e assegna il nome
alla periferica.
</p>

<p>
Successivamente (<e>numero della periferica sul bus</e>) viene controllato il
numero con cui il dispositivo viene identificato sul bus . Per esempio i numeri
assegnati ai dispositivi sul bus PCI cambiano di rado durante il ciclo di vita
di un sistema, per cui questo si può considerare un buon metodo per assegnare
un nome a una periferica PCI (non-hot-swappable).
</p>

<p>
Si può assegnare un nome a un dispositivo anche sulla base di come questo è
fisicamente collegato al bus (<e>topologia del bus</e>), anche questo è un
metodo abbastanza valido per identificare univocamente una periferica, almeno
fino a quando non viene variata la disposizione fisica dei dispositivi
all'interno del sistema.
</p>

<p>
È possibile definire una regola che sostituisca staticamente (<e>nome
assegnato staticamente</e>), al nome fornito dal kernel, una stringa
arbitraria.
</p>

<p>
Se non esistono regole per assegnare un nome a un dispositivo, il comportamento
predefinito di udev è di assegnare alla periferica il nome fornito dal kernel
(<e>nome fornito dal kernel</e>). Nella maggior parte dei casi questo è
sufficiente visto che il nome corrisponde a quello assegnato.
</p>

</body>
</section>
<section>
<title>libsysfs</title>
<body>

<p>
udev interagisce con il kernel tramite il filesystem sysfs. Il progetto
libsysfs mette a disposizione un API per accedere alle informazione fornite
dal filesystem sysfs. Questo consente di poter interrogare qualunque tipo di
dispositivo hardware senza doversi preoccupare di che tipo di hardware si
tratti.
</p>

</body>
</section>
<section>
<title>udev</title>
<body>

<p>
Ogni volta che il kernel riceve un evento nella struttura dei dispositivi,
chiede ad udev di controllare. udev segue le regole contenute nella directory
<path>/etc/udev/rules.d/</path>, successivamente usa le informazioni date dal
kernel per eseguire le azioni necessarie nella struttura di <path>/dev</path>
(creando o eliminando file di periferica).
</p>

</body>
</section>
</chapter>

<chapter>
<title>Usare udev su Gentoo</title>
<section>
<title>Requisiti</title>
<body>

<p>
udev è stato pensato per funzionare in combinazione con i kernel 2.6 (come per
esempio <c>gentoo-sources</c> con il profilo di default 2007.0). Oltre ad avere
installato un kernel 2.6 è necessario avere installato sul proprio sistema anche
la versione più recente di <c>sys-apps/baselayout</c>.
</p>

<pre caption="Installare udev">
# <i>emerge udev</i>
</pre>

<p>
Ricordarsi inoltre di attivare le seguenti opzioni nel kernel:
</p>

<pre caption="Opzione del kernel richieste">
File systems ---&gt;
    [*] Inotify file change notification support
    [*]   Inotify support for userspace
  Pseudo filesystems ---&gt;
    [*] /proc file system support
    [*] Virtual memory file system support (former shm fs)
</pre>

<p>
Se si utilizza <c>genkernel</c> non occorre fare nulla di speciale. Genkernel
imposterà udev in modo predefinito.
</p>

</body>
</section>

<section>
<title>Configurazione</title>
<body>

<p>
Se si è voluto utilizzare le impostazioni specifiche per Gentoo di udev creata
per semplificare la vita all'utente allora la procedura è terminata. Gentoo
utilizzerà udev ma continuerà a mantenere un link statico in <path>/dev</path>
in modo che non mancherà mai il nodo ad un particolare dispositivo. Gli script
init di Gentoo non avvieranno più il servizio devfsd e disattiveranno devfs al
boot.
</p>

<p>
Ma se si vuole un sistema udev puro, senza modifiche, cosa che peraltro è
contemplata dai suoi sviluppatori (incluso il problema dei dispositivi mancanti
visto che ancora udev non li supporta), proseguire nella lettura.
</p>

<p>
La prima cosa da fare è quella di disattivare la regola che salva la struttura
dei dispositivi in un archivio tarball allo spegnimento del PC: modificare la
variabile <c>RC_DEVICE_TARBALL</c> in <path>/etc/conf.d/rc</path> e impostarla a
<c>no</c>:
</p>

<pre caption="/etc/conf.d/rc">
RC_DEVICE_TARBALL="no"
</pre>

<p>
Se si è lasciato attivo il supporto a devfs nel kernel lo si può disattivare
nel file di configurazione del bootloader aggiungendo il parametro del kernel
<c>gentoo=nodevfs</c>. Se invece si volesse riattivare il supporto a devfs
disabilitando udev, si dovrà passare al kernel il parametro
<c>gentoo=noudev</c>.
</p>

</body>
</section>
</chapter>

<chapter>
<title>Problemi Noti</title>
<section>
<title>File di periferica mancanti all'avvio</title>
<body>

<p>
Se non si riesce ad avviare il sistema con successo perché si ottiene un errore
che dice che il file <path>/dev/null</path> non è stato trovato, o perché la
console iniziale non esiste, il problema è dovuto alla mancanza di alcuni file
che devono essere presenti nel sistema <e>prima</e> che
<path>/dev</path> venga montato e udev inizi a gestirlo. Questo è un errore
piuttosto comune sui sistemi Gentoo installati usando supporti vecchi.
</p>

<p>
Avendo installato <c>sys-apps/baselayout-1.8.12</c>, o più recente, questo
inconveniente dovrebbe essere alleviato dal fatto che il sistema dovrebbe essere
comunque in grado di avviarsi. Comunque, per evitare noiosi avvisi, è
sufficiente creare i file di periferica mancanti, come descritto in seguito.
</p>

<p>
Per visualizzare i dispositivi che sono disponibili prima che il filesystem
<path>/dev</path> venga montato bisogna eseguire i seguenti comandi:
</p>

<pre caption="Elenco dei dispositivi disponibili all'avvio">
# <i>mkdir test</i>
# <i>mount --bind / test</i>
# <i>cd test/dev</i>
# <i>ls</i>
</pre>

<p>
I dispositivi necessari per avviare correttamente il sistema sono
<path>/dev/null</path> e <path>/dev/console</path>. Se non vengono
visualizzati durante il test precedente vanno creati manualmente. I comandi
seguenti vanno eseguiti nella directory <path>test/dev/</path> creata in
precedenza:
</p>

<pre caption="Creare i file di periferica mancanti">
# <i>mknod -m 660 console c 5 1</i>
# <i>mknod -m 660 null c 1 3</i>
</pre>

<p>
Quando si sono impartiti questi comandi è necessario ricordarsi di smontare la
directory <path>test/</path>:
</p>

<pre caption="Smontare la directory test/">
# <i>cd ../../</i>
# <i>umount test</i>
# <i>rmdir test</i>
</pre>

</body>
</section>
<section>
<title>udev e nvidia</title>
<body>

<p>
Nel caso si usassero i driver proprietari di nVidia e il server grafico X non
partisse, in un sistema configurato per utilizzare solo udev, bisogna
assicurarsi che:
</p>

<ul>
  <li>
    il modulo <c>nvidia</c> sia elencato nel file
    <path>/etc/modules.autoload.d/kernel-2.6</path>
  </li>
  <li>
    sia installata una versione di baselayout uguale o maggiore di
    <c>sys-apps/baselayout-1.8.12</c>
  </li>
</ul>

</body>
</section>
<section>
<title>Assegnazione differente dei nomi tra DevFS e udev</title>
<body>

<p>
Nonostante l'intento di mantenere inalterati i nomi assegnati ai dispositivi da
entrambi i sistemi di gestione, in qualche caso può capitare che ci siano
delle differenze tra il nome assegnato da DevFS e quello assegnato da udev.
</p>

<p>
Un caso conosciuto riguarda il controller RAID HP Smart Array 5i (più
precisamente il modulo del kernel <c>cciss</c>). Con udev, il dispositivo viene
chiamato <path>/dev/cciss/cXdYpZ</path> dove X, Y e Z sono numeri. Con devfs, il
dispositivo si chiama <path>/dev/hostX/targetY/partZ</path> o é un link
simbolico a <path>/dev/cciss/cXdY</path>.
</p>

<p>
In questo caso é necessario ricordarsi di modificare il file
<path>/etc/fstab</path> e i file di configurazione del bootloader.
</p>

<p>
La stessa cosa succede con molti dei link simbolici che venivano creati in
<path>/dev</path>, come per esempio <path>/dev/mouse</path>, che ora
<c>udev</c> non crea più. Assicurarsi che nel proprio file di configurazione
di X il percorso del dispositivo del mouse punti ad un file esistente.
</p>

<p>
Un'altro problema è nella differenza di denominazione dei terminali tra devfs e
udev. Mentre in devfs i terminali vengono chiamati <c>tty</c>, udev li chiama
<c>vc</c> e <c>tty</c>. Ciò può dare dei problemi nel caso siano state applicate
delle restrizioni, attraverso l'uso di  <path>/etc/securetty</path>, riguardo le
login da utente root. Assicurarsi che entrambe le voci <c>tty1</c> e <c>vc/1</c>
siano elencate nel file di configurazione <path>/etc/securetty</path>, per
permettere a root di effettuare il login tramite la console.
</p>

</body>
</section>
<section>
<title>Rinominare i dispositivi a blocchi</title>
<body>

<p>
Le versioni recenti di udev (dalla 104 in poi) unitamente alle nuove versioni
del kernel (2.6.19 e successive) potrebbero cambiare i nomi di dispositivo dei
propri dischi, a causa di una modifica nell'implementazione del kernel riguardo
a libata: per esempio una periferica CD-RW mappata in precedenza come
<path>/dev/hdc</path> potrebbe cambiare in <path>/dev/sr0</path>. Mentre questo
di norma non rappresenta un problema, potrebbe dare dei problemi a qualche
applicazione che ha codificato internamente la ricerca dei dispositivi in altre
locazioni. Per esempio, <c>media-sound/rip</c> si aspetta di trovare i dischi in
<path>/dev/cdrom</path>, e ciò diventa un problema se si usa un nuovo kernel e
udev rinomina il proprio dispositivo in <path>/dev/cdrom1</path>.
</p>

<p>
Per aggirare questi problemi, bisogna modificare
<path>/etc/udev/rules.d/70-persistent-cd.rules</path> e assegnare il nome
corretto al dispositivo.
</p>

<p>
Per ulteriori informazioni riguardanti la scrittura di regole per udev,
assicurarsi di leggere la <uri
link="http://www.reactivated.net/udevrules.php">guida</uri> di Daniel Drake.
</p>

</body>
</section>
<section>
<title>Rinominare i dispositivi di rete</title>
<body>

<p>
Talvolta disconnettendo e riconnettendo un disposirivo di rete (come una scheda
USB WiFi) essa può venire rinominata ogni volta, incrementandone il numero di
un'unità.
</p>

<p>
Quando ciò accade, la si vedrà diventare <c>wlan0</c>, <c>wlan1</c>,
<c>wlan2</c>, ecc. Ciò avviene perchè udev sta aggiungendo delle regole
aggiuntive al proprio file delle regole, invece di ricaricare le regole
esistenti. Siccome udev controlla la propria directory delle regole tramite
inotify, bisogna avere il supporto a inotify nella propria configurazione del
kernel:
</p>

<pre caption="Abilitare il supporto a nel kernel">
File systems ---&gt;
    [*] Inotify file change notification support
    [*]   Inotify support for userspace
</pre>

<p>
Ora udev ricorderà correttamente i nomi dei propri dispositivi di rete.
</p>

</body>
</section>
<section>
<title>udev carica i moduli in un ordine non previsto</title>
<body>

<p>
Qualche volta udev carica i moduli in un ordine non indesiderato, non previsto o
abbastanza casuale. Tale comportamento è abbastanza comune in special modo per i
sistemi che hanno dispositivi multipli dello stesso tipo, per esempio
periferiche multimediali. Questo influenza i numeri assegnati ai dispositivi;
per esempio, nelle schede sonore talvolta potrebbero invertirsi di numero.
</p>

<p>
Ci sono alcune soluzioni per correggere la numerazione dei dispositivi e/o
l'ordine di caricamento dei moduli. Idealmente, basterebbe usare i parametri
del modulo per specificare il numero di dispositivo desiderato. Alcuni moduli,
come ALSA, include il parametro "index" (indice, ndt). I moduli che usano il
parametro index possono essere adeguati come mostrato. Questo esempio è per
sistemi con due schede sonore. La scheda con un indice uguale a 0 viene
designata come prima scheda. Una volta che i parametri vengono modificati, i
file di configurazione del modulo devono essere aggiornati.
</p>

<pre caption="Specificare i parametri del modulo">
# <i>echo "option snd-ice1724 index=0" >> /etc/modprobe.d/alsa.conf</i>
# <i>echo "option snd-ymfpci index=1" >> /etc/modprobe.d/alsa.conf</i>
# <i>update-modules</i>
</pre>

<p>
L'esempio precedente è la soluzione preferibile, purtroppo non tutti i moduli
supportano dei parametri come index. Per tali moduli, bisogna forzare il loro
corretto ordine di caricamento. Per prima cosa, bisogna impedire ad udev di
caricare automaticamente i moduli inserendoli in una "lista nera" (blacklist).
Assicurarsi di usare il nome esatto del modulo che viene caricato. Per i
dispositivi PCI, bisogna usare i nomi dei moduli ottenuti dall'output di
<c>pcimodules</c>, disponibile nel pacchetto <c>pciutils</c>. L'esempio seguente
usa i moduli DVB.
</p>

<pre caption="Inserire i moduli nella lista nera">
# <i>echo "blacklist b2c2-flexcop-pci" >> /etc/modprobe.d/dvb</i>
# <i>echo "blacklist budget" >> /etc/modprobe.d/dvb</i>
# <i>update-modules</i>
</pre>

<p>
Successivamente, caricare i moduli nell'ordine corretto. Aggiungerli al file
<path>/etc/modules.autoload.d/kernel-2.6</path> <e>nell'ordine esatto con cui si
desidera vengano caricati</e>.
</p>

<pre caption="Caricare i moduli nell'ordine corretto">
# <i>echo "budget" >> /etc/modules.autoload.d/kernel-2.6</i>
# <i>echo "b2c2-flexcop-pci" >> /etc/modules.autoload.d/kernel-2.6</i>
</pre>

</body>
</section>
<section>
<title>Altri problemi</title>
<body>

<p>
Se il file corrispondente ad una periferica non viene creato quando si carica
il modulo relativo tramite il file
<path>/etc/modules.autoload.d/kernel-2.6</path>, ma compare caricando
manualmente il modulo tramite modprobe, provare ad installare
<c>sys-apps/baselayout-1.8.12</c>, o una versione più recente.
</p>

<p>
Il supporto per i dispositivi framebuffer (<path>/dev/fb/*</path>) è stato
introdotto a partire dalla versione 2.6.6-rc2 del kernel.
</p>

<p>
Nel caso si usasse un kernel precedente alla versione 2.6.4 è necessario
includere esplicitamente il supporto per il filesystem <path>/dev/pts</path>.
</p>

<pre caption="Abilitare il filesystem /dev/pts">
File systems ---&gt;
  Pseudo filesystems ---&gt;
    [*] /dev/pts file system for Unix98 PTYs
</pre>

</body>
</section>
</chapter>

<chapter>
<title>Risorse &amp; Riconoscimenti</title>
<section>
<body>

<p>
La conferenza sul sistema udev tenuta da Greg Kroah-Hartman (IBM Corporation) al
Linux Symposium (Ottawa, Canada - 2003) è un ottimo punto di partenza per una
buona comprensione di udev.
</p>

<p>
<uri
link="http://webpages.charter.net/decibelshelp/LinuxHelp_UDEVPrimer.html">
Decibel's UDEV Primer</uri> è un documento molto approfondito riguardo all'uso
di udev su Gentoo.
</p>

<p>
<uri link="http://www.reactivated.net/udevrules.php">Writing udev rules</uri>,
scritto dallo sviluppatore di Gentoo Daniel Drake, è un documento eccellente per
imparare come personalizzare la proprio installazione di udev.
</p>

</body>
</section>
</chapter>
</guide>
