<?xml version='1.0' encoding="UTF-8"?>
<!DOCTYPE guide SYSTEM "/dtd/guide.dtd">
<!-- $Header: /var/cvsroot/gentoo/xml/htdocs/doc/it/gcc-upgrading.xml,v 1.19 2008/08/18 11:07:26 scen Exp $ -->

<guide link="/doc/it/gcc-upgrading.xml" lang="it">
<title>Guida all'aggiornamento di GCC per Gentoo</title>

<author title="Autore">
  <mail link="amne@gentoo.org">Wernfried Haas</mail>
</author>
<author title="Autore">
  <mail link="jkt@gentoo.org">Jan Kundrát</mail>
</author>
<author title="Redazione">
  <mail link="halcy0n"/>
</author>
<author title="Redazione">
  <mail link="nightmorph"/>
</author>
<author title="Traduzione">
  <mail link="richard77@libero.it">Federico Della Ricca</mail>
</author>

<abstract>
Questo documento guida gli utenti attraverso il processo di aggiornamento di
GCC.
</abstract>

<!-- The content of this document is licensed under the CC-BY-SA license -->
<!-- See http://creativecommons.org/licenses/by-sa/2.5 -->
<license/>

<version>23</version>
<date>2008-07-19</date>

<chapter id="intro">
<title>Introduzione</title>
<section>
<title>Aggiornamento di GCC</title>
<body>

<p>
Benché il suo corretto funzionamento sia critico per il sistema, il
compilatore GCC è un pacchetto come tutti gli altri, e necessita di essere
aggiornato. Fra i motivi che possono richiedere l'aggiornamento di GCC vi
sono: la correzione di bachi che possono interferire con il sistema,
l'aggiunta di nuove funzionalità  o semplicemente il desiderio di mantenere
aggiornato il proprio sistema. Se nessuna di queste motivazioni si applica al
proprio caso, si può rimandare tranquillamente l'aggiornamento finché la
propria versione di GCC gode del supporto degli sviluppatori Gentoo.
</p>

<p>
Se si effettua un aggiornamento significativo di GCC (come il passaggio da
3.3.6 a 3.4.5), il sistema non userà automaticamente la nuova versione.
Poiché potrebbero essere richiesti passaggi aggiuntivi, bisogna effettuare il
passaggio esplicitamente. Se si decide di non passare alla nuova versione,
Portage userà la vecchia finché non si cambia idea, o il vecchio compilatore
non viene rimosso dal sistema. In caso di aggiornamenti di manutenzione (come
per esempio da 3.4.5 a 3.4.6) viene utilizzata automaticamente la nuova
versione.
</p>

<p>
Questa guida spiega i passi necessari per effettuare un aggiornamento sicuro
del compilatore di un sistema Gentoo. Una sezione specifica è dedicata
all'<uri link="#upgrade-3.3-to-3.4">aggiornamento dal GCC-3.3 alla
versione 3.4</uri> e alle problematiche coinvolgenti <c>libstdc++</c>.
Un'altra sezione è dedicata agli utenti <uri link="#first-install">di
una nuova installazione</uri> di Gentoo utilizzando un pacchetto dello stage
3 dopo che è stata rilasciata una nuova versione del GCC.
</p>

<warn>
Va notato che nell'aggiornare dal GCC-3.4 (o 3.3) al GCC-4.1 o successivo
bisogna seguire le <uri link="#upgrade-general">istruzioni per un
aggiornamento generale</uri>, in quanto GCC-3.4 e GCC-4.1 hanno interfacce
binarie (ABI) leggermente differenti.
</warn>

</body>
</section>
</chapter>

<chapter id="upgrade-general">
<title>Istruzioni per un aggiornamento generale</title>
<section>
<title>Introduzione</title>
<body>

<impo>
Se si cercano istruzioni specifiche per l'aggiornamento dal GCC-3.3 al
GCC-3.4, si consulti la <uri link="#upgrade-3.3-to-3.4">sezione
apposita</uri>.
</impo>

<impo>
Se si cercano istruzioni specifiche per l'aggiornamento del GCC per nuove
installazioni, <uri link="#first-install">sezione apposita</uri>.
</impo>


<p>
In generale, gli aggiornamenti <e>di manutenzione</e> (che eliminano bachi
della precedente versione), come quello dalla versione 3.3.5 alla versione
3.3.6 dovrebbero poter essere fatti in tutta tranquillità, basta installare
la nuova versione, impostare il sistema in modo che la utilizzi e ricompilare
l'unico altro pacchetto coinvolto, <c>libtool</c>. A volte, gli aggiornamenti
di GCC spezzano la compatibilità a livello di codice binario, in tali casi la
ricompilazione dei pacchetti coinvolti (o addirittura della <e>toolchain</e>
[insieme dei programmi necessari per la compilazione dei pacchetti, NdT] o di
tutti i pacchetti di base [il <e>system</e> NdT]) potrebbe rendersi
necessaria.
</p>

<p>
La norma per cui il nuovo compilatore non viene utilizzato automaticamente ha
una importante eccezione negli aggiornamenti di manutenzione, come quello fra
le versioni 3.3.5 e 3.3.6, a meno di non utilizzare la <e>feature</e>
"multislot", che permette di farli coesistere nello stesso sistema. Questa
<e>feature</e> è normalmente disabilitata, in quanto la maggior parte degli
utenti non ne trarrebbe beneficio.
</p>

<pre caption="Aggiornamento di GCC">
# <i>emerge -uav gcc</i>

<comment>(Si ponga attenzione a sostituire "i686-pc-linux-gnu-4.1.1" con la versione del GCC a cui si è aggiornato e l'impostazione per CHOST del proprio sistema)</comment>
# <i>gcc-config i686-pc-linux-gnu-4.1.1</i>
# <i>env-update &amp;&amp; source /etc/profile</i>

<comment>Se è stato effettuato l'upgrade dal gcc 3 al 4 (dal 3.4.6 al 4.1.1 in questo esempio) bisogna eseguire manualmente fix_libtool_files.sh</comment>
<comment>Sostituire $CHOST con il proprio CHOST, come si trova in /etc/make.conf</comment>
<comment>(Sostituire a &lt;gcc-versione&gt; con la nuova versione di GCC a cui si è aggiornato)</comment>
# <i>/usr/share/gcc-data/$CHOST/&lt;gcc-versione&gt;/fix_libtool_files.sh 3.4.6</i>


<comment>(Ricompilazione di libtool)</comment>
# <i>emerge --oneshot -av libtool</i>
</pre>

<p>
Per essere certi che il sistema sia in uno stato coerente, <e>bisogna</e>
ricompilare il sistema di base e i pacchetti appartenenti al profilo world
per utilizzare il nuovo compilatore.
</p>

<pre caption="Ricompilazione del sistema">
# <i>emerge -eav system</i>
# <i>emerge -eav world</i>
</pre>

<p>
Ora è possibile rimuovere le vecchie versioni di GCC. Se lo si ritenesse
opportuno, si esegua il comando seguente (sostituendo ovviamente a
<c>=sys-devel/gcc-3.4*</c> la versione che si vuole rimuovere):
</p>

<pre caption="Rimozione delle vecchie versioni di GCC">
# <i>emerge -aC =sys-devel/gcc-3.4*</i>
</pre>

<impo>
Si ponga attenzione al fatto che GCC 4.1 e successivi sono in grado di compilare
solo i kernel successivi al 2.4.34. Non rimuovere le vecchie versioni di GCC se
si pensa di usare un kernel più datato.
</impo>

<impo> <!-- FIXME: do we really want to keep it here? -non tradotto,
trattandosi di un commento nel sorgente xml NdT-->
Nel caso si stia aggiornando dal GCC-3.3, è consigliabile eseguire
<c>emerge --oneshot sys-libs/libstdc++-v3</c> per ottenere la compatibilità
con vecchie applicazioni C++ binarie.
</impo>


</body>
</section>
</chapter>

<chapter id="upgrade-3.3-to-3.4">
<title>Aggiornamento da GCC-3.3 a 3.4</title>
<section>
<title>Introduzione</title>
<body>

<p>
L'aggiornamento dal GCC-3.3 al 3.4 presenta delle problematiche particolari,
in quanto l'ABI C++ è cambiata fra queste versioni. Inoltre bisogna porre
particolare attenzione ai possibili problemi dati dalla libreria
<c>libstdc++</c>.
</p>

</body>
</section>
<section id="upgrade-3.3-to-3.4-choices">
<title>Modalità dell'aggiornamento</title>
<body>


<impo>
Se si sta aggiornando dal gcc 3.4 al 4.1, si seguano le <uri
link="#upgrade-general">istruzioni per un aggiornamento
generale</uri>.
</impo>

<impo>
Se si sta effettuando l'aggiornamento su un sistema SPARC, bisogna <uri
link="#upgrade-3.3-to-3.4-emerge-e">ricompilare l'intero
sistema</uri> a causa di cambiamenti nell'<uri
link="http://gcc.gnu.org/gcc-3.4/sparc-abi.html">ABI interna</uri>, ovvero il
modo in cui GCC passa i parametri al proprio interno.  [Il link porta a una
pagina in inglese. NdT]
</impo>

<p>
Se si aggiorna dal gcc 3.3 al 3.4, ci sono due metodi per effettuare
l'aggiornamento. Il <uri
link="#upgrade-3.3-to-3.4-revdep-rebuild">primo metodo</uri> è più
veloce e implica l'uso del programma <c>revdep-rebuild</c> contenuto nel
pacchetto <c>gentoolkit</c> mentre il <uri
link="#upgrade-3.3-to-3.4-emerge-e">secondo</uri> ricompila l'intero
sistema da zero, sfruttando le nuove caratteristiche del GCC. Ogni utente può
decidere per l'uno o l'altro metodo. Nella maggior parte dei casi, il primo
metodo è sufficiente.
</p>

<p>
Se si aggiorna da gcc 3.3 al 4,1, non usare il metodo basato su
revdep-rebuild, ma <uri link="#upgrade-3.3-to-3.4-emerge-e">si ricompili
l'intero sistema</uri>.
</p>

</body>
</section>
<section id="upgrade-3.3-to-3.4-revdep-rebuild">
<title>Utilizzo di revdep-rebuild</title>
<body>

<p>
Questo metodo richiede l'installazione di <c>gentoolkit</c> se non già
presente nel sistema. Fatto ciò, si proceda all'aggiornamento del GCC e al
passaggio al nuovo compilatore. Si ricompili anche il pacchetto
<c>libtool</c> per assicurare la corretta funzionalità del sistema di
compilazione.
</p>

<pre caption="Installazione di gentoolkit e aggiornamento del GCC">
# <i>emerge -an gentoolkit</i>
# <i>emerge -uav gcc</i>
<comment>(Si ponga attenzione a sostituire "i686-pc-linux-gnu-3.4.5" con la
versione del GCC a cui si è aggiornato e l'impostazione per CHOST del proprio
sistema)</comment>
# <i>gcc-config i686-pc-linux-gnu-3.4.5</i>
# <i>source /etc/profile</i>

<comment>(Ricompilazione di libtool)</comment>
# <i>emerge --oneshot -av libtool</i>
</pre>

<p>
A questo punto si generi la lista dei pacchetti che revdep-rebuild prevede di
ricompilare. Poi si usi revdep-rebuild per ricompilarli. Si presti attenzione
al fatto che questa fase può richiedere diverso tempo.
</p>

<pre caption="Utilizzo di revdep-rebuild">
# <i>revdep-rebuild --library libstdc++.so.5 -- -p -v</i>
# <i>revdep-rebuild --library libstdc++.so.5</i>
</pre>

<note>
E' possibile che si verifichino dei problemi con delle versioni non esistenti
di pacchetti, perché obsolete o mascherate. In questo caso si può usare
<c>revdep-rebuild</c> con l'opzione <c>--package-names</c>, per far sì che i
pacchetti siano ricompilati sulla base solo del nome del pacchetto, piuttosto
che sulla combinazione esatta del nome del pacchetto e sua versione.
</note>

<p>
Per ottenere la compatibilità con vecchie applicazioni C++ binarie ed
eventuali pacchetti che potrebbero non essere stati aggiornati da
revdep-rebuild, bisogna installare <c>sys-libs/libstdc++-v3</c> prima di
rimuovere GCC 3.3 dal sistema.
</p>

<pre caption="Installazione di libstdc++-v3 e rimozione di GCC 3.3">
# <i>emerge --oneshot sys-libs/libstdc++-v3</i>
# <i>emerge -aC =sys-devel/gcc-3.3*</i>
</pre>

</body>
</section>
<section id="upgrade-3.3-to-3.4-emerge-e">
<title>Utilizzo di emerge -e</title>
<body>

<p>
Questo metodo, benché molto più lento, effettua la ricompilazione dell'intero
sistema, per essere sicuri che tutti i pacchetti siano ricompilati con il
nuovo compilatore, ed è perciò più sicuro. Per prima cosa, si aggiornino GCC
e libtool e si effettui il passaggio al nuovo compilatore.
</p>

<pre caption="Aggiornamento di GCC">
# <i>emerge -uav gcc</i>
<comment>(Si ponga attenzione a sostituire "i686-pc-linux-gnu-3.4.5" con la versione del GCC a cui si è aggiornato e l'impostazione per CHOST del proprio sistema)</comment>
# <i>gcc-config i686-pc-linux-gnu-3.4.5</i>
# <i>source /etc/profile</i>

<comment>Se si sta aggiornando dalla versione 3 alla 4 di gcc (dal 3.3.6 al 4.1.1 in questo esempio) bisogna eseguire manualmente fix_libtool_files.sh</comment>
<comment>Sostituire $CHOST con il proprio CHOST, come si trova in /etc/make.conf</comment>
<comment>(Sostituire a &lt;gcc-versione&gt; con la nuova versione di GCC a cui si è aggiornato)</comment>
# <i>/usr/share/gcc-data/$CHOST/&lt;gcc-versione&gt;/fix_libtool_files.sh 3.3.6</i>

<comment>(Ricompilazione di libtool)</comment>
# <i>emerge --oneshot -av libtool</i>
</pre>

<p>
Per ottenere la compatibilità con vecchie applicazioni C++ binarie, bisogna
installare <c>sys-libs/libstdc++-v3</c>.
</p>

<pre caption="Installazione di libstdc++-v3">
# <i>emerge --oneshot sys-libs/libstdc++-v3</i>
</pre>

<p>
Ora si proceda a ricompilare per primo l'insieme di pacchetti (profilo) system,
poi il profilo world. Questa fase richiede molto tempo, a seconda del numero di
pacchetti installati, in quanto ricompila il sistema di base (toolchain) e poi
tutti i pacchetti installati (compresa la toolchain). Questo è necessario per
assicurarsi che tutti i pacchetti siano stati compilati la nuova toolchain,
compresa la toolchain stessa.
</p>

<pre caption="Ricompilazione di system e world">
# <i>emerge -e system</i>
# <i>emerge -e world</i>
</pre>

<p>
Si possono anche rimuovere in sicurezza le vecchie versioni del GCC:
</p>

<pre caption="Rimozione delle vecchie versioni del GCC">
# <i>emerge -aC =sys-devel/gcc-3.3*</i>
</pre>

</body>
</section>
</chapter>


<chapter id="first-install">
<title>Aggiornamento di GCC su una nuova installazione</title>
<section>
<title>Introduzione</title>
<body>

<p>
L'aggiornamento del GCC su un sistema dopo l'installazione da un pacchetto dello
stage 3 è semplice.  Un vantaggio degli utenti di nuove installazioni è che non
hanno un gran numero di pacchetti installati che utilizzano le librerie del
vecchio GCC. Il seguente esempio è per l'aggiornamento del GCC-3.3 al 3.4. Certi
particolari potrebbero differire se si effettua l'aggiornamento da altre
versioni di GCC. Per esempio i nomi per le librerie utilizzate per il comando
<c>revdep-rebuild</c> qui sotto sono specifiche per il GCC 3.3, così come la
necessità di installare <c>libstdc++-v3</c>.
</p>

<p>
Se non si sono effettuate personalizzazioni al sistema, ci sono pochi passi da
fare per aggiornare il GCC. Come con l'aggiornamento da GCC-3.3 a 3.4, ci sono
due opzioni. Comunque, diversamente dall'aggiornamento da GCC-3.3 al 3.4, ci
sono meno complicazioni e ci sono meno differenze fra i metodi. Il <uri
link="#first-install-revdep-rebuild">primo metodo</uri> è più rapido e utilizza
il comando <c>revdep-rebuild</c> del pacchetto <c>gentoolkit</c>, in modo simile
alla procedura già vista. Utilizzando revdep-rebuild si ricompilano solo i
pacchetti che davvero fanno uso delle librerie del GCC, mentre <uri
link="#first-install-emerge-e">il secondo metodo</uri> ricompila l'intero
sistema con il nuovo GCC e richiede molto più tempo. Questo secondo metodo non è
mai richiesto ed è incluso solo per completezza.
</p>

<p>
Queste prime istruzioni sono comuni ai due metodi, e dovrebbero essere
seguite da tutti.
</p>

<pre caption="Aggiornamento di GCC">
# <i>emerge -uav gcc</i>
<comment>(Si ponga attenzione a sostituire "i686-pc-linux-gnu-3.4.5" con la versione del GCC a cui si è aggiornato e l'impostazione per CHOST del proprio sistema)</comment>
# <i>gcc-config i686-pc-linux-gnu-3.4.5</i>
# <i>source /etc/profile</i>

<comment>(Ricompilare libtool)</comment>
# <i>emerge --oneshot -av libtool</i>
</pre>

<p>
Per ottenere la compatibilità con vecchie applicazioni C++ binarie, bisogna
installare <c>sys-libs/libstdc++-v3</c>.
</p>

<pre caption="Installazione di libstdc++-v3">
# <i>emerge --oneshot sys-libs/libstdc++-v3</i>
</pre>

</body>
</section>

<section id="first-install-revdep-rebuild">
<title>Utilizzo di revdep-rebuild</title>
<body>

<p>
Questo metodo richiede che si installi il pacchetto <c>gentoolkit</c> se non
lo si è già fatto. A questo punto si generi la lista dei pacchetti che
<c>revdep-rebuild</c> prevede di ricompilare. Poi si usi
<c>revdep-rebuild</c> per ricompilarli.
</p>

<pre caption="Installazione di gentoolkit e utilizzo di revdep-rebuild">
# <i>emerge -an gentoolkit</i>
# <i>revdep-rebuild --library libstdc++.so.5 -- -p -v</i>
# <i>revdep-rebuild --library libstdc++.so.5</i>
</pre>

<note>
E' possibile che si verifichino dei problemi con delle versioni non esistenti
di pacchetti, perché obsolete o mascherate. In questo caso si può usare
<c>revdep-rebuild</c> con l'opzione <c>--package-names</c>, per far sì che i
pacchetti siano ricompilati sulla base solo del nome del pacchetto, piuttosto
che sulla combinazione esatta del nome del pacchetto e sua versione.
</note>

</body>
</section>
<section id="first-install-emerge-e">
<title>Utilizzo di emerge -e</title>
<body>

<p>
Questo metodo, benché più lento, ricompila l'intero insieme di dei pacchetti
base (<c>system</c>) per assicurarsi che tutto sia ricompilato con il nuovo
compilatore. Questo non è necessario, ma è un buon metodo se contestualmente
si effettuano cambiamenti alle CFLAG o a altre variabili del file make.conf
che influenzino la compilazione del sistema.
</p>

<p>
Trattandosi di una nuova installazione, non è necessario ricompilare il
<c>world</c> come in un sistema già in uso. Comunque, è possibile aggiornare
<c>world</c> per maggiore completezza.
</p>

<pre caption="Ricompilazione di system">
# <i>emerge -e system</i>
</pre>
</body>
</section>
<section id="first-install-cleaning-up">
<title>Pulizia del sistema</title>
<body>

<p>
Ora è possibile rimuovere le vecchie versioni di GCC. Si sostituisca a
<c>VERSIONE-NUOVO-GCC</c> la versione a cui si è aggiornato:
</p>

<pre caption="Pulizia del sistema">
# <i>emerge -aC "&lt;sys-devel/gcc-VERSIONE-NUOVO-GCC"</i>
</pre>

</body>
</section>
</chapter>

<chapter id="common-pitfalls">
<title>Problemi comuni</title>
<section>
<body>

<p>
Ci si assicuri di aver disabilitato <c>distcc</c> durante l'aggiornamento.
Mescolare sui vari nodi versioni differenti del compilatore <e>causerà</e>
problemi di ricompilazione. Non è necessario disabilitare <c>ccache</c>, in
quanto la cache dei file oggetto sarà comunque ignorata, perché non più
valida.
</p>

<p>
Si usi sempre la stessa versione di GCC per compilare il kernel e moduli
aggiuntivi del kernel. Una volta ricompilato il  world con il nuovo GCC, i
moduli esterni (come ad esempio <c>app-emulation/qemu-softmmu</c>) non
potranno essere caricati. Ricompilare il kernel con il nuovo GCC risolve il
problema.
</p>

<p>
Se si sta effettuando l'aggiornamento di un sistema SPARC, rieseguire <c>silo
-f</c> dopo aver riemerso world per evitare problemi.
</p>

</body>
</section>

<section>
<title>Messaggi di errore frequenti</title>
<body>

<p>
Se si verifica l'errore: <e>libtool: link:
`/usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.6/libstdc++.la' is not a valid
libtool archive,</e> si esegua il comando
<c>/usr/share/gcc-data/$CHOST/&lt;gcc-versione&gt;/fix_libtool_files.sh 3.3.6</c>
(sostituendo "3.3.6" con il numero di versione del messaggio di errore, e
$CHOST e &lt;gcc-versione&gt; con i valori del proprio sistema).
</p>

<p>
Se si verifica l'errore: <e>error: /usr/bin/gcc-config: line 632:
/etc/env.d/gcc/i686-pc-linux-gnu-3.3.5: No such file or directory,</e> si
provi a cancellare <c>/etc/env.d/gcc/config-i686-pc-linux-gnu</c> e si
riesegua <c>gcc-config</c>, seguito da <c>source /etc/profile</c>. E'
possibile farlo solo se non sono stati impostati dei compilatori per altri
sistemi (cross-compilers).
</p>

<p>
Se un pacchetto non si compila durante l'esecuzione del comando <c>emerge -e
system</c> o <c>emerge -e world</c>, si può riprendere l'operazione con
<c>emerge --resume</c> Se un pacchetto fallisce ripetutamente, si può passare
al successivo con <c>emerge --resume --skipfirst</c>. Non eseguire altre
instanze di emerge nel frattempo o si perderanno le informazioni necessarie
per poter riprendere la ricompilazione.
</p>

<p>
Se si verifica l'errore: <e>spec failure: unrecognized spec option</e>
durante l'aggiornamento del compilatore, si provi  a ritornare al compilatore
predefinito, azzerare il valore della variabile <c>GCC_SPECS</c> e aggiornare
nuovamente il GCC:
</p>

<pre caption="Ripristino della configurazione predefinita">
# <i>gcc-config 1</i>
# <i>source /etc/profile</i>
# <i>unset GCC_SPECS</i>
# <i>emerge -uav gcc</i>
</pre>

</body>
</section>
</chapter>
</guide>
