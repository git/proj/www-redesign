<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE sections SYSTEM "/dtd/book.dtd">

<!-- The content of this document is licensed under the CC-BY-SA license -->
<!-- See http://creativecommons.org/licenses/by-sa/2.5 -->
<!-- Текст этого документа распространяется на условиях лицензии CC-BY-SA -->
<!-- См. http://creativecommons.org/licenses/by-sa/2.5 -->

<!-- $Header: /var/cvsroot/gentoo/xml/htdocs/doc/ru/handbook/hb-install-config.xml,v 1.14 2006/09/25 20:35:24 achumakov Exp $ -->

<sections>

<version>7.3</version>
<date>2006-09-12</date>

<section>
<title>Параметры файловых систем</title>
<subsection>
<title>Что такое fstab?</title>
<body>

<p>
В Linux все разделы, используемые системой, должны быть перечислены в
<path>/etc/fstab</path>.
В этом файле указываются точки подключения разделов (mountpoints,
местоположение разделов в файловой системе), порядок подключения,
а также дополнительные параметры (автоматический или ручной режим подключения,
достаточность прав пользователя для подключения и т.п.)
</p>

</body>
</subsection>
<subsection>
<title>Создание /etc/fstab</title>
<body>

<p>
В <path>/etc/fstab</path> используется специальный формат. Каждая строка
состоит из шести полей, разделяемых пробелами, знаками табуляции или их
сочетанием. Каждое поле имеет свое назначение:
</p>

<ul>
<li>
Первое поле обозначает <b>раздел (partition)</b> (путь к файлу устройства).
</li>
<li>
Второе поле указывает <b>точку подключения (mountpoint)</b>, в которую
монтируется раздел.
</li>
<li>
Третье поле задает тип <b>файловой системы (filesystem)</b>, используемой в 
разделе.
</li>
<li>
В четвертом поле указываются <b>параметры подключения (mountoptions)</b>, 
используемые <c>mount</c> при подключении раздела. Поскольку для каждой 
файловой системы существуют свои параметры, рекомендуется прочитать страницу 
справки по mount (<c>man mount</c>), где приведен их полный перечень. При 
указании нескольких параметров подключения их следует разделять запятыми.
</li>
<li>
Пятое поле используется <c>dump</c> для определения, требуется ли резервное 
копирование раздела средствами dump. Обычно это поле можно просто установить в 
<c>0</c> (ноль).
</li>
<li>
Шестое поле используется <c>fsck</c> для определения порядка <b>проверки 
(check)</b> файловых систем после некорректного завершения работы системы. Для 
корневой файловой системы рекомендуется значение <c>1</c>, а для остальных 
&mdash; <c>2</c> (или <c>0</c>, когда проверка файловой системы не требуется).
</li>
</ul>

<impo>
Вариант файла <path>/etc/fstab</path> по умолчанию, входящий в Gentoo, <e>не 
является работоспособным</e>. Вам <b>потребуется создать</b> свой собственный 
<path>/etc/fstab</path>.
</impo>

<pre caption="Открытие /etc/fstab">
# <i>nano -w /etc/fstab</i>
</pre>

</body>
<body test="func:keyval('/boot')">

<p>
Рассмотрим, как указываются параметры загрузочного раздела <path>/boot</path>. 
Это лишь пример, и если вам не нужно или вы не можете создать раздел 
<path>/boot</path>, не копируйте пример дословно.
</p>

<p test="contains(func:keyval('/boot'), '/dev/hd')">
В нашем примере разбивки для <keyval id="arch"/> по умолчанию,
<path>/boot</path> &mdash; это обычно раздел <path><keyval id="/boot"/></path>
(или <path>/dev/sda*</path> при использовании дисков SCSI или SATA) с файловой
системой <c>ext2</c>. При загрузке требуется его проверка. Напишем следующее:
</p>

<p test="contains(func:keyval('/boot'), '/dev/sd')">
В нашем примере разбивки для <keyval id="arch"/> по умолчанию,
<path>/boot</path> &mdash; это обычно раздел <path><keyval id="/boot"/></path>
с файловой системой <c>ext2</c>. При загрузке требуется его проверка. Напишем
следующее:
</p>

<pre caption="Пример строки /boot в /etc/fstab">
<keyval id="/boot"/>   /boot     ext2    defaults        1 2
</pre>

<p>
Некоторые пользователи предпочитают не подключать раздел <path>/boot</path>
автоматически, чтобы повысить безопасность системы. Для этого нужно заменить
<c>defaults</c> на <c>noauto</c>. В таком случае вам придется подключать раздел 
вручную каждый раз, когда он потребуется.
</p>

</body>
<body>

<p test="not(func:keyval('arch')='SPARC')">
Укажите правила, соответствующие вашей схеме разбивки, и добавьте правила для
<path>/proc</path>, для <c>tmpfs</c>, для своих дисководов CD-ROM
(если есть другие разделы или устройства, их тоже можно указать).
</p>

<p test="func:keyval('arch')='SPARC'">
Укажите правила, соответствующие вашей схеме разбивки, и добавьте правила для
<path>/proc/openprom</path>, <path>/proc</path>, <c>tmpfs</c>, для своих
дисководов CD-ROM (если есть другие разделы или устройства, их тоже можно
указать).
</p>

<p>
Теперь на основе приведенного <e>примера</e> создайте собственный файл 
<path>/etc/fstab</path>:
</p>

<pre caption="Пример полного /etc/fstab" test="func:keyval('arch')='AMD64' or func:keyval('arch')='x86'">
<keyval id="/boot"/>   /boot        ext2    defaults,noatime     1 2
/dev/hda2   none         swap    sw                   0 0
/dev/hda3   /            ext3    noatime              0 1

none        /proc        proc    defaults             0 0
none        /dev/shm     tmpfs   nodev,nosuid,noexec  0 0

/dev/cdrom  /mnt/cdrom   auto    noauto,user          0 0
</pre>

<pre caption="Пример полного /etc/fstab" test="func:keyval('arch')='HPPA'">
<keyval id="/boot"/>   /boot        ext2    defaults,noatime     1 2
/dev/sda3   none         swap    sw                   0 0
/dev/sda4   /            ext3    noatime              0 1

none        /proc        proc    defaults             0 0
none        /dev/shm     tmpfs   nodev,nosuid,noexec  0 0

/dev/cdrom  /mnt/cdrom   auto    noauto,user          0 0
</pre>

<pre caption="Пример полного /etc/fstab" test="func:keyval('arch')='Alpha' or func:keyval('arch')='MIPS'">
<keyval id="/boot"/>   /boot        ext2    defaults,noatime     1 2
/dev/sda2   none         swap    sw                   0 0
/dev/sda3   /            ext3    noatime              0 1

none        /proc        proc    defaults             0 0
none        /dev/shm     tmpfs   nodev,nosuid,noexec  0 0

/dev/cdrom  /mnt/cdrom   auto    noauto,user          0 0
</pre>

<pre caption="Пример полного /etc/fstab" test="func:keyval('arch')='SPARC'">
/dev/sda1   /               ext3        noatime              0 1
/dev/sda2   none            swap        sw                   0 0
/dev/sda4   /usr            ext3        noatime              0 2
/dev/sda5   /var            ext3        noatime              0 2
/dev/sda6   /home           ext3        noatime              0 2

none        /proc/openprom  openpromfs  defaults             0 0
none        /proc           proc        defaults             0 0
none        /dev/shm        tmpfs       nodev,nosuid,noexec  0 0

/dev/cdrom  /mnt/cdrom      auto        noauto,user          0 0
</pre>

<note test="func:keyval('arch')='PPC'">
Среди типов машин PPC есть важные различия. Пожалуйста, адаптируйте следующий
пример к своей системе.
</note>

<pre caption="Пример полного  /etc/fstab" test="func:keyval('arch')='PPC'">
/dev/hda4   /            ext3    noatime              0 1
/dev/hda3   none         swap    sw                   0 0

none        /proc        proc    defaults             0 0
none        /dev/shm     tmpfs   nodev,nosuid,noexec  0 0

/dev/cdrom  /mnt/cdrom   auto    noauto,user          0 0
</pre>

<pre caption="Пример полного /etc/fstab" test="func:keyval('arch')='PPC64'">
/dev/sda4   /            ext3    noatime              0 1
/dev/sda3   none         swap    sw                   0 0

none        /proc        proc    defaults             0 0
none        /dev/shm     tmpfs   nodev,nosuid,noexec  0 0

/dev/cdrom  /mnt/cdrom   auto    noauto,user          0 0
</pre>

<p>
Параметр <c>auto</c> позволяет <c>mount</c> определять тип файловой системы 
автоматически (рекомендуется для съемных носителей, которые могут оказаться 
размечены в одной из множества существующих файловых систем), а <c>user</c> 
позволяет монтировать компакт-диски обычным пользователям.
</p>

<p>
Чтобы повысить быстродействие, большинству пользователей стоит добавить 
параметр <c>noatime</c> в параметры подключения, что приведет к ускорению за 
счет отключения регистрации отметки времени доступа к файлам (обычно в ней все 
равно нет необходимости):
</p>

<p>
Перепроверьте свой файл <path>/etc/fstab</path>, сохраните его, и 
выйдите из редактора, чтобы продолжить настройку.
</p>

</body>
</subsection>
</section>
<section>
<title>Параметры сети</title>
<subsection>
<title>Hostname, Domainname и т. д.</title>
<body>

<p>
Еще один вопрос, который нужно решить пользователю &mdash; как назвать свой 
компьютер. Он кажется довольно простым, но <e>многие</e> затрудняются дать 
подходящее имя для своей Linux-системы. Чтобы вам стало легче, запомните, что
какое бы имя вы не выбрали, потом его всегда можно изменить. Например, вы могли 
бы просто назвать свою систему <c>tux</c>, а домен &mdash; <c>homenetwork</c>.
</p>

<pre caption="Установка имени узла">
# <i>nano -w /etc/conf.d/hostname</i>

<comment>(присвойте переменной HOSTNAME имя своего узла)</comment>
HOSTNAME="<i>tux</i>"
</pre>

<p>
Во-вторых, установим имя домена (domainname) в <path>/etc/conf.d/net</path>:
</p>

<pre caption="Установка имени домена">
# <i>nano -w /etc/conf.d/net</i>

<comment>(присвойте переменной DNSDOMAIN имя своего домена)</comment>
dns_domain_lo="<i>homenetwork</i>"
</pre>

<p>
Если у вас есть домен NIS (а если вы не знаете, что это такое, то у вас его 
точно нет), его также необходимо указать: 
</p>

<pre caption="Установка имени NIS-домена">
# <i>nano -w /etc/conf.d/net</i>

<comment>(укажите название своего домена NIS переменной nis_domain)</comment>
nis_domain_lo="<i>my-nisdomain</i>"
</pre>

</body>
</subsection>
<subsection>
<title>Настройка сети</title>
<body>

<p>
Прежде, чем возмутиться: &laquo;Эй, мы же все это уже делали!&raquo; &mdash; 
вспомните, что подключение к сети, настроенное вначале, было предназначено лишь 
для установки Gentoo. Теперь же вы настраиваете сеть для постоянного 
использования.
</p>

<note>
Более подробные сведения о сетях, включая дополнительные темы, такие как 
объединение, образование мостов, настройка виртуальных сетей (VLAN) 802.1Q или
беспроводных сетей, представлены в разделе <uri link="?part=4">настройка сети в
Gentoo</uri>.
</note>

<p>
Все настройки сети собраны в файле <path>/etc/conf.d/net</path>. В
нем используется простой формат, хотя, если вы не знакомы с ручной 
настройкой сети, он не слишком очевиден. Но не бойтесь, мы все объясним. В 
файле <path>/etc/conf.d/net.example</path> приведен подробно 
прокомментированный пример, охватывающий много различных конфигураций.
</p>

<p>
По умолчанию используется DHCP. Чтобы DHCP заработал, требуется установить
DHCP-клиент, как описано далее в разделе <uri
link="?part=1&amp;chap=9#networking-tools">Установка нужных системных
средств</uri>. Не забудьте установить DHCP-клиент.
</p>

<p>
Если настройка сетевого подключения нужна вам для указания специфических 
параметров DHCP, или из-за того, что вы вообще не используете DHCP, откройте
<path>/etc/conf.d/net</path> в своем любимом редакторе (в этом примере 
использован <c>nano</c>):
</p>

<pre caption="Открытие /etc/conf.d/net для изменения">
# <i>nano -w /etc/conf.d/net</i>
</pre>

<p>
Вы увидите следующее:
</p>

<pre caption="/etc/conf.d/net по умолчанию">
# This blank configuration will automatically use DHCP for any net.*
# scripts in /etc/init.d.  To create a more complete configuration,
# please review /etc/conf.d/net.example and save your configuration
# in /etc/conf.d/net (this file :]!).
<comment>
(# Этот пустой файл настройки приводит к автоматическому использованию
 # DHCP всеми сценариями net.* из /etc/init.d. Для создания более полной
 # настройки, пожалуйста, просмотрите /etc/conf.d/net.example, а свою
 # настройку сохраните в /etc/conf.d/net (в этом файле :]!).            )</comment>
</pre>

<p>
Чтобы указать свой собственный адрес IP, маску сети и шлюз, потребуется 
настроить как <c>config_eth0</c>, так и <c>routes_eth0</c>:
</p>

<pre caption="Ручная настройка параметров IP для eth0">
config_eth0=( "192.168.0.2 netmask 255.255.255.0 brd 192.168.0.255" )
routes_eth0=( "default gw 192.168.0.1" )
</pre>

<p>
Чтобы при использовании DHCP указать специфические параметры, определите
<c>config_eth0</c> и <c>dhcp_eth0</c>:
</p>

<pre caption="Автоматическое получение адреса IP для eth0">
config_eth0=( "dhcp" )
dhcp_eth0="nodns nontp nonis"
</pre>

<p>
Список допустимых параметров дан в файле <path>/etc/conf.d/net.example</path>.
</p>

<p>
Если у вас несколько сетевых интерфейсов, повторите эти шаги для 
<c>config_eth1</c>, <c>config_eth2</c> и т.д.
</p>

<p>
Теперь сохраните параметры и выйдите из редактора, чтобы продолжить настройку.
</p>

</body>
</subsection>
<subsection>
<title>Автоматический запуск сетевого подключения при загрузке</title>
<body>

<p>
Для запуска сетевых интерфейсов при загрузке необходимо добавить их в уровень 
запуска по умолчанию. Если у вас интерфейсы типа PCMCIA, пропустите этот шаг, 
поскольку интерфейсы PCMCIA запускаются сценарием инициализации PCMCIA.
</p>

<pre caption="Добавление net.eth0 в уровень запуска default">
# <i>rc-update add net.eth0 default</i>
</pre>

<p>
Если у вас несколько сетевых интерфейсов, потребуется создать для них 
соответствующие сценарии инициализации <path>net.eth1</path>, 
<path>net.eth2</path> и т.д. Для этого можно использовать <c>ln</c>:
</p>

<pre caption="Создание дополнительных сценариев инициализации">
# <i>cd /etc/init.d</i>
# <i>ln -s net.lo net.eth1</i>
# <i>rc-update add net.eth1 default</i>
</pre>

</body>
</subsection>
<subsection>
<title>Указание сетевых узлов</title>
<body>

<p>
Теперь расскажем системе Linux о вашей сети. Эти сведения указываются в 
<path>/etc/hosts</path>, и помогают разрешению имен в IP-адреса для узлов, не 
обрабатываемых сервером имен. Требуется определить вашу систему. Также можно
определить другие системы в сети, если вы не собираетесь устанавливать
собственную систему DNS.
</p>

<pre caption="Открытие /etc/hosts">
# <i>nano -w /etc/hosts</i>
</pre>

<pre caption="Указание сведений об узлах сети">
<comment>(определение текущей системы)</comment>
127.0.0.1     tux.homenetwork tux localhost

<comment>(определите другие машины в своей сети,
для этого у них должен быть статический IP-адрес.)</comment>

192.168.0.5   jenny.homenetwork jenny
192.168.0.6   benny.homenetwork benny
</pre>

<p>
Чтобы продолжить настройку, сохраните файл и выйдите из редактора.
</p>

<p test="func:keyval('arch')='AMD64' or func:keyval('arch')='x86' or substring(func:keyval('arch'),1,3)='PPC'">
Если у вас нет PCMCIA, можете перейти к разделу <uri 
link="#sysinfo">параметры системы</uri>. Пользователям PCMCIA рекомендуется
прочитать следующий раздел, посвященный PCMCIA.
</p>

</body>
</subsection>
<subsection test="func:keyval('arch')='AMD64' or func:keyval('arch')='x86' or substring(func:keyval('arch'),1,3)='PPC'">
<title>Дополнительно: запуск PCMCIA</title>
<body>

<p>
Прежде всего, пользователям PCMCIA нужно установить пакет <c>pcmcia-cs</c>. Это 
относится и к пользователям, работающим с ядром 2.6 (даже если они не будут 
пользоваться драйверами PCMCIA из указанного пакета). При установке необходимо
указать <c>USE="-X"</c>, чтобы избежать установки xorg-x11:
</p>

<pre caption="Установка pcmcia-cs">
# <i>USE="-X" emerge pcmcia-cs</i>
</pre>

<p>
После установки <c>pcmcia-cs</c> включите <c>pcmcia</c> в уровень запуска <e>по
умолчанию</e>:
</p>

<pre caption="Добавление pcmcia в уровень запуска default">
# <i>rc-update add pcmcia default</i>
</pre>

</body>
</subsection>
</section>
<section id="sysinfo">
<title>Параметры системы</title>
<subsection>
<title>Пароль root</title>
<body>

<p>
Прежде всего, нужно установить пароль root (администратора), набрав:
</p>

<pre caption="Установка пароля root">
# <i>passwd</i>
</pre>

<p>
Если вы хотите, чтобы root мог входить в систему через последовательный 
терминал, добавьте <c>tts/0</c> в <path>/etc/securetty</path>:
</p>

<pre caption="Добавление tts/0 to /etc/securetty">
# <i>echo "tts/0" &gt;&gt; /etc/securetty</i>
</pre>

</body>
</subsection>
<subsection>
<title>Параметры системы</title>
<body>

<p>
Для общей настройки системы в Gentoo используется <path>/etc/rc.conf</path>.
Откройте <path>/etc/rc.conf</path> и с удовольствием прочитайте все 
комментарии, находящиеся в этом файле :)
</p>

<pre caption="Открытие /etc/rc.conf">
# <i>nano -w /etc/rc.conf</i>
</pre>

<p>
Завершив изменение <path>/etc/rc.conf</path> сохраните файл и выйдите из 
редактора.
</p>

<p>
Как видите, этот файл подробно прокомментирован, что поможет вам в настройке
необходимых конфигурационных переменных. Можно настроить систему на 
использование unicode, а также указать редактор по умолчанию и диспетчер 
отображения (например, gdm или kdm).
</p>

<p>
Для управления раскладками клавиатуры в Gentoo используется 
<path>/etc/conf.d/keymaps</path>. Для настройки своей клавиатуры измените его. 
</p>

<pre caption="Открытие /etc/conf.d/keymaps">
# <i>nano -w /etc/conf.d/keymaps</i>
</pre>

<p>
Будьте особенно тщательны при установке переменной раскладки клавиатуры
<c>(KEYMAP)</c>: выбрав неверную раскладку, вы можете получить непредсказуемый
результат при попытке ввода с клавиатуры.
</p>

<note test="func:keyval('arch')='SPARC'">
Пользователям систем <b>SPARC</b>, снабженных USB, и клонов <b>SPARC</b> может
потребоваться вместо раскладки &laquo;sunkeymap&raquo; указать раскладку i386
(например, американскую &mdash; &laquo;us&raquo;).
</note>

<note test="substring(func:keyval('arch'),1,3)='PPC'">
На большинстве систем <b>PPC</b> используются раскладки x86. Пользователям,
желающим при загрузке иметь возможность использования раскладок клавиатуры ADB,
необходимо разрешить обработку клавиатурных кодов ADB в ядре, и установить
раскладку mac/ppc в <path>/etc/conf.d/keymaps</path>.
</note>

<p>
По завершении изменения <path>/etc/conf.d/keymaps</path> сохраните файл и 
выйдите из редактора.
</p>
 
<p>
Для настройки параметров часов в Gentoo используется 
<path>/etc/conf.d/clock</path>. Изменяйте его в соответствии со своими
потребностями.
</p>

<p>
Если аппаратные часы вашей системы настроены не на часовой пояс UTC (Гринвич), 
в файл необходимо добавить строку <c>CLOCK="local"</c>. В противном случае 
вы заметите сдвиг часового пояса.
</p>

<p>
После завершения настройки <path>/etc/conf.d/clock</path> сохраните файл
и выйдите из редактора.
</p>

<p test="not(func:keyval('arch')='PPC64')">
Переходите к <uri link="?part=1&amp;chap=9">установке нужных системных
средств</uri>.
</p>

</body>
</subsection>
<subsection test="func:keyval('arch')='PPC64'">
<title>Настройка консоли</title>
<body>

<note>
Следующие сведения относятся к аппаратным платформам IBM PPC64.
</note>

<p>
Если вы запускаете Gentoo на системе IBM PPC64 с использованием виртуальной
консоли, необходимо раскомментировать соответствующую строку в 
<path>/etc/inittab</path>, чтобы виртуальная консоль могла порождать запрос 
входа в систему.
</p>

<pre caption="Включение поддержки hvc или hvsi в /etc/inittab">
hvc0:12345:respawn:/sbin/agetty -L 9600 hvc0
hvsi:12345:respawn:/sbin/agetty -L 19200 hvsi0
</pre>

<p>
При этом следует убедиться, что соответствующая консоль указана в 
<path>/etc/securetty</path>.
</p>

<p>
Теперь можете переходить к <uri link="?part=1&amp;chap=9">установке
нужных системных средств</uri>.
</p>

</body>
</subsection>
</section>
</sections>

<!-- *$Localization:
target-language: Russian
target-version: 7.3-r1
target-date: 2006-09-25
source-cvs-revision: 1.84
translated-by: (unknown); Alexey Chumakov [achumakov@gentoo.org]
edited-by:  Alexey Chumakov
-->
