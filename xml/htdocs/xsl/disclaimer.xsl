<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

  <!--
  call-template show-disclaimer
  This prints an obsolete disclaimer on documents that have this field
  Typically this is used to note obsolete documents (see @disclaimed='obsolete')
  Optionally offer a redirect link to a new page fir @redirect
  -->
  <xsl:template name="show-disclaimer">
    <!-- Disclaimer stuff -->
    <xsl:if test="/*[1][@disclaimer] or /*[1][@redirect]">
      <table class="ncontent" align="center" width="90%" border="2px" cellspacing="0" cellpadding="4px">
        <xsl:if test="/*[1]/@disclaimer='obsolete'">
          <xsl:attribute name="style">margin-top:40px;margin-bottom:30px</xsl:attribute>
        </xsl:if>
        <tr>
          <td bgcolor="#ddddff">
            <p class="note">
              <xsl:if test="/*[1][@disclaimer]">
                <xsl:if test="/*[1]/@disclaimer='obsolete'">
                  <xsl:attribute name="style">font-size:1.3em</xsl:attribute>
                </xsl:if>
                <b><xsl:value-of select="func:gettext('disclaimer')"/>: </b>
                <xsl:apply-templates select="func:gettext(/*[1]/@disclaimer)"/>
              </xsl:if>
              <xsl:if test="/*[1][@redirect]">
                <xsl:apply-templates select="func:gettext('redirect')">
                  <xsl:with-param name="paramlink" select="/*[1]/@redirect"/>
                </xsl:apply-templates>
              </xsl:if>
            </p>
          </td>
        </tr>
      </table>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>
