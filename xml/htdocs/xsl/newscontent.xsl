<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">


  <xsl:template name="news-icon-selector">
    <xsl:param name="category"/>
    <xsl:choose>
      <xsl:when test="$category='birthday'">
        <img class="newsicon" src="/images/birthday_cake.png" alt="Happy Birthday"/>
      </xsl:when>
      <xsl:when test="$category='gentoo'">
        <img class="newsicon" src="/images/icon-gentoo.png" alt="gentoo"/>
      </xsl:when>
      <xsl:when test="$category='main'">
        <img class="newsicon" src="/images/icon-stick.png" alt="stick man"/>
      </xsl:when>
      <xsl:when test="$category='linux'">
        <img class="newsicon" src="/images/icon-penguin.png" alt="tux"/>
      </xsl:when>
      <xsl:when test="$category='moo'">
        <img class="newsicon" src="/images/icon-cow.png" alt="Larry the Cow"/>
      </xsl:when>
      <xsl:when test="$category='plans'">
        <img class="newsicon" src="/images/icon-clock.png" alt="Clock"/>
      </xsl:when>
      <xsl:when test="$category='planet'">
        <img class="newsicon" src="/images/G-Earth.png" alt="Planet Earth"/>
      </xsl:when>
      <!-- old ones, kept to display very very old news items -->
      <xsl:when test="$category='alpha'">
        <img class="newsicon" src="/images/icon-alpha.gif" alt="AlphaServer GS160"/>
      </xsl:when>
      <xsl:when test="$category='kde'">
        <img class="newsicon" src="/images/icon-kde.png" alt="KDE"/>
      </xsl:when>
      <xsl:when test="$category='ibm'">
        <img class="newsicon" src="/images/icon-ibm.gif" alt="ibm"/>
      </xsl:when>
      <xsl:when test="$category='nvidia'">
        <img class="newsicon" src="/images/icon-nvidia.png" alt="Nvidia"/>
      </xsl:when>
      <xsl:when test="$category='freescale'">
        <img class="newsicon" src="/images/icon-freescale.gif" alt="Freescale Semiconductor"/>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <!--
  TODO(antarus): What does this actually do?  Does anything still use it?

  It takes 3 paramters, thenews, summary, link.
  TODO(antarus): The icon selector should be its own template.
  -->
  <xsl:template name="newscontent">
    <xsl:param name="thenews"/>
    <xsl:param name="summary"/>
    <xsl:param name="link"/>

    <div class="news">
      <p class="newshead" lang="en">
        <a href="${link}"><xsl:value-of select="$thenews/title"/></a>
        <br/>
        <font size="0.90em">
        <xsl:choose>
          <xsl:when test="$thenews/until">
            Posted between <xsl:copy-of select="func:format-date($thenews/date)"/> and <xsl:copy-of select="func:format-date($thenews/until)"/>
          </xsl:when>
          <xsl:otherwise>
            Posted on <xsl:copy-of select="func:format-date($thenews/date)"/>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:variable name="poster">
          <xsl:call-template name="smart-mail">
          <xsl:with-param name="mail" select="$thenews/poster"/>
          </xsl:call-template>
        </xsl:variable>
        by <xsl:value-of select="$poster"/>
        </font>
      </p>

      <xsl:call-template name="news-icon-selector">
        <xsl:with-param name="category" select="$thenews/@category"/>
      </xsl:call-template>

      <div class="newsitem">
      <xsl:choose>
        <xsl:when test="$thenews/summary and $summary='yes'">
          <xsl:apply-templates select="$thenews/summary"/>
          <br/>
          <a href="{$link}"><b>(full story)</b></a>
        </xsl:when>
        <xsl:when test="$thenews/body">
          <xsl:apply-templates select="$thenews/body"/>
        </xsl:when>
      </xsl:choose>
      </div>
    </div>
  </xsl:template>
</xsl:stylesheet>
