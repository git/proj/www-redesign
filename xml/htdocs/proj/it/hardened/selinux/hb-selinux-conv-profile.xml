<?xml version='1.0' encoding="utf-8"?>
<!DOCTYPE sections SYSTEM "/dtd/book.dtd">

<!-- The content of this document is licensed under the CC-BY-SA license -->
<!-- See http://creativecommons.org/licenses/by-sa/1.0 -->

<!-- $Header: /var/cvsroot/gentoo/xml/htdocs/proj/it/hardened/selinux/hb-selinux-conv-profile.xml,v 1.4 2010/08/03 22:37:30 scen Exp $ -->

<sections>
<version>2.1</version>
<date>2010-06-15</date>

<section>
<title>Cambiare il profilo</title>
<subsection>
<body>

<warn>
Allo stato attuale, sono utilizzabili solamente ext2/3, JFS, XFS e Btrfs: altri
filesystem non supportano completamente gli attributi estesi.
</warn>

<warn>
E' sempre consigliabile effettuare la conversione a partire da un profile pari
o superiore al 2006.1, altrimenti l'operazione potrebbe portare risultati
inaspettati.
</warn>

<impo>
Come sempre, procurarsi un LiveCD nel caso in cui si verifichino dei problemi.
</impo>

<p>
Anzitutto occorre cambiare il proprio profilo con quello di SELinux secondo
l'architettura di cui si dispone:
</p>

<pre caption="Cambiare profilo">
# <i>rm -f /etc/make.profile</i>

<comment>x86 (server):</comment>
# <i>ln -sf /usr/portage/profiles/selinux/v2refpolicy/x86/server /etc/make.profile</i>
<comment>x86 (Hardened):</comment>
# <i>ln -sf /usr/portage/profiles/selinux/v2refpolicy/x86/hardened /etc/make.profile</i>
<comment>AMD64 (server):</comment>
# <i>ln -sf /usr/portage/profiles/selinux/v2refpolicy/amd64/server /etc/make.profile</i>
<comment>AMD64 (Hardened):</comment>
# <i>ln -sf /usr/portage/profiles/selinux/v2refpolicy/amd64/hardened /etc/make.profile</i>
</pre>

<note>
Tramite il comando eselect del pacchetto gentoolkit, è possibile cambiare il 
proprio profilo. Dal momento che il numero delle opzioni disponibili e la loro 
numerazione cambia in funzione del sistema in uso, il cambiamento di profilo
non verrà trattato in questa guida.
</note>

<impo>
Non utilizzare profili differenti da quelli appena elencati, anche se possono
sembrare superati: i profili di SELinux non vengono necessariamente creati così
spesso come quelli predefiniti di Gentoo.
</impo>

<impo>
Il profilo di SELinux dispone di un numero di flag USE significativamente
ridotto rispetto a quello predefinito. Utilizzare <c>emerge info</c> per
individuare quali flag USE debbano essere abilitate nuovamente in make.conf.
</impo>

<note>
Non è necessario aggiungere la flag USE "selinux" al proprio make.conf: il
profilo di SELinux la contiene già.
</note>

<note>
È possibile che portage visualizzi questo messaggio: "!!! SELinux module not
found. Please verify that it was installed." È un avvertimento normale, che
verrà corretto in seguito durante il processo di conversione.
</note>

</body>
</subsection>
</section>

<section>
<title>Aggiornare gli Header del Kernel</title>
<subsection>
<body>

<p>
Anzitutto occorre aggiornare alcuni pacchetti essenziali. Cominciare
controllando quale versione di linux-headers è installata.
</p>

<pre caption="Controllare la versione di linux-headers installata">
# <i>emerge -s linux-headers</i>
<comment>oppure con gentoolkit:</comment>
# <i>equery list -i linux-headers</i>
</pre>

<p>
Se è presente una versione antecedente alla 2.4.20, è necessario emergere una
versione più aggiornata.
</p>

<pre caption="Installare degli header aggiornati">
# <i>emerge \>=sys-kernel/linux-headers-2.4.20</i>
</pre>

</body>
</subsection>
</section>

<section>
<title>Aggiornare Glibc</title>
<subsection>
<body>

<p>
Dopo aver installato i nuovi header, oppure se non si è sicuri che la libreria
glibc sia stata compilata con gli header più recenti, occorre ricompilare glibc.
</p>

<pre caption="Ricompilare glibc">
# <i>emerge glibc</i>
</pre>

<impo>
Questa è una operazione critica. È necessario che glibc sia compilato con gli
header più recenti, altrimenti alcune operazioni potranno presenteranno dei
malfunzionamenti.
</impo>

</body>
</subsection>
</section>
</sections>