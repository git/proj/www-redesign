<?xml version='1.0' encoding="UTF-8"?>
<?xml-stylesheet href="/xsl/guide.xsl" type="text/xsl"?>

<!-- $Header: /var/cvsroot/gentoo/xml/htdocs/proj/en/infrastructure/tenshi/index.xml,v 1.6 2005/07/25 14:55:17 lcars Exp $ -->

<!DOCTYPE guide SYSTEM "/dtd/guide.dtd">

<guide link="/proj/en/infrastructure/tenshi/index.xml">

<title>Gentoo Linux Documentation -- Tenshi</title>

<author title="Author">
  <mail link="lcars@gentoo.org">Andrea Barisani</mail>
</author>

<abstract>
This page introduces tenshi, a log monitoring and reporting tool.
</abstract>

<version>0.3.4</version>
<date>2005-07-25</date>

<chapter>
<title>Introduction</title>
<section>
<body>

<p>
Tenshi is a log monitoring program, designed to watch one or more log files for
lines matching user defined regular expressions and report on the matches. The
regular expressions are assigned to queues which have an alert interval and a
list of mail recipients.
</p>

<p>
Queues can be set to send a notification as soon as there is a log line assigned
to it, or to send periodic reports.
</p>

<p>
Additionally, uninteresting fields in the log lines (such as PID numbers) can be
masked with the standard regular expression  grouping  operators ( ). This
allows cleaner and more readable reports. All reports are separated by hostname
and all messages are condensed when possible.
</p>

<p>
The program reads a configuration file and then forks a deamon for monitoring
the specified log files.
</p>

<p>
Please read the example <uri
link="http://www.gentoo.org/~lcars/tenshi/tenshi.conf">tenshi.conf</uri> and
tenshi.8 man page for usage instructions.<br/><br/>
</p>

<impo>
This package was formerly known as <c>Wasabi</c>, the name was changed due to
trademark infringement issues.
</impo>

</body>
</section>
</chapter>

<chapter>
<title>Examples</title>
<section>
<body>

<p>
Consider the following settings in tenshi.conf:
</p>

<pre caption="tenshi.conf queues settings">

...

set hidepid on

set queue mail     tenshi@localhost sysadmin@localhost [0 */12 * * *]
set queue misc     tenshi@localhost sysadmin@localhost [0 */24 * * *]
set queue critical tenshi@localhost sysadmin@localhost [now]

group ^ipop3d:

mail ^ipop3d: Login user=(.+)
mail ^ipop3d: Logout user=(.+)
mail ^ipop3d: pop3s SSL service init from (.+)
mail ^ipop3d: pop3 service init from (.+)
mail ^ipop3d: Command stream end of file, while reading.+
mail ^ipop3d: Command stream end of file while reading.+

critical ^ipop3d: Login failed.+

trash ^ipop3d:.+

group_end

critical ^sudo: (.+) : TTY=(.+) ; PWD=(.+) ; USER=root ; COMMAND=(.+)

misc .*
</pre>

<p>
Every ipop3d message not matched by the regexps assigned to the queue
<e>mail</e> or critical will be matched by the queue <e>trash</e> (a builtin
null queue), any other message will be matched by queue <e>misc</e>. Fields
enclosed in <c>(.+)</c> are masked.
</p>

<p>
This is a sample report for the <e>mail</e> queue (sent every 12 hours):
</p>

<pre caption="Sample Report - queue [mail]">

host1:
    79: ipop3d: Login user=___
    74: ipop3d: Logout user=___

host2:
    30: ipop3d: Login user=___
    30: ipop3d: Logout user=___
    19: ipop3d: pop3 service init from ___
    12: ipop3d: pop3s SSL service init from ___
    1: ipop3d: Command stream end of file while reading line user=??? host=bogus.domain.net [192.168.0.1]
    1: ipop3d: Command stream end of file, while reading authentication host=bogus1.domain.net [10.1.7.1]
</pre>

<p>
These are sample reports for the <e>critical</e> queue (sent every time a
message matches the regexp):
</p>

<pre caption="Sample Report - queue [critical]">
host1:
    1: /usr/bin/sudo: ___ : TTY=___ ; PWD=___ ; USER=root ; COMMAND=/bin/dmesg
</pre>

<pre caption="Sample Report - queue [critical]">
host1:
    1: /usr/bin/sudo: ___ : TTY=___ ; PWD=___ ; USER=root ; COMMAND=/bin/bash
</pre>

<pre caption="Sample Report - queue [critical]">
host2:
    1: ipop3d: Login failed user=admin auth=admin host=bogus1.domain.net [10.1.7.1]
</pre>

<pre caption="Sample Report - queue [critical]">
host2:
    1: ipop3d: Autologout user=??? host=bogus.domain.net [192.168.0.1]
</pre>

</body>
</section>
</chapter>

<chapter>
<title>Requirements</title>
<section>
<body>

<p>
Tenshi needs a working 'tail' implementation, it also requires Net::SMTP module
for mailing reports which should be included in your perl installation.
</p>

<p>
Gentoo Linux users can simply install <e>app-admin/tenshi</e> ebuild.
</p>

</body>
</section>
</chapter>

<chapter>
<title>Resources</title>
<section>
<body>

<p>
The most recent release of <c>tenshi</c> can be found at <uri
link="http://www.gentoo.org/~lcars/tenshi/tenshi-latest.tar.gz">tenshi-latest.tar.gz</uri>.
</p>

<p>
All releases are available at <uri>http://www.gentoo.org/~lcars/tenshi</uri>.
</p>

<p>
Please send requests/suggestions/bug reports to <mail>tenshi@gentoo.org</mail>.
</p>

</body>
</section>
</chapter>

</guide>
