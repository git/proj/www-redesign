<?xml version='1.0' encoding='UTF-8'?>
<?xml-stylesheet href="/xsl/guide.xsl" type="text/xsl"?>

<!DOCTYPE guide SYSTEM "/dtd/guide.dtd">
<guide link = "dev-machines.xml">
<title>Gentoo Developer/Test Machines</title>
<author title="Author">
    <mail link="vapier@gentoo.org">Mike Frysinger</mail>
</author>
<author title="Editor">
    <mail link="klieber@gentoo.org">Kurt Lieber</mail>
</author>
<abstract>
This document describes the computers that are available 
to Gentoo developers specifically for development purposes.
</abstract>

<version>2.0</version>
<date>2010-08-29</date>

<chapter>
<title>Gentoo Developer Hosts</title>

<section>
<title>Introduction</title>
<body>
<p>
This document describes the computers that are available to Gentoo developers
for the specific purpose of developing Gentoo.  Most often, this is just a
way to get access to different architectures or fast build machines.  Many
hosts are provided by developers themselves and thus maintained by them.  This
page exists only to help developers locate the admins of specific boxes for
gaining access.  Specific implementation is left up to the maintainer (who is
often the owner of the machine).
</p>
</body>
</section>

<section>
<title>Development Boxes</title>
<body>

<table>
 <tr>
  <th>Arch</th>
  <th>DNS</th>
  <th>Contact</th>
  <th>CPU</th>
  <th>RAM</th>
  <th>Misc</th>
 </tr>

 <tr>
  <ti>alpha</ti>
  <ti><uri link="http://dev.gentoo.org/~klausman/monolith.jpg">monolith.alpha.dev.gentoo.org</uri></ti>
  <ti><uri link="mailto:klausman@gentoo.org">Tobias Klausmann</uri></ti>
  <ti>4xEV68AL 833MHz</ti>
  <ti>8G</ti>
  <ti>AlphaServer ES40 w/6x18GB,6x72GB HD</ti>
 </tr>

 <tr>
  <ti>amd64</ti>
  <ti>pitr.amd64.dev.gentoo.org</ti>
  <ti><uri link="mailto:pitr-admins@gentoo.org">Mike Doty</uri></ti>
  <ti>2xOpteron 842 ~1.6GHz</ti>
  <ti>2.0G</ti>
  <ti>Donated by <uri link="http://amd.com/">AMD</uri>, Gentoo Foundation, and Gentoo/AMD64</ti>
 </tr>

 <tr>
  <ti>amd64</ti>
  <ti>dustpuppy.amd64.dev.gentoo.org</ti>
  <ti><uri link="mailto:pitr-admins@gentoo.org">Mike Doty</uri></ti>
  <ti>2xOpteron 842 ~1.6GHz</ti>
  <ti>1.0G</ti>
  <ti>Donated by <uri link="http://amd.com/">AMD</uri>, Gentoo Foundation, and Gentoo/AMD64</ti>
 </tr>

 <tr>
  <ti>amd64</ti>
  <ti>miranda.amd64.dev.gentoo.org</ti>
  <ti><uri link="mailto:pitr-admins@gentoo.org">Mike Doty</uri></ti>
  <ti>Dual Dual Core AMD Opteron(tm) Processor 280</ti>
  <ti>16G</ti>
  <ti>2 x 72GB U320 SCSI; Donated by <uri link="http://gni.com/">GNi</uri>, Global Netoptex, Inc.</ti>
 </tr>

 <tr>
  <ti>arm</ti>
  <ti><uri link="http://gentoo.org/~vapier/pics/arm-netwinder/">coral.arm.dev.gentoo.org</uri></ti>
  <ti><uri link="mailto:vapier@gentoo.org">Mike Frysinger</uri></ti>
  <ti>StrongARM110 ~275MHz</ti>
  <ti>128M</ti>
  <ti>Netwinder w/40G HD</ti>
 </tr>
 
 <tr>
  <ti>arm</ti>
  <ti>mv78100.arm.dev.gentoo.org</ti>
  <ti><uri link="mailto:robbat2@gentoo.org">Robin H. Johnson</uri></ti>
  <ti>Feroceon MV78100-A1 @ 1Ghz</ti>
  <ti>4G (3G usable)</ti>
  <ti>Marvell DB-78100-BP-A1 w/ 160G HD; Donated by <uri link="http://marvell.com/">Marvell</uri>; IPv6-accessible only</ti>
 </tr>

 <tr>
  <ti>hppa</ti>
  <ti><uri link="http://gentoo.org/~vapier/pics/hppa-c3600/">hake.hppa.dev.gentoo.org</uri></ti>
  <ti><uri link="mailto:vapier@gentoo.org">Mike Frysinger</uri></ti>
  <ti>PA8600 552MHz</ti>
  <ti>3G</ti>
  <ti>C3600 w/50G HD</ti>
 </tr>

 <tr>
  <ti>ia64</ti>
  <ti>beluga.ia64.dev.gentoo.org</ti>
  <ti><uri link="mailto:armin76@gentoo.org">Raúl Porcel</uri></ti>
  <ti>2 x Itanium 2 1.6GHz</ti>
  <ti>12G</ti>
  <ti>HP Integrity rx2600; Supported by <uri link="http://hp.com/">HP</uri>!  This machine is meant more for people doing serious IA64 development; general arch testing should be done on dolphin.</ti>
 </tr>

 <tr>
  <ti>ia64</ti>
  <ti>dolphin.ia64.dev.gentoo.org</ti>
  <ti><uri link="mailto:armin76@gentoo.org">Raúl Porcel</uri></ti>
  <ti>2 x Itanium 2 900MHz</ti>
  <ti>4G</ti>
  <ti>HP Integrity zx6000; Supported by <uri link="http://hp.com/">HP</uri>!</ti>
 </tr>

 <tr>
  <ti>ia64</ti>
  <ti>guppy.ia64.dev.gentoo.org</ti>
  <ti><uri link="mailto:halcy0n@gentoo.org">Mark Loeser</uri></ti>
  <ti>2 x Itanium 2 dual core 1.6GHz</ti>
  <ti>16G</ti>
  <ti>HP Integrity rx3600; Supported by <uri link="http://hp.com/">HP</uri>!  This machine is meant more for people doing serious IA64 development; general arch testing should be done on dolphin.</ti>
 </tr>

 <tr>
  <ti>mipsel</ti>
  <ti><uri link="http://gentoo.org/~vapier/pics/mipsel-raq2/">raq2.mips.dev.gentoo.org</uri></ti>
  <ti><uri link="mailto:vapier@gentoo.org">Mike Frysinger</uri></ti>
  <ti>QED RM5231 250MHz</ti>
  <ti>256M</ti>
  <ti>Raq2 Cobalt w/60G HD; requests must include a reference from a senior MIPS developer</ti>
 </tr>

 <tr>
  <ti>mipsel</ti>
  <ti>taijia (202.47.55.78:2207)</ti>
  <ti><uri link="mailto:redhatter@gentoo.org">Stuart Longland</uri></ti>
  <ti>660MHz Loongson 2E</ti>
  <ti>512MB DDR1</ti>
  <ti>40G HD; requests must include a reference from a senior MIPS developer</ti>
 </tr>

 <tr>
  <ti>s390</ti>
  <ti>lgentoo1.osdl.marist.edu</ti>
  <ti><uri link="mailto:vapier@gentoo.org">Mike Frysinger</uri></ti>
  <ti>1 x IBM/S390 z990</ti>
  <ti>512 MiB</ti>
  <ti>12 GiB; System for building/testing s390 packages and stages</ti>
 </tr>

 <tr>
  <ti>s390x</ti>
  <ti>lgentoo2.osdl.marist.edu</ti>
  <ti><uri link="mailto:vapier@gentoo.org">Mike Frysinger</uri></ti>
  <ti>1 x IBM/S390x z990</ti>
  <ti>512 MiB</ti>
  <ti>12 GiB; System for building/testing s390x packages and stages</ti>
 </tr>

 <tr>
  <ti>sh</ti>
  <ti><uri link="http://gentoo.org/~vapier/pics/sh4-lantank/">lantank.sh.dev.gentoo.org</uri></ti>
  <ti><uri link="mailto:vapier@gentoo.org">Mike Frysinger</uri></ti>
  <ti>SuperH4 266MHz</ti>
  <ti>64M</ti>
  <ti>LanTANK SH4 (SH7751R) w/200G HD</ti>
 </tr>

 <tr>
  <ti>sparc</ti>
  <ti>bender.sparc.dev.gentoo.org</ti>
  <ti><uri link="mailto:armin76@gentoo.org">Raúl Porcel</uri></ti>
  <ti>8-core UltraSPARC T1 @ 1.2 Ghz</ti>
  <ti>32G</ti>
  <ti>Sun T2000; Donated by <uri link="http://www.sun.com/">Sun Microsystems</uri></ti>
 </tr>
</table>

<note>If you want to get your machine added/updated in this list, just contact
<uri link="mailto:vapier@gentoo.org">Mike Frysinger</uri> :).</note>

<table>
 <tr>
  <th>Host</th>
  <th>SSH Key</th>
 </tr>

 <tr>
  <ti>monolith.alpha</ti>
  <ti>64:b9:3c:c7:55:ac:35:11:11:68:e1:f3:82:91:7f:cd</ti>
 </tr>

 <tr>
  <ti>pitr.amd64</ti>
  <ti>c4:35:fe:b4:72:32:6f:a0:ea:80:5d:cf:6f:25:50:6d</ti>
 </tr>

 <tr>
  <ti>miranda.amd64</ti>
  <ti>91:da:2c:15:49:77:a7:d7:20:b0:25:53:24:a2:ec:36</ti>
 </tr>

 <tr>
  <ti>coral.arm</ti>
  <ti>45:6d:f7:38:83:a2:ca:87:35:95:77:11:ea:cc:5e:cc</ti>
 </tr>
 
 <tr>
  <ti>mv78100.arm</ti>
  <ti>RSA 1b:43:1d:c2:e8:b8:9a:14:61:c8:5c:b9:d7:4e:84:7d DSA af:25:b1:e4:ed:e6:8d:03:fd:51:bd:4f:1c:65:d2:0e</ti>
 </tr>

 <tr>
  <ti>hake.hppa</ti>
  <ti>5a:6a:ba:14:e2:a3:0c:4a:2e:6b:63:0d:f2:db:1b:3f</ti>
 </tr>

 <tr>
  <ti>beluga.ia64</ti>
  <ti>1e:a1:ea:04:c5:87:4c:7b:75:80:04:d5:47:94:3a:45</ti>
 </tr>

 <tr>
  <ti>dolphin.ia64</ti>
  <ti>c8:fe:4f:1e:48:48:46:5a:40:7e:96:6c:a4:73:aa:50</ti>
 </tr>

 <tr>
  <ti>raq2.mips</ti>
  <ti>f6:0c:4e:b9:5b:cb:e5:0e:bf:b8:67:5c:be:5d:94:d7</ti>
 </tr>

 <tr>
  <ti>lgentoo1.s390</ti>
  <ti>c3:f0:e1:ba:9d:3a:eb:f1:39:fa:3c:5c:f5:24:76:a0</ti>
 </tr>

 <tr>
  <ti>lgentoo2.s390x</ti>
  <ti>24:e2:5a:a3:0b:34:49:1e:4d:2e:22:0f:8a:f9:7a:14</ti>
 </tr>

 <tr>
  <ti>lantank.sh</ti>
  <ti>22:fd:a1:94:a0:f8:82:ee:3f:25:19:95:13:5e:ce:b8</ti>
 </tr>

 <tr>
  <ti>bender.sparc</ti>
  <ti>bc:9a:a7:51:4f:76:c7:c8:1b:ed:37:6a:38:23:e9:3f</ti>
 </tr>
</table>

</body>
</section>

</chapter>
</guide>
