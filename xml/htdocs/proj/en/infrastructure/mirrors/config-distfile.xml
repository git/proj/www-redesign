<?xml version='1.0' encoding="UTF-8"?>
<?xml-stylesheet href="/xsl/guide.xsl" type="text/xsl"?>

<!DOCTYPE guide SYSTEM "/dtd/guide.dtd">

<guide link = "config-distfile.xml">
<title>Instructions for setting up a new distfile mirror.</title>
<author title="Author">
    <mail link="klieber@gentoo.org">Kurt Lieber</mail>
</author>
<abstract>
This guide documents how to administer our distfile mirroring system, including setting up, deleting and changing distfile mirrors. 
</abstract>
<version>0.9</version>
<date>27 August 2003</date>
<chapter>
<title>Creating a new distfile mirror</title>
<section>
<title>Abstract</title>
<body>
<warn>This file refers to the OLD administration system. <uri link="/proj/en/infrastructure/mirror-wrangling.xml">New system documentation available here.</uri></warn>
<p>
Setting up a new distfile mirror nvolves the following steps:
</p>
<ul>
	<li>Acknowledging the bug on bugzilla</li>
	<li>Send the mirror admin instructions on accessing the private master distfile mirror</li>
	<li>Testing the mirror for a period of time to ensure consistency</li>
	<li>Creating an entry on the <uri link="/main/en/mirrors.xml">Main Gentoo Mirrors</uri> page</li>
</ul>
</body>
</section>
<section>
<title>Bugzilla</title>
<body>
<p>
The first step in creating a new distfile mirror is for the user to file a bug on <uri link="http://bugs.gentoo.org">http://bugs.gentoo.org</uri>.  From there, the mirror admin should acknowledge the bug and verify that the following information was provided as part of the initial report:
</p>
<ul>
	<li>server name</li>
	<li>IP address of the mirror</li>
	<li>contact information of the server administrator</li>
	<li>maximum number of concurrent connections allowed</li>
	<li>server specs, including CPU, RAM and connection speed to the internet</li>
	<li>any other requirements listed on the <uri link="/doc/en/source_mirrors.xml">Distfile Mirrors Policy</uri> document</li>
</ul>
<p>
If part of the information is missing, please request it as a follow-up to the bug.  Then, continue to the next step.
</p>
</body>
</section>
<section>
<title>Accessing the Private Master Distfile Mirror</title>
<body>
<p>
Gentoo maintains a private, master distfile mirror which only other distfile mirrors are allowed to use.  This ensures that our mirrors are always able to access the latest files when a new version of a popular package, such as KDE or GNOME, is released.  As part of the distfile mirror setup process, you should send the mirror admin instructions on accessing this private mirror.  If you do not have these instructions, you can obtain them from <mail link="klieber@gentoo.org">Kurt Lieber</mail>.
</p>
<p>
Once you have sent the instructions, please make a note in the bug report asking the mirror admin to update their cron scripts to point to the new master mirror.
</p>
</body>
</section>
<section>
<title>Probationary Period</title>
<body>
<p>
New distfile mirror candidates need to go through a probationary period of at least 48 hours and preferably 96 hours where their server is checked periodically for timely updates with <e>gentoo.oregonstate.edu</e>.  During this time, monitor the distfile mirror to ensure it is updating at 4 hour intervals.  If the mirror shows inconsistencies of any type, report them on the bug entry and work with the server admin to resolve them if possible.  The easiest way to do this is to check the <e>timestamp.chk</e> file which can be found in the <e>/distfiles/</e> directory on the mirror.
</p>
<note>
A "TODO" item is to write a script that will automate the monitoring process during the probationary period.
</note>
</body>
</section>
<section>
<title>Adding Entries to the Main Mirrors page</title>
<body>
<p>
After the mirror candidate has passed the probationary period, the mirror admin should create an entry for the mirror on our <uri link="/main/en/mirrors.xml">Gentoo Mirrors</uri> page.
</p>
<note>
Make one entry per protocol that the new mirror is running
</note>
<p>
To create the entries, check out the latest version of the mirrors.xml file from CVS.  Make the updates to that file and commit it back to CVS. 
</p>
</body>
</section>
</chapter>
</guide>
