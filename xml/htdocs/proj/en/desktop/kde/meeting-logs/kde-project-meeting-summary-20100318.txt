1) Removal of innactive KDE team members
Since there were no objections last month for scarabeus' draft, he'll proceed in
removing some members that don't follow the rules. The draft is at
http://dev.gentoo.org/~scarabeus/kde-team-rules.html

2) Review work flow for KDE minor bumps and improve collaboration with arch teams
Nothing more was to be said from the last meeting, both scarabeus and jmbsvicetto's
proposals were accepted.

3) KDE-4.4 status
4.4.1 seems to be a good release, with not many problems so far. Also, there were
people working on the kdebindings which was suffering mostly. It will be a stable
candidate, and the bug will be openned at start of next month.

5) enterprise useflag for kdepim stuff (a use flag to follow the enterprise branch of the
kdepim repo, only for trunk)
The call for help didn't bring in any volunteers, so there was no progress on this. Sput told
that this is very important to be fixed, as kdepim may not be ready for KDE 4.5. Tampakrap will
try again to raise the issue in ml.

7) koffice status
KOffice still is in bad status, tampakrap and wired will work on this.

8) change kde-meta (and @kde-*) to include all modules (plus the developer specific ones)
Following last meeting's discussion on this, tampakrap opened a mail asking for people's
opinion on this in the gentoo-desktop mailing list, and many people liked the idea. So,
with scarabeus' permission, tampakrap will add this to kde-meta ebuilds. Kdebindings will
stay out for now until they all compile.

9) patches of kde-packager
No actions were done on this, jmbsvicetto said he'll try to take care of it.

10) Split desktop profile
After a long discussion in gentoo-dev and gentoo-desktop mailing lists, and with the
approval of the gnome and qt teams (the leads mostly, leio and yngwin), tampakrap
will finally split the desktop profile to kde and gnome subprofiles. Leio raised the
need of a better profile system that will support mixed profiles. Tampakrap liked the
idea and is willing to work on this, but it is going to be long-term
