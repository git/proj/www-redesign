Summary for the Gentoo KDE herd meeting on Thursday, 6th of March 2008

Participants:
caleb, cryos, deathwing00, genstef, ingmar, jmbsvicetto, keytoaster, philantrop, tgurr,	zlin

1. Bump to KDE 3.5.9:

Apart from a few minor issues, the bump to KDE 3.5.9 went well. 

https://bugs.gentoo.org/211116 (mixing split and monolithic ebuilds) was
discussed. 3 participants didn't want the current behaviour changed, 3 were in
favour of changing it. Thus, it was agreed that Ingmar, who volunteered to 
change it, should feel free to do it.

2. KDE 3.5.8 in stable for 2008.0.

Philantrop stated that KDE 3.5.8 will be in the 2008.0 release.

3. Removal of KDE < 3.5.8 from the tree.

gentoofan23 brought up the issue of the removal of older (specifically 3.5.7)
stable KDE versions from the tree after about 19 days and asked for a longer
period for users to upgrade to the newest stable version.
30 days were suggested as a rule of thumb and agreed upon by the majority of
the participants.

4. State of KDE4 in the tree.

Ingmar and zlin gave a short information about KDE4 in the tree. The ebuilds in
the tree are in a pretty good state. Their dependencies need to be updated for
compatibility with the newly introduced split Qt-4.4 ebuilds.

KDE 4.0.2 is currently planned to be included in the tree during the weekend.
(p.masked)

Anyone who wants to help with the bump to 4.0.2 should simply mail Philantrop
his ssh public key. Interested users who can show they're able and willing to
work on ebuilds are *explicitly* invited, too.
The bump to 4.0.2 is taking place in a git overlay again to allow for non-ebuild
devs to participate, too, and it's a great way to become a dev as Ingmar and
zlin can verify.

5. Herd Testers: Current state and next steps.

The HT stuff was delayed because of the trustee elections, etc.
jmbsvicetto and deathwing00 agreed to flesh out the work of a KDE Herd Tester
and publish information about it on the usual Gentoo news channels (pr@g.o, 
GMN, Forums).

6. Lead election:

Having a sub-lead was discussed to ensure not to have a single point of failure
in case the lead disappears. In a vote, 3 herd members expressed their wish for
a sub-lead, 5 voted against it and there was one abstention. Thus, the notion
was rejected.

deathwing00 initiated a vote for the lead. Philantrop was nominated and accepted
the nomination.
Philantrop was voted for by 9 of the participants, no votes against him and no
abstentions. Thus, Philantrop was elected the lead of the Gentoo KDE project.

deathwing00 is going to inform the GMN, pr@g.o and post it on the forums.

7. Review of the project page

gentoofan23 and deathwing00 volunteered to update the project page with respect
to KDE versions and other information in the sections 3, 4, 8 and 9 till the
next meeting.

8. Miscellaneous

It was decided to re-instate the monthly meetings on the first Thursday each 
month.

9. Date of the next meeting and closing.

The next KDE herd meeting takes place on Thursday, 3rd of April 2008.

Philantrop thanked all the Gentoo KDE people for their great work and their
trust in him.
