Roll-call:
pesa - excused (no net)
tampakrap - excused (family issues)
scarabeus, ABCD, ayoy, patrick, dagger, jmbsvicetto, revartm, wired, yngwin

KDE-3:
- As discussed before, the KDE team is going to move all KDE3 ebuilds to an overlay. The new
overlay hasn'te been named yet, but will probably be called kde-junk, kde-sunset or something
similar as we plan to use it for KDE stuff that is removed from the tree.
KDE3 is going to be moved to the overlay  either after KDE-4.4 is out if nothing evil happens,
or after we get 2 KDE4 minor versions marked stable - which likely means around the end of
the year.
- We are still looking for KDE maintainers. It seems sping might be interested - yngwin will
talk to him.
- Due to the current state of the KDE3 tree source, the lack of support by upstream and the
increasing security concerns, it's likely that we'll mask KDE3 soon. We're delaying the mask
until we get a version of KDE4 marked stable - unless more security issues crop up.
We plan to make an anouncement on Gentoo homepage and to write a news item about the status
of the KDE3 tree and the security implications.
We plan to keep KDE3 around in the tree until KDE-4.4 is released (but it will likely remain
masked) until it's moved to the new overlay.
- Before we remove KDE3 from the tree, we plan to have a news item, planet blog entries, forums
thread and front page announcement about it so the kde3ers won't scream for help all over the
place -  jmbsvicetto.

KDE-4:
- It was decided by a vote not to ask for 4.2.4 stabilization. It will be dropped from the tree.
- Instead, the current plan is to get 4.3.1 marked stable. For that, we need to focus on the
bugs in the tracker[1] and everyone needs to work on it.

QT4-TNG eclass:
Will be sent for review onto -dev in a week with the -tng name. No better name found out.

Future projects:
- Documentation polishing
- Branding the KDE
- Fix upstream buildsystem to allow install of different versions into a shared prefix

Sets:
. Adjust it as for bug #272488 or from the ground up for exact specs we need -
jmbsvicetto (we need to poke him about it often so he won't forget to do it)
