<?xml version='1.0' encoding="UTF-8"?>

<!DOCTYPE guide SYSTEM "/dtd/guide.dtd">

<guide link="prelude-ids.xml">
<title>Gentoo Linux Documentation -- Prelude Intrusion Detection System</title>
<author title="Author"><mail link="zack@tehunlose.com">
	Zack Gilburd</mail>
</author>
<author title="Contributors">
  <mail link="mboman@gentoo.org">Michael Boman</mail>
</author>
<author title="Contributors">
  <mail link="kzaraska@student.uci.agh.edu.pl">Krzysztof Zaraska</mail>
</author>

<abstract>
	This guide will assist you in setting up the Prelude Intrustion Detection System along with the rules needed to make it useful.
</abstract>
<version>0.0.99</version>
<date>17 Jul 2003</date>
<chapter>
	<title>About Prelude</title>
	<section>
		<title>Background Information</title>
		<body>
			<p>
			Prelude was founded and writen by Yoann Vandoorselaere in 1998. Many others have also greatly contributed to it.
			</p>
			<p>
			Prelude is a hybrid intrustion detection system that will detect and monitor security instrusions, whether they happen in an attack mobilized over the Internet or an attack mobilzed locally.  The monitoring work that Prelude does is made possible via an LML (Log Monitoring Lackey).  Prelude can also utilize the rulesets from intrusion detection systems such as Snort.
			</p>
		</body>
	</section>
	<section>
		<title>What Are the Components?</title>
		<body>
			<ul><li><path>prelude-manager</path> : The manager is the place where all the main logging is done.  When the manager receives a signal from the sensors, it logs the signal so the user can investigate.  Logging can either be done to a file or to a datebase such as MySQL.  The latter is the recommended solution.</li></ul>
			<ul><li><path>prelude-nids</path> : NIDS is a plugin for Prelude and stands for Network Intrusion Detection System.  The prelude-nids package should definately be used along side Prelude proper, but is not mandatory. The NIDS package also provides for functionality like that of <uri link="http://snort.org">Snort</uri></li></ul>
			<ul><li><path>prelude-lml</path> : The LML stands for Log Monitoring Lackey.  Like the NIDS, it is also a sensor.  The LML watches your logfiles and looks for anything out of the ordinary.  Should abnormalities be found, an alert is sent to the manager.</li></ul>
			<ul><li><path>libprelude</path> : libprelude provides for the libraries necessary in order for the manager to be able to talk to the other plugins.  It also provides the sensors with extra features.</li></ul>
			<ul><li><path>piwi</path> : PIWI stands for Prelude Intrusion (Detection System) Web Interface.  The title pretty much describes the said package; it is an interface powered by perl that can help the end user manage their rules and see when attacks are happening or have happened.</li></ul>
		</body>
	</section>
</chapter>
<chapter>
	<title>Installing Prelude</title>
	<section>
		<title>Emerging the Packages</title>
		<body>
			<p>
				We will now begin by adding <path>ssl</path> to our <path>make.conf</path>, then emerging each of the packages described above.
			</p>
			<pre caption="/etc/make.conf">
<comment>You do not have to delete other entries from your USE, just add ssl.</comment>
USE="ssl"
</pre>
			<pre caption="Starting the Emerges">
<comment>Emerging the libraries.</comment>
# <i>emerge libprelude</i>
<comment>Now for the log lackey.</comment>
# <i>emerge prelude-lml</i>
<comment>Installing the Network Intrustion Detection System</comment>
# <i>emerge prelude-nids</i>
<comment>Now for the most important component: The manager.</comment>
# <i>emerge prelude-manager</i>
<comment>Lastly, we will install PIWI.</comment>
# <i>emerge piwi</i>
</pre>
		</body>
	</section>
</chapter>
<chapter>
	<title>Configuring Prelude</title>
	<section>
		<title>Setting up the Manager</title>
		<body>
			<p>
				We will now edit the Manager's main configuration file, <path>prelude-manager.conf</path>.  Two of the most important settings are for changing where Prelude will listen.  For instance, if you have two IPs but only one Prelude to listen on one of them, you would supply the said IP in the configuration.</p>
			<pre caption="/etc/prelude-manager/prelude-manager.conf">
# Sensor Server is listening on any IP 
sensors-srvr = 0.0.0.0; 
# Admin Server is listening on any IP 
admin-srvr = 0.0.0.0; 
</pre>
		</body>
	</section>
	<section>
		<title>Setting up the Database</title>
		<body>
		<p>
			If you want to set up Prelude to work with its backend being a database like MySQL or PostgreSQL (and believe me, you do), then you will want to continue with this section.  If you really and truly would rather use plaintext logging, then you can skip this section.
		</p>
		<impo>Your SQL server, whether it be MySQL or PostgreSQL, needs to be running before you proceed.</impo>
		<pre caption="Creating the Database">
# <i>/usr/bin/prelude-manager-db-create.sh</i>

Prelude Database Support Installation
=====================================

*** Phase 0/7 ***

Warning: if you want to use database support with prelude
 You should dedicate the database for this job only.

So if you ever have a database running for another job
 please think about taking it away, because this script
 will install prelude as a dedicated database and you
 could meet some troubles with your old bases.

<comment>Since we want database support, we are going to say &quot;y&quot; here.</comment>
Do you want to install a dedicated database for prelude ?
 (y)es / (n)o : y


*** Phase 1/7 ***

<comment>Here you can either chose to have your database be MySQL (mysql) or
PostgreSQL (pgsql).  I'll be choosing MySQL.</comment>
Enter the type of the database [mysql|pgsql]: mysql


*** Phase 2/7 ***

<comment>Unless you are going to be running the MySQL server on a different
box than Prelude, just hit ENTER here to choose &quot;localhost&quot;.</comment>
Enter the name of the host where the database is running [localhost]:


*** Phase 3/7 ***

<comment>3306 is the default port for MySQL, so unless you plan on running
the MySQL daemon on a different port, then just hit ENTER here.</comment>
Enter the port where the database is running [3306]:


*** Phase 4/7 ***

<comment>Hit ENTER here to have the database that stores all the information
that Prelude keeps track of be named &quot;prelude&quot;.</comment>
Enter the name of the database that should be created to stock alerts [prelude]:

*** Phase 5/7 ***

<comment>You can go ahead and hit ENTER here unless you have your MySQL super-user
set up under a different name.</comment>
This installation script has to connect to your mysql database in order to creat
e a user dedicated to stock prelude's alerts
What is the database administrative user ? [root]:

We need the password of the admin user "root" to log on the database.
By default under mysql, root has an empty password.
Please enter a password:
Please confirm entered password:

*** Phase 6/7 ***

We need to create a database user account that will be used by the Prelude Manag
er in order to access the "prelude" database.

Username to create [prelude] :

We need to set a password for this special "prelude" account.
This password will have to be used by prelude-manager to access the database.
Please enter a password:
Please confirm entered password:

*** Phase 7/7 ***

Please confirm those information before processing :

Database name   : prelude
Database admin user: root
Database admin password: (not shown)

prelude owner user: prelude
prelude owner password: (not shown)

Is everything okay ? (yes/no) : yes

Creating the database prelude...

Creating user "prelude" for database "prelude",
using "root" to connect to the database.

Creating tables with /usr/share/prelude-manager/mysql/mysql.sql

-------------- End of Database Support Installation -------------
If it succeeded, you should now be able to launch prelude-manager like that :
==&gt;  prelude-manager --mysql --dbhost localhost --dbname prelude --dbuser pre
lude --dbpass xxxxxx

Or you may modify the prelude-manager configuration file (/usr/local/etc/prelude
-manager/prelude-manager.conf by default) in order to launch prelude-manager wit
hout database arguments:
---------- cut here ---&gt;
[MySQL]
# Host the database is listening on.
dbhost = localhost;
# Port the database is listening on.
dbport = 3306;
# Name of the database.
dbname = prelude;
# Username to be used to connect the database.
dbuser = prelude;
# Password used to connect the database.
dbpass = xxxxxx;
&lt;--- cut here ----------

Replace xxxxxx by the password you choose for the manager account
-----------------------------------------------------------------

</pre>
      </body>
    </section>
    <section>
      <title>NIDS Configuration</title>
      <body>
	<p>
	  Now we just need to set up NIDS so it knows which ethernet device to monitor.</p>
	  <pre caption="/etc/conf.d/prelude-nids">
<comment>Change eth0 to match the ethernet device to be monitored.</comment>
OPTIONS="-i eth0"
</pre>
      </body>
    </section>
  </chapter>
  <chapter>
    <title>Installing Sensors</title>
    <section>
      <title>Prerequisit Configuration</title>
      <body>
	<p>
	  We will now be setting up the default configuration for the sensors in the <path>/etc/prelude-sensors/sensors-default.conf</path> file.  This will be used globally for the sensors.  You can edit the below and then place it in the configuration file.
	  </p>
	<pre caption="/etc/prelude-sensors/sensors-default.conf">
<comment># Replace this with the IP of the manager.</comment>
manager-addr = 192.168.0.1; 
<comment># Here you will want to fill in your full hostname.</comment>
node-name = yourbox.yourdomain.com; 
<comment># This is just a plaintext descriptor.  You can put almost anything here.</comment>
node-location = Rack 2, Server 5. Monitoring Network A from an SPAN port on switch 28A; 
[Node Adress] 
<comment># The IP address of the box Prelude is being set up on.</comment>
address = 192.168.0.1; 
<comment># The netmask for the box.</comment>
netmask = 255.255.255.0; 
</pre>
	<p>
	  We will now be adding our sensors to the manager.  There are two ways of setting up the manager to talk to the sensors: via an SSL encrypted connection and via an unencrypted connection.  The only time when you will want to opt for the latter is when the manager and the sensor are on the same box.</p>
      </body>
    </section>
    <section>
      <title>Installing the NIDS Sensor</title>
      <body>
	<p>
	We will now run the necessary commands to set up the SSL connection.
	  </p>
      <pre caption="Setting Up the Encrypted Connection">
# <i>manager-adduser</i>

No Manager key exist... Building Manager private key...

<comment>How many bits should the encryption be?  I would recommend just hitting
ENTER here.</comment>
What keysize do you want [1024] ?


Please specify how long the key should be valid.
        0    = key does not expire
        &lt;n&gt;  = key expires in n days

<comment>Here you can hit ENTER again to select a key that does not expire.</comment>
Key is valid for [0] :


Key length        : 1024
Expire            : Never
<comment>Granted everything is okay, type in &quot;yes&quot; and hit enter.</comment>
Is this okay [yes/no] : yes


Generating a 1024 bit RSA private key...
................++++++
...........................++++++
Writing new private key to '/etc/prelude-manager/prelude-manager.key'.
Adding self signed Certificate to '/etc/prelude-manager/prelude-manager.key'


<comment>This password is VERY important.  Do NOT lose it until you've completed the sensor-adduser.</comment>
Generated one-shot password is "p=7f6N7+".

This password will be requested by "sensor-adduser" in order to connect.
Please remove the first and last quote from this password before using it.

waiting for install request from Prelude sensors...
<comment>Do not close this terminal!  Leave it open an open another session to
continue the guide.</comment>
</pre>
	<p>
	  Now open up another terminal if you have not already done so and proceed to add the sensor user.  Right now we will be adding the user for the NIDS component to Prelude.
	  </p>
	  <impo>Remeber that if both the sensor and the manager are running on the same machine, it is important to specify the machines ethernet IP, not <path>127.0.0.1</path>.  If you specify <path>127.0.0.1</path>, <c>sensor-adduser</c> will default to an unencrypted connection.<br /><br />However, if you do not want to use SSL, specify the said IP.
	  </impo>
	  <pre caption="Adding the Sensor User">
<comment> You will want to change &quot;192.168.1.102&quot; if the manager is on a different IP.</comment>
# <i>sensor-adduser -s prelude-nids -m 192.168.1.102 -u 0</i>


Now please start "manager-adduser" on the Manager host where
you wish to add the new user.

Please remember that you should call "sensor-adduser" for each configured
Manager entry.

<comment>We have already done this; hit ENTER.</comment>
Press enter when done.

Please use the one-shot password provided by the "manager-adduser" program.

<comment>Enter that password that I talked about above.  I hope you did not lose it ;).
Also, be aware that while I am going to fill in the fields here, the password will
not echo back to you.</comment>
Enter registration one shot password : p=7f6N7+
Please confirm one shot password : p=7f6N7+
<comment>If you do not see that the connection suceeded then you closed the terminal
that I told you not to.  Remove /etc/prelude-manager/prelude-manager.key and start
again with manager-adduser.</comment>
connecting to Manager host (127.0.0.1:5553)... Succeeded.


What keysize do you want [1024] ? 1024


Please specify how long the key should be valid.
        0    = key does not expire
        &lt;n&gt;  = key expires in n days

Key is valid for [0] : 0


Key length        : 1024
Expire            : Never

Is this okay [yes/no] : yes
Generating a 1024 bit RSA private key...
...........++++++
........................................++++++
Writing new private key to '/etc/prelude-sensors/ssl/prelude-nids-key.0'.
Adding self signed Certificate to '/etc/prelude-sensors/ssl/prelude-nids-key.0'
writing Prelude Manager certificate.
Using already allocated ident for prelude-nids@yourbox: 1057315311.
</pre>
	<p>
	  Now switch back to the terminal with manager-adduser running in it.  You should see output that resembles that below.
	  </p>
	<pre caption="manager-adduser Output">
Connection from 192.168.1.102.
sensor choose to use SSL communication method.
Writing Prelude certificate to /etc/prelude-manager/prelude-sensors.cert
Registration completed.
</pre>
      </body>
    </section>
    <section>
      <title>Adding the LML Sensor</title>
      <body>
	<p>
	  We will now set up the Log Monitoring Lackey.
	  </p>
	<note>You may realize that there are quite a bit of lines of output &quot;missing&quot; from this example.  In fact, the lines of output that are not present in this example go away after the initial <c>manager-adduser</c></note>
	<pre caption="Setting up the Manager for the LML">
# <i>manager-adduser</i>


Generated one-shot password is "4;%f7%1Y".

This password will be requested by "sensor-adduser" in order to connect.
Please remove the first and last quote from this password before using it.

waiting for install request from Prelude sensors...
</pre>
	<p>
	  Again, switch over to another terminal and proceed with the next example.
	  </p>
	<note>
	  We will be using the same methods we used in the NIDS example, so the same comments in red from before apply here, too.
	  </note>
	<pre caption="Setting up the LML">
# <i>sensor-adduser -s prelude-lml -m 192.168.101 -u 0</i>


Now please start "manager-adduser" on the Manager host where
you wish to add the new user.

Please remember that you should call "sensor-adduser" for each configured
Manager entry.

<comment>Hit enter; we have already started manager-adduser.</comment>
Press enter when done.



Please use the one-shot password provided by the "manager-adduser" program.

Enter registration one shot password : 4;%f7%1Y
Please confirm one shot password : 4;%f7%1Y
connecting to Manager host (127.0.0.1:5553)... Succeeded.

What keysize do you want [1024] ? 1024


Please specify how long the key should be valid.
        0    = key does not expire
        &lt;n&gt;  = key expires in n days

Key is valid for [0] : 0


Key length        : 1024
Expire            : Never

Is this okay [yes/no] : yes
Generating a 1024 bit RSA private key...
...............++++++
.++++++
Writing new private key to '/etc/prelude-sensors/ssl/prelude-lml-key.0'.
Adding self signed Certificate to '/etc/prelude-sensors/ssl/prelude-lml-key.0'
writing Prelude Manager certificate.
Using already allocated ident for prelude-lml@yourbox: 1057887742.
</pre>
      </body>
    </section>
  </chapter>
  <chapter>
    <title>Post Installation</title>
    <section>
      <title>Testing the Manager</title>
      <body>
	<p>
	  On the manager box, start the Prelude manager in the foreground.
	  </p>
	<pre caption="Starting the Manager">
# <i>prelude-manager</i>
- Initialized 2 reporting plugins.
- Initialized 1 database plugins.
- Subscribing Prelude NIDS data decoder to active decoding plugins.
- Initialized 1 decoding plugins.
- Initialized 0 filtering plugins.
- Subscribing TextMod to active reporting plugins.
- sensors server started (listening on 127.0.0.1:5554).
</pre>
	<p>
	  Now go ahead and switch over to the sensor box.  We will test the communication by using the NIDS sensor.
	  </p>
	<pre caption="Starting the NIDS Sensor">
<comment>Remember to change the manager address if it differs from the example.</comment>
# <i>prelude-nids -i eth0 --manager-addr 127.0.0.1</i>
- Initialized 3 protocols plugins.
- Initialized 5 detections plugins.

- RpcMod subscribed for "rpc" protocol handling.
- TelnetMod subscribed for "telnet" protocol handling.
- HttpMod subscribed for "http" protocol handling.
- Done loading Unicode table (663 Unichars, 0 ignored, 0 with errors)
- ScanDetect subscribed to : "[TCP,UDP]".
- ArpSpoof subscribed to : "[ARP]".
/etc/prelude-nids/ruleset/web-misc.rules (7) Parse error: Unknow key regex
/etc/prelude-nids/ruleset/web-misc.rules (65) Parse error: Unknow key regex
- Signature engine added 890 and ignored 2 signature.
- Connecting to Unix prelude Manager server.
- Plaintext authentication succeed with Prelude Manager.

- Initializing packet capture.
</pre>
	<p>
	  Make sure that your output looks relatively the same.  Let us make sure that we have the important output displaying correctly.
	  </p>
	<pre caption="Important output from NIDS">
- Connecting to Unix prelude Manager server.
- Plaintext authentication succeed with Prelude Manager.
</pre>
	<pre caption="Important output from the manager after we have started NIDS">
[unix] - accepted connection.
[unix] - plaintext authentication succeed.
[unix] - sensor declared ident 578232824809457160.
</pre>
	<p>
	  If you do not see those two sets of output, make sure that the manager is listening on the right IP and that the manager address is supplied properly for NIDS.
	  </p>
      </body>
    </section>
  </chapter>
  <chapter>
    <title>Running and Managing Prelude</title>
    <section>
      <title>Starting up the Prelude Daemons</title>
      <body>
	<p>
	  There are several init scripts that control the different parts to Prelude, so we will want to start those up now.
	</p>
	<pre caption="Starting the Prelude Daemons">
<comment>First, we will start up the manager.</comment>
# <i>/etc/init.d/prelude-manager start</i>
<comment>Next, it is time to start the NIDS</comment>
# <i>/etc/init.d/prelude-nids start</i>
<comment>And finally, we will start up the LML.</comment>
# <i>/etc/init.d/prelude-lml start</i>
</pre>
	<p>
	  Most likely, you are going to want Prelude and its components to start up when you boot up the computer.  In order to achieve this, we will add the necessary components to the default runlevel.
	</p>
	<pre caption="Adding the Daemons to the Run Level">
# <i>rc-update add prelude-manager default</i>
# <i>rc-update add prelude-nids default</i>
# <i>rc-update add prelude-lml default</i>
</pre>
	</body>
      </section>
      <section>
	<title>Installing PIWI</title>
	<body>
	  <p>
	    The first thing we will do to get PIWI working is emerge it.
	  </p>
	    <pre caption="Emerging PIWI">
# <i>emerge piwi</i>
</pre>
	    <p>
		We will now follow the instructions that the emerge process gives us
	    </p>
	    <impo>Depending on what version of Apache you are running, the following file names may vary.  If you are using Apache2, the files will be located in <path>/etc/apache2/conf</path> and the files will be named differently.  Usually, the file names will differ only by a present "2" that is not there in the Apache1 file names.  For example, <path>apache.conf</path> becomes <path>apache2.conf</path> in Apache2.</impo>
	    <pre caption="/etc/apache/conf/apache.conf">
<comment>The best place for this line is probably at the end of the file.</comment>
Include /etc/piwi/piwi-apache.conf
</pre>
	    <p>Now we will tell Apache to load the PIWI specific configuration directives.  If we were to skip this step, when you go to the location of your website with the PIWI files, the Perl scripts will likely just show up as plain text.</p>
	    <note>If you are already loading other Apache modules, you merely have to add <path>-D PIWI</path> rather than replacing the whole <path>APACHE_OPTS</path> line.</note>
	    <pre caption="/etc/conf.d/apache">
APACHE_OPTS="-D PIWI"
</pre>
	    <p>
	    Next, we need to edit the PIWI configuration file to match our MySQL database settings that we used for Prelude.
	    </p>
	    <pre caption="/etc/piwi/config.pl">
<comment>Edit the next two lines to suit your setup.</comment>
$conf{'dblogin'}='prelude';
$conf{'dbpasswd'}='dbpass';
</pre>
	    <p>
	    All that is left to do is start up Apache and check to make sure that the PIWI scripts are being processed correctly.
	    </p>
	    <pre caption="Starting Apache">
# <i>/etc/init.d/apache start</i>
</pre>
	    <p>
	    Now point your browswer to <path>http://yoursite/piwi</path> and you should be greeted by a Web interface.
	    </p>
	</body>
      </section>
    </chapter>
    <chapter>
      <title>Credits</title>
      <section>
	<title>Works Cited</title>
	<body>
	  <ul><li>Collective Work.  PreludeIntrusionDetectionSystem - Gentoo Wiki.</li></ul>
	  <ul><li><mail link="polombo@cartel-securite.fr">Polombo, Daniel</mail>.  <uri link="http://prelude-ids.org/article.php3?id_article=6">Prelude Hybrid IDS</uri>.</li></ul>
	</body>
      </section>
    </chapter>
</guide>
