<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet href="/xsl/project.xsl" type="text/xsl"?>
<?xml-stylesheet href="/xsl/guide.xsl" type="text/xsl"?>
<!DOCTYPE project SYSTEM "/dtd/project.dtd">
<project>
  <name>hardened</name>
  <longname>Hardened Gentoo</longname>

  <description>Hardened Gentoo brings advanced security measures to
  Gentoo Linux.</description>

  <longdescription><p>Hardened Gentoo is a project which oversees the
  research, implementation, and maintainence of security oriented
  projects for Gentoo Linux. We are a team of very competent
  individuals dedicated to bringing advanced security to Gentoo
  with a number of subprojects.</p></longdescription>

  <goals><p>Hardened Gentoo's purpose is to make Gentoo viable for
  high security, high stability production server environments.
  This project is not a standalone project disjoined from Gentoo
  proper; it is intended to be a team of Gentoo developers which
  are focused on delivering solutions to Gentoo that provide strong
  security and stability. These solutions will be available in
  Gentoo once they've been tested for security and stability by the
  Hardened team.</p></goals>

  <dev description="SELinux">pebenito</dev>
  <dev description="PaX/Grsecurity Hardened Toolchain">gengor</dev>
  <dev description="PaX/Grsecurity Hardened Toolchain">zorry</dev>
  <dev description="PaX/Grsecurity Hardened Toolchain">blueness</dev>
  <dev description="Bastille">Battousai</dev>
  <dev description="PPC arch team liaison">nixnut</dev>

  <subproject ref="/proj/en/hardened/selinux/index.xml" inheritresources="yes" />
  <subproject ref="/proj/en/hardened/rsbac/index.xml" inheritresources="yes" />
  <extraproject name="PaX/Grsecurity" lead="gengor">
  Grsecurity is a complete security solution
  providing such features as a MAC or RBAC system, Chroot
  restrictions, address space modification protection (via PaX),
  auditing features, randomization features, linking restrictions
  to prevent file race conditions, ipc protections and much more.
  </extraproject>

  <extraproject name="Hardened Toolchain" lead="gengor">Transparent
  implementation of 
  <uri link="http://pax.grsecurity.net/docs/aslr.txt">
  PaX</uri> address space layout randomizations and stack smashing
  protections using ELF shared objects as executables.</extraproject>

  <extraproject name="Hardened-Sources" lead="gengor">A kernel which
  provides patches for hardened subprojects, and stability/security
  oriented patches. Includes Grsecurity and SELinux.</extraproject>

  <extraproject name="Bastille" lead="Battousai">Bastille is an
  interactive application which gives the user suggestions on
  securing their machine. It will be customized to make suggestions
  about other Hardened Gentoo subprojects.</extraproject>

  <plannedproject name="Security Documentation">Maintain
  documentation about best practices, and general security measures
  such as process limiting, setting quotas, securing systems with
  kerberos, chrooting, tightening services, etc.</plannedproject>

  <resource link="http://www.gentoo.org/proj/en/hardened/primer.xml">
  Introduction to Hardened Gentoo</resource>
  <resource link="http://www.gentoo.org/proj/en/hardened/hardenedfaq.xml">
  Hardened Frequently Asked Questions</resource>
  <resource link="http://www.gentoo.org/proj/en/hardened/roadmap.xml">
  Hardened Roadmap</resource>
  <resource link="http://www.gentoo.org/proj/en/hardened/hardenedxorg.xml">
  Using Xorg with Hardened</resource>
  <resource link="http://www.gentoo.org/proj/en/hardened/hardened-toolchain.xml">
  Hardened Toolchain Technical Description</resource>
  <resource link="http://www.gentoo.org/proj/en/hardened/pax-quickstart.xml">
  A quickstart covering PaX and Hardened Gentoo</resource>
  <resource link="http://www.gentoo.org/proj/en/hardened/pax-utils.xml">
  PaX Utils</resource>
  <resource link="http://www.gentoo.org/proj/en/hardened/grsecurity.xml">
  Grsecurity2 QuickStart Guide</resource>
  <resource link="http://www.gentoo.org/proj/en/hardened/capabilities.xml">
  Capabilities Listing</resource>
  <resource link="http://www.gentoo.org/proj/en/hardened/pic-guide.xml">
  PIC Intro (beginner)</resource>
  <resource link="http://www.gentoo.org/proj/en/hardened/pic-internals.xml">
  PIC Internals (intermediate)</resource>
  <resource link="http://www.gentoo.org/proj/en/hardened/pic-fix-guide.xml">
  PIC Fixing (advanced)</resource>
  <resource link="http://www.gentoo.org/proj/en/hardened/gnu-stack.xml">
  GNU Stack Quickstart</resource>

  <extrachapter position="bottom">
    <title>I Want to Participate</title>
    <section>
      <body>
        <p>To participate in the Hardened Gentoo project first join
        the mailing list at 
        <c>gentoo-hardened@gentoo.org</c>. Then ask if there are
        plans to support something that you are interested in,
        propose a new subproject that you are interested in or
        choose one of the planned subprojects to work on. You may
        talk to the developers and users in the IRC channel 
        <c>#gentoo-hardened</c> on 
        <c>irc.freenode.net</c> for more information or just to chat
        about the project or any subprojects. If you don't have the
        ability to actively help by contributing work we will
        always need testers to maintain the security and stability
        of the overall product. All development, testing, and
        productive comments and feedback will be greatly
        appreciated.</p>
      </body>
    </section>
  </extrachapter>
  <herd name="hardened" />
</project>
