<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE guide SYSTEM "/dtd/guide.dtd">
<guide link="/doc/en/why_build_from_source.xml" lang="en">
  <title>Why Build From Source</title>

  <author title="Author">
    <mail link="nichoj@gentoo.org">Joshua Nichols</mail>
  </author>
  <author title="Editor">
    <mail link="wltjr@gentoo.org">William L. Thomson Jr.</mail>
  </author>
  <abstract>
    This document provides information on why one should and why we do build
    FOSS Java applications from source instead of shipping pre-compiled
    binaries.
  </abstract>

  <!-- The content of this document is licensed under the CC-BY-SA license -->
  <!-- See http://creativecommons.org/licenses/by-sa/2.5 -->
  <license/>

  <version>1.1</version>
  <date>2006-12-08</date>

  <chapter>
    <title>Why build from source?</title>
    <section>
      <body>
        <dl>
          <dd>
            <ul>
              <li>
		Building from source ensures that the source exists and
		compiles properly, which is the basis of regular open-source 
		development.
              </li>
              <li>
		Bugs may be found and fixed between upstrem releases. By
		compiling from source, we are able to isolate patches to
		address such problems, without having to wait for upstream.
              </li>
              <li>
		When security flaws are found, we want to be able to issue fix
		as soon as possible. It is not always feasible to wait for the
		upstream project to make a new release. With binary-only
		packages, the only fix we can offer is disabling the software
		entirely by masking it.
              </li>
              <li>
		For immature library packages, where documentation is often
		scanty, the user can easily run javadoc on the sources, as a 
		stop-gap measure, without needing to dig out the source code
		from upstream.
              </li>
              <li>
		Although bytecode runs on every vm that supports the version of
		the bytecode in question it does not mean that the bytecode
		produced by different compilers is equal. Bytecode can be
		optimized too. By building from source, we make it easy to
		switch the compiler used to compile your installed java
		programs.
              </li>
              <li>
		In the future building, native compiling from Java source code
		using gcj could become a serious option. If we try to add all
		packages to the tree in a way where they are getting built from
		their source code, we could make it possible in the future to
		create native binaries from Java packages. With binary packages
		this wouldn't be possible if upstream doesn't provide natively
		compiled packages.
              </li>
              <li>
		USE flags will normally impact on which features are to be
		compiled in, and which dependencies are brought in.
              </li>
              <li>
		End-users may want to manually patch or tweak the sources
		between src_unpack and src_compile. 
              </li>
              <li>
		It is common that we need to apply Gentoo-specific tweaks and
		intermediary patches when upstream takes a long time to issue a
		new release, which is almost always only possible when we
		compile from sources.
              </li>
            </ul>
          </dd>
        </dl>
      </body>
    </section>
  </chapter>
</guide>
