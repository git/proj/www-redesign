<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE guide SYSTEM "/dtd/guide.dtd">
<!-- $Header: /var/cvsroot/gentoo/xml/htdocs/proj/en/java/java-devel.xml,v 1.38 2009/05/10 19:53:55 betelgeuse Exp $ -->

<guide link="/doc/en/java-devel.xml">
<title>Gentoo Java Packaging Guide</title>

<author title="Author">
  <mail link="karltk@gentoo.org">Karl Trygve Kalleberg</mail>
</author>

<author title="Author">
  <mail link="nichoj@gentoo.org">Joshua Nichols</mail>
</author>

<author title="Author">
  <mail link="betelgeuse@gentoo.org">Petteri Räty</mail>
</author>

<abstract>
This document serves two purposes. The first is to discuss the specifics of how Gentoo handles the Java platform. The second is to discuss how to package Java packages for Gentoo. This document assumes you are familiar with the Java User Guide.
</abstract>

<!-- The content of this document is licensed under the CC-BY-SA license -->
<!-- See http://creativecommons.org/licenses/by-sa/2.5 -->
<license/>

<version>1.0.6</version>
<date>2008-01-18</date>

<chapter>
<title>Background</title>

<section>
<body>
<p>
Before going into details of how Java is handled on Gentoo, it is worthwhile to take a few moments to discuss current trends in the Java programming world.
</p>
</body>
</section>

<section>
<title>Java Build Systems</title>
<body>

<p>
There are a few build systems commonly used for Java projects. Apache's <c>ant</c> (Another Neat Tool) is by far the most common. It is a task oriented system. This means that you give Ant instructions on what tasks to perform and how to perform them to build your project. For example, compile these source files using this class path, create a jar of these classes, and so on. It is fairly easy to get up and running with Ant. Unfortunately, it leaves a lot of room for variation, so Ant scripts are terribly inconsistent from one project to the next. 
</p>

<p>
<c>Maven</c> is another Apache project which has been gaining popularity. In contrast to <c>ant</c>, it is a project based system. This means that you give maven information about your project, and once that's established, you can do many things without any further configuration. For example, you tell <c>maven</c> your project's name and where your source is, and you can then tell <c>maven</c> "Hey <c>maven</c>, make me a jar and some javadocs too!" Another feature of maven is its ability take care of making a project's dependencies available by downloading them from mirrors.
</p>

<p>
You can also find a number of packages using the classic combination of autoconf and automake. They will mostly be found on projects that interact with existing C and C++  libraries. Unfortunately, automake and autoconf are used mostly dealing with the non-Java bits, leaving the Java bits wedged where it can fit.
</p>
<p>
Lastly, you may find custom scripts which will attempt to run <c>javac</c>, <c>jar</c>, and <c>javadoc</c> itself, or it may just be a wrapper for ant to properly prepare the build environment.
</p>

</body>
</section>

<section>
<title>Bundled Dependencies</title>
<body>

<p>
One of the features of Java has always been "compile once, run everywhere." A consequence of this is that Java developers traditionally bundle all library dependencies with their project's distribution. While this might be convenient for the end-user, it rapidly becomes problematic from a packager's point of view.
</p>

<p>
For example, project A depends on C, and project B depends on C, and you have project A and B installed, then you'd have two copies of C installed to support it. And if there's new release of C with a bug or security fix , you would want both A and B to take advantage of it.
</p>

</body>
</section>
</chapter>

<chapter>
<title>Java on Gentoo</title>

<section>
<body>
<p>
This section will give you more insight into how Gentoo handles Java. You should be familiar with the Java User Guide before proceeding.
</p>
</body>
</section>

<section>
<title>Virtual Machines (VMs)</title>
<body>
<p>
As discussed in the User Guide, there are several VMs available from portage.
</p>

<p>
Testing all packages to ensure they build and run with every VM is a huge undertaking, and there simply are not enough resources to guarantee every package will build and run with every VM.
</p>
<p>
We now maintain a list of "supported virtual machines" for each architecture. These are the VMs we will test the packages against before committing changes to portage. When you emerge a package, it will by default try to use the best "supported virtual machine."
</p>

<p>
Of course, Gentoo and Linux in general are about choice, so if you prefer a different VM over the "supported virtual machines", you can easily use that VM instead. However, you should also know that bugs reported with one of the non-"supported virtual machine" will get a low priority if it isn't present using a "supported virtual machine".
</p>
</body>
</section>

<section>
<title>Configuring which VM to Use</title>
<body>

<p>
You can choose which VM to use on a per-user basis, and which VM to use for the system (ie when running things as root). Both of these are configured using java-config.
</p>

<p>
A user's current VM is represented by a symlink located at <path>~/.gentoo/java-config-2/current-user-vm</path>. This symlink points to the <c>JAVA_HOME</c> of the chosen VM. Similarly, the system VM is represented by a symlink at <path>/etc/java-config-2/current-system-vm</path>.
</p>

<p>
The current VM can also be changed on the fly. This can be accomplished setting the environment <c>GENTOO_VM</c> to contain the name of a VM that java-config knows about.
</p>

</body>
</section>

<section>
<title>Java Tools</title>
<body>

<p>
The traditional Java tools, ie, <c>java</c>, <c>javac</c>, <c>javadoc</c>, etc, are all located in /usr/bin. They are actually all symlinks to the <c>run-java-tool</c> script. This script will call the appropriate tool, depending on how the script is invoked, from the current VM. <c>GENTOO_VM</c> is first checked, then the user VM, and lastly the system VM.
</p>

</body>
</section>

<section>
<title>Build-time VM Switching</title>
<body>

<p>
As outlined in the User Guide, and mentioned briefly earlier, the VM will switch at build time to accommodate the needs of the package. The VM to use is first determined by <c>JAVA_PKG_FORCE_VM</c>, then <path>/etc/java-config-2/build/jdk.conf</path>, and lastly the system VM.
</p>

</body>
</section>

<section id="compatibility">
<title>Bytecode Compatibility</title>
<body>

<p>
The default behavior of <c>javac</c> is to compile bytecode that will compatible with the current VM version and higher (ie forward compatible). This becomes particularly problematic when trying to use a lower versioned VM. For example, source compiled with 1.4 will be compatible with 1.4 and 1.5, and source compiled with 1.5 will only be compatible with 1.5.  This makes it particularly difficult to revert to an earlier VM.
</p>
<p>
It is possible to specify which VM to compile for to provide the best compatibility.
</p>
<p>
At build time, the <c>DEPEND</c> and <c>RDEPEND</c> will determine what VM to compile for based on virtual/jdk and virtual/jre. Additionally, this can be controlled by the environment variables <c>JAVA_PKG_WANT_SOURCE</c> and <c>JAVA_PKG_WANT_TARGET</c>.
</p>
<p>
There is a wrapper for <c>javac</c>, <c>ejavac</c>, which will use the appropriate VM's <c>javac</c>, and then specify the appropriate -target and -source. For projects that use <c>ant</c>, the build.xml can be translated to specify the appropriate -target and -source.
</p>

</body>
</section>
</chapter>

<chapter>
<title>Gentoo Java Packaging Policy</title>

<section>
<body>
<p>
In addition to other Gentoo policies, there are a few unique to Java packages, or that need special attention.
</p>
</body>
</section>

<section>
<title>Why build from source?</title>
<body>

<p>
We should strive to only accept ebuilds that build from source code. For 
reasons why, please refer to 
<uri link="why-build-from-source.xml">Why Build From Source</uri>.
</p>

</body>
</section>

<section>
<title>Filesystem Layout</title>
<body>

<p>
In general, the directory policies are handled for you by the helper functions
in the <uri link="#java-utils-2.eclass">java-utils-2</uri> eclass.
</p>

<p>
These functions adhere to the following path name conventions:
</p>

<ul>
	<li>
		/usr/share/${PN}-${SLOT}/package.env contains information about the package
	</li>

	<li>
		.jar files created from source code are installed to /usr/share/${PN}-${SLOT}/lib/ 
	</li>

	<li>
		.jar pre-built files not compiled by the ebuild are installed to /opt/${PN}-${SLOT}/lib
	</li>

	<li>
		Javadoc documentation is installed to /usr/share/doc/${PF}/html/api/ 
	</li>

	<li>
		Source archives are installed to /usr/share/${PN}-${SLOT}/source/ 
	</li>

	<li>
		User-executable scripts are installed to /usr/bin 
	</li>

	<li>
		System-wide environment files are in installed to /usr/env.d/java 
	</li>

	<li>
		User-specific environment files can be put into ${HOME}/.gentoo/env.d/
	</li>
</ul>

</body>
</section>
<section>
<title>Slotting</title>
<body>

<p>
Libraries should be slotted according to the API they provide. If two version have the same API, or if a new version is fully compatible with the previous version, then they should be in the same SLOT.
</p>

<p>
Java libraries have a tendency to sometimes break API and ABI between minor revisions, ie from 2.1.1 to 2.1.2. As a result, it is necessary to slot early, and slot often.
</p>

<p>
For applications, it is mostly sufficient to keep only the latest version. If the application comes in series, such as Eclipse, we want to keep the latest revision in each series. Very old series may eventually be dropped completely.
</p>

</body>
</section>

<section>
<title>Dependencies</title>
<body>

<p>
	Packages should not use bundled jars. Instead, they should make use of jars from already installed packages.
</p>

<p>
When depending on a package, take care that you depend on a sufficiently recent version, and explicitly ensure at building time that the providing package gives you the correct interface, i.e. the correct <c>SLOT</c>. 
</p>

</body>
</section>

</chapter>

<chapter id="writing_the_ebuild">
<title>Writing the ebuild</title>
<section id="general_guidelines">
<title>General Guidelines</title>
<body>

<p>
In addition to standard Gentoo ebuild guidelines, there are a number specific for Java packages:
</p>

<ul>
  <li>In RDEPEND, use ">=virtual/jre-[minimal-version]". If you need a JDK for normal operation, like www-servers/tomcat used to, then you should use ">=virtual/jdk-[minimal-version]". The jre atom MUST have a version.</li>

  <li>In DEPEND, use ">=virtual/jdk-[minimal-version]", unless the ebuild is not compiling source. The jdk atom MUST have a version.</li>

  <li>For packages that use Ant to build, try to DEPEND on just dev-java/ant-core when possible instead of dev-java/ant.
	  dev-java/ant-core is automatically added to DEPEND if you inherit java-ant-2.
	  If the package makes use of 'optional' ant tasks, you'll need to DEPEND on dev-java/ant or
	  add WANT_ANT_TASKS="ant-task" before inherit. The latter is of course preferred. see the 
      <uri link="ant-guide.xml">Ant Guide</uri> for details.</li>

  <li>For packages that are distributed as zip, you need to DEPEND on app-arch/unzip to unpack</li>

  <li>Always use slot dependencies (EAPI 1 or later) as jars are installed to slot specific paths. Your DEPEND and RDEPEND should look like dev-java/some-library:1.2 or dev-java/another:0.</li>

  <li>Avoid using bundled .jar files at all costs for source-based packages. Instead, they should use system installed versions with the help of our eclass functions.</li>

  <li>If you only need the path to installed libraries, you can use java-pkg_getjar(s). Don't call java-config directly, because it will not be recorded as a dependency in the package env.</li>

  <li>Always provide an easily understandable reason after 'die', so that end-users will provide the maintainers with sensible feedback if the build should fail.</li>

  <li>
  Avoid cluttering up the environment by adding environment files to /etc/env.d/. Instead, store your env file in /etc/env.d/java/, and then have user scripts source its environment file when it launches. Otherwise, developers, who regularly override CLASSPATH, CATALINA_HOME and other env vars, will have problems running regular apps.  If you use the <uri link="#launcher">launcher</uri> it will also automatically source the appropriate env file.
</li>

  <li>Make sure you always compile with correct a source/target. This is important to ensure future and backwards <uri link="#compatibility">compatibility</uri>.  If the packages use ant, this can be done for you automatically. See <uri link="#java-ant-2.eclass">java-ant-2.eclass</uri>.  If not you will have to patch it to pass <uri link="#func_query">$(java-pkg_javac-args)</uri> to javac.</li>

  <li>Do no install jars which contain versions in their filename, ie castor-0.9.7.jar. Use java-pkg_newjar to rename and install versioned jars to not contain the version in their filename.</li>

</ul>

</body>
</section>

<section>
<title>USE flags</title>
<body>

<p>
	If a manual or other extensive documentation is available, it should be installed using the doc USE flag.  If the build system can, you should build javadocs also using the <c>doc</c> USE flag. If it does not, you should consider patching it to do so, and provide the patch upstream. HTML documentation should be installed using <uri link="http://devmanual.gentoo.org/function-reference/install-functions/index.html">dohtml</uri> and javadocs using <uri link="#func_install">java-pkg_dojavadoc</uri>.
</p>

<p>
If the program provides examples, in the form of source code or some other format, and you think they may be worthwhile for some users to have, install them optionally with the examples use flag using the <uri link="#func_install">java-pkg_doexamples</uri> function.
</p>

<p>
If you want to go all the way, add the source USE flag that installs the
complete source code as a .zip file. Use <uri
link="#func_install">java-pkg_dosrc</uri> for this purpose.  This allows IDEs
such as Eclipse and NetBeans to do complete source-level debugging. You will need to also DEPEND="source? ( app-arch/zip )" or add JAVA_PKG_IUSE="source" before inherit. Using JAVA_PKG_IUSE means that we can remove the app-arch/zip requirement in the future and use for example jar provided by virtual/jdk.
</p>

<p>
If your package comes with unit tests, you can enable these using the test
FEATURE, in src_test. If you need extra dependencies for the testing you can
pull these in with the test useflag (for example dev-java/junit).
We will no longer use the junit use flag for this.
</p>

</body>
</section>

<section>
<title>Typical Examples</title>
<body>

<p>
Without further ado, here are a few examples.
</p>

<pre caption="Example: Pure java package">

EAPI="2"
JAVA_PKG_IUSE="doc examples source"

inherit eutils java-pkg-2 java-ant-2

DESCRIPTION="Fictional example ebuild."
HOMEPAGE="http://www.gentoo.org/"
SRC_URI="mirror://gentoo/${P}-src.tar.gz"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~x86 ~sparc ~ppc ~amd64 ppc64"
IUSE=""

COMMON_DEP="
        dev-java/xerces:2
        >=dev-java/log4j-1.2.8:0"

RDEPEND=">=virtual/jre-1.4
        ${COMMON_DEP}"

DEPEND=">=virtual/jdk-1.4
        ${COMMON_DEP}"

S=${WORKDIR}/${P}-src

java_prepare() {
        cd "${S}/lib"
        rm -v *.jar || die

        java-pkg_jar-from xerces-2
        java-pkg_jar-from log4j log4j.jar log4j-1.2.8.jar
}

src_install() {
        java-pkg_newjar target/${P}-dev.jar ${PN}.jar

        use doc &amp;&amp; java-pkg_dojavadoc dist/api
        use source &amp;&amp; java-pkg_dosrc src/java/org
        use examples &amp;&amp; java-pkg_doexamples src/java/examples
}
</pre>

<pre caption="Example: Optional java support">

EAPI="2"

inherit eutils java-pkg-opt-2

DESCRIPTION="Fictional example ebuild"
HOMEPAGE="http://www.gentoo.org/"
SRC_URI="mirror://gentoo/${P}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~ia64 ~ppc ~ppc64 ~sparc ~x86"
IUSE="java doc nls"

DEPEND="java? ( >=virtual/jdk-1.4 )"
RDEPEND="java? ( >=virtual/jre-1.4 )"

java_prepare() {
        epatch "${FILESDIR}/${P}.patch"
}

src_compile() {
        local myconf="$(use_enable java)"
        if use java; then
                myconf="${myconf} --with-javac-args=\"$(java-pkg_javac-args)\""
        fi

        econf $(use_enable nls) ${myconf} || die

        emake || die
}

src_install() {
        make install DESTDIR=${D} || die

        if use java; then
                java-pkg_newjar build/java/${P}.jar ${PN}.jar

                if use doc; then
                        java-pkg_dohtml -r doc/java
                fi
        fi
}
</pre>

</body>
</section>
</chapter>

<chapter>
<title>Java Eclass Reference</title>
<section>
<title>Overview</title>
<body>

<p>
Currently there are six Java related eclasses.
</p>

<table>
<tr>
  <th>Eclass</th><th>Usage</th>
</tr>
<tr>
  <ti><uri link="#java-pkg-2.eclass">java-pkg-2.eclass</uri></ti>
  <ti>Any and all java based packages</ti>
</tr>
<tr>
  <ti><uri link="#java-pkg-opt-2.eclass">java-pkg-opt-2.eclass</uri></ti>
  <ti>Package that have optional java support</ti>
</tr>
<tr>
  <ti><uri link="#java-ant-2.eclass">java-ant-2.eclass</uri></ti>
  <ti>Ant based packages (see also the <uri link="ant-guide.xml">Ant Guide</uri>)</ti>
</tr>
<tr>
  <ti><uri link="#java-utils-2.eclass">java-utils-2.eclass</uri></ti>
  <ti>
    Inherited by the java-pkg* eclasses and contains all the functions and dark
    voodoo
  </ti>
</tr>
<tr>
  <ti><uri link="#java-vm-2.eclass">java-vm-2.eclass</uri></ti>
  <ti>Helper functions for packages that provide a VM</ti>
</tr>
<tr>
	<ti><uri link="#java-osgi.eclass">java-osgi.eclass</uri></ti>
	<ti>Helper functions for packages that need to be OSGi compliant (special manifest inside the jar)</ti>
</tr>
</table>

</body>
</section>
<section id="java-pkg-2.eclass">
<title>java-pkg-2.eclass</title>
<body>

<p>
This is the eclass you should for any package using Java.
It inherits java-utils-2, and gives you all the needed function.
It also depends on the correct version of java-config the eclass
needs to do its work.
It also exports the pkg_setup phase, where it switchs the vm
and setup the environment for your VM.
</p>

</body>
</section>
<section id="java-pkg-opt-2.eclass">
<title>java-pkg-opt-2.eclass</title>
<body>

<p>
This is the eclass you should use for packages with optional java support.
It does the same as java-pkg-2.eclass but only when the 'java' USE flag is on.
The USE flag that is used to enable Java features can be changed by setting 
JAVA_PKG_OPT_USE before inheriting this eclass.
</p>

</body>
</section>
<section id="java-vm-2.eclass">
<title>java-vm-2.eclass</title>
<body>

<p>
This eclass should be inherited by all packages that provide a VM.  If no
system-vm can be found it will set the one currently being merged as the 
system-vm.  It also has a function to install the env file and create all 
necessary symlinks.
</p>

<ul>
  <li><c>set_java_env</c>
  <ul>
    <li>
      Takes the env file, fills with with the appropriate variables (ie name, version, etc) and places it in /etc/env.d/java, then
	  creates the jvm symlink in /usr/lib/jvm/
    </li>
    <li>Takes the base env file as argument</li>
  </ul>
  </li>
  <li><c>install_mozilla_plugin</c>
  <ul>
    <li>Creates a symlink for the mozilla plugin</li>
    <li>Takes the path to the oij plugin</li>
  </ul>
  </li>
</ul>

</body>
</section>
<section id="java-ant-2.eclass">
<title>java-ant-2.eclass</title>
<body>

<p>
You should inherit this eclass when your package uses ant. This eclass automatically rewrites the build.xml unless JAVA_PKG_BSFIX is set to off.
fex: Add source/target attributes to javac calls.
</p>
<p>
The rewriting is done in java-ant-2_src_configure for EAPI 2 or eant for earlier EAPIs.
</p>
<p>
Some variables you can set from your ebuild. (Usually not needed)
</p>

<ul>
  <li><c>JAVA_PKG_BSFIX</c>
  <ul>
    <li>
      After src_unpack, should we try to 'fix' ant build files to include (the
      correct) target and source attributes
    </li>
    <li>Default: on</li>
  </ul>
  </li>
  <li><c>JAVA_PKG_BSFIX_ALL</c>
  <ul>
    <li>Should we try to fix all build files we can find</li>
    <li>Default: yes</li>
  </ul>
  </li>
  <li><c>JAVA_PKG_BSFIX_NAME</c>
  <ul>
    <li>The name or names(space separated) of the build xml we should fix</li>
    <li>Default: build.xml</li>
  </ul>
  </li>
  <li><c>JAVA_PKG_BSFIX_TARGET_TAGS</c>
  <ul>
    <li>Tags to add the target attribute to</li>
    <li>Default: javac xjavac javac.preset</li>
  </ul>
  </li>
  <li><c>JAVA_PKG_BSFIX_SOURCE_TAGS</c>
  <ul>
    <li>Tags to add the source attribute to</li>
    <li>Default: javadoc javac xjavac javac.preset</li>
  </ul>
  </li>
</ul>

</body>
</section>

<section id="java-osgi.eclass">
	<title>java-osgi.eclass</title>
	<body>
		
		<p>
			You should inherit this eclass when your package needs to be OSGi compliant. This eclass contains functions similar to those those in java-utils-2, but that create a jar containing a manifest with special values. This manifest will allow the jar to be used as an OSGi bundle in applications based on OSGi, for example Eclipse.
		</p>
		<p>
			If a package is using this eclass, it means that another Gentoo package based on OSGi needs it. Thus, version bumps of the package should still use the install functions from this eclass.
		</p>
		<p>
			Some of the functions in this eclass request a Manifest file. In this case, be careful about the file you provide. It's usually best to copy the Manifest from a safe source, eg. from Eclipse's bundled jars for example. Note that upstream Eclipse creates those files manually.
		</p>
		
		<ul>
			<li><c>java-osgi_dojar</c>
				<ul>
					<li>
						Similar to java-pkg_dojar. Installs a jar, and records it in the package env. Make sure the jar name does not contain a version. The manifest in the jar will be filled with headers populated from the arguments given to this function.
					</li>
					<li>Takes four parameters:
						<ul>
							<li>path to the jar (like in java-pkg_dojar)</li>
							<li>bundle symbolic name</li>
							<li>bundle name</li>
							<li>export-package-header. This is the most important thing, you must provide a valid OSGi export header. Refer to the OSGi documentation for help with this.</li>
     					</ul>
					</li>
				</ul>
			</li>
			<li><c>java-osgi_newjar</c>
				<ul>
					<li>
						Similar to java-pkg_newjar. Use this if you need to rename the jar. The manifest in the jar will be filled with headers populated from the arguments given to this function.
					</li>
					<li>Takes four or five parameters:
						<ul>
							<li>path to the jar (like in java-pkg_newjar)</li>
							<li>(Optional) name of the renamed jar. If absent, it will be ${PN}.jar</li>
							<li>bundle symbolic name</li>
							<li>bundle name</li>
							<li>export-package-header. This is the most important thing, you must provide a valid OSGi export header. Refer to the OSGi documentation for help with this.</li>
						</ul>
					</li>
				</ul>
			</li>			
			<li><c>java-osgi_dojar-fromfile</c>
				<ul>
					<li>
						Similar to java-osgi_dojar, except that instead of creating the manifest from the given arguments, it takes a path to a Manifest file.
					</li>
					<li>Takes three or four parameters:
						<ul>
							<li>(Optional) --no-auto-version. This option disables automatic rewriting of the version in the Manifest file. If not present, the Gentoo package version will be written to the Manifest.</li>
							<li>path to the jar</li>
							<li>path to the Manifest file</li>
							<li>bundle name</li>
						</ul>
					</li>
				</ul>
			</li>
			<li><c>java-osgi_newjar-fromfile</c>
				<ul>
					<li>
						Similar to java-osgi_newjar, except that instead of creating the manifest from the given arguments, it takes a path to a Manifest file.
					</li>
					<li>Takes from three to five parameters:
						<ul>
							<li>(Optional) --no-auto-version. This option disables automatic rewriting of the version in the Manifest file. If not present, the Gentoo package version will be written to the Manifest.</li>
							<li>path to the jar</li>
							<li>(Optional) name of the renamed jar. If absent, it will be ${PN}.jar</li>
							<li>path to the Manifest file</li>
							<li>bundle name</li>
						</ul>
					</li>
				</ul>
			</li>
		</ul>
		
	</body>
</section>

</chapter>

<chapter id="java-utils-2.eclass">
<title>java-utils-2.eclass</title>
<section id="eant / ejavac">
<title>eant/ejavac</title>
<body>

<p>
Since a lot of people prefer to compile their packages with jikes or
eclipse-ecj, and it was becoming a hassle to add these in every ebuild we
provide 2 wrapper scripts to call either ant with that compiler or that
compiler itself.
</p>

<p>
It will read the file <path>/etc/java-config/compilers.conf</path> and try the
entries front to end until it find an available(installed) compiler, and use
it. It can contain any combination of: 'ecj' 'jikes' and 'javac'.
</p>

<p>
You should only use this if the package is known to work all 3 of the
compilers, if later on a problem does occur with one of them, you can set
JAVA_PKG_FILTER_COMPILER to it in the ebuild, and it will no longer try to use
it.
</p>

<p>
It also listens to the JAVA_PKG_FORCE_COMPILER environment variable to force
one of them, this can be useful when testing a new ebuild with every compiler.
</p>

</body>
</section>
<section id="func_install">
<title>Install functions</title>
<body>

<ul>
  <li><c>java-pkg_dojar</c>
  <ul>
    <li>
      Installs a jar, and records it in the package env. Make sure the jar name
      does not contain a version.
    </li>
    <li>Takes one or more paths to a jars</li>
    <li>will die on errors</li>
  </ul>
  </li>
  <li><c>java-pkg_newjar</c>
  <ul>
    <li>
      If you need to rename the jar, since we don't allow versions in the jar
      name, then calls _dojar.
    </li>
    <li>If you only pass one argument it will name it ${PN}.jar</li>
    <li>will die on errors</li>
  </ul>
  </li>
  <li><c>java-pkg_dowar</c>
    <ul>
      <li>
        Installs a war into a packages webapps directory.
      </li>
      <li>War is not recorded within package.env and is not integrated with java-config or any servlet container (e.g. Tomcat)</li>
      <li>Takes one or more paths to a war</li>
      <li>will die on errors</li>
    </ul>
  </li>
  <li><c>java-pkg_dohtml</c>
  <ul>
    <li>Deprecated, use dohtml now.
    </li>
  </ul>
  </li>
  <li><c>java-pkg_dojavadoc</c>
  <ul>
    <li>
      Installs javadoc documentation and records it to package.env
    </li>
    <li>Takes only one argument namely the directory of the javadoc root.</li>
    <li>Will die if the argument is not a directory or does not exist.</li>
  </ul>
  </li>
  <li><c>java-pkg_doexamples</c>
  <ul>
    <li>
      Installs examples to the standard location
    </li>
	<li>Takes multiple arguments.</li>
	<li>If given only one directory, installs dir/* into examples/.</li>
    <li>Will die on errors</li>
  </ul>
  </li> <li><c>java-pkg_addcp</c>
  <ul>
    <li>Sometimes you need specials things on the package's classpath</li>
    <li>Classpath string</li>
  </ul>
  </li>
  <li><c>java-pkg_regjar</c>
  <ul>
    <li>record an already installed jar in he packages env</li>
    <li>Takes one or more paths to a jars. Will strip ${D}</li>
    <li>will die on error</li>
  </ul>
  </li>
  <li><c>java-pkg_doso</c>
  <ul>
    <li>Install a jni library, and register its location it the package env</li>
    <li>Takes one ore more path to a library</li>
    <li>will die on error</li>
  </ul>
  </li>
  <li><c>java-pkg_regso</c>
  <ul>
    <li>record an already installed library in the package env</li>
    <li>takes one or more paths to a library</li>
    <li>will die on error</li>
  </ul>
  </li>
  <li><c>java-pkg_jarinto</c>
  <ul>
    <li>Change the location java-pkg_dojar installs to</li>
  </ul>
  </li>
  <li><c>java-pkg_sointo</c>
  <ul>
    <li>Change the location java-pkg_doso installs to</li>
  </ul>
  </li>
  <li><c>java-pkg_dosrc</c>
  <ul>
    <li>
      Install a zip containing the source, so it can used in IDE's like
      Eclipse or Netbeans.
    </li>
    <li>Takes a list of paths to the base of the source directories like com or org.</li>
    <li>Will die on error</li>
  </ul>
  </li>
  <li><c>java-pkg_dolauncher</c>
  <ul>
    <li>
      Makes a wrapper script to launch/start this package.<br/>
      This wrapper will (attempt to) automatically figure out the dependency
      and build up the classpath and library path when needed. It will also
      make sure it uses a vm that is capable of running the package and every
      dependency. 
    </li>
	<li>You can call the function without any arguments if the ebuild only installs one jar. 
		In that case the function uses ${PN} as the binary name and the jar is launched using the Main-class
		attribute (the same as running with java -jar). Otherwise use the following arguments.
      <ul>
        <li><c>--main</c> The class to start</li>
        <li><c>--jar</c> The jar to launch (this will ignore classpath)</li>
        <li><c>--java_args</c> Extra arguments to pass to java</li>
        <li><c>--pkg_args</c> Extra arguments to pass to the package</li>
        <li><c>--pwd</c> Directory it should launch the app from</li>
        <li><c>-into</c> Directory where it should put the launch wrapper</li>
        <li><c>-pre</c> Location of a (bash) script to include and run prior to launching</li>
      </ul>
    </li>
  </ul>
  </li>
</ul>

</body>
</section>
<section id="func_query">
<title>Query functions</title>
<body>

<ul>
  <li><c>java-pkg_jar-from</c>
  <ul>
    <li>Creates symlinks to the jars of a package in the cwd</li>
    <li>Can be called with
    <ul>
      <li>A comma separated list of packages</li>
      <li>A single package and the jar name you want from that package</li>
      <li>A single package, the jar name, and the name of symlink</li>
    </ul>
    </li>
    <li>Will die on errors</li>
  </ul>
  </li>
  <li><c>java-pkg_getjars</c>
  <ul>
    <li>echos the classpath, and record the dependency</li>
    <li>Takes a comma separated list of packages</li>
    <li>Does not die on errors, returns 1</li>
  </ul>
  </li>
  <li><c>java-pkg_getjar</c>
  <ul>
    <li>echos the classpath of the jar, and records the dependency</li>
    <li>A single package, and the name of the jar</li>
    <li>Does not die on errors, returns 1</li>
  </ul>
  </li>
</ul>

</body>
</section>
<section id="func_other">
<title>Other functions</title>
<body>

<ul>
  <li><c>eant</c>
  <ul>
    <li>Wrapper to call ant and use the preferred compiler</li>
    <li>Will pass any args to ant</li>
    <li>Will die on error</li>
    <li>
      Affected by the ANT_TASKS and related variables - see the 
      <uri link="ant-guide.xml">Ant Guide</uri> for details.
    </li>
  </ul>
  </li>
  <li><c>ejavac</c>
  <ul>
    <li>Wrapper to call the preferred compiler</li>
    <li>Will pass any args to javac</li>
    <li>Will die on error</li>
  </ul>
  </li>
  <li><c>ejunit</c>
  <ul>
    <li>Wrapper start junit.textui.TestRunner</li>
	<li>Will pass on any args</li>
	<li>Appends junit and full recorded deptree to -cp or -classpath</li>
    <li>Will die on error</li>
  </ul>
  </li>
  <li><c>java-pkg_get-source</c>
  <ul>
    <li>Get the -source value</li>
  </ul>
  </li>
  <li><c>java-pkg_get-target</c>
  <ul>
    <li>Get the -target value</li>
  </ul>
  </li>
  <li><c>java-pkg_javac-args</c>
  <ul>
    <li>Get the arguments that should be passed to javac</li>
  </ul>
  </li>
  <li><c>java-pkg_switch-vm</c>
  <ul>
    <li>Attempts to switch to the best vm</li>
  </ul>
  </li>
  <li><c>java-pkg_ensure-vm-version-sufficient</c>
  <ul>
    <li>die if the current vm cannot build this package</li>
    <li>Takes a version: 1.4 1.5</li>
  </ul>
  </li>
  <li><c>java-pkg_ensure-vm-version-eq</c>
  <ul>
    <li>die if the current vm's version isn't equal to  ${1}</li>
    <li>Takes a version: 1.4 1.5</li>
  </ul>
  </li>
  <li><c>java-pkg_is-vm-version-eq</c>
  <ul>
    <li>test is the active vm's version equals $1</li>
    <li>Takes a version: 1.4 1.5</li>
  </ul>
  </li>
  <li><c>java-pkg_ensure-vm-version-ge</c>
  <ul>
    <li>die if the current vm's version isn't at least ${1}</li>
    <li>Takes a version: 1.4 1.5</li>
  </ul>
  </li>
  <li><c>java-pkg_is-vm-version-ge</c>
  <ul>
    <li>test if the active vm's version is at least $1</li>
    <li>Takes a version: 1.4 1.5</li>
  </ul>
  </li>
  <li><c>java-pkg_register-dependency</c>
  <ul>
    <li>Register runtime dependency on a package</li>
    <li>Useful for binary packages and things loaded by custom classloading</li>
    <li>Takes a package list: xerces-2,xalan</li>
    <li>Or one jar from a package: ant-core ant.jar</li>
  </ul>
  </li>
  <li><c>java-pkg_register-optional-dependency</c>
  <ul>
    <li>Same as register-dependency but the package is not expected to be installed at runtime</li>
  </ul>
  </li>
</ul>

</body>
</section>
<section id="ext_vars">
<title>External variables</title>
<body>

<p>
Some variables that can come in handy, you are <e>not</e> allowed to use these
from an ebuild.
</p>

<ul>
  <li><c>JAVA_PKG_ALLOW_VM_CHANGE</c>
  <ul>
    <li>
      Allow the eclass to switch the vm at merge time.  If you set this to no,
      it will die when the active vm isn't sufficient
    </li>
  <li>Default: yes</li>
  </ul>
  </li>
 <li><c>JAVA_PKG_WANT_TARGET</c>
  <ul>
    <li>Ignore what DEPEND claims it needs, and use the value of this var for -target</li>
    <li>Default: unset</li>
  </ul>
  </li>
  <li><c>JAVA_PKG_WANT_SOURCE</c>
  <ul>
    <li>Ignore what DEPEND claims it needs, and use the value of this var for -source</li>
    <li>Default: unset</li>
  </ul>
  </li>
  <li><c>JAVA_PKG_FORCE_VM</c>
  <ul>
    <li>Force a specific jdk at build time.</li>
    <li>Default: unset</li>
  </ul>
  </li>
  <li><c>JAVA_PKG_FORCE_COMPILER</c>
  <ul>
    <li>Force a specific compiler to use at build time.</li>
    <li>Default: unset</li>
  </ul>
  </li>
  <li><c>JAVA_PKG_DEBUG</c>
  <ul>
    <li>Turn on very verbose output in for example build.xml rewriting and ant.</li>
    <li>Default: unset</li>
  </ul>
  </li>
  <li><c>JAVA_PKG_STRICT</c>
  <ul>
    <li>Enables additional checks in Java eclasses. This variable must be set when developing Java ebuilds.</li>
    <li>Default: unset</li>
  </ul>
  </li>
</ul>

</body>
</section>
</chapter>

<chapter>
<title>External Resources</title>
<section>
<body>
<ul>
  <li>
    <uri link="/proj/en/devrel/handbook/handbook.xml">Developer Handbook</uri>
  </li>
  <li>
    <uri link="http://devmanual.gentoo.org">Devmanual</uri>
  </li>
</ul>
</body>
</section>
</chapter>

</guide>
