http://dev.gentoo.org/~swift/blog/articles/20050706-council.html

Now that the Gentoo Foundation nominations are done, we are here again for the 
nominations of the Gentoo Project itself. Unlike with the Foundation, this 
nomination is for development-related matters. And yes, I am putting myself in 
as a candidate again.

For those who don't know me, my name is Sven Vermeulen, nickname SwifT, and I 
am currently in charge of the Gentoo Documentation Project together with 
neysx. I'm also active in the Gentoo PR as "the guy behind www@gentoo.org" 
which apparently is used by a lot of folks as a support channel :p

I am also quite active on #gentoo in periods (I generally don't chat while 
doing real-life work), I work on the Gentoo Website Redesign project together 
with the freshly-appointed curtis119, am kind-of responsible for the website 
(not appointed, more by activity :) and am also a Gentoo Foundation Trustee.

I have joined the Gentoo forces around April 2003 if I recall correctly after 
pushing the former documentation lead with lots of Dutch translations. In 
those days, Dutch was the most active and up-to-date (translation) language. 
After a while I shifted my attention to the English documents, first as 
editor, later on as the freshly appointed Documentation Lead position I am 
currently still in.

I have been one of the Gentoo Managers ever since then which gave me a great 
incentive to discover how the various projects inside Gentoo work. And because 
every project needs documentation eventually, I was (and still am) in a 
perfect position to learn a lot and help projects improve themselves.

As documentation editor I am involved in the Gentoo release process since I am 
to blame for the off-line documentation that is generated for the installation 
CDs and the Gentoo Handbook in general. Yes, the handbook is my brain fart :) 
You will also find a lot of guides and HOWTOs that have my name as the primary 
author.

When you would ask me if I fit in nicely in a team, I do try to think I do. 
You may ask any of my GDP minions of course. I do know I do not easily send 
out heated e-mails or flame-bait; I always remain calm. Unlike some, I do have 
some diplomacy running through my blood vessels :p Although it can happen on 
occasion, I am generally not too impulsive in my actions, which can be 
devastating when you need to decide on broad problems.

So why am I putting myself in as candidate for the Gentoo Project if I already 
am a Gentoo Foundation Trustee?

For one, the Gentoo Foundation has nothing to do with the Gentoo development.

Second, I feel the upcoming Gentoo Council can greatly benefit from an active 
documentation editor and project lead: the feedback I receive from the Gentoo 
community gives me a good idea what the user community is asking for or where 
improvements should be made. It should be invaluable for the decisions that 
the Council will need to make. My knowledge about the Gentoo structure 
(current and future) will help me find the most optimal solution for any given 
problem and propagate my ideas to others.

As a third point I would like to say I don't easily get in a fight with people 
and you won't find me name-calling people. Being kind to each other, 
especially in a volunteer project, is too important to just blow up. Therefore 
the council will always be able to discuss topics properly without resulting 
in internal quarrels.

So, I hope this sums up my Gentoo status sufficiently so that you, Gentoo 
Developers, can vote for who you think is the best suited for a position 
within the Council.
