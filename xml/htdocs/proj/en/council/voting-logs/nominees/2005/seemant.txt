http://planet.gentoo.org/developers/seemant/2005/07/11/i_am_not_a_jedi

Gentoo's organisational structure is undergoing a change. This is the 
culmination of a lot of proposals from a lot of different people. The old TLP 
management structure simply did not scale well enough to retain its 
effectivity. It's no secret that the Gentoo project has grown: very large very 
fast. It's been doing this for about 3 years now, with no real signs of 
letting up.

Now, slarti did nominate me to be on the council. I'd like to thank him for 
thinking of me. I was a part of the initial metastructure: I believe I headed 
up devrel and qa and something else at the time. Devrel was quickly handed off 
to avenj who had been my parter-in-crime in being devrel before it officially 
existed. The QA project never really took off, despite my best efforts. Let me 
put that into perspective. I was heading up base, x11, and devrel which took 
most of my time. I took on QA with the express purpose of getting someone else 
to take it over. I tried a bunch of people (some of whom already actually _do_ 
qa), but nobody wanted it. Finally, out of the blue, Sven approached me and qa 
exists and is in process. The page I've linked to is just a placeholder, so 
don't take it too seriously. In the next week watch for massive updates!

As for devrel, we did some things right and some things wrong. The wrongs are 
being fixed currently and I believe that the new and improved devrel under 
Deedra's leadership will be a stronger and better one. It'll probably never be 
a popular one though (at least not for the right reasons).

I'll take QA as my failure to build a team. However, that's been my strong 
point and the recurring theme throughout my time as a Gentoo developer. It's 
what I do, it's my M.O.: find what's broken, find someone who can fix, put the 
two together, back away slowly.

Also, I'm the head (once again) of bug-wranglers, heading a team of two! Jakub 
of course is the primary bug-wrangler currently. So anyway, without getting 
too long winded -- I'm not *highly* technical in the coding sense, but I have 
some pretty good ideas (cascading profiles, runlevels etc) and I can put 
together teams to make them come to pass (even if it's two years after the 
fact).
