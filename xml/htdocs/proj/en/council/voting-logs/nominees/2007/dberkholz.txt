<http://archives.gentoo.org/gentoo-dev/msg_147362.xml>

> 1) What you will do
> 2) Why you will do it
> 3) How you will do it
> 4) What is the timescale for doing it
> 5) What experience do you have with this or a similar role
> 6) Why do you think you are qualified
> 7) How you plan to balance a council role with your current Gentoo
> role 8) How much time can you dedicate to the council role

After much thought and many encouraging words from other developers,
I've decided to accept the nomination for the council.

I wrote about some of my goals recently on my blog [1]. My primary
focus is to make Gentoo a better tool. They are all reasonable goals
that should be doable over the course of the next year.

Also, I want to join the council to ensure that Gentoo's long-term
values are upheld. Since I joined Gentoo four years ago, I've become
one of our senior developers. That's given me a strong understanding of
our core philosophies. It's crucial for our council to maintain our
philosophies through its actions, so the council should be composed of
people who understand what Gentoo really means.

I've held a number of leadership roles within Gentoo, including X team
lead, clustering project lead and desktop manager. I spent quite a
while on devrel as a recruiter, which helped me learn about mentoring
new developers and dealing with difficult old ones. I try to be a voice
of reason on the mailing lists, and I care deeply about building the
strength of our community, both of which I've worked on over my time as
a developer.

Many of our current council members who chose to decline their
nominations mentioned that dealing with difficult people was what
determined their choice. One of my strengths is remaining calm under
pressure, so I can deal with this.

A year ago, I declined nominations for both the council and the board of
trustees. This year gave me time to consider what I really care about in
Gentoo. The conclusion I came to is that what I care about most is our
community and making Gentoo a better tool, both of which I can better
accomplish by joining the council.

Although I do have a number of projects [2] in the works, they've been
on the back burner for a while and I'm content to let them stay there
while I work on my true priorities in Gentoo. The X work has gotten a
lot easier since we got to a smooth, fully modular setup. I'd like to
get another recruit to help with bugs now that Josh retired, but it's
not so time-consuming that I don't have time for the council.

Thanks,
Donnie

1. http://spyderous.livejournal.com/91151.html
2. http://dev.gentoo.org/~dberkholz/proj/
