[21:59] *** robbat2 sets the channel mode to 'moderated'.
[21:59] <wolf31o2|mobile> and I have those profiles mostly done... I've been trying to update them a bit so they're not so far out of date
[21:59] <wolf31o2|mobile> heh
[22:00] <Kugelfang> wolf31o2|mobile: what we could discuss is, when to use it
[22:00] <kloeri> lo all
[22:00] <wolf31o2|mobile> hi
[22:00] <Flameeyes> good, 22CEST here, time to start
[22:00] <robbat2> heya
[22:00] * KingTaco peeks in
[22:00] <Kugelfang> wolf31o2|mobile: care to send them to me in a free minute or two?
[22:00] --> diox has joined this channel (n=diox@gentoo/contributor/diox).
[22:00] <Flameeyes> vapier
[22:00] *** Kugelfang sets the channel mode to 'moderated'.
[22:00] <Kugelfang> consider this as "start" :-)
[22:00] <Flameeyes> so who's starting?
[22:01] <robbat2> besides wolf31o2's agenda, could we all mention stuff we're working on lately?
[22:01] <wolf31o2|mobile> Kugelfang: well... they're really rough right now... I was planning on sending them to everyone
[22:01] <robbat2> i've got a few bits on infra things
[22:01] <wolf31o2|mobile> I'll start, if nobody minds
[22:01] <Flameeyes> robbat2, to which detail?
[22:01] <Flameeyes> wolf31o2|mobile, you have the stage
[22:01] <Kugelfang> wolf31o2|mobile: go ahead
[22:01] <robbat2> wolf31o2|mobile, go
[22:01] <Kugelfang> i hope vapier isn't late again :-)
[22:02] <wolf31o2|mobile> games, gwn, genkernel, catalyst... other than that, I've been working on a set of profiles that allow for multiple inheritance, which I plan on showing to everyone once I've got them mostly workable
[22:02] <Flameeyes> Kugelfang, he was discussing with mcummings on #-dev a while ago
[22:02] <vapier> your mom is late again
[22:02] <wolf31o2|mobile> we've started taking requests for 2007.0 on the gentoo-releng mailing list, so planning for that has begun
[22:02] <wolf31o2|mobile> that's about it
[22:03] * wolf31o2|mobile hands the floor to robbat2 
[22:03] <robbat2> besides the tree-signing (i'll return to it in a moment), I've been working with KingTaco on two infra projects
[22:03] <robbat2> firstly is anoncvs/anonsvn
[22:03] <robbat2> it's ready to roll, with the exception of one weird iptables issue affecting bandwidth
[22:04] <robbat2> until that's solved, you can use it, but it is painfully slow
[22:05] <robbat2> secondly is the new bugzie
[22:05] <wolf31o2|mobile> yay!
[22:05] <Kugelfang> (yay)
[22:05] <Kugelfang> :-)
[22:05] <Flameeyes> new bugzie or new bugzie setup?
[22:05] <robbat2> that ones a lot further behind unfortuntely, for a couple of reasons
[22:05] <kingtaco|laptop> you'll get both
[22:06] <robbat2> originally we were going to go with a cluster-aware FS for the two database boxes, but then found the status of that in Linux (esp the kernels used by infra), was badly lacking
[22:07] <robbat2> I've come up with another idea instead for the DB stuff, and I've been prototyping it on the fibrechannel gear I have at home, but it may fall over because of some of the bugzilla code
[22:07] <robbat2> worst case here, is that we can't use both of the DB boxes we've got
[22:07] <kingtaco|laptop> in this way
[22:08] <kingtaco|laptop> remember, they still have local 320
[22:08] <kingtaco|laptop> which is nothing to sneeze at
[22:08] * Kugelfang has been slacking due to exam and first week.... i'm going to remove my away status tonight and will try to build a set of test stages for amd64 using latest portage and wolf's implementation of the multiple-inheritance profiles
[22:08] --> thunder has joined this channel (n=thunder@gentoo/developer/thunder).
[22:08] <Kugelfang> s/week/% of the semester/
[22:08] <robbat2> the problem with bugzilla, is that it writes to on-disk tables for some searches done by users
[22:08] <robbat2> (the 'regetlastsearch')
[22:09] <robbat2> and if you happen to be sent to the other DB box, you get errors saying your search is invalid
[22:09] <robbat2> or nonexistent
[22:09] <wolf31o2|mobile> how much RAM do the boxes have?  Is RAM-based clustering possible?
[22:09] <kingtaco|laptop> 4G on the 2 db nodes
[22:09] <wolf31o2|mobile> I know disk-based in mysql is still considered alpha
[22:10] <robbat2> wolf31o2|mobile, given the limitations of mysql NDB, no
[22:10] <wolf31o2|mobile> k
[22:10] <robbat2> it's mainly a matter of hacking out something that will work here
[22:10] <robbat2> it seems that in other big deployments of bugzilla
[22:10] <robbat2> they've simply gone with larger and larger DB machines
[22:11] <robbat2> singular DB
[22:11] <robbat2> we get to work with what's donated however, so life is 'interesting'
[22:11] <wolf31o2|mobile> right
[22:11] <kingtaco|laptop> robbat2, why can't we do single master/slave
[22:12] <kingtaco|laptop> if multimaster is going to be such a issue
[22:12] <robbat2> kingtaco|laptop, because of the write problems with the searches :-(
[22:12] <wolf31o2|mobile> what? writes on master, reads on slave?
[22:12] <kingtaco|laptop> robbat2, we had it set up on pitr
[22:12] --> so|home has joined this channel (n=so@gentoo/developer/so).
[22:12] <robbat2> the replication time in the middle breaks searches, at least when I was testing :-(
[22:12] <kingtaco|laptop> robbat2, I suppose it's possible that wasn't tested
[22:13] <robbat2> anyway, we'll get it going soon I hope
[22:13] <Flameeyes> we all hope so :)
[22:13] <robbat2> i'll come back to tree-signing later I think, depending on how the rest of this meeting goes.
[22:13] <kingtaco|laptop> robbat2, anywho, if that's the only thing holding us back, we can simply disable that function
[22:13] <robbat2> kingtaco|laptop, is there anything other than that infra work you want to add that you've been doing?
[22:13] <robbat2> kingtaco|laptop, disable searches? you're nuts
[22:14] <kingtaco|laptop> robbat2, erm, you said redo last search
[22:14] <vapier> he means the fact you write it to disk
[22:14] --> jmbsvicetto has joined this channel (n=jmbsvice@gentoo/developer/jmbsvicetto).
[22:14] <kingtaco|laptop> robbat2, not really, some box moves, that's about it
[22:14] <robbat2> vapier, maybe you next then?
[22:15] <kingtaco|laptop> utf8 on pecker
[22:16] --> edit_lp2 has joined this channel (n=edwho@about/uk/editlp2).
[22:16] <vapier> does anyone really care what i work on ?
[22:16] <Flameeyes> vapier, we care what you *don't* work on
[22:16] <Flameeyes> [it's quicker to say]
[22:17] <robbat2> vapier, if there's anything amongst it that affects the Gentoo in a big way (eg anoncvs/bugzie), it's probably worth hearing
[22:17] <vapier> not really
[22:17] <Flameeyes> vapier, GCC 4.2?
[22:17] <vapier> i spend most of my day working on the toolchain and/or base-system
[22:18] <vapier> what about it ?  we have snapshots but no releases
[22:18] <Flameeyes> vapier, any trouble with it we must be aware of?
[22:18] <vapier> *shrug*
[22:19] <Flameeyes> myself, I'm working on Gentoo/FreeBSD as usual...
[22:20] <Kugelfang> ahhh, do you think you'll be able to release a set of stages for next release?
[22:20] <Flameeyes> a part the problems to get the new box up now, catalyst works fine to generate the stages, baselayout is now merged back so we have a pretty gentooish setup, should minimise the porting effort in the future
[22:20] <Flameeyes> Kugelfang, not sure, but if multiple inheritance profiles will be available for that, I might be able to get something
[22:21] <Kugelfang> cool
[22:21] <Flameeyes> I can ensure the first stage built with catalyst worked fine, as it's what I'm using now to rebuild the box ;)
[22:21] <wolf31o2|mobile> =]
[22:21] <Flameeyes> [and all the trouble I had was hardware/bootloader related up to now]
[22:22] <Flameeyes> I doubt it can be of interest to list the daily maintainership troubles with this and that :P
[22:23] <robbat2> so that just leaves Kloeri?
[22:23] <kloeri> I'm trying to figure out which areas devrel needs to be more proactive in and how we can best improve those aspects of devrel
[22:24] <kloeri> also, somewhat devrel related I'm trying to do more graphs on developer activity, how many developers joins and leaves the team etc.
[22:25] <kloeri> hopefully that's going to show some interesting facts important to all the discussions about solving problems by adding more devs etc.
[22:25] <kloeri> ebuild stuff, I'm trying to bring alpha back up to speed and seeing what I need to do for ia64 now that plasmaroo has resigned
[22:26] <kloeri> I should be setting up some tinderbox stuff soonish on alpha and ia64 (that's the plan at least)
[22:26] <Kugelfang> yeah, we need somebody to step up (or to be appointed) to ia64 release coordinator
[22:26] <kloeri> and finally I'm going to the UK linuxawards on wednesday with Christel and hopefully bringing back an award for "Best Linux / Open source project" :)
[22:26] <christel> :D
[22:27] <Flameeyes> christel, sst you shouldn't be talking here ;)
[22:27] <kloeri> Kugelfang: that's going to be me or agriffis I guess but plasmaroo have promised to help
[22:27] <christel> oops. sorry!
[22:28] <kloeri> I'm also trying to pass the bugday project on to eroyf (new dev that's very dedicated to userrel and bugday) to free up some time for stuff where I'm more needed
[22:29] <robbat2> ok, sounds good
[22:29] <Kugelfang> kloeri: while we're discussing you and devrel....
[22:29] <kloeri> ohh, I'm also trying to clean up some of the developer data in ldap/roll-call together with robbat2
[22:29] <kloeri> that's sort of a longterm project though
[22:29] <Kugelfang> have you all seen the bug patrick filed against kloeri?
[22:30] <Flameeyes> no
[22:30] <robbat2> no
[22:30] <Kugelfang> I'd like the council to offically back kloeri there
[22:30] <Kugelfang> lemme dig it up
[22:30] <Kugelfang> 150851
[22:30] <vapier> other devrel members weighed in as did i
[22:31] <vapier> the issue seems to have settled, do we need to add another comment ?
[22:31] <Kugelfang> no comment... it's just that in this case a devrel bug has been filed against devrel member
[22:32] <kloeri> s/member/lead/
[22:32] <Kugelfang> complaints rose up in regard to closing w/o discussion
[22:32] <robbat2> i'm reading it still
[22:32] <kloeri> but I agree the issue have settled for now
[22:33] <Kugelfang> i for one (as laready stated) back him in his decission and would like to have council state that publicly, i.e. in the summary mail to -dev
[22:33] <vapier> whatever floats your boat, i dont see any mishandling of the issue as kloeri knows
[22:33] <Kugelfang> i don't see any mishandling either :-)
[22:33] <wolf31o2|mobile> me either
[22:34] <kloeri> I don't think there's any current issues but I'm sure it's going to flare up again if/when patrick requests to become a developer again
[22:34] <wolf31o2|mobile> (though this should have been taken to us, as it really falls under "appeal" more than a report against kloeri)
[22:34] <kloeri> but devrel will just have to handle that when it comes up
[22:35] <Kugelfang> can we vote on this please?
[22:35] <Flameeyes> I don't see any particular reason trying to start a flame, as vapier and kloeri said, the issue is settled now
[22:35] <robbat2> ok, read it now. in the summary, could we please make some note that the OBJECTIVE is to auto-retired developers with 60 days of inactivity, unless otherwise provable
[22:35] <robbat2> and that provable is actually doing something, not just offering to do something
[22:36] <robbat2> eg, you say you're working with me on X, and I can confirm it.
[22:36] <robbat2> eg Patrick noting that he offered to help infra, and noted he was ignored
[22:36] <kloeri> robbat2: I'm trying to figure out how to make current policy more clear and will discuss it with devrel soon
[22:37] <Kugelfang> seems my proposal isn't being voted on :-)
[22:38] <Kugelfang> shall we proceed with wolf's atgenda then?
[22:38] <Flameeyes> I'd say so
[22:38] <robbat2> Kugelfang, i'm not certain I'd stand with you here, I do see the old (previously undertaken) policy as insufficently clear
[22:39] <wolf31o2|mobile> Kugelfang: to have a vote, specify in a single succinct sentence, etc what to vote on... ex. "Do you think we need to stomp on kloeri for retiring inactive devs?" or "Should we buy a soft-service ice cream machine for the developer lounge?"
[22:39] <Kugelfang> robbat2: i think that devrel has a certain area of interpretation there
[22:39] <robbat2> yup, wolf's stuff next
[22:39] <kloeri> wolf31o2|mobile: yes and yes :)
[22:39] <Kugelfang> wolf31o2|mobile: retracted my vote :-)
[22:39] * wolf31o2|mobile stomps on kloeri 
[22:40] <kloeri> thanks
[22:40] <wolf31o2|mobile> welcome
[22:40] <Kugelfang> where's my ice cream machine?
[22:40] <Kugelfang> wolf31o2|mobile: your first point was inter-project communication iirc
[22:41] <wolf31o2|mobile> yes
[22:41] <wolf31o2|mobile> and I thin we got devrel/infra covered
[22:41] <kloeri> I think the policy can be improved but would also note that it's somewhat intentionally vague as it's impossible to state exactly what constitutes activity with the amount of different roles people have (and new roles getting invented often enough)
[22:42] <kloeri> I'm probably going to discuss improvements to that part of policy internally in devrel first and then on -devrel ML when we have some proposed improvements
[22:42] <-- nox-Hand has left this server (Read error: 104 (Connection reset by peer)).
[22:42] <robbat2> kloeri, could you throw me a copy when you sent it to the devrel ML?
[22:43] <kloeri> robbat2: sure
[22:43] <Kugelfang> wolf31o2|mobile: portage communication improved lately imho
[22:43] <kloeri> think I've done enough interproject communication now :)
[22:43] <wolf31o2|mobile> yes, it has
[22:44] <Kugelfang> wolf31o2|mobile: zmedico's mails are quite nice, and the rfcs actually do create discussion with less flames than usual :-)
[22:44] <wolf31o2|mobile> and for the people that didn't get the email... the basis here was to identify several projects that we thought required more communications... we came up with (basically) infra, devrel, and portage... (from my memory anyway)
[22:44] <robbat2> i'd like to make an interesting observation point - the discussions about the lack of communication have actually started to improve communication - because many of the lurkers are reading them and being constructive about results ;-)
[22:45] <kloeri> trustees as well I guess
[22:45] <wolf31o2|mobile> robbat2 and KingTaco covered infra... kloeri covered devrel... and portage has improved....
[22:45] <Kugelfang> wolf31o2|mobile: so next point?
[22:45] <wolf31o2|mobile> trustees are kinda waiting on the votes, last I knew... I haven't seen much activity, but I also keep forgetting to join the stupid invite-only channel
[22:45] <Flameeyes> gentoo/freebsd should be pretty covered by me and roy on planet :P
[22:45] <wolf31o2|mobile> ;]
[22:46] <Kugelfang> wolf31o2|mobile: design phase?
[22:46] <wolf31o2|mobile> Kugelfang: go for it
[22:46] <Kugelfang> ah, one thing re inter-project commonication still
[22:46] <Kugelfang> can we put the council summary on planet.g.o?
[22:46] <wolf31o2|mobile> I don't see why not... might be a good place for it
[22:46] <Kugelfang> with a link to the log?
[22:47] <Kugelfang> do we need a vote? :-P
[22:47] <wolf31o2|mobile> how about we just ask if anyone objects?
[22:47] <wolf31o2|mobile> heh
[22:48] * kingtaco|laptop objects to people who object
[22:48] <robbat2> yeah, planet sounds good
[22:48] <kloeri> that reminds me.. some users told me that they were kind of afraid interrupting on -dev ML so they'd be quite happy to see meeting announcements and possibly even logs on forums.g.o
[22:48] <Flameeyes> who can handle the publishing of the summary then?
[22:48] <Kugelfang> Flameeyes: i'll poke beandog :-)
[22:48] <Flameeyes> I can do the log, alright, but I'm not good at summarising
[22:48] <kloeri> forums sucks for discussion but we could easily announce our meetings at least
[22:48] <wolf31o2|mobile> yeah
[22:49] * Kugelfang volunteers for both forums announcement and summary and planet
[22:49] <wolf31o2|mobile> sold!
[22:49] <Kugelfang> both?
[22:49] <Kugelfang> s/both/
[22:49] <kloeri> this meeting was actually announced on forums btw
[22:49] <kloeri> yeah, both
[22:50] <Kugelfang> :-)
[22:50] <Flameeyes> next item?
[22:52] <Kugelfang> mandatory design phase for projects
[22:52] <robbat2> i think some folk might object to the term mandatory ;-)
[22:52] <Flameeyes> I think wolf did a pretty good speech about this on his mail
[22:53] <kloeri> I think requiring a design phase is a good idea (even if it's very short for some projects)
[22:53] <Kugelfang> robbat2: hm... probably the wrong word... lemme check the dictionary :_)
[22:53] <Kugelfang> no, i meant mandatory
[22:54] <robbat2> call them project proposals instead maybe
[22:54] <Flameeyes> robbat2, I'd avoid the word "proposal"
[22:54] * Flameeyes points at the bad gleps fame
[22:54] <Kugelfang> no GLEP
[22:54] <wolf31o2|mobile> Really, it should be something along the lines of a RFC being posted.
[22:54] <wolf31o2|mobile> It should list the project's intended goal, and how they plan on getting
[22:54] <wolf31o2|mobile> there.  This is mostly to solve the problems that can occur when a new
[22:54] <wolf31o2|mobile> project forms and they say what they plan on doing without any knowledge
[22:54] <wolf31o2|mobile> being passed of how they plan on getting there.
[22:54] <wolf31o2|mobile> Essentially, it needs 3 things:
[22:54] <wolf31o2|mobile> 1. goal(s)
[22:54] <wolf31o2|mobile> 2. plan - how do they achieve #1?
[22:54] <wolf31o2|mobile> 3. resources - do they need infra? money?
[22:54] <Flameeyes> Kugelfang, indeed.. the problem is that "proposal" resembles too much gelp ;)
[22:54] <wolf31o2|mobile> ^^^ from the email nobody but us read... :P
[22:55] <kloeri> wolf31o2|mobile: yeah, I'd definitely agree with that
[22:55] <Kugelfang> dito here
[22:55] <Flameeyes> ibid
[22:55] <robbat2> yup, that's it. maybe even reuse the RFC name in some way?
[22:55] <robbat2> request for comments does describe what it needs to be
[22:56] <kloeri> agreed
[22:57] <wolf31o2|mobile> honestly, that info could even be on their project page... though the point was to discuss before the project is built... the idea here is not to disallow projects to be formed, or even to allow blocking a project... but just to convey information to other people so you don't have people (like I do) freaking out any time there's a new project that sounds like suddenly someone (like releng) is going to have to do a lot more wo
[22:57] <wolf31o2|mobile> rk when they really don't, at all
[22:57] <Flameeyes> what does the thesaurus say wrt proposal?
[22:57] <Kugelfang> New projects need to post an RFC containing information about their goals, the plan on how to implement their goals and the necessary resources to -core prior to creating the project
[22:57] <Kugelfang> ^^^???
[22:57] <Flameeyes> s/-core/-dev/
[22:57] <wolf31o2|mobile> Kugelfang: sounds succinct and to-the-point... vote?
[22:57] <wolf31o2|mobile> yeah, -dev
[22:58] <Kugelfang> hm, -dev then
[22:58] <robbat2> vote (i'm counting): "New projects need to post an RFC containing information about their goals, the plan on how to implement their goals and the necessary resources to -dev prior to creating the project"
[22:58] <Kugelfang> vote: New projects need to post an RFC containing information about their goals, the plan on how to implement their goals and the necessary resources to -dev prior to creating the project
[22:58] * Kugelfang votes yes
[22:58] <wolf31o2|mobile> heh
[22:58] <Flameeyes> vote: yes
[22:58] <wolf31o2|mobile> yes
[22:58] <robbat2> yes
[22:58] <kloeri> yes
[22:58] <Flameeyes> kingtaco, vapier?
[22:58] <kingtaco|laptop> yes
[22:59] * Flameeyes wonders if "kingtaco, vapier" could have been replaced with "mikes?"
[22:59] <kingtaco|laptop> he's an impostor
[23:00] <Kugelfang> vote: cyborg vapier and force him to monitor this channel for eternity
[23:00] <Flameeyes> vote: yes (if you also cyborg me)
[23:00] <Kugelfang> no way, i just got one ki
[23:00] <Kugelfang> t
[23:01] <robbat2> vapier, ping
[23:01] <Kugelfang> vapier: your vote?
[23:01] <kloeri> the slacker rule should also count when vapier falls asleep :)
[23:01] <Kugelfang> hehehe
[23:02] <Flameeyes> we can say the request passes with 6 yes and 1 absent? :P
[23:02] <kingtaco|laptop> only takes 4
[23:02] <wolf31o2|mobile> one abstained... heh
[23:02] <wolf31o2|mobile> what else do we have? I've got a meeting?
[23:02] <Kugelfang> unless he votes later during the meeting
[23:02] <Flameeyes> wolf31o2|mobile, do you have one?
[23:02] <wolf31o2|mobile> err... no ? on the second one
[23:02] <wolf31o2|mobile> have one what?
[23:02] <Flameeyes> wolf31o2|mobile, meeting
[23:03] <Flameeyes> anyway, next item on your agenda was about devrel
[23:03] <wolf31o2|mobile> yeah
[23:03] <Flameeyes> kloeri?
[23:03] <wolf31o2|mobile> go for it
[23:03] <kloeri> hmm?
[23:03] <wolf31o2|mobile> I'll bbiab
[23:03] <Flameeyes> kloeri, it was the etiquette thing mostly
[23:03] <kloeri> ahh, yeah
[23:04] <kloeri> devrel needs to figure out how to be more proactive about issues such as etiquette etc.
[23:04] <kloeri> there's a couple things we need to figure out really
[23:04] <Flameeyes> kloeri, should we take it as "you're working on it" (seeing the status update above) and wait for the next month to address that?
[23:05] <kloeri> actually, I'd like to hear some ideas about which areas we should try to attack
[23:05] <Kugelfang> kloeri: there is still kingtaco's proposal of silencing subscriber to -dev for a small amount of time
[23:05] <Kugelfang> kloeri: which i back :-)
[23:05] <kloeri> yeah but there's a lot of different issues we could attack really
[23:06] <kloeri> like devs bickering about useless crap like top posting, devs going at each others throat in #-dev etc.
[23:06] <kloeri> and we definitely got an issue with manpower if we need to do much about any of that
[23:06] <wolf31o2|mobile> heh... back
[23:06] <Kugelfang> wolf31o2|mobile: that was quick
[23:06] <wolf31o2|mobile> I'm just good like that
[23:06] <kloeri> so my biggest concern is where we should spend our limited ressources
[23:07] <kloeri> I'll be having a meeting with the rest of devrel about this but ideas and suggestions are certainly welcome :)
[23:07] <kloeri> other than that we should get back to it next month I think
[23:08] <Kugelfang> *nod*
[23:08] <wolf31o2|mobile> well... my main concern really was people who consistently break QA, which means it would be a joint thing... now, these people don't necessarily need "punishment" so much as we need to fix the problem, which is bad things making it into the tree
[23:08] <Kugelfang> as far as i know, QA go a t list of people
[23:09] <Flameeyes> wolf31o2|mobile, we still need qa to complete the policy, though
[23:09] <kloeri> people breaking QA should be handled by the QA project imo
[23:09] <Kugelfang> for know there are two people on it, who i won't name in here
[23:09] <Flameeyes> there are still shady points that needs to be addressed
[23:09] <wolf31o2|mobile> kloeri: and that's fine... then what powers, if any, do QA need to fulfill that role? do they just give people the virtual smackdown, and if it keeps up they file devrel bugs?
[23:09] <kloeri> and if things gets out of hand devrel is happy to help but a warning (or some friendly advice) from QA should be enough in most cases
[23:10] <kloeri> yes, that's what I'm thinking at least
[23:10] <kingtaco|laptop> wolf31o2|mobile, no "powers" needed
[23:10] <kingtaco|laptop> just teamwork
[23:10] <robbat2> one comment on the QA side (partially in regards to patrick)
[23:10] <kloeri> I've been discussing this with spb on a few occasions and I think he's happy about that arrangement as well
[23:10] <wolf31o2|mobile> yeah, that's fine
[23:10] <wolf31o2|mobile> that was really my question... do they "escalate" it to devrel once it's a real problem
[23:10] <kingtaco|laptop> QA should be able to ask devrel for help in that department if the other QA stuff has failed first
[23:11] <robbat2> anybody should be able to file QA issues, and for the most part, any dev that feels motivated should be able to fix them
[23:11] <Kugelfang> 1rtt is already implemented
[23:11] <kingtaco|laptop> and devrel should do what QA needs
[23:11] <Kugelfang> robbat2: see the QAcanfix keyword
[23:11] <wolf31o2|mobile> So do we (we being all developers) need to send information about QA violations to the QA team?
[23:11] <kingtaco|laptop> they need to nail down a policy
[23:11] <Kugelfang> robbat2: besides... whenever i saw i QA bug i could fix, i did
[23:11] <kingtaco|laptop> where is sbp
[23:11] <wolf31o2|mobile> specifically things that aren't being addressed
[23:12] <Kugelfang> robbat2: hadn arrangement with jakub on that :-)
[23:12] <Flameeyes> kingtaco|laptop, who's sbp? :P
[23:12] *** kingtaco|laptop gives spb the permission to talk.
[23:12] <Flameeyes> the one for firewire access?
[23:12] <kingtaco|laptop> spb, any news on QA?
[23:12] <robbat2> patrick said he wanted to join QA, but there was no lead - so the logical course should have been for him to file bugs to QA
[23:12] <Kugelfang> robbat2: wrong
[23:13] <kloeri> I think common sense should rule tbh - if you see some horrific QA violation you should probably contact QA about it
[23:13] <Kugelfang> robbat2: patrick as to join QA, and the QA lead said:'QA recrutieuits from activtive evuild devs'
[23:13] <Kugelfang> damn lag
[23:13] <kloeri> Kugelfang: without all the typos though :)
[23:13] <Kugelfang> 'QA recruits from active ebuild devs'
[23:14] <Kugelfang> robbat2: as patrick as neither active nor an build dev, he didn't qaulify
[23:14] <Kugelfang> qualify
[23:14] <robbat2> there no reason that you can't have a non-dev checking ebuilds for specific problems
[23:14] <Kugelfang> robbat2: true... still doesn't warant membership in QA project
[23:14] <Kugelfang> robbat2: users file bugs, too
[23:15] <kloeri> QA is also about fixing problems, writing up policy etc. which really should be left to ebuild devs imo
[23:15] <robbat2> on the side of finding problems, some dedicated searchers could fill a similar slot as arch-testers
[23:15] <Flameeyes> qa-testers?
[23:16] <Kugelfang> that sounds.... odd
[23:16] <Kugelfang> robbat2: i guess that's up to how qa wants to handl eit :-)
[23:16] <robbat2> i got invited to joined Gentoo, because I filed a whack of bugs on use.desc being out of sync with the tree
[23:17] <Kugelfang> back in 5 mins
[23:17] <wolf31o2|mobile> k
[23:17] <robbat2> i need to bail in about 10 myself
[23:18] <wolf31o2|mobile> yeah, we're over on time... what else was on the agenda?
[23:18] <robbat2> spb: not sure if you are here, but you could please consider a well-defined way for users to bring various QA issues to the attension of the QA team
[23:18] <robbat2> wolf31o2, err, anything else on your list, else it's open floor
[23:18] <Kugelfang> back already
[23:18] <Kugelfang> robbat2: in contrast to filing bugs?
[23:18] <kloeri> QA is kinda special in that a lot of stuff they're doing (or at least supposed to do) require fairly intricate ebuild knowledge as they're overseeing the work of all the other ebuild devs
[23:18] <-- thunder has left this server (Client Quit).
[23:18] <wolf31o2|mobile> can we hold off on the other and perhaps schedule another meeting in $short_amount_of_time to cover it, then open the floor?
[23:19] <Kugelfang> wolf31o2|mobile: like, next week?
[23:19] <wolf31o2|mobile> sure... though looking over... everything else was the devrel/qa stuff
[23:19] <wolf31o2|mobile> so there's not really another topic
[23:19] <wolf31o2|mobile> just this one, which I think could be discussed quite a bit
[23:19] <wolf31o2|mobile> heh
[23:19] <kingtaco|laptop> Kugelfang, I'm out of time until after the 26th
[23:19] <Kugelfang> wolf31o2|mobile: there is a GLEP in the works by malverian :-)
[23:20] <Kugelfang> kingtaco|laptop: ah, ok
[23:20] <Flameeyes> so let's reschedule for next month the rest of stuff about qa/devrel ?
[23:20] <robbat2> kloeri, while you may need to be an experience ebuild writer to find some of the really weird stuff, that doesn't user-level can't spot some basic mistakes
[23:20] <Kugelfang> robbat2: again, what does it need else but bugs.g.o.?
[23:20] <Kugelfang> robbat2: people file bugs, and jakub assign stuff to qa
[23:20] <Kugelfang> robbat2: i'm watching the QA alias, and this works pretty well
[23:21] <kloeri> robbat2: not saying you couldn't have some "QA-lite" team, just noting that we have no such team now and I'm not sure if QA would really want to go that way
[23:21] <wolf31o2|mobile> Flameeyes: I say yes
[23:21] <Flameeyes> agreed then
[23:21] <wolf31o2|mobile> well... I tend to think that QA has their hands full... what they need most is helping hands fixing stuff, I would guess
[23:21] <Flameeyes> if nobody object, that is
[23:21] <wolf31o2|mobile> anyway... let's say we hold off on this until next time, and open the floor?
[23:21] <Kugelfang> i agree
[23:22] <robbat2> i'm not sure about my schedule for next week, but i'll see anyway
[23:22] *** You set the channel mode to 'unmoderated'.
[23:22] <kloeri> rescheduling for next month is fine
[23:22] <wolf31o2|mobile> k
[23:22] <Flameeyes> open floor then
[23:22] <Kugelfang> Flameeyes: you got a log to send to me?
[23:22] <kloeri> I'm at the UK linuxawards / linuxexpo wednesday and thursday next week so more or less completely offline I guess
[23:22] <Kugelfang> Flameeyes: so i can summarise :-)
[23:23] <Flameeyes> Kugelfang, I'd wait till the end of the openfloor, but if you want it now, I can
[23:23] <Kugelfang> please do so
