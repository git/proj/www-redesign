Roll Call:
===========
Betelgeuse: here
Cardoe: here
dertobi123: here 
dev-zero: here
leio: here
lu_zero: here
tanderson(secretary): here
ulm: here

Topics:
===========

    - Filling the empty council seat.
        Donnie Berkholz resigned from the council so there is an empty spot
        that needs to be filled. ssuominen and ulm were tied for the next
        spot, but ssuominen relinquished his seat to ulm. To fill the spot,
        ulm needed to be unanimusly voted in by the current members.

        Conclusion:
            Unanimously voted to fill the seat. Ulrich Müller will fill Donnie
            Berkholz's seat for the rest of the current term.

    - EAPI 3 status report from Zac Medico.
        No progress yet. Zac said he'd have a recent recruit of his work with
        him on it.

        Conclusion:
            Zac will work on EAPI 3 features with the help of his recruit. He
            will also blog about what features need to be done so the general
            community can pitch in.

    - Removal of Old Eclasses.
        Jorge(jmbsvicetto) requested that the council discuss removing
        eclasses from the tree that are no longer needed. The problem with
        this is that old(<2.1.4) portage versions used the eclasses from the
        tree to run uninstall phases. Thus, the removal of eclasses would
        break users who have a portage older than 2.1.4.

        Conclusion:
            The council voted that to remove eclasses devs should take the
            following steps:
                1) Deprecate eclasses.
                2) Removal of all functionality relating to installing.
                3) After two years the eclass may be removed.
            Thomas Anderson(tanderson) will write up patches for devmanual so
            that this policy is documented.

    - Handling EAPI Versioning in a forwards-compatible way.
        Various developers have raised concerns that GLEP 55 only describes a
        solution and doesn't clearly show the problems being solved(if any).
        Luca(lu_zero) mentioned a few things in the "Problem" section that he
        thought could be clarified, listed below:

            1) For "Change the behaviour of inherit in any way", it would be
            useful to include references to bugs where requested inherit
            changes would require GLEP 55.

            2) For "Add new global scope functions in any way", defining
            'Sane'.

            3) For "Extend versioning rules in an EAPI", removal of all
            mentions of GLEP 54 would remove circularity. In addition,
            mentioning other version format changes would be useful.

            4) For "Use newer bash features", listing useful(including
            in-tree) bash features not available in the bash version mandated
            by PMS would be useful.

        Conclusion:
            The council voted on whether they recognized the problem that GLEP
            55 is attempting to solve is real. The vote was affirmative in
            recognition of the problem with two abstentions(leio and ulm). '
            Cardoe was no longer at the meeting for this vote and will post 
            his voteon-list.
