Roll call
=========
betelgeuse here
cardoe     here [dang]
dberkholz  here
dertobi123 here
halcy0n    here
jokey      here
lu_zero    slacker

New topics
==========

EAPI-2
------
Goal: Vote on approval

Requirements: 

  - Put a generated copy (preferably HTML) in the PMS project webspace. 
    People who want to refer to an EAPI=2 reference don't necessarily 
    want to install all the dependencies to build it.
  - Let's tag the git repository something like 
    eapi-$EAPI-approved-$DATE.

Result: EAPI=2 is approved.


PROPERTIES in cache
-------------------
Goal: Vote: Does council need to approve cache changes?
  Goal: Vote on approval

Result: Since it's related to the EAPI, this should be another issue 
that package-manager developers resolve amongst themselves and only 
present to council if they can't agree.

They agree on adding it to the cache as a value that package managers 
can ignore, so it is.


PROPERTIES=interactive in ebuilds
---------------------------------
Goal: Vote: Does council need to approve global-variable changes in 
      ebuilds?

Result: This is a retroactive, backwards-compatible EAPI change and thus 
is handled the same as any other EAPI change -- it requires council 
approval.

Goal: Vote on approval

Result: PROPERTIES=interactive is approved.
