Roll Call:
===========
Betelgeuse: here
Cardoe: here
dberkholz: here
dertobi123: here
dev-zero: here
Halcy0n: absent
lu_zero: here
tanderson(secretary): here

Topics:
===========

Secretary:
    - Should the council have a dedicated secretary?
        Previously dberkholz fulfilled this roll, but he became busy.
	Because fulfilling the secretary duties can distract from the
	meeting, a dedicated, non-council member secretary is ideal.

	Conclusion:
	   tanderson is the new secretary. Logs and summary are to be
	   posted on the -council mailing list. If no objections to it
	   are raised in 1 day, it is posted to the council page and lists.

Elections:
    - Staggered elections
    	Should there be staggered elections every 6 months where half the
	    council members stand for reelection?

	Conclusion:
	    Leave as-is, elections every 6 months is too cumbersome. Full elections
        will be held once a year.

    - Lack of nominated candidates
    	What happens if there aren't enough candidates nominated to fill all
	    the council seats?

	Conclusion:
	    If the pseudo-candidate '_reopen_nominations' appears in 7th place
	    or higher those candidates that rank above '_reopen_nominations'
	    will be the current council. A second period of nominations will
	    be opened for the remaining council seats. No third period of
	    nominations will be opened in the event '_repoen_nominations'
	    ranks higher than the candidates necessary to fill the council.

Technical Issues:
    - Prepalldocs
        Should the 'prepalldocs' be allowed in current EAPIs?

        Conclusion:
            Prepalldocs is banned in current EAPIs(0,1,2). It should be
            removed from ebuilds. Petteri Räty(Betelgeuse) will make QA
            checks for repoman.

    - BASH version allowed in the tree.
        PMS states that ebuilds can only rely on BASH 3.0 features. However,
        some code in gentoo-x86 uses BASH 3.1 features('+=' being the most
        notable) and so is not in conformance with PMS. It was suggested that
        BASH versions newer than 3.0 be allowed in a future EAPI. Ciaran
        Mccreesh, however, commented that this would require GLEP 55 being
        accepted so that a package manager would not have to source the ebuild
        before knowing what BASH version it requires.

        Conclusion:
            No decision. Doug(Cardoe) will follow this up with
            Tiziano(dev-zero) as a backup.

Open Bugs
=========

Technical:
    - GLEP 54(-scm package version suffix, bug 234711[1])
	    GLEP 54 solves two problems, version ordering and periodic reinstall
	    of live packages. The Live Template proposal[2] overlaps in that it also
	    allows for periodic reinstall of live packages. Luca(lu_zero)
        maintains that Live Template provides proper version ordering, while
        Ciaran(ciaranm) maintains that it does not. 

        Conclusion:
            No decision. The council cracked the whip on Luca(lu_zero) and
            he's going to handle the issue.

    - GLEP 55(.ebuild-$eapi ebuild suffix)
        Should .ebuild-$eapi be approved? This ties in with "BASH version
        allowed in the tree" issue mentioned above.

        Conclusion:
            No decision. Tiziano(dev-zero) will be handling this bug.

Non-Technical:
    - Code of Conduct
        No discussion.

        Conclusion:
            No decision. Donnie(dberkholz) will be handling this bug.

References:
[1]: http://bugs.gentoo.org/show_bug.cgi?id=234711
[2]: http://dev.gentoo.org/~lu_zero/glep/liveebuild.rst
