Roll Call:
===========
Betelgeuse: here
Cardoe: absent, receives slacker mark
dberkholz: proxied by ssuominen
dertobi123: proxied by ulm
dev-zero: here
leio: here
lu_zero: here 
tanderson(secretary): physically late, irc client logged everything

Topics:
===========

    - Approve wording of PMS for EAPI 3
        This call for approval comes from the perspective that package manager
        developers currently do not know the specifics of what to code for
        EAPI 3. With this approved package manager developers can write code
        and testcases for each feature and know that the specifics of each
        feature is final(some features may be removed however).

        Conclusion:
            Approved, EAPI 3 specifications have been merged to the main PMS
            repository. EAPI 3 will be tagged when developers are able to use
            it.

    - Vote on GLEP 54
        This vote was called for by dertobi123. The vote was on whether to
        approve GLEP 54 conditional on whether GLEP 55 is passed. The reason
        for this is that GLEP 54 is unimplementable without the problems
        mentioned in GLEP 55 being solved.

        Conclusion:
            Conditionally approved on whether GLEP 55 is approved.

    - Vote on GLEP 55
        A vote was required on this GLEP since GLEP 54 was already passed
        conditional on this vote. 

        Conclusion:
            After quite a bit of confusion in the voting(people changing their
            votes), a tie(3-3) vote was reached. Therefore, no decision was
            reached. This vote will be brought up again next meeting so that
            the tie can be broken(hopefully with everyone present).

    - Discussion of dropping static libraries automatically.
        Peter Alfredsen(loki_val) asked the council to discuss the ability to
	    automatically drop static libraries from installs and the best way to
	    do so.

	    Conclusion:
		    The council unanimously voted that developers, at their
            discretion, can drop static libraries but it will not be the
            default. The council also expressed support for an EAPI 4 proposal 
            to automatically disable static libraries via configure options.

    - Council Election Update
        The election team decided to hold nominations for the Gentoo Council
        from June 1st to June 14th with the voting period running from June
        16th to June 30th. Results will likely be announced on July 2nd. The
        election officials for this election are NeddySeagoon, rane, and
        jmbsvicetto with fox2mike as the infrastructed liaison.
