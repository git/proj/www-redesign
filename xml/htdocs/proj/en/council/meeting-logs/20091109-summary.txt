Summary of Gentoo council meeting 9 November 2009
=================================================

Roll call
---------
betelgeuse
calchan
dertobi123
leio
lu_zero
patrick (proxy for solar)
ulm

EAPI 3 status
-------------
Some progress, see tracker bug 273620. 8 out of 20 items are still
missing.

Upgrade path for old systems
----------------------------
Vote (unanimous): The ebuild tree must provide an upgrade path to a
stable system that hasn't been updated for one year.

Action: leio will start a discussion on gentoo-dev on if and how to
support upgrading systems that are outdated more than a year.

Prefix support in main Portage tree
-----------------------------------
The council unanimously supports the general idea, but sees need for
additional discussion.

Action: ulm will follow up on the open questions on gentoo-dev.

Usage of bash 3.2 features in Portage tree
------------------------------------------
Vote (6 yes, 1 no): Usage of bash 3.2 features in the Portage tree is
allowed. PMS will be updated accordingly.

Vote (6 yes, 1 no): Ebuilds must be completely parsable with =bash-3.2*,
any use of later bash features will be reverted.

Preservation of file modification times in EAPI 3
-------------------------------------------------
Postponed.

Next meeting
------------
7 December 2009, 19 UTC.
