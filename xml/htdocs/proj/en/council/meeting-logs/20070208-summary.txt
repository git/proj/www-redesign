- change to council GLEP 39 to cover when a Council member is no longer part
  of the Gentoo project (the reason/rhyme is irrelevant).  Idea is to
  streamline slightly the bureaucracy (even if new wording is more verbose).
Old wording:
 * If a council member who has been marked a slacker misses any further meeting
   (or their appointed proxy doesn't show up), they lose their position and a
   new election is held to replace that person. The newly elected council member
   gets a 'reduced' term so that the yearly elections still elect a full group.
New wording:
 * If a council member who has been marked a slacker misses any further meeting
   (or their appointed proxy doesn't show up), they lose their position.
 * Whenever a member of the Council loses their position (the reason is
   irrelevant; they could be booted for slacking or they resign or ...), then
   the next person in line from the previous Council election is offered the
   position.  If they decline, it is offered to the next person in line, and so
   forth.  If they accept and the current Council unanimously accepts the new
   person, they get the position with a 'reduced' term such that the yearly
   elections still elect a full group.  If the Council does not accept that
   person, then a new election is held to choose a new member.

- GLEP 23 (ACCEPT_LICENSE) is still valid.  Should have asked genone to show up
  ahead of time to clarify what was asked.

- GLEP 44 (Manifest2) is looking good and we'll work on getting the remainder
  packages fixed.  Idea is to have it in place in the 2007.0 timeframe.

- the tr1 issue (how do we support it properly in dependencies) would be
  researched to see what packages actually need it and a decision would be
  made at the next meeting.  options include eclass, virtuals, ||(atoms).

- mailing list docs (reply-to and spf) look pretty much done.  need to be
  converted to guidexml and posted.
