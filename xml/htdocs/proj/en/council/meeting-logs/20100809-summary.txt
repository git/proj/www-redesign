Gentoo Council 2010/08/09 meeting agenda / summary:


1) allow all members to show up (5 min)
  Petteri (Betelgeuse) 12 min late
  Mark (Halcy0n) 15 min late

2) voting
  2a) we have nothing to vote this time, nobody wanted anything YAY :)

3) discussion
  3a) from last council meeting: the mailing list situation
    * merge -project and -council mls into one (-project)
      ** for: scarabeus, betelgeuse, chainsaw, ferringb (as experiment first
         that could be reverted), halcy0n 
      ** against: jmbsvicetto, wired
    Fill up bug for infra to do this. (scarabeus will fill the bug)

  3b) from last council meeting: eclass API changes
    * document better what PV means http://bugs.gentoo.org/331921
    * make it easier for QA to take action when common sense is *not* used and
      things are broken
    * QA team will update its internal policies about the process to request the
      suspension of commit rights from a developer. The revision might involve an
      update to GLEP 48. (QA mailinglist) 

  3Y) additional deviation: fallout and suspension policies (per ferringb
    request)
    * learn from the python breakage to promote the importance of current
      policies and why developers should use them properly
    * use this as an example of how not do things and how it should be dealt
      with and start a discussion of how to recover from it and how to ensure we
      can reach users in such cases
    * continue discussion on -project ml (per 3a) 

  3Z) council comment about the Portage breakage fallout
    * jmbsvicetto argued that Gentoo needs to acknowledge the incident, ensure
      affected users can find correct documentation on how to fix it and make it
	  clear that the practices that lead to it are neither appropriate nor acceptable 

  3c) EAPI 4 status (jmbsvicetto nominate this so we actualy do something new)
    * ferringb will review the status, and try to find some minion to help him

 4) Bugs assigned to council@ in bugzilla and their progress
	http://bugs.gentoo.org/buglist.cgi?quicksearch=assignedto,cc:council@gentoo.org

    234706
      halcy0n will create new draft proposal soonish
    256451
      last summary tbd by betelgeuse
    256453
      wired wrote nice patch and it will be updated according to the comments on
      -project mailinglist
    237381
      jmbsvicetto plans to have something to show next mont meeting

5) select the chair for following meeting
  chair: wired
  date: 2010-08-23 
