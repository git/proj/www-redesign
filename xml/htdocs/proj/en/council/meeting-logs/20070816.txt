[16:02] --- kingtaco|work sets modes [#gentoo-council +m]
[16:02] <kingtaco|work> lets get this shit started
[16:02] <-- amne has quit (Remote closed the connection)
[16:02] <kingtaco|work> roll call
[16:02] <Uber> I'm 'ere
[16:02] <kingtaco|work> Kugelfang, robbat2, SpanKY, Uber, wolf31o2|work
[16:02] <robbat2> hi
[16:03] --> amne (n=amne@85-124-176-198.dynamic.xdsl-line.inode.at) has joined #gentoo-council
[16:03] <kingtaco|work> ....
[16:04] <kingtaco|work> I don't have time to idle
[16:04] <vapier> ?
[16:04] <Kugelfan3> pon
[16:04] <Kugelfan3> g
[16:04] <kingtaco|work> anyone have any agenda?
[16:04] <Kugelfan3> me neither
[16:04] <vapier> i dont one's been posted, but i'd like to know what's up with pms repo on gentoo.org
[16:04] <vapier> and we should decide about sept
[16:04] <-- desultory has quit (Client Quit)
[16:05] --> desultory (n=dean@gentoo/developer/desultory) has joined #gentoo-council
[16:05] <kingtaco|work> whats up with sept?
[16:05] <Uber> maybe it's on one of the servers that was taken down recently
[16:05] <kingtaco|work> um, we don't have any servers by that name
[16:05] --> hparker (n=hparker@gentoo/developer/hparker) has joined #gentoo-council
[16:05] <vapier> as in the month
[16:05] <vapier> toolbox
[16:06] --> dostrow (n=dostrow@gentoo/developer/dostrow) has joined #gentoo-council
[16:06] <vapier> this is supposed to be our last meeting, new council in sept
[16:06] <vapier> however due to delays, voting doesnt end soon enough
[16:06] <kingtaco|work> fox2mike announced the vote this morning
[16:06] <kingtaco|work> I suppose a meeting is skipped
[16:06] <vapier> i'd make the statement: sept is a floating month ... if new council isnt voted in soon enough, existing council handles sept
[16:07] <Kugelfan3> sounds fair enough
[16:07] <vapier> but regardless, aug will continue to be the official last month
[16:07] <Uber> yeah, we should always have a council
[16:07] <vapier> so we dont have to deal with an ugly sliding window of "1 year"
[16:07] <kingtaco|work> sure
[16:07] <robbat2> my bad on the pms repo, but the PMS guys haven't done much lately either - http://pastebin.ca/660160
[16:07] <robbat2> there was still flak from them on why to have it moved anyway
[16:07] <robbat2> more on that in a bit
[16:08] <vapier> one sec
[16:08] <vapier> we agree on the sept issue ?
[16:08] --> astinus (n=alex@gentoo/developer/astinus) has joined #gentoo-council
[16:08] * Uber votes yes
[16:08] <vapier> sound off like you have a pair
[16:08] <kingtaco|work> vote: old council will stay past their date in the event a new council hasn't been elected
[16:08] <Kugelfan3> yes
[16:08] <robbat2> yes
[16:08] <vapier> yes
[16:08] <kingtaco|work> yes
[16:08] <wolf31o2|work> yes
[16:08] * kingtaco|work prods Uber 
[16:09] <vapier> i'll tweak the glep/docs/whatever and make announce later
[16:09] <kingtaco|work> WFM
[16:09] <kingtaco|work> next
[16:09] <kingtaco|work> pms stuff
[16:09] <kingtaco|work> robbat2, vapier ?
[16:09] <vapier> i'm not going to hound pms until gentoo.org repo is ready
[16:09] <vapier> and i have yet to hear "it is" from robbat2
[16:10] <kingtaco|work> robbat2, ?
[16:10] <robbat2> there's give and take that they didn't want to give up being able to commit directly, that's also what other projects are saying that want external folk working on things (overlays most notably)
[16:10] <robbat2> i still haven't gotten the ACL stuff safe to my satisfaction either
[16:10] --> jaervosz (n=jaervosz@3305ds1-ar.0.fullrate.dk) has joined #gentoo-council
[16:10] <robbat2> that's why I haven't called it 'ready'
[16:10] --- kingtaco|work sets modes [#gentoo-council +o jaervosz]
[16:11] <robbat2> it's as up to date as the upstream SVN
[16:11] <robbat2> which isn't moving fast at all
[16:11] <kingtaco|work> delay yet another month?
[16:12] <robbat2> i'd like to ask
[16:12] <vapier> here's the part i want: when you say the repo is in a usable state with ACL's able to grant/revoke write access
[16:12] <vapier> when that is ready, we move it and we're done
[16:13] <robbat2> i see some repos moving away from Gentoo infra already because people want external contributors, and infra has historically said we won't give it to them
[16:13] <kingtaco|work> I don't see that changing anytime soon
[16:13] <kingtaco|work> not on the cvs/svn server
[16:13] <robbat2> from a security point of view
[16:13] <robbat2> it's not safe to do with CVS and SVN
[16:13] <kingtaco|work> maybe someplace else like overlays or soc or sunrise
[16:13] <robbat2> but is doable safely with Git
[16:13] <robbat2> witness repo.or.cz
[16:13] <robbat2> which is where some of the gentoo repos have moved
[16:14] <kingtaco|work> they can stay IMO
[16:14] <kingtaco|work> infra is taxed enough as it is
[16:14] <kingtaco|work> we don't need to be supporting every single rcs
[16:14] <Kugelfan3> that begs the question why PMS can't... (playing advocatus diaboli)
[16:14] <vapier> if there were a gentoo git server, it'd be a non-issue
[16:15] <kingtaco|work> vapier, read my statement that infra is over taxed
[16:15] <kingtaco|work> and before you start the bring more people on, we just did
[16:15] <robbat2> if I were less busy i'd have the ACLs done already
[16:15] <vapier> everyone is taxed
[16:15] <vapier> that's the nature of open source
[16:16] <robbat2> so the better question, continuing Kugelfan3's point - why force PMS to move if we are letting others stay out there?
[16:16] <robbat2> it's certainly less load on infra if they stay out
[16:16] <Uber> so ciaranm has commit access I thought
[16:16] <kingtaco|work> because they are working on overlays and shit like that, not core policy
[16:16] <vapier> i dont believe in something that Gentoo relies critically on can live outside of Gentoo
[16:17] <wolf31o2|work> I tend to agree... something like the specification that defines what is a Gentoo package manager should live within Gentoo
[16:17] * Uber agrees also
[16:17] <wolf31o2|work> after all, the package management system is likely the main defining point of Gentoo
[16:17] <robbat2> it's a document, we don't rely on it, you can break it, it won't break anything in systems
[16:17] <vapier> i think you need to review your definition of "rely"
[16:18] <Uber> robbat2: it's like hosting our mission statement on windows servers
[16:18] <wolf31o2|work> if the document is incorrect and a package manager is released following the incorrect spec, you *can* break boxes
[16:18] <kingtaco|work> s/can/will
[16:19] <vapier> the pms doc is a guarantee ... if you use a pm that follows the pms, then things in the tree should work
[16:19] <wolf31o2|work> exactly
[16:19] <robbat2> if the overlays weren't so overloaded, we could just trivially move PMS's SVN there
[16:19] <kingtaco|work> that said, do we(gentoo) need a pms or is it more for the external pm
[16:20] <kingtaco|work> looking at who contributes to it, it's mainly the paludis group
[16:20] <wolf31o2|work> kingtaco|work: Gentoo has no need for a PMS if we're only supporting portage... it was written pretty much exclusively to allow external package managers to be on the same page as portage
[16:20] <kingtaco|work> which makes me wonder, does gentoo define is specs by what's in the tree
[16:20] <robbat2> Jokey points out that the finnish translations already use the overlays box in the same fashion that I suggest for PMS
[16:20] <robbat2> so there is certainly precedent
[16:20] <robbat2> and it works
[16:20] <kingtaco|work> it's possible that any PMS is of primary use for external projects and perhaps we don't need to involve ourselves
[16:21] <robbat2> i see the goal of PMS as allowing external PMs to be supported in Gentoo
[16:21] <robbat2> because they do the same thing as Portage
[16:21] <wolf31o2|work> has anyone ever thought to ask if Gentoo even cares to support external package managers?
[16:21] <kingtaco|work> I have no desire to support anything other than what gentoo calls official
[16:21] <-- jaervosz has quit (Read error: 104 (Connection reset by peer))
[16:22] <Uber> we have some Gentoo users who do care
[16:22] <kingtaco|work> I also have no desire to verify that an external manager is indeed PMS compliant
[16:23] <wolf31o2|work> Uber: users don't have to actually do the support, so I'm not sure that make s a bit of difference... after all, users can do whatever they want to their systems... doesn't mean we have to support it in any way
[16:23] <kingtaco|work> I'd rather let the portage devs dictate what EAPI does what
[16:23] <Kugelfan3> afaik portage devs do want PMS
[16:23] <Kugelfan3> but i might be wrong
[16:23] <kingtaco|work> if an external manager wants to follow what we do fine.  if they don't thats fine too.
[16:24] <wolf31o2|work> do they really want it? or do they just want it to shut up the external guys? (I'm honestly asking, I have no clue)
[16:24] <kingtaco|work> I suspect the latter
[16:24] <-- test has quit (Remote closed the connection)
[16:25] <Kugelfan3> kingtaco|work: that is what you suspect... why don't you ask them?
[16:25] <Kugelfan3> like zmedico
[16:25] <Uber> wolf31o2|work: I don't see it as any different compared to say supporting a another OS inside of portage
[16:25] --- kingtaco|work sets modes [#gentoo-council +v zmedico]
[16:25] <kingtaco|work> zmedico, ping
[16:25] <wolf31o2|work> Uber: I don't get your meaning
[16:26] <zmedico> pong
[16:26] <vapier> Uber: in that case, there are developers actively working on it because they care about it
[16:26] <Uber> wolf31o2|work: do you expect package owners to fix freebsd bugs with their packages or the freebsd team?
[16:26] <wolf31o2|work> Uber: that has nothing to do with an external project, so again, I don't get the bearing on this conversation
[16:27] <kingtaco|work> zmedico, regarding PMS, would the portage team rather develop portage and define EAPI bumps along the way or does the team feel it's important to go the PMS route?
[16:28] <Uber> wolf31o2|work: no, but it has everything todo with package support which is your bone of contention
[16:28] <wolf31o2|work> the gentoo/freebsd team *is* a gentoo project, not external... as for who I would expect to fix the packages... both... package maintainers should be writing ebuilds in a portable manner and the alt arch guys should be pointing out issues as they see them and either fixing them or the maintainer fixing them, depending on the severity and extent of the issue at hand... but again, that has nothing to do with external projects
[16:28] <wolf31o2|work> ok... that's not what I'm saying, at all
[16:28] <wolf31o2|work> but I really don't care to continue trying to state my point repeatedly... so I'll just say "sure" and we can move on
[16:29] <vapier> portable isnt quite the word ... we've got an informal standard of things that are/are not OK
[16:29] <vapier> which is fluid and changes as agreed on gentoo-dev mailing list
[16:29] * wolf31o2|work hands the pedantic hat to vapier 
[16:29] <wolf31o2|work> I mean things like... using "cp -a"
[16:29] <kingtaco|work> gimme my hat back bitch
[16:29] <zmedico> kingtaco|work: EAPI bumps should be based on input from the general ebuild developer community I think, since the the purpose of EAPI bumps is to give them features that they want.
[16:29] <kingtaco|work> zmedico, thanks
[16:29] <vapier> OK: gnu make NOT: crappy POSIX make
[16:30] <wolf31o2|work> sure
[16:30] <wolf31o2|work> and if I start using "cp -a" in ebuilds, I'd expect the freebsd guys to come give me a good hard ass kicking
[16:31] <Uber> yes you would :P
[16:31] <kingtaco|work> ok, so I ask again, why does gentoo itself care about PMS
[16:31] <Uber> cp -RPp
[16:31] <vapier> but *only* because we agreed on the ass kicking ahead of time on the gentoo-dev mailing list
[16:31] * Uber nods
[16:31] <wolf31o2|work> we care about it only to define what features are supported in a given EAPI version so it can be used by ebuilds devs
[16:31] <kingtaco|work> no, the discussion on -dev ml defines eapi bumps
[16:32] <vapier> which brings us back to do we just merge the doc into portage svn or overlay svn and be done
[16:32] <vapier> when we agreed on project originally, we gave it to the QA team to maintain
[16:32] <kingtaco|work> or just ignore it, and start working on eapi1
[16:32] <kingtaco|work> which has been needed for quite a while
[16:33] <kingtaco|work> vapier, would seem they've dropped the ball
[16:33] <zmedico> it's more specific features that are needed than just an "eapi1"
[16:33] <wolf31o2|work> I agree... it's been a year and we still don't have a completed spec...
[16:34] <vapier> eh, i would look at it more along the lines of is the QA team even appropriate
[16:34] <kingtaco|work> in it's current form, I'd say no
[16:34] <vapier> having it split between teams seems to have just added overhead
[16:35] <-- Ingmar^ has quit (Read error: 60 (Operation timed out))
[16:35] <wolf31o2|work> I would say no...
[16:35] <vapier> if the route we're going is that we dont add crazy things to EAPI/PMS unless we cover it in gentoo-dev, then having it be with the current package manager would lessen that maintenance
[16:36] <wolf31o2|work> ok... do new features require council buy-in?
[16:36] <vapier> i think originally the idea was that we needed QA team to watch over it as we had a much more fluid "standard"
[16:36] <wolf31o2|work> well, it was the qa team that was pretty much asking for it, too
[16:36] <vapier> but the portage team has reeled themselves in wrt keeping things stable
[16:36] <vapier> true
[16:36] --> jaervosz (n=jaervosz@3305ds1-ar.0.fullrate.dk) has joined #gentoo-council
[16:36] <vapier> i dont think stating council buy in is appropriate
[16:37] <vapier> handle it like anything else ... let it sort itself out and if it doesnt, then we stick a foot in it
[16:37] <vapier> the figurative "Council Boot" if you will
[16:38] <wolf31o2|work> ok... reason I am asking is it would determine where pms would best fit... being a global technical document, the best place for it really is the council... in fact, all of our technical specifications really belong under the council, being the council is the primary technical decision-making body...
[16:38] <wolf31o2|work> which would still work with the "council boot"
[16:39] <vapier> we take a more fallback guidance roll rather than being on the fore-front
[16:39] <kingtaco|work> is there a license on the pms stuff?
[16:39] <-- jaervosz has quit (Read error: 104 (Connection reset by peer))
[16:39] <vapier> it's all create commons
[16:39] <vapier> so yes, we can just take it
[16:39] <kingtaco|work> iirc it's ccsa, but I don't remember exactly
[16:39] <kingtaco|work> so just take it and be done
[16:40] <kingtaco|work> the cryers are going to cry no matter what we do
[16:40] <vapier> the community decides, council steps in when community cant sort itself out
[16:40] <kingtaco|work> people will leave
[16:40] <kingtaco|work> shit happens
[16:40] <kingtaco|work> the community hasn't said anything on it in months
[16:40] <vapier> i think anyone who would leave over pms has already left ;)
[16:40] <kingtaco|work> we're in limbo
[16:40] <kingtaco|work> lets get out and be done with it
[16:40] <robbat2> nobody has brought a new GLEP up in many months
[16:40] <Kugelfan3> vapier: no, i haven't yet
[16:40] <Kugelfan3> vapier: but i will
[16:41] <wolf31o2|work> so we pull it in house, finalize it, publish it as a finished spec, then move onto the next thing...
[16:41] <vapier> whatever floats your boat
[16:41] <kingtaco|work> I don't understand why we have to accomodate any external party
[16:41] <wolf31o2|work> well, no matter what, the finished version of the spec needs to be on our infra somewhere... even if the repo behind it isn't... I just dont' see a point in keeping them separate
[16:41] <kingtaco|work> we might choose to, but being forced like this is silly
[16:42] <wolf31o2|work> we aren't forced to
[16:42] <robbat2> if we fork it to inhouse, will the inhouse fork still have enough momentum?
[16:42] <wolf31o2|work> we can pull the repo right now
[16:42] <kingtaco|work> then why are we doing all this git shit?
[16:42] <wolf31o2|work> robbat2: what momentum?
[16:42] <wolf31o2|work> heh
[16:42] <robbat2> touche
[16:42] <vapier> yeah, what momentum
[16:42] <vapier> there is no fork, there is only what represents the tree
[16:43] <Uber> well, i would imagine there would be some momentum if that happens
[16:43] <Uber> cue lots of posts to -dev by the usual suspects
[16:43] <vapier> Gentoo is open sourced, people can do whatever they like with the code
[16:44] <robbat2> (next agenda item: ML review, requested by tomk)
[16:44] <wolf31o2|work> right... so we pull it in and the naysayers be damned?
[16:45] <vapier> it isnt like where we'd be putting it would damn people
[16:45] <wolf31o2|work> I mean, we already know that any decision (including the lack of one) will cause a flame war
[16:45] <Uber> i would say yes to bring it under our full control at lesat
[16:45] <wolf31o2|work> so let's just do what we think is best and deal with it
[16:45] <robbat2> so move it to overlays, just like the .fi translations?
[16:45] <vapier> Kugelfang, spb, whoever can still commit all they like
[16:45] <wolf31o2|work> robbat2: how far away is the SoC box?
[16:45] <robbat2> wolf31o2|work, ask kingtaco|work
[16:46] <wolf31o2|work> (I'd prefer not abuse overlays if we don't have to)
[16:46] <wolf31o2|work> kingtaco|laptop: ^^
[16:47] <kingtaco|work> it's been ready
[16:47] <kingtaco|work> gimme a svn dump and it's a command away
[16:47] <robbat2> vote: move PMS as-is SVN to SoC/Overlays/svn.g.o
[16:47] <kingtaco|work> user management is vanilla so that'll suck
[16:47] <kingtaco|work> but it's doable
[16:48] <vapier> what is the SoC box ?
[16:48] <kingtaco|work> a box meant for hosting SoC projects and the like
[16:48] <kingtaco|work> the closest thing we've got to allowing external people repo access
[16:49] <wolf31o2|work> vapier: it is a new box that is half-dev/half-infra that will host repos allowing for non-gentoo contributors... for the SoC projects but also usable for this sort of thing
[16:49] <kingtaco|work> sorta like sunrise
[16:49] <vapier> does svn http://
[16:49] <wolf31o2|work> my vote: yes
[16:49] <kingtaco|work> ssh currently
[16:50] <kingtaco|work> hold on a sec
[16:50] <kingtaco|work> on that vote
[16:50] <kingtaco|work> vote for it to go to a specific place, not a "throw the problem at infra"
[16:50] <vapier> we've already thrown it at infra
[16:50] <wolf31o2|work> why? the council doesn't care what specific box it resides on... that's infra's job... all we care about is if it is on our infra or not
[16:50] * vapier looks at robbat2 
[16:51] <kingtaco|work> why?
[16:51] <wolf31o2|work> do you really want the council telling you how to run your infra?
[16:51] <kingtaco|work> because infra doesn't want to choose acls for it
[16:51] <kingtaco|work> so maybe reword that
[16:52] <vapier> i recall infra getting angry last time we told them how to run infra
[16:52] <Uber> why does it need acls? any dev can commit to the tree, why not pms?
[16:52] <kingtaco|work> what about external people?
[16:52] <kingtaco|work> that's the problem
[16:52] <wolf31o2|work> uhh... wouldn't the acls be per-repo anyway?
[16:52] <kingtaco|work> ok, you're missing the point
[16:52] <wolf31o2|work> apparently
[16:53] <kingtaco|work> only current gentoo developers and staff have access to the cvs/svn repos
[16:53] <kingtaco|work> that will not change
[16:53] <wolf31o2|work> correct... on our main svn/cvs box
[16:53] <kingtaco|work> that would be the default place we would put something like this
[16:53] <kingtaco|work> there is an open unanswered question about external contributors
[16:53] <kingtaco|work> answer that and infra will work the magic
[16:53] <kingtaco|work> grok?
[16:53] <wolf31o2|work> then ask that question rather than beating around the bush... :P
[16:53] <wolf31o2|work> yep
[16:54] <wolf31o2|work> so... do we care about external contributors being able to directly commit at this point?
[16:54] <kingtaco|work> the way the vote was worded forced that question onto infra
[16:54] <kingtaco|work> hence my objection
[16:54] <Uber> i care for them not to directly
[16:54] <wolf31o2|work> I don't see there being enough activity to justify it anymore... so I would say that we are not at the mercy of external contributors
[16:55] <kingtaco|work> so the vote really is: allow direct external contributors to spec repo
[16:55] <Uber> and if you are skilled enough to contrib to the PMS then you should by default be a Gentoo dev anyway
[16:55] --> jaervosz (n=jaervosz@3305ds1-ar.0.fullrate.dk) has joined #gentoo-council
[16:55] <robbat2> Uber, careful with that, re ciaranm
[16:55] <wolf31o2|work> Uber: that's untrue and PMS is a prime example
[16:55] <wolf31o2|work> ex-devs can be skilled enough, too
[16:55] <wolf31o2|work> ;]
[16:56] <kingtaco|work> Uber, technical skill alone does not make a gentoo dev
[16:56] <Uber> yes, but isn't that the point - ex dev means not a gentoo developer. Hence as they're not a gentoo developer then they have less power to affect gentoo development
[16:56] <robbat2> how about this then: infra gets it moved to a Gentoo box asap, and shuffles it to soc/overlays later if there is a large demand for direct external commits
[16:57] <wolf31o2|work> I'd agree to that
[16:58] <Uber> ok
[16:58] <-- jaervosz has quit (Read error: 104 (Connection reset by peer))
[16:58] --> Ingmar^ (n=ingmar@83.101.12.130) has joined #gentoo-council
[16:58] <kingtaco|work> vote: fork current external pms repo into svn.gentoo.org and follow up if/when external contributors whine
[16:59] <Kugelfan3> no
[16:59] <vapier> what is the follow up
[16:59] <vapier> what i've heard in the past was "no"
[17:00] <kingtaco|work> the follow up is someone deciding if/when to move it to a place where external people can commit directly
[17:00] <robbat2> Kugelfan3, do you want it straight to somewhere that externals can commit directly?
[17:01] <robbat2> or just don't want it to move
[17:01] <wolf31o2|work> does it matter?
[17:01] <wolf31o2|work> it's a yes or no vote
[17:01] <wolf31o2|work> ;]
[17:01] <Kugelfan3> heh
[17:01] <robbat2> i'm wondering why he said no
[17:01] <Kugelfan3> robbat2: don't move....
[17:01] <wolf31o2|work> my vote: yes
[17:01] <vapier> that doesnt answer his question
[17:02] <kingtaco|work> y'all can vote any time...
[17:02] <robbat2> yes
[17:02] <kingtaco|work> don't get shy now
[17:02] <Uber> yes
[17:02] <kingtaco|work> vapier, ?
[17:03] <vapier> i'm for it ... unify current + portage access to the repo
[17:03] <robbat2> kingtaco|laptop, jaervosz?
[17:04] <kingtaco|work> my vote is yes
[17:04] <kingtaco|work> jaervosz is a slacker today
[17:04] <robbat2> and jaervosz seems to have timed out again
[17:04] <robbat2> ok, so it's passed and done
[17:04] <kingtaco|work> any other topics?
[17:04] <robbat2> infra needs to get an svndump from the existing site
[17:05] <robbat2> and then just put it online
[17:05] <kingtaco|work> robbat2, I thought you had one?
[17:05] <Kugelfan3> ask spb for it
[17:05] <robbat2> kingtaco|work, I made my git clone via a pull
[17:05] <wolf31o2|work> mailing lists is next
[17:05] <robbat2> tomk wanted to know abouts lists
[17:05] <robbat2> gentoo-dev-announce exists
[17:05] <robbat2> for cross-posting
[17:06] <robbat2> there is a hiccup
[17:06] <robbat2> if you send the message only to gentoo-dev-announce, the auto cross-post fails
[17:06] <kingtaco|work> I don't think it necessary to vote on moderating -dev list.  -project and -dev-announce seem to have resolved everything
[17:06] <robbat2> if you put both addresses in to/cc, then the manual copy AND the auto-cross-post get to -dev
[17:06] <Uber> kingtaco|work: true enough - -dev is running smoothly along :)
[17:06] <wolf31o2|work> why is dev-announce auto-forwarding anyway?
[17:06] <wolf31o2|work> it makes it *so* much less useful that way
[17:07] <robbat2> because people asked for it
[17:07] <kingtaco|work> so devs can sub to only dev-announce
[17:07] <kingtaco|work> or only -dev
[17:07] <kingtaco|work> and noone misses out on announcements
[17:07] --> windzor (n=windzor@82.143.229.102) has joined #gentoo-council
[17:07] <wolf31o2|work> but it makes it completely useless for other lists
[17:07] <kingtaco|work> there is some training that has to be done there
[17:07] <robbat2> tell them just to sub?
[17:07] <robbat2> to dev-announce or both
[17:07] <Uber> yeah, just auto-subscribe all devs or tell em to
[17:07] <wolf31o2|work> I think it would be easier to just sub all devs to core and announce
[17:07] <wolf31o2|work> right
[17:08] <wolf31o2|work> let them decide if they want to sub to projects and dev themselves
[17:08] <kingtaco|work> that works
[17:08] <wolf31o2|work> so, for example, I can send a mail to dev-announce announcing something on the releng list
[17:08] <kingtaco|work> still have to train people to send to the right list though
[17:08] <wolf31o2|work> that's fine... brow beating works wonders for that sort of thing
[17:08] <wolf31o2|work> =]
[17:08] <robbat2> ok, so i'll go and turn off auto-forwarding and sub alls devs to dev-announce
[17:09] <kingtaco|work> also need to make devrel aware of the changes so they can adjust recruiters procedures
[17:09] <robbat2> i think that part is scripted, so just adjust the script
[17:09] <wolf31o2|work> cool... I'm going to research if it is possible to allow for a default reply-to that can be overridden... so I can set reply-to tp gentoo-releng on my releng announcements
[17:09] <wolf31o2|work> =]
[17:09] <kingtaco|work> um, it wasn't when I left
[17:09] <kingtaco|work> we had to go to the mlmmj interface for -core
[17:10] <kingtaco|work> who's a recruiter in here?
[17:10] <wolf31o2|work> it still isn't, according to phreak when I asked about adding a gwn email to their recruitment scripts
[17:10] --- kingtaco|work sets modes [#gentoo-council +v Betelgeuse]
[17:10] <kingtaco|work> Betelgeuse, you here?
[17:10] <robbat2> the unsub portion is definetly scripted
[17:10] <kingtaco|work> but not the sub
[17:11] <robbat2> what lists? -core, -dev-announce? i'll do a script right now
[17:11] <kingtaco|work> ok, do we need to vote on this?
[17:11] <robbat2> i don't think so
[17:11] <kingtaco|work> nor I
[17:12] <vapier> if it's part of the recruitment process, just let em decide
[17:12] <vapier> auto subscribe to all the lists that devs are *supposed* to be one
[17:12] <vapier> if they dont want to be on them, they can unsub
[17:12] <kingtaco|work> WFM
[17:12] <wolf31o2|work> yeah, I don't think it needs a vote...w e all seem to be in agreement
[17:12] <kingtaco|work> any thing else on this topic?
[17:12] <kingtaco|work> no?
[17:12] <kingtaco|work> ok
[17:13] <vapier> umm, i think there was something on 4chan.org/b
[17:13] <kingtaco|work> anyone have anything else?
[17:13] <kingtaco|work> hahaahah
[17:13] <kingtaco|work> anonymous++
[17:13] <vapier> kingtaco|laptop: you post logs/summary for last meeting
[17:13] <Betelgeuse> kingtaco|work: yes
[17:13] <wolf31o2|work> yes, we're going to pull a feed from 4chan to the front page, right?
[17:13] <Betelgeuse> kingtaco|work: what do you need?
[17:13] <vapier> cause if you didnt, you need to
[17:13] <kingtaco|work> vapier, I don't log anymore
[17:13] <kingtaco|work> if you don't see my base nick I don't have a log
[17:13] <vapier> i was a slacker at that meeting
[17:14] <kingtaco|work> Betelgeuse, how new devs are sub'd to -core
[17:14] <robbat2> i'll give you my logs, and you can summarize
[17:14] <vapier> so someone needs the logs for it
[17:14] <Betelgeuse> kingtaco|work: recruiters add them
[17:14] <Betelgeuse> kingtaco|work: via a web interface
[17:14] <robbat2> Betelgeuse, care for a one-shot script?
[17:14] <kingtaco|work> Betelgeuse, k, that's what we assumed, wanted to make sure it didn't change
[17:14] <vapier> robbat2: just e-mail them to me please
[17:14] <Betelgeuse> robbat2: script?
[17:14] <Betelgeuse> robbat2: For what?
[17:14] <kingtaco|work> ok, any other topics?
[17:15] <kingtaco|work> we really gotta stay focused
[17:15] <kingtaco|work> people got work to do
[17:15] --> jaervosz (n=jaervosz@3305ds1-ar.0.fullrate.dk) has joined #gentoo-council
[17:15] <kingtaco|work> last chance....
[17:15] <vapier> i think we're done
[17:15] <-- igli (n=igli@unaffiliated/igli) has left #gentoo-council
[17:15] --- kingtaco|work sets modes [#gentoo-council -m]
[17:15] <kingtaco|work> open floor
[17:15] <vapier> robbat2: actually i see 20070712 posted, so someone did it
[17:15] <kingtaco|work> flame away
[17:16] <agaffney> heh, I apparently missed the council meeting
[17:16] <vapier> robbat2: actually it looks like *you* did it
[17:16] <wolf31o2|work> go andrew!
[17:16] <agaffney> I just switched to this window
[17:16] <fmccor|home> Suggestion for next meeting --- why not postpone it until after election, like this one was postponed for LWE?
[17:16] <eroyf> Now, you've been talking about moving PMS for many months now. What is the technical reason for such a move?
[17:16] <agaffney> did I miss anything interesting? :P
[17:16] <vapier> you missed the no-pants-vote
[17:16] <genone> Kugelfan3: while you're here, care to say who's in charge of eselect while you and spb aren't around?
[17:17] <robbat2> vapier, well do you have logs of this one?
[17:17] <dostrow> vapier: quick question....once pms is on gentoo infra when will it be officially stamped as "accepted"?
[17:17] <vapier> robbat2: yes
[17:17] <fmccor|home> Similar situation --- this one was moved because council could not be here last week.  Move next for same reason.
[17:17] <Kugelfan3> genone: ask pioto
[17:17] <vapier> dostrow: dont see why not once we get the few things missing merged
[17:17] <vapier> fmccor: makes sense
[17:18] <fmccor|home> vapier, By accident, that happens on occasion. :)
[17:18] <wolf31o2|work> fmccor: the council isn't only the council for the meetings... they're the council the entire time... one way or another, we have to stay on as council until the election is done
[17:18] <wolf31o2|work> whether we end up at the next meeting or not
[17:18] <fmccor|home> Sure.  I was only talking of the meeting date.
[17:19] <vapier> both good points
[17:19] <fmccor|home> So that next council would have their full 12 meetings.
[17:19] <vapier> i'll post log/summary if no one else wants to
[17:19] <eroyf> Noone able to answer my question?
[17:19] <robbat2> eroyf, it was answered during the council meeting
[17:19] <Uber> vapier: i think you've just talked yourself into doing it :P
[17:19] <vapier> READ THE LOG
[17:19] <eroyf> I didn't see it.
[17:19] <Uber> WHEN HE POSTS IT
[17:19] <vapier> IN MY BUTT
[17:20] <vapier> i'll audit that part
[17:20] <eroyf> I saw that someone didn't want to commit to PMS because it was not on Gentoo infrastructure.
[17:20] <Uber> lol
[17:20] <-- Jointy (n=j0inty@dslb-084-058-236-123.pools.arcor-ip.net) has left #gentoo-council
[17:21] <-- Ingmar^ has quit (Read error: 60 (Operation timed out))
[17:22] <kingtaco|work> fmccor|home, we can always vote to simply adjurn next meeting
[17:22] <vapier> going for a run first
[17:22] <kingtaco|work> then the new kids can hold their own
[17:22] <vapier> work off my "LWE 15"
[17:22] <vapier> i blame ktaco
[17:22] <kingtaco|work> haha
[17:22] <kingtaco|work> strike me down!
[17:22] <Uber> is that 15 inches extra?
[17:22] <kingtaco|work> your mom wishes!
[17:22] <vapier> rofl
[17:23] --> Ingmar^ (n=ingmar@83.101.12.130) has joined #gentoo-council
[17:23] --- rbrown`_ is now known as rbrown`
[17:23] <Philantrop> eroyf: The argument was not primarily a technical one but an organisational one - PMS (quoting wolf31o2|work) "being a global technical document, the best place for it really is the council... in fact, all of our technical specifications really belong under the council, being the council is the primary technical decision-making body". PMS defines how a PM should work and how things at the heart of Gentoo work. Thus, it should be
[17:23] <Philantrop> stored on Gentoo infrastructure. (Everything not marked with quotation marks is my understanding only.)
[17:24] <fmccor|home> kingtaco|work, sure.  I just thought that since election ends in a month, just moving the meeting date by a week (or 2 weeks, however it works out) was simplest.  Moving this meeting set a precedent for that sort of thing.
[17:24] <kingtaco|work> fmccor|home, not really, we've moved meetings before
[17:24] <fmccor|home> Even better. :)
[17:25] <jmbsvicetto> fmccor|home: The date for the meetings is also set by each council
[17:25] <jmbsvicetto> fmccor|home: The next council might decide to have meeting on full moons thursdays at 2AM UTC ;)
[17:25] <eroyf> Philantrop: so basicly. There's no technical reason
[17:26] <wolf31o2|work> oh wait... back onto dev-announce... should we disable the reply-to munging on it or no?
[17:26] <fmccor|home> jmbsvicetto, I suppose.  Does that guarantee any meetings?
[17:26] <Philantrop> eroyf: That is my understanding.
[17:26] <eroyf> Philantrop: yup.
[17:26] <cruxeternus> eroyf: Doesn't sound like it.  What's you're next question?
[17:26] <jmbsvicetto> fmccor|home: I think that would give us a meeting every 28 days ;)
[17:26] <robbat2> wolf31o2|work, yes
[17:26] <genone> wolf31o2|work: is it possible to add reply-to if noone is set by the sender?
[17:27] <wolf31o2|work> robbat2: k... I'm on it
[17:27] <robbat2> unforuntely not
[17:27] <robbat2> wolf31o2|work, i'm there already
[17:27] <robbat2> don't touch
[17:27] <fmccor|home> jmbsvicetto, except for that "Thursday" requirement.
[17:27] <wolf31o2|work> genone: I'm trying to fidn out... I'd love to be able to do that, but I don't think that we can
[17:27] <wolf31o2|work> robbat2: ok... I was already there, too
[17:27] <wolf31o2|work> heh
[17:27] <genone> sucks
[17:28] <robbat2> wolf31o2|work, at some point, we need to make mlmmj pass the mail through procmail just before it goes into outgoing delivery
[17:28] * Uber outs
[17:28] <wolf31o2|work> robbat2: doesn't the customheaders allow for regex like the others do? if so, could we make up a regex that defaults to gentoo-dev if it's empty?
[17:28] <wolf31o2|work> robbat2: k
[17:28] <wolf31o2|work> we could definitely do it w/ procmail in there, too
[17:29] <fmccor|home> jmbsvicetto, My point was simply that although this council is the council of record until after the election, it does not need to meet again (because we will have a new council in mid-September anyway).
[17:29] <robbat2> wolf31o2|work, customheaders just get added on completely blindly
[17:30] <wolf31o2|work> damn
[17:30] <robbat2> that's why there was delheaders Reply-To then customheaders
[17:30] <-- mpagano has quit (Client Quit)
[17:30] <jmbsvicetto> fmccor|home: I understand and agree. I was just adding in, that we don't know when the next council will want to meet. So if they decide to have meetings at the end of the month, they won't even need to postpone their first meeting
[17:30] <wolf31o2|work> I had always wondered about that... so you have to delheaders it if you're adding it to customheaders... makes sense...
[17:30] <-- sybille (n=sybille@brc29-2-88-162-36-171.fbx.proxad.net) has left #gentoo-council
[17:30] <fmccor|home> True.
[17:32] <-- jaervosz has quit (Read error: 104 (Connection reset by peer))
[17:32] <fmccor|home> jmbsvicetto, I suppose I was saying that this council need not meet again because there will be a new one next month, and 2nd Thursday is not cast in stone.
[17:33] <jmbsvicetto> fmccor|home: true
[17:34] * fmccor|home wanders away; seems the excitement is about over.
