Quick summary
=============

GLEP 46 (Allow upstream tags in metadata.xml): Approved

Slacker arches: Vapier's proposal is going out tonight.

Minimal activity for ebuild devs: We're trusting the judgment of the 
undertakers. Also looking into Ohloh for commit stats.

Initial comments on PMS: Unapproved EAPIs cannot go into the approved 
document.


Roll call
=========

(here, proxy [by whom] or slacker?)

amne        here
betelgeuse  here
dberkholz   here
flameeyes   proxy [tsunam]
lu_zero	    slacker
vapier      here
jokey       here


Updates to last month's topics
==============================

	http://www.gentoo.org/proj/en/council/meeting-logs/20080313-summary.txt


	Document of being an active developer
	-------------------------------------
	Last month:
		No updates
	Updates:
		No updates


	Slacker arches
	--------------
	3 months ago:
		vapier will work on rich0's suggestion and repost it for 
		discussion on -dev ML
	Last month:
		vapier said he was going to work on it this weekend.
	Updates:
		vapier said he's finishing it up and will have it posted tonight.


	GLEP 46: Allow upstream tags in metadata.xml
	--------------------------------------------
	http://www.gentoo.org/proj/en/glep/glep-0046.html

	2 months ago:
		Caveat on approval about allowed protocols
	Updates:
		Restriction to http/https has been dropped as pointed out by 
		council members (amne and Flameeyes if I'm right).  The point 
		for restricting the URLs to the mentioned protocols was that 
		they shouldn't link to automatically updated ressources.  This 
		has been replaced by an explicit specification and a 
		recommendation that http/http should be favoured over 
		ftp/svn/gopher/etc to make the implementation for automated 
		update discovery tools easier (they should of course ignore URLs 
		they can't handle).

		Approved.


New topics
==========


	Minimal activity for ebuild devs
	--------------------------------
	Current is 1 commit every 60 days. Should it be higher?

	Agreement was hard to find. Some people thought it should be 1 
	commit / week, others said that people have busy lives and 
	questioned the benefits.

	A number of people did agree that we should trust the judgment of 
	the undertakers.

	dberkholz suggested that low commit rates may not maintain the 
	quality of the committer, and we should more carefully review the 
	commits of these people.

	Ways to track commit stats of various sorts came up, such as cia.vc 
	and ohloh. cia seems to have too much downtime to rely on. ciaranm 
	talked with ohloh people already. ohloh would require some 
	modifications to ohcount to recognize ebuilds and eclasses, and a 
	full copy of the cvs repository to start, but it seems worth 
	exploring. Betelgeuse said he would tar up a copy of the gentoo-x86 
	repo.


	Initial comments on PMS
	-----------------------
	http://git.overlays.gentoo.org/gitweb/?p=proj/pms.git

	Are there any major changes needed, or just tuning details?

	The council voted that kdebuild-1 and other unapproved EAPIs could 
	not be in an approved PMS document. The spec isn't a place for 
	proposals or things that will never be submitted for approval by the 
	council. It's a specification, a reference of what is allowed in the 
	main tree.


	Open floor
	----------

	blackace asked about complaints against philantrop, eroyf, and spb.
	vapier referred that to devrel. Betelgeuse said that there's been no
	rejection or action on those complaints yet, and internal discussion 
	is ongoing. Philantrop complained that he hadn't heard anything 
	about complaints, and Betelgeuse said that since some members 
	already left, he didn't want to take matters into his own hands in 
	sharing private information.
