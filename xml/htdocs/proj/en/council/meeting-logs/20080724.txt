--- Log opened Thu Jul 24 15:59:35 2008
15:59 <@dertobi123> jmbsvicetto: yes, we're going to first mail to the people in question, then make the decision public. should happen soonish.
15:59 < Caster> I'm for Betelgeuse but pls give me 5 minutes
15:59  * dertobi123 is here
15:59 < jmbsvicetto> dertobi123: thanks
16:00 <@Halcy0n> Alright, so: musick (for donnie), jokey, dertobi123, caster (for betelgeuse), me 
16:01 <@Halcy0n> We'll give lu_zero and Flameeyes a few minutes
16:06  * jokey notes flameeyes is away on jabber as well
16:06 <@dertobi123> let's start?
16:06 <@Caster> ok
16:06 <@musikc|laptop> ready whenever you folks are
16:06 <@Halcy0n> Yea, I sent lu_zero and Flameeyes something on jabber a few minutes ago.  We can start now and just mark them as not here (unless they show up in the next few minutes)
16:07 <@jokey> go for it
16:07 <@jokey> who takes chair for donnie? ;)
16:07 <@Halcy0n> I can if no one else wants to.
16:07 -!- dertobi123 changed the topic of #gentoo-council to: Council meeting today - July 24th 2000UTC | agend: 1) userrel authority 2) coc extent
16:07 -!- dertobi123 changed the topic of #gentoo-council to: Council meeting today - July 24th 2000UTC | agenda: 1) userrel authority 2) coc extent
16:07 <@dertobi123> bleh
16:08 <@jokey> Halcy0n: then do so :)
16:08 <@Halcy0n> Alright, first thing to get out of the way is to announce that we have come to a decision on the appeals.  We will be sending emails to the parties involved directly before sending anything out publically.  Just for everyone that was wondering.
16:09 <@musikc|laptop> good to hear, thx
16:10 <@Halcy0n> So, for item #1:  When does everyone think they can reply to the thread on council with your ideas and concerns?  Is there anything in there that you would like to note as something that needs to be clarified.
16:11 < antarus> coc!
16:11 < jmbsvicetto> Halcy0n: In case you're considering my "proposal", let me know if you need me to clarify anything
16:11 <@musikc|laptop> Halcy0n, are some council members already noted as having commented?
16:12 <@Halcy0n> musikc|laptop: looking at the thread, it looks like myself, Donnie, Luca, and Petteri have commented.
16:13 <@musikc|laptop> Halcy0n, cool i just wanted to be sure that people knew who did so those folks didnt think they had to again :)
16:13 <@dertobi123> from my pov there's not that much to discuss there, "our house, our rules" as luca stated. therefore the coc is in place for users as well, except technical limitations when it comes to disciplinary actions
16:13 <@dertobi123> i.e. we can ban names but not people
16:14 <@Halcy0n> So, can we say that everyone will have read the thread and atleast posted what they agree with on there by August 1st?  That way by the next meeting we should have something to vote on.
16:14 <@dertobi123> agreed
16:15 <@Caster> yup
16:15 <@musikc|laptop> Halcy0n, so waiting on dertobi123, flameeyes, and jokey right?
16:15 <@jokey> yap
16:15 <@jokey> luca!
16:15 <@lu_zero> hi
16:16 <@lu_zero> Diego got hospitalized again
16:16 <@Halcy0n> Alright, Diego won't be making it due to situations outside of his control.
16:16 <@Halcy0n> What he said
16:16  * lu_zero got him on phone
16:16 <@jokey> oh noes
16:16 <@Caster> :(
16:16 <@lu_zero> tomorrow he will know for how long hopefully
16:16 <@musikc|laptop> lu_zero, serious condition or recovering?
16:16 <@lu_zero> unknown so far =|
16:17  * musikc|laptop nods
16:17 <@lu_zero> hm
16:17 <@musikc|laptop> so crew, not to be callous, with the other two responding by 8/1 is that sufficient?
16:18 <@lu_zero> musikc|laptop what's the subject?
16:18 <@Halcy0n> Yes, that's fine.  lu_zero we were discussing when everyone can reply to the userrel thread.
16:18 <@musikc|laptop> <Halcy0n> So, for item #1:  When does everyone think they can reply to the thread on council with your ideas and concerns?  Is there anything in there that you would like to note as something that needs to be clarified.
16:18  * jokey has no questions on the thread so vote works for me
16:18 < jmbsvicetto> musikc|laptop: 01/08/2008 for us non-USians ;)
16:18 <@musikc|laptop> lu_zero, waiting on dertobi123 and jokey i believe
16:19 <@musikc|laptop> jmbsvicetto, ya i speak non-us before 9am and after 5pm
16:19 < jmbsvicetto> musikc|laptop: I had to read that three times to understand what you meant ;)
16:19 <@musikc|laptop> ;)
16:19 <@dertobi123> as i said, no need for discussion for me - i'm ready to vote if we want to
16:19 <@musikc|laptop> jmbsvicetto, just gotta keep you on your toes
16:19 <@Halcy0n> Alright, so by the next meeting (in 2 weeks) we should be ready to make a decision on that issue, correct?
16:20 -!- Halcy0n changed the topic of #gentoo-council to: Council meeting today - July 24th 2000UTC | agenda: 1) userrel authority (decision by next meeting, council members to post by Aug 1st) 2) coc extent
16:20 <@jokey> ++
16:20 <@musikc|laptop> Halcy0n, do we wait two weeks when the remaining two folks said they need no more time or is it best to have them post their views first?
16:21 < fmccor> Hard to discuss if they don't post.
16:21 <@Halcy0n> musikc|laptop: we can make that decision before the next meeting if there seems to be outstanding issues that need further discussion.
16:22 <@jokey> well dertobi123, any questions left or should we just vote?
16:22 <@dertobi123> 22:20 <@dertobi123> as i said, no need for discussion for me - i'm ready to vote if we want to
16:22 <@musikc|laptop> fmccor, not much to discuss i suppose as the thread is dead
16:22 <@Halcy0n> Okay, then we can vote if everyone is ready.
16:23 < fmccor> Which are you voting on, please?
16:23 <@Halcy0n> We are still on #1.
16:25 <@Halcy0n> So, we are voting on this single point: Does userrel have the authority to enforce the CoC on users like devrel does with developers?
16:25 <@Halcy0n> Everyone here is ready to vote on that?
16:25 <@Caster> ready
16:25 <@musikc|laptop> user rel has done bans on users before, this really isnt a new precident (rbrown for example was done for a week)
16:26  * musikc|laptop is also ready on dberkholz's behalf
16:26 <@dertobi123> ready
16:27  * lu_zero is ready as well
16:28 <@Halcy0n> Okay, then lets vote:
16:28 <@Halcy0n> Yes
16:28 <@jokey> Yes
16:28 <@musikc|laptop> Yes
16:28 <@Caster> Yes
16:29 <@lu_zero> yes]
16:30 <@dertobi123> yes
16:31 <@Halcy0n> Alright, so #1 has been approved.  Lets move on to #2, extent of CoC enforcement.
16:31 -!- Halcy0n changed the topic of #gentoo-council to: Council meeting today - July 24th 2000UTC | agenda: 1) userrel authority (approved) 2) coc extent
16:33 <@Halcy0n> This one definitely needs discussion on the lists so we can come up with some concrete proposals for the questions Donnie posted.  Would this be something everyone is able to comment on by the next meeting and we can check the status to see if everyone is ready to vote?
16:34 <@jokey> sounds good to me, given discussion is still ongoing there
16:34 <@lu_zero> fine as well
16:35 <@musikc|laptop> would be nice, no one has responded to that discussion since my last post
16:35 < fmccor> May I suggest something as well?
16:35 <@musikc|laptop> perhaps someone on council could kick it back into discussion
16:35 <@Halcy0n> musikc|laptop: I'll post something later tonight after I read through the most recent posts.
16:35 <@Halcy0n> fmccor: ?
16:36 < fmccor> One problem here is that Code of Conduct as posted is badly out of date (talks of proctors and such), and probably incomplete as well.
16:37 <@musikc|laptop> fmccor, look up the word proctor
16:37 <@musikc|laptop> i suspect you read it as a position and not its literal meaning
16:37 <@Halcy0n> fmccor: this sounds like something that should be brought up in that discussion (if it hasn't been already).  I'd suggest having that conversation on the mailing list though.
16:37 < fmccor> No, I read it as meant at the time and as implemented.
16:37 <@musikc|laptop> "an official charged with various duties, esp. with the maintenance of good order."
16:38 <@musikc|laptop> fmccor, you werent around for  the conversations about the chosen word, it was quite dilberate
16:39 < fmccor> I remember that we established a proctors project, and now don't have one, and I remember discussions last year about updating it.
16:39 <@musikc|laptop> fmccor, perhaps before we revise the CoC, we should continue the conversation about to what extent it should be enforced so we could make a single revision instead of one this week and one in two weeks
16:40 <@Halcy0n> musikc|laptop: agreed.  Revising the CoC will likely be a part of any decision we make.
16:40 <@jokey> so definitely defer it to ml
16:40 < jmbsvicetto> fmccor / musikc|laptop: If by extent you include my proposal, as I've stated, it doesn't need to be directly tied to CoC
16:41 <@dertobi123> jokey: yup
16:41 < jmbsvicetto> fmccor / musikc|laptop: I see it as the final line for gentoo involvement, so it could be under userrel/devrel policy
16:42 <@Halcy0n> Okay, so lets have everyone please comment on the CoC thread by next meeting and we will get a status update at that point to see if everyone is ready to vote, or if more discussion needs to take place.
16:42 < fmccor> jmbsvicetto, More userrel, probably, because as written, your proposal (I think) does not apply to developers?
16:42 <@Caster> Halcy0n: right
16:42 <@jokey> ++
16:42 <@Halcy0n> If everyone agrees to that, then we are done since that was everything on the agenda.
16:43 <@musikc|laptop> Halcy0n, sounds good. same question as last time. do we know who has already commented and who remains?
16:43 <@Halcy0n> musikc|laptop: looks like myself and dertobi123 were the only ones to comment.
16:43 < wolf31o2> if anybody wants any information on the "intent" behind CoC stuff, feel free to ask me... since I was one of the primary people pushing it and was involved in all aspects of its creation, rather than listening to anyone who has only heard things "second hand" and wasn't involved...
16:44 <@Halcy0n> wolf31o2: good to know.  We may have questions for you if anything comes up in the discussion on the mailinst list then
16:44 <@musikc|laptop> wolf31o2, not sure. fmccor has mentioned a few times to see christel or kloeri regarding intent, ive advised that you and kingtaco really led it
16:44 < wolf31o2> I've had fun over the last few days reading other people pushing their own ideas as if they were some kind of golden ticket to the intentions of the people involved, rather than simply *asking* said people
16:45 < fmccor> I  had thought that last council had revised it or were going to, but I can't find the changes if they ever did.
16:46 < wolf31o2> musikc|laptop: christel was acting as a secretary... she had no real part in the creation of it other than being present and being the one tasked with writing it and editing it... she did the first... the latter had to be reassigned to someone else because we couldn't track her down to work on it... when we finally did, she was off drinking with somebody (Astinus, I think, actually) and couldn't be bothered to participate
16:46 <@musikc|laptop> cant that be found in CVS?
16:46 < wolf31o2> so using her as a reference for something she barely had part of isn't exactly the best method for getting accurate responses
16:46 < wolf31o2> ;]
16:46 < igli> ugh
16:47 <@Halcy0n> Alright, and with that I think we can say this meeting is adjourned :)  Thanks everyone.
16:47 <@musikc|laptop> thanks for chairing Halcy0n :)
